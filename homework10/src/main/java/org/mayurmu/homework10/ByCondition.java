package org.mayurmu.homework10;

/**
 * Функциональный интерфейс для условия фильтрации
 */
@FunctionalInterface
public interface ByCondition
{
    boolean isOK(int number);
}
