package org.mayurmu.homework10;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Перечисление возможных условий фильтрации для метода {@link Sequence#filter(int[], ByCondition)} (с их реализацией в виде лямбд)
 */
public enum Condition
{
    /**
     * Пустое условие
     */
    EMPTY("", x -> true),
    /**
     * Проверка на чётность
     * @implNote 0 считаем бесконечно чётным (как в лекциях)
     */
    EVEN_NUMBER("Чётность числа", x -> x % 2 == 0),
    /**
     * Проверка, является ли сумма цифр элемента четным числом.
     * @implNote На основе кода из лекции <a href=https://gitlab.com/HaKooNaMaTaTa/stc-22-13/-/blob/main/Lections/1.6%20Architecture%2C%20First%20Program%2C%20Arrays/1.6%20Architecture%2C%20First%20Program%2C%20Arrays.md#%D0%BD%D0%B0%D1%88%D0%B0-%D0%BF%D0%B5%D1%80%D0%B2%D0%B0%D1%8F-%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0-%D0%BD%D0%B0-java>
     *     1.6 Architecture, First Program, Arrays (Инфраструктура Java, Первая программа на Java, Массивы)</a>
     */
    DIGIT_SUM_IS_EVEN("Чётность суммы всех цифр числа", x -> {
        int sum = 0;
        while (x != 0)
        {
            sum += x % 10;
            x /= 10;
        }
        return EVEN_NUMBER.predicate.isOK(sum);
    }),
    /**
     * Проверка на четность всех цифр числа.
     * @apiNote (Доп. задача)
     * @implSpec Число состоит из цифр
     */
    ALL_EVEN_DIGITS("Чётность всех цифр числа", x -> {
        boolean isAllEven = true;
        while (x != 0 && isAllEven)
        {
            isAllEven = EVEN_NUMBER.predicate.isOK(x % 10);
            x /= 10;
        }
        return isAllEven;
    }),
    /**
     * Проверка является ли число Палиндромом
     * @apiNote (Доп. задача)
     * @implNote Число читается одинаково и слева, и справа -> 11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.
     * <p>
     *  <i>Числа состоящие из <b>единственной цифры</b>, как и <b>отрицательные числа</b>, палиндромами <b>не являются!</b></i>
     * </p>
     */
    PALINDROME_NUMBER("Число Палиндром?", x -> {
        if (x <= 0)
        {
            return false;
        }
        ArrayList<Integer> digits = new ArrayList<>();
        while (x != 0)
        {
            // сохраняем последнюю цифру числа
            digits.add(x % 10);
            // Уменьшаем число на одну (последнюю) цифру
            x /= 10;
        }
        if (digits.size() == 1)
        {
            return false;
        }
        ArrayList<Integer> reverseDigits = new ArrayList<>(digits);
        Collections.reverse(reverseDigits);
        boolean isPalindrome = digits.equals(reverseDigits);
        return isPalindrome;
    }),
    /**
     * Проверка является ли число Палиндромом (быстрый вариант - без коллекций)
     * @implNote описание алгоритма взято отсюда: <a href=shorturl.at/akV79></a>
     */
    PALINDROME_NUMBER_FAST("Число Палиндром? (быстрый вариант)", x -> {
        if (x < 11)    // отрицательные числа и числа из одной цифры Не являются палиндромами
        {
            return false;
        }
        int reverseNumber = 0;
        int originalNumber = x;
        while (x != 0)
        {
            // сохраняем последнюю цифру числа
            int temp = x % 10;
            // Уменьшаем число на одну (последнюю) цифру
            x /= 10;
            reverseNumber *= 10;
            reverseNumber += temp;
        }
        return originalNumber == reverseNumber;
    });
    
    public final ByCondition predicate;
    
    Condition(String name, ByCondition byCondition)
    {
        predicate = byCondition;
    }
}
