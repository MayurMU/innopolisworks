package org.mayurmu.homework10;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Класс содержащий метод фильтрации массивов
 */
public class Sequence
{
    /**
     * Метод фильтрации переданного массива
     * @param array Входной массив
     * @param condition Условие отбора элементов
     * @return Массив отфильтрованный по указанному условию
     * @exception IllegalArgumentException При {@code condition == null}
     */
    public static int[] filter(int[] array, ByCondition condition)
    {
        if (condition == null)
        {
            throw new IllegalArgumentException("Параметр condition Не может быть NULL!");
        }
        if (array.length == 0)
        {
            return new int[0];
        }
        // N.B. без ArrayList придётся проходиться по входному массиву два раза - сначала, чтобы посчитать кол-во эл-тов
        //      и создать выходной массив подходящего размера и второй раз, чтобы уже эти элементы в него скопировать.
        // Или же, можно проверять условие один раз, но тогда нужно создавать промежуточный массив равный длине исходного,
        //      а затем уже из этого массива копировать эл-ты в выходной, зная их точное кол-во полученное в ходе проверки.
        ArrayList<Integer> tempList = new ArrayList<>();
        for (int elem : array)
        {
            if (condition.isOK(elem))
            {
                tempList.add(elem);
            }
        }
        final int count = tempList.size();
        if (count == array.length)
        {
            return Arrays.copyOf(array, count);
        }
        int[] result = new int[count];
        // как я понял, в Java это единственный вариант преобразования списка в массив примитивных типов (см.: https://stackoverflow.com/a/9572820/2323972)
        for (int i = 0; i < count; i++)
        {
            result[i] = tempList.get(i);
        }
        return result;
    }
}
