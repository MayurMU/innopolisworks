package org.mayurmu.homework10;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Random;


class SequenceTest
{
    
    //region 'Исключения'
    
    @Test
    @DisplayName("Метод упадёт при передаче Null вместо ссылки на массив")
    void filter_Array_Null_Exception()
    {
        Assertions.assertThrows(NullPointerException.class, () -> Sequence.filter(null, Condition.EMPTY.predicate));
    }
    
    @Test
    @DisplayName("Метод упадёт при передаче Null вместо условия фильтрации")
    void filter_Null_Condition_Exception()
    {
        Assertions.assertThrows(IllegalArgumentException.class, () -> Sequence.filter(new int[1], null));
    }
    
    //endregion 'Исключения'
    
    
    
    //region 'Негативные сценарии'
    
    @Test
    @DisplayName("Метод не должен падать при массиве 0-вой длины")
    void filter_Zero_Length_Array()
    {
        int[] zeroLengthArray = new int[0];
        Assertions.assertArrayEquals(zeroLengthArray, Sequence.filter(zeroLengthArray, Condition.EMPTY.predicate));
    }
    
    @Test
    @DisplayName("Метод не должен падать на массиве из единственного элемента")
    void filter_Single_Element_Array()
    {
        int[] singleElementArray = { 5 };
        Assertions.assertArrayEquals(singleElementArray, Sequence.filter(singleElementArray, Condition.EMPTY.predicate));
    }
    
    @Test
    @DisplayName("Метод не должен падать при условии \"по умолчанию\"")
    void filter_None_Condition()
    {
        int[] testArray = new Random().ints(10).toArray();
        Assertions.assertArrayEquals(testArray, Sequence.filter(testArray, Condition.EMPTY.predicate));
    }
    
    @Test
    @DisplayName("Метод не должен падать, когда нет элементов, подходящих под условие")
    void filter_No_Such_Elements()
    {
        int[] testArray = {1, 3, 5};
        Assertions.assertArrayEquals(new int[0], Sequence.filter(testArray, Condition.EVEN_NUMBER.predicate));
    }
    
    //endregion 'Негативные сценарии'
    
    
    
    
    //region 'Позитивные сценарии'
    
    @ParameterizedTest()
    @EnumSource(value = Condition.class)
    @DisplayName("Метод должен выполняться при любом члене из Condition")
    void filter_All_Conditions(Condition condition)
    {
        int[] allMatchArray;        // массив заведомо полностью удовлетворяющий условию
        int[] noMatchArray;         // массив Без элементов удовлетворяющих условию
        int[] someMatchArray;       // массив частично удовлетворяющий условию
        int[] expectedResult;       // ожидаемый результат для массива частично, подходящего под условие
        switch (condition)
        {
            case EVEN_NUMBER:           // элемент массива - чётное число
            {
                allMatchArray = new int[] { 0, 2, -4, 6 };
                noMatchArray = new int[] { -1, 5, 3, 17 };
                someMatchArray = new int[] { 0, -2, 1 };
                expectedResult = new int[] { 0, -2 };
                break;
            }
            case DIGIT_SUM_IS_EVEN:     // сумма цифр элемента массива - чётное число
            {
                allMatchArray = new int[] { 0, 2, 15 };
                noMatchArray = new int[] { -1, 7, 23 };
                someMatchArray = new int[] { 2, 23, 2, 13, 11, 1 };
                expectedResult = new int[] { 2, 2, 13, 11 };
                break;
            }
            case ALL_EVEN_DIGITS:       // все цифры элемента массива - чётные
            {
                allMatchArray = new int[] { 0, 2, 24, 482 };
                noMatchArray = new int[] { 11, 3, 175 };
                someMatchArray = new int[] { 2, 11, 46, 3 };
                expectedResult = new int[] { 2, 46 };
                break;
            }
            case PALINDROME_NUMBER_FAST:
            case PALINDROME_NUMBER:     // элемент массива - палиндром
            {
                allMatchArray = new int[] { 11, 3333, 1221, 555 };
                noMatchArray = new int[] { -11, 3, 12, 3211 };
                someMatchArray = new int[] { 11, 6, 46064, 3, 123, 115115 };
                expectedResult = new int[] { 11, 46064 };
                break;
            }
            default:    // метод не должен падать на неизвестных членах перечисления
            {
                int[] testArray = { 1, 2, 0, -5 };
                Assertions.assertDoesNotThrow(() -> Sequence.filter(testArray, condition.predicate));
                Assertions.assertNotNull(Sequence.filter(testArray, condition.predicate));
                return;
            }
        }
        Assertions.assertArrayEquals(allMatchArray, Sequence.filter(allMatchArray, condition.predicate));
        Assertions.assertArrayEquals(expectedResult, Sequence.filter(someMatchArray, condition.predicate));
        Assertions.assertArrayEquals(new int[0], Sequence.filter(noMatchArray, condition.predicate));
    }
    
    //endregion 'Позитивные сценарии'
    
}