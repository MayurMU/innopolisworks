package org.mayurmu.homework3;

import org.mayurmu.common.Utils;
import org.mayurmu.exceptions.UserInputCancelledException;

import java.util.Scanner;

/**
 * Программа для перевода систем счисления
 *
 * */
public class Task_3
{
    /**
     * Поддерживаемы типы СИ
     * */
    enum NumberSystem
    {
        /**
         * Неизвестный тип - значение по умолчанию
         * */
        None(0),
        /**
         * Двоичная
         * */
        Bin(2),
        /**
         * Восьмеричная
         * */
        Oct(8),
        /**
         * Десятичная
         * */
        Dec(10),
        /**
         * Шестнадцатеричная
         * */
        Hex(16);
        
        public final int baseCode;
        
        NumberSystem(int baseCode)
        {
            this.baseCode = baseCode;
        }
    
        @Override
        public String toString()
        {
            return super.toString();
        }
    }
    
    private static final Scanner _scn = new Scanner(System.in);
    
    /**
     * При выходе из программы возвращает код (-1), если пользователь прервал выполнение.
     *  (-2) - неожиданная ошибка в программе.
     * */
    public static void main(String[] args)
    {
        boolean isUserCancelled = false;
        boolean isUnexpectedError = false;
        try
        {
            NumberSystem sourceNumSys = inputNumberSystem("Введите основание Исходной Системы счисления (СИ):");
            NumberSystem destNumSys = inputNumberSystem("Введите основание Целевой Системы счисления (СИ):");
            if (sourceNumSys == destNumSys)
            {
                System.out.println("Преобразование между одной и той же СИ не имеет смысла!");
                return;
            }
            int sourceNumber = 0; int destNumber = 0;
            sourceNumber = Utils.inputIntNumber(_scn,  String.format("Ведите число в %d-чной СИ (только цифры - без префиксов (0, 0x)):", sourceNumSys.baseCode),
                    sourceNumSys.baseCode, true);
            System.out.printf("Начинаю преобразование из %d-ной в %d-ную СИ...%n", sourceNumSys.baseCode, destNumSys.baseCode);
            String convertedNumber = convertNumberSystem(sourceNumber, sourceNumSys, destNumSys);
            String formatedNumber = formatNumber(sourceNumber, sourceNumSys);
            System.out.println("Число " + formatedNumber + " преобразовано из " + sourceNumSys + " в " + destNumSys + ": ");
            System.out.println(formatNumber(convertedNumber, destNumSys));
            System.out.println("Проверка методом стандартной библиотеки:");
            System.out.println(formatNumber(sourceNumber, destNumSys));
        }
        catch (UserInputCancelledException e)
        {
            isUserCancelled = true;
            System.out.println(e.getMessage());
        }
        catch (Exception ex)
        {
            isUnexpectedError = true;
            System.err.println(ex.toString());
            ex.printStackTrace();
        }
        finally
        {
            _scn.close();
            System.out.println("Программа завершена");
            if (isUserCancelled)
            {
                System.exit(-1);
            }
            if (isUnexpectedError)
            {
                System.exit(-2);
            }
        }
    }
    
    /**
     * Форматирует строковое представление числа разбивая его на разряды ("тысячи")
     *  (двоичные строки при этом добиваются спереди нулями до длины кратной 4-м)
     *
     * @param source Строковое представление исходного числа - может начинаться со знака
     * */
    private static String formatNumber(String source, NumberSystem numSys)
    {
        StringBuilder sb = new StringBuilder();
        int thousandLength = numSys == NumberSystem.Bin || numSys == NumberSystem.Hex ? 4 : 3;
        source = source.trim();
        String sign = source.startsWith("-") || source.startsWith("- ") ? "- " : "";
        source = source.replace('-', '\0').trim();
        for (int i = source.length() - 1; i >= 0;)
        {
            int j = 0;
            for (; j < thousandLength && i >= 0; j++, i--)
            {
                sb.append(source.charAt(i));
            }
            if (numSys == NumberSystem.Bin && j < thousandLength)
            {
                for (int k = 0; k < thousandLength - j; k++)
                {
                    sb.append('0');
                }
            }
            sb.append(' ');
        }
        sb.deleteCharAt(sb.length() - 1);   // убираем последний лишний пробел
        sb.reverse();
        return sign + sb.toString();
    }
    
    /**
     * Выводит число в указанной СИ с разбиением на разряды ("тысячи" через пробел)
     * */
    private static String formatNumber(int sourceNumber, NumberSystem numSys)
    {
        String res;
        if (numSys == NumberSystem.None)
        {
            throw new IllegalArgumentException("Тип СИ неизвестен! " + numSys);
        }
        switch (numSys)
        {
            case Dec:
            {
                res = String.format("%,d", sourceNumber);
                break;
            }
            case Bin:
            case Hex:
            case Oct:
            {
                res = formatNumber(Integer.toUnsignedString(Math.abs(sourceNumber), numSys.baseCode), numSys);
                res = sourceNumber < 0 ? "- " + res : res;
                break;
            }
            default:
            {
                throw new IllegalArgumentException("Тип СИ неизвестен! " + numSys);
            }
        }
        return res;
    }
    
    /**
     * Метод преобразования числа из одной системы счисления в другую
     * @param number Исходное число
     * @exception  IllegalArgumentException Выбрасывается, когда оба параметра {@code sourceNumSys} и {@code destNumSys}
     *  равны, либо один из них неизвестен.
     * @return Строковое представление исходного числа преобразованного в указанную СИ
     *
     * @implNote Ряд преобразований в 16-ю и 8-ю систему идут через 10-ю (в тех случаях, когда исходная система "меньше" целевой)
     * */
    private static String convertNumberSystem(int number, NumberSystem sourceNumSys, NumberSystem destNumSys)
    {
        // при невыполнении условия приводит к генерации исключения среды выполнения AssertionError, но только если
        // программа запущена с ключом -enableassertions, например java -ea MyApp
        // Можно также разрешать Assertions только для отдельных пакетов или отдельных классов.
        // Кроме того, можно действовать по принципу "чёрного списка" - разрешить диаг. утверждения для всей программы,
        // но запретить их для отдельных классов и/или пакетов (java -da:MyClass MyApp).
        // (+) такого подхода в том, что эти условия не проверяются в продакшене, и, т.о., не замедляют программу.
        //assert (sourceNumSys != NumberSystem.None) : "Исходная СИ неизвестна: " + NumberSystem.None.toString();
        //assert (destNumSys != NumberSystem.None) : "Целевая СИ неизвестна: " + NumberSystem.None.toString();
        // P.S. Диагностические утверждения можно включить даже для системных классов (java -esa)!
        if (number == 0)
        {
            return "0";
        }
        String res = "";
        // т.к. в Java остатки от деления могут быть отрицательными, то запоминаем знак исходного числа и берём его модуль
        boolean wasNegative = number < 0;
        int source = Math.abs(number);
        if (sourceNumSys == NumberSystem.None || destNumSys == NumberSystem.None)
        {
            throw new IllegalArgumentException("Как минимум одна СИ пустая!");
        }
        // выбираем алгоритм (умножение цифр числа на степени основания целевой СИ, либо рекурсивное целочисленное деление числа на основание целевой СИ)
        int remainder = 0;
        if (sourceNumSys.baseCode > destNumSys.baseCode)
        {
            StringBuilder sb = new StringBuilder(Integer.SIZE);
            // нужно делить
            while(source > 0)
            {
                remainder = source % destNumSys.baseCode;
                sb.append(remainder);
                source /= destNumSys.baseCode;
            }
            sb.reverse();
            res = sb.toString();
        }
        else if (sourceNumSys.baseCode < destNumSys.baseCode)
        {
            int result = 0;
            // этот блок по сути не нужен, т.к. число и так хранится в 10-й системе - приведено для демонстрации - полезно, если на входе метода будет строка
            {
                // т.к. введя число (именно в виде числа, а не строки) в нужной СИ - оно Не хранится в цифрах указанной нами исходной СИ - получается
                // нужно выполнять двойную работу - преобразовывать сначала число в строковое представление исходной СИ... - второй вариант - вводить
                // число сразу в виде символов нужной СИ и хранить в виде строки, но тогда сложнее контролировать его корректный ввод.
                String temp = convertNumberSystem(source, destNumSys, sourceNumSys);
                // нужно умножать - но это НЕ работает для преобразований не в 10-ю СИ, т.к. тут мы всегда получаем десятичный результат
                for (int i = temp.length() - 1, j = 0; i >= 0; i--, j++)
                {
                    result += Character.digit(temp.charAt(i), sourceNumSys.baseCode) * Math.pow(sourceNumSys.baseCode, j);
                }
            }
            switch (destNumSys)
            {
                case Hex:
                {
                    res = String.format("%x", result);
                    break;
                }
                case Oct:
                {
                    res = String.format("%o", result);
                    break;
                }
                default:
                {
                    res = String.valueOf(result);
                    break;
                }
            }
        }
        else  //sourceNumSys.baseCode == destNumSys.baseCode
        {
            throw new IllegalArgumentException("Целевая и Исходная СИ должны отличаться!");
        }
        return wasNegative ? "- " + res : res;
    }
    
    /*
    * Метод выбора СИ
    * */
    private static NumberSystem inputNumberSystem(String messsage) throws UserInputCancelledException
    {
        boolean incorrectInput = false;
        NumberSystem selectedSystem = NumberSystem.None;
        do
        {
            int numSysBase = Utils.inputIntNumber(_scn, messsage + " варианты (2, 8, 10, 16):", true);
            for (NumberSystem ns : NumberSystem.values())
            {
                if (ns.baseCode == numSysBase)
                {
                    selectedSystem = ns;
                    incorrectInput = false;
                    break;
                }
            }
            if (selectedSystem == NumberSystem.None)
            {
                incorrectInput = true;
                System.err.println("Такая СИ неизвестна!");
            }
        }
        while (incorrectInput);
        return selectedSystem;
    }
}
