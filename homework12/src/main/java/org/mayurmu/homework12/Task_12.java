package org.mayurmu.homework12;

import java.io.BufferedInputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Косс реализующий задачу 12 - Collection API
 */
public class Task_12
{
    public static final String URL_TO_ANALYZE = "https://gitlab.com/MayurMU/innopolisworks/-/raw/develop/common/src/org/mayurmu/common/Utils.java";
    
    /**
     * Метод получает из указанного URL'a текст и подсчитывает статистику слов в нём циклом и через Stream API
     */
    public static void main(String[] args)
    {
        System.out.println("Загружаю исходный текст библиотеки common-code из GitLab...");
        String text;
        try
        {
            try (BufferedInputStream inputStream = new BufferedInputStream(new URL(URL_TO_ANALYZE).openStream()))
            {
                // Простое считывание всего файла в строку через сканер (см.: https://stackoverflow.com/a/5445161/2323972)
                try (Scanner netScanner = new Scanner(inputStream, StandardCharsets.UTF_8.name()).useDelimiter("\\A"))
                {
                    text = netScanner.hasNext() ? netScanner.next() : "";
                }
            }
            System.out.println("Текст загружен");
            System.out.println("Начинаю анализ функцией...");
            Map<String, Long> wordsCount = getWordStatistics(text);
            System.out.println("Анализ завершён, результаты:");
            System.out.println();
            printMap(wordsCount);
            System.out.println("*************************************************************************************");
            System.out.println();
            System.out.println("Начинаю проверочный анализ через Stream API...");
            // не учитываем числа, т.к. мы и так потеряли знаки минуса (-) после удаления пунктуации
            Map<String, Long> expectedResult = Arrays.stream(removePunctAndLinebreaks(text).split(" ")).filter(x ->
                    !x.replaceAll("\\d", "").trim().isEmpty()).sorted().collect(Collectors.groupingBy(
                            String::trim, LinkedHashMap::new, Collectors.counting()
            ));
            System.out.println("Анализ завершён, результаты:");
            System.out.println();
            printMap(expectedResult);
            System.out.println();
            System.out.println("*************************************************************************************");
            boolean isOk = wordsCount.equals(expectedResult);
            System.out.printf("Словари равны: %b %n", isOk);
            if (!isOk)
            {
                System.out.printf("Длина первого: %d, второго %d %n", wordsCount.size(), expectedResult.size());
            }
        }
        catch (Exception ex)
        {
            System.err.println("Неожиданная ошибка");
            ex.printStackTrace();
        }
    }
    
    /**
     * Метод печати словаря в столбик вида "Имя    -    Значение"
     */
    public static <K, V> void printMap(Map<K, V> mp)
    {
        for (Map.Entry<K, V> wordEntry : mp.entrySet())
        {
            System.out.printf("%50s \t - \t %s %n", wordEntry.getKey(), wordEntry.getValue());
        }
    }
    
    /**
     * Метод подсчёта статистики слов в тексте
     * @param text Исходный текст
     * @return Статистика
     * @implNote Подсчитывает кол-во слов после удаления из текста всех знаков препинания, числа Не считает за слова!
     *          (Плохо подходит для исходного текста, т.к. там слишком много служебного синтаксиса, вроде "\n" или @аннотаций)
     */
    public static Map<String, Long> getWordStatistics(String text)
    {
        Map<String, Long> wordsCount = new LinkedHashMap<>();               // такая Map сохраняет исходный порядок слов
        String[] source = removePunctAndLinebreaks(text).split(" ");
        Arrays.sort(source);
        for (String word : source)
        {
            String clearedWord = word.replaceAll("\\d", "").trim();
            if (!clearedWord.isEmpty())
            {
                wordsCount.compute(word.trim(), (k, v) -> v == null ? 1 : v + 1);
            }
        }
        return wordsCount;
    }
    
    public static String removePunctAndLinebreaks(String source)
    {
        return source.replaceAll("[\n\r]|\\p{Punct}", " ");
    }
}