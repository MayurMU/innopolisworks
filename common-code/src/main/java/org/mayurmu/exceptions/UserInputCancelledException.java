package org.mayurmu.exceptions;

public class UserInputCancelledException extends Exception
{
    @Override
    public String getMessage()
    {
        String parantMes = super.getMessage();
        return (parantMes != null ? parantMes : "" + " Пользователь отказался от ввода").trim();
    }
}
