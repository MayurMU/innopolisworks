package org.mayurmu.common;

import org.mayurmu.exceptions.UserInputCancelledException;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;


public class Utils
{
    private static Random _rand;
    
    /**
     * Метод гарантированного ввода целого числа - не даёт пользователю ввести ничего другого, использует приветствие по умолчанию
     * */
    public static int inputIntNumber(Scanner scn)
    {
        return inputIntNumber(scn, "Введите целое число: ");
    }
    
    /**
     * Метод гарантированного ввода целого числа, использует приветствие по умолчанию - позволяет поль-лю отказаться от ввода.
     *
     * @implNote Использует стандартную строку приглашения ввода
     *
     **/
    public static int inputIntNumber(Scanner scn, boolean allowExitOnEmptyString) throws UserInputCancelledException
    {
        if (allowExitOnEmptyString)
        {
            return inputIntNumber(scn, "Ведите целое число - пустая строка для выхода", true);
        }
        else
        {
            return inputIntNumber(scn);
        }
    }
    public static int inputIntNumber(Scanner scn, String helloText, boolean allowExitOnEmptyString) throws UserInputCancelledException
    {
        return inputIntNumber(scn, helloText, 10, allowExitOnEmptyString);
    }
    /**
     * Метод гарантированного ввода целого числа, использует приветствие по умолчанию - позволяет поль-лю отказаться от ввода.
     *
     * @param allowExitOnEmptyString Выходит по при вводе пустой строки - уведомляет поль-ля о существовании такой возможности
     * @param helloText Приветствие для пользователя - может быть пустой строкой
     * @param numberSystemBase Основание СИ
     *
     * @return Число введённое пользователем, если поль-ль не отказался от ввода (см.: {@code allowExitOnEmptyString})
     *
     * @apiNote при {@code allowExitOnEmptyString == false} логичнее сразу вызывать {@link #inputIntNumber(Scanner)},
     *  чтобы не приходилось обрабатывать исключение {@link UserInputCancelledException}.
     *
     * @exception UserInputCancelledException Выбрасывается при вводе пользователем пустой строки
     * */
    public static int inputIntNumber(Scanner scn, String helloText, int numberSystemBase, boolean allowExitOnEmptyString) throws UserInputCancelledException
    {
        final String HELLO_TEXT_END = " - пустая строка для выхода:";
        String userInput;
        int res = -1;
        if (allowExitOnEmptyString)
        {
            boolean incorrectInput;
            do
            {
                helloText = helloText.trim();
                System.out.println(helloText.endsWith(":") ? helloText.replace(":", HELLO_TEXT_END) : helloText +
                        HELLO_TEXT_END);
                userInput = scn.nextLine().trim();
                if (userInput.isEmpty())
                {
                    throw new UserInputCancelledException();
                }
                try
                {
                    res = Integer.parseInt(userInput, numberSystemBase);
                    incorrectInput = false;
                }
                catch (NumberFormatException nfe)
                {
                    incorrectInput = true;
                    System.err.format("Вы должны вводить только целые числа!%n").println(nfe);
                }
            }
            while (incorrectInput);
        }
        else
        {
            res = inputIntNumber(scn);
        }
        return res;
    }
    
    /**
     * Метод гарантированного ввода целого числа - не даёт пользователю ввести ничего другого
     *
     * @param helloText     Текст приглашения для ввода (может быть пустым)
     *
     * @implNote    При неправильном вводе выводит текст ошибки в консоль (поток ошибок)
     * */
    public static int inputIntNumber(Scanner scn, String helloText)
    {
        boolean incorrectInput;
        int a = -1;
        do
        {
            try
            {
                if (helloText != null && !helloText.isEmpty())
                {
                    System.out.println(helloText);
                }
                a = scn.nextInt();
                incorrectInput = false;
            }
            catch (InputMismatchException ime)
            {
                System.err.format("Вы должны вводить только целые числа!%n").println(ime);
                scn.skip(".*\n");  // пропускаем некорректный ввод, иначе он снова будет прочитан при следующем вызове
                incorrectInput = true;
            }
        }
        while (incorrectInput);
        return a;
    }
    
    /**
     * Универсальный метод вывода массива на консоль (каждый элемент с новой строки)
     * @implNote Неэффективен для примитивных типов, т.к. будет приводить к лишней упаковке (boxing)
     * */
    public static void printArray(Object[] arr)
    {
        printArray(arr, System.lineSeparator());
    }
    
    /**
     * Универсальный метод печати массива с указанным разделителем
     * */
    public static void printArray(Object[] arr, String separator)
    {
        for (Object ob : arr)
        {
            if (ob != null)
            {
                System.out.append(ob.toString()).print(separator);
            }
        }
    }
    
    /**
     * Метод возвращает фамилию с правильным родовым окончанием на основе переданного имени (для русских имён)
     *
     * @param firstName Русское имя
     * @param lastName  Русская фамилия
     * @return Фамилия с правильным родовым окончанием
     */
    public static String getCorrectGenderLastName(String firstName, String lastName)
    {
        final String FEMALE_NAME_OR_LASTNAME_ENDING = "а";
        final String FEMALE_NAME_ENDING_2 = "я";
        final String UNIVERSAL_LAST_NAME_ENDING = "о";
        
        if (lastName.endsWith(UNIVERSAL_LAST_NAME_ENDING))
        {
            return lastName;
        }
        
        final String[] IRREGULAR_MALE_NAMES = { "Никита", "Илья" };
        final String IRREGULAR_FEMALE_NAME = "Любовь";
        
        String result = lastName;
        
        // пытаемся создать согласованные имена, т.к. JFaker, к сожалению, генерирует их без учёта пола (родовых окончаний)
        boolean isIrregularFemalename = firstName.equalsIgnoreCase(IRREGULAR_FEMALE_NAME);
        boolean isIrregularMaleName = !isIrregularFemalename && Arrays.stream(IRREGULAR_MALE_NAMES).anyMatch(n ->
            n.equalsIgnoreCase(firstName));
        // Если имя женское - делаем фамилию женской
        if ((isIrregularFemalename || (!isIrregularMaleName &&
            (firstName.endsWith(FEMALE_NAME_OR_LASTNAME_ENDING) || firstName.endsWith(FEMALE_NAME_ENDING_2)))) &&
            !lastName.endsWith(FEMALE_NAME_OR_LASTNAME_ENDING))
        {
            result = lastName + FEMALE_NAME_OR_LASTNAME_ENDING;
        }
        // Если же фамилия женская, а имя мужское - удаляем из фамилии женское окончание
        else if (lastName.endsWith(FEMALE_NAME_OR_LASTNAME_ENDING) && (!isIrregularFemalename && (isIrregularMaleName ||
                (!firstName.endsWith(FEMALE_NAME_OR_LASTNAME_ENDING) && !firstName.endsWith(FEMALE_NAME_ENDING_2)))
            )
        )
        {
            result = lastName.substring(0, lastName.length() - 1);
        }
        return result;
    }
    
    public static Random getRandomGenerator()
    {
        if (_rand == null)
        {
            _rand = new Random();
        }
        return _rand;
    }
    
    /**
     * Метод возвращает согласованное название улицы вида "{пл.|площадь Юбилейная}"
     */
    public static String getCorrectRussianStreetName(String streetName)
    {
        final String MALE_STREET_PREFIXES_PATTERN = "проспект|пр.|переулок|пер.";
        final String FEMALE_STREET_PREFIXES_PATTERN = "площадь|пл.|улица|ул.";
        final String[] FEMALE_STREET_PREFIXES = FEMALE_STREET_PREFIXES_PATTERN.split("\\|");
        final String[] MALE_STREET_PREFIXES = MALE_STREET_PREFIXES_PATTERN.split("\\|");
        final Random rand = getRandomGenerator();
        String temp = streetName.replaceAll(MALE_STREET_PREFIXES_PATTERN + "|" +
                FEMALE_STREET_PREFIXES_PATTERN, "").trim();
        String prefix = temp.endsWith("я") ? FEMALE_STREET_PREFIXES[rand.nextInt(FEMALE_STREET_PREFIXES.length)] :
                MALE_STREET_PREFIXES[rand.nextInt(MALE_STREET_PREFIXES.length)];
        return prefix + " " + temp;
    }
}
