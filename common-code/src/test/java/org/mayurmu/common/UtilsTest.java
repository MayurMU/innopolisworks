package org.mayurmu.common;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class UtilsTest
{
    @Nested
    @DisplayName("Группа тестов метода получения согласованных по роду Имён и Фамилий...")
    class GetCorrectGenderLastName
    {
        /**
         * Вспомогательный метод тестового класса для парсинга данных и получения ожидаемого и реального результатов
         *
         * @param sourceAndResult Здесь после (=) ожидаемый результат проверки, если после знака равно ничего нет, значит
         *                        пара уже правильная.
         * @return Пара "Ожидаемый_Результат", "Полученный_Результат"
         */
        private String[] getExpectedAndRealResult(String sourceAndResult)
        {
            final String[] data = sourceAndResult.split("=");
            final String[] nameLastNamePair = data[0].split(" ");
            final String correctedLastName = Utils.getCorrectGenderLastName(nameLastNamePair[0], nameLastNamePair[1]);
            final String expectedResult = data.length >= 2 ? data[1] : nameLastNamePair[1];
            return new String[] { expectedResult, correctedLastName };
        }
        
        @DisplayName("Проверяем имена исключения")
        @ParameterizedTest()
        @ValueSource(strings = {"Илья Мечникова=Мечников", "Илья Голунов=", "Любовь Варламов=Варламова",
                                "Любовь Сторожева=", "Никита Боченкова=Боченков", "Никита Свечников="})
        void getCorrectGenderLastName_IrregularNames(String sourceAndResult)
        {
            String[] expectedAndRealResult = getExpectedAndRealResult(sourceAndResult);
            assertEquals(expectedAndRealResult[0], expectedAndRealResult[1]);
        }
    
        @DisplayName("Проверяем \"нормальные\" имена")
        @ParameterizedTest()
        // Здесь после (=) ожидаемый результат проверки, если после знака равно ничего нет, значит пара уже правильная.
        @ValueSource(strings = {"Александр Мечникова=Мечников", "Михаил Голунов=", "Елена Варламов=Варламова",
                                "Галина Сторожева=", "Лилия Боченков=Боченкова", "Лилия Свечникова=", "Елизавета Бортко="})
        void getCorrectGenderLastName_RegularNames(String sourceAndResult)
        {
            String[] expectedAndRealResult = getExpectedAndRealResult(sourceAndResult);
            assertEquals(expectedAndRealResult[0], expectedAndRealResult[1]);
        }
    }
}