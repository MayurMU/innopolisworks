package org.mayurmu.finalwork.gorsvet.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;

class UriUtilsTest
{
    @ParameterizedTest
    @ValueSource(strings = {"http://fun.de/path/a/b/c?query&first=1&another=3#asdf",
                            "http://fun.de/path/a/b/c?query&another=3#asdf",
                            "http://fun.de/path/a/b/c?query&another=3",
                            "http://fun.de/path/a/b/c?query",
                            "http://fun.de/path/a/b/c",
                            "http://fun.de/path",
                            "http://fun.de",
    })
    void addPath_ToRootURL(String root)
    {
        URI rootUrl = URI.create(root);
        assertDoesNotThrow(() -> UriUtils.addPath(rootUrl, "auxPath"));
        assertDoesNotThrow(() -> UriUtils.addPath(rootUrl, "/auxPath"));
    }
}