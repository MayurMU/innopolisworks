package org.mayurmu.finalwork.gorsvet;

import org.junit.jupiter.api.*;
import org.mayurmu.finalwork.gorsvet.model.Bot;
import org.mayurmu.finalwork.gorsvet.model.PageUrlRefEntry;
import org.mayurmu.finalwork.gorsvet.model.Role;
import org.mayurmu.finalwork.gorsvet.repository.BotRepository;
import org.mayurmu.finalwork.gorsvet.repository.PageUrlRefEntryRepository;
import org.mayurmu.finalwork.gorsvet.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.mayurmu.finalwork.gorsvet.component.DonEnergoParser.SiteParserDonEnergoImpl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Тесты пока не работают из-за обращения к репозиторию {@link PageUrlRefEntryRepository} в конструкторе {@link SiteParserDonEnergoImpl}
 */
@SpringBootTest
@Disabled
public class GorSvetApplicationTests
{
	@Nested
	class ContextTest
	{
		@Test
		void contextLoads()
		{
		}
	}
	
	@Nested
	@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
	@SuppressWarnings("SpringJavaAutowiredMembersInspection")
	class test_Initial_Data_Load
	{
		@Autowired
		private BotRepository botRepository;
		@Autowired
		private RoleRepository roleRepository;
		@Autowired
		private PageUrlRefEntryRepository pageUrlRefEntryRepository;
		
		private long getExpectedEntityCountFromScriptResource(String resourceName, String searchText) throws URISyntaxException, IOException
		{
			return Files.readAllLines(Paths.get(Objects.requireNonNull(GorSvetApplicationTests.test_Initial_Data_Load.class.
					getClassLoader().getResource(resourceName)).toURI())).stream().filter(s -> s.toLowerCase().
					contains(searchText)).count();
		}
		
		/**
		 * Проверяем, что все известные в коде роли существуют в БД после старта
		 */
		@Test
		public void all_Roles_Must_Exists_After_Init()
		{
			for (Role r : Role.ROLES)
			{
				assertTrue(roleRepository.existsByType(r.getType()), "В базе данных не найдена Роль: " + r);
			}
		}
		
		/**
		 * Проверяем, что срабатывает скрипт первоначальной загрузки данных Ботов в БД и что среди них есть хотя бы один
		 * 	в активном состоянии.
		 *
		 * @implNote Проверка идёт довольно примитивным образом: сравнивается кол-во словосочетаний "insert into bot" в
		 * 			стартовом SQL-скрипте с тем кол-вом записей таблицы Bot, которое оказывается в БД.
		 */
		@Test
		public void check_Bot_Data_After_Init_Script() throws IOException, URISyntaxException
		{
			long expectedCount = this.getExpectedEntityCountFromScriptResource("add_bots.sql", "insert into bot");
			assertEquals(expectedCount, botRepository.count(), "Не все боты загрузились в БД!");
			assertTrue(botRepository.findAll().stream().anyMatch(Bot::getIsAlive), "Нет Активных ботов!");
		}
		
		/**
		 * Проверяем, что все адреса Страниц загружаются в БД успешно и что среди них есть хотя бы одна Активная
		 */
		@Test
		public void check_Site_Urls_After_Init_Script() throws URISyntaxException, IOException
		{
			long expectedCount = this.getExpectedEntityCountFromScriptResource("add_pages.sql",
					"insert into page_url_reference");
			assertEquals(expectedCount, pageUrlRefEntryRepository.count(), "Не все адреса страниц загрузились в БД!");
			assertTrue(pageUrlRefEntryRepository.findAll().stream().anyMatch(PageUrlRefEntry::getIsActive), "Нет Активных страниц!");
		}
	}
	

	


}
