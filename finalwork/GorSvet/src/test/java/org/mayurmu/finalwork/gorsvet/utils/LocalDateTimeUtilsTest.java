package org.mayurmu.finalwork.gorsvet.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.format.DateTimeParseException;

import static org.junit.jupiter.api.Assertions.*;

class LocalDateTimeUtilsTest
{
    
    /**
     * @apiNote Вариант независимый от форматера
     */
    @ParameterizedTest
    @DisplayName("Даты в известных форматах без сверки")
    @CsvSource({"26.09.22, 09=00",         // normal case
                "30.09.22г., 13=00",      // irregular Year
                "27.09.22, 13 =30",       // space in time
                "28.09.2022, 17=00",      // 4 digit year
                "01.10.2022г., 12-00",     // horizontal slash time
                "01.10.2022г., 12-00"     // horizontal slash time
    }
    )
    void parseSiteDateAndTime_DifferentCases(String date, String time)
    {
        assertDoesNotThrow(() -> LocalDateTimeUtils.parseSiteDateAndTime(date, time));
    }
    
    /**
     * @apiNote Note зависит от формата {@link LocalDateTimeUtils#rusFormatter} - при каждом его изменении нужно менять
     *      строки ожидаемых результатов!
     */
    @ParameterizedTest
    @DisplayName("Даты в известных форматах со сверкой по строке")
    @CsvSource({"26.09.22, 09=00, 26.09.2022 9:00",         // normal case
                "30.09.22г., 13=00, 30.09.2022 13:00",      // irregular Year
                "27.09.22, 13 =30, 27.09.2022 13:30",       // space in time
                "28.09.2022, 17=00, 28.09.2022 17:00",      // 4 digit year
                "01.10.2022г., 12-00, 01.10.2022 12:00"     // horizontal slash time
    }
    )
    void parseSiteDateAndTime_DifferentCases_WithStringCompare(String date, String time, String expectedResult)
    {
        String result = LocalDateTimeUtils.parseSiteDateAndTime(date, time).format(LocalDateTimeUtils.rusFormatter);
        assertEquals(expectedResult, result);
    }
    

    
    /**
     * Негативный тест
     */
    @ParameterizedTest
    @DisplayName("Даты в известных форматах со сверкой по строке")
    @CsvSource({"26 сентября 2022, 09=00",         // normal case
                "30-го 09 22г., 13=00",      // irregular Year
                "2022.09.27, 15:00",       // space in time
                "28\\09\\2022, 07.00",      // 4 digit year
                "01-10-2022г., 12-00",     // horizontal slash time
                "01/10/2022г., 12-00"     // horizontal slash time
    }
    )
    void parseSiteDateAndTime_MustFail(String date, String time)
    {
        assertThrows(DateTimeParseException.class, () -> LocalDateTimeUtils.parseSiteDateAndTime(date, time));
    }
}