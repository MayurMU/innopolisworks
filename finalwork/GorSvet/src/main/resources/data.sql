-- Во всех случаях отдаётся предпочтение тем данным, что уже есть в БД, и.е. если записи уже существуют, скрипт не будет делать НИЧЕГО.

-- Скрипт заполнения таблицы ботов 3-мя начальными значениями - для ботов Новочеркасска, Ростова-на-Дону и 'Общего бота'
INSERT INTO bot VALUES (DEFAULT, 'Главный Бот - основная точка входа в программу', '15.09.2022 12:00',
                        true, '15.09.2022 12:00', '5786277848:AAHbAEvy2KdQE8i4RLeq6L0LyhDq-YvF788',
                        'GorodSvetBot')
                ON CONFLICT DO NOTHING;
INSERT INTO bot VALUES (DEFAULT, 'Бот для ускорения поиска адреса в Новочеркасске', '16.09.2022 12:30',
                        false, '16.09.2022 12:30', '1542997414:AAFvhQtVg1eJgxdiMsLeuDV5UoWCGjCFzBI',
                        'NovochSvetBot')
                ON CONFLICT DO NOTHING;
INSERT INTO bot VALUES (DEFAULT, 'Бот для ускорения поиска адреса в Ростове-на-Дону', '17.09.2022 13:00',
                        false, '17.09.2022 13:00', '5687999396:AAHwq6wcim5DbAhqDW240lL3-UMdrGlgEWE',
                        'RostovSvetBot')
                ON CONFLICT DO NOTHING;

-- Добавляем в справочник страниц записи об известных страницах для поиска информации (активной делаем только одну - Новочеркасск)
INSERT INTO page_url_reference VALUES (DEFAULT, null, 'Азовские межрайонные электрические сети (АМЭС)',
                                       false, '', 'Azov', null,null,'52/1/')
                ON CONFLICT DO NOTHING;
INSERT INTO page_url_reference VALUES (DEFAULT, null, 'Батайские межрайонные электрические сети (БМЭС)',
                                       false, '', 'Bataysk', null, null,'53/1/')
                ON CONFLICT DO NOTHING;
INSERT INTO page_url_reference VALUES (DEFAULT, null, 'Волгодонские межрайонные электрические сети (ВМЭС)',
                                       false, '', 'Volgodonsk', null, null,'62/1/')
                ON CONFLICT DO NOTHING;
INSERT INTO page_url_reference VALUES (DEFAULT, null, 'Западные межрайонные электрические сети (ЗМЭС)',
                                       false, '', 'Zapad', null, null,'64/1/')
                ON CONFLICT DO NOTHING;
INSERT INTO page_url_reference VALUES (DEFAULT, null, 'Каменские межрайонные электрические сети (КМЭС)',
                                       false, '', 'Kamensk', null, null,'55/1/')
                ON CONFLICT DO NOTHING;
INSERT INTO page_url_reference VALUES (DEFAULT, null, 'Миллеровские межрайонные электрические сети (ММЭС)',
                                       false, '', 'Millerovo', null, null,'56/1/')
                ON CONFLICT DO NOTHING;
INSERT INTO page_url_reference VALUES (DEFAULT, null, 'Новочеркасские межрайонные электрические сети (НчМЭС)',
                                       true, '', 'Novochek', null, null,'57/1/')
                ON CONFLICT DO NOTHING;
INSERT INTO page_url_reference VALUES (DEFAULT, null, 'Ростовские городские электрические сети (РГЭС)',
                                       false, '', 'Rostov', null, null,'59/1/')
                ON CONFLICT DO NOTHING;
INSERT INTO page_url_reference VALUES (DEFAULT, null, 'Сальские межрайонные электрические сети (СМЭС)',
                                       false, '', 'Salsk', null, null,'60/1/')
                ON CONFLICT DO NOTHING;
INSERT INTO page_url_reference VALUES (DEFAULT, null, 'Шахтинские межрайонные электрические сети (ШМЭС)',
                                       false, '', 'Shahty', null, null,'61/1/')
                ON CONFLICT DO NOTHING;

