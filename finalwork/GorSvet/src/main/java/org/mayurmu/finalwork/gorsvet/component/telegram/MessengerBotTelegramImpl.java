package org.mayurmu.finalwork.gorsvet.component.telegram;

import org.apache.commons.lang3.StringUtils;
import org.mayurmu.finalwork.gorsvet.component.MessengerBot;
import org.mayurmu.finalwork.gorsvet.component.telegram.menu.BotMenuState;
import org.mayurmu.finalwork.gorsvet.model.Bot;
import org.slf4j.Logger;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.Nullable;
import java.text.MessageFormat;
import java.util.*;

/**
 * Реализация Телеграм-бота
 */
public class MessengerBotTelegramImpl extends TelegramLongPollingCommandBot implements MessengerBot
{
    private static final String classMame = MessengerBotTelegramImpl.class.getSimpleName();
    // TODO: почему-то с командами Telegram на Java работать не хочет - поэтому приходится рассматривать их как текст.
    private static final String MAIN_MENU_COMMAND_NAME = "/main_menu";
    // макс. длина сообщения телеграм (в символах).
    private static final int MAX_MESSAGE_LENGTH = 4096;
    
    private final Logger logger;
    private final String username;
    private final String token;
    private final boolean useEchoRepeat;
    private final Map<Long, BotMenuState> _userStates = new HashMap<>();
    private final BotLogic botLogic;
    private final Bot linkedBotModel;
    
    /**
     * Конструктор с базовыми значениями (остальное передаётся в сеттерах)
     */
    public MessengerBotTelegramImpl(String botUserName, String botSecretKey, boolean useEchoRepeat, BotLogic botLogic,
                                    Bot linkedBotModel,
                                    Logger logger)
    {
        super();
        this.botLogic = botLogic;
        this.logger = logger;
        this.username = botUserName;
        this.token = botSecretKey;
        this.useEchoRepeat = useEchoRepeat;
        this.linkedBotModel = linkedBotModel;
        // TODO: Разобраться - почему-то это не работает - все неизвестные команды всё равно приходят в виде простых сообщений
        registerDefaultAction(((absSender, message) -> {
            logger.warn("{}: (бот: {}) пользователь \"{} ({})\" попытался ввести неизвестную команду \"{}\"", classMame,
                    getBotUsername(), message.getFrom().getUserName(), message.getFrom().getId(), message);
            try
            {
                absSender.execute(new SendMessage(String.valueOf(message.getChatId()), "Такая команда мне неизвестна"));
            }
            catch (Exception ex)
            {
                String error = MessageFormat.format("{0}: : Ошибка отправки сообщения от Телеграм бота \"{1}\" для клиента с ИД: {2}",
                        classMame, getBotUsername(), message.getChatId());
                logger.error(error, ex);
            }
        }));
    }
    
    
    @Override
    public Bot getLinkedBotModel()
    {
        return linkedBotModel;
    }
    
    @Override
    public String getBotUsername()
    {
        return username;
    }
    
    @Override
    public String getBotToken()
    {
        return token;
    }
    
    /**
     * После этого события обычно идёт ещё отвязка от веб-хука (и вот она может виснуть, если ранее была попытка его
     *      установить - см. исходники библиотеки телеграм)
     */
    @Override
    public void onRegister()
    {
        super.onRegister();
        logger.info("(!) {}: Начата регистрация бота {}...", classMame, getBotUsername());
    }
    
    /**
     * TODO: разобраться - почему-то не работает - все команды приходят как простые сообщения
     */
    @Override
    public void processInvalidCommandUpdate(Update update)
    {
        logger.warn("{}: (бот: {}) пользователь попытался ввести неизвестную команду \"{}\"", classMame, getBotUsername(),
                update.getMessage());
        sendMessage(update.getMessage().getChatId(), "Такая команда мне неизвестна");
    }
    
    /**
     * Запрещаем чужим ботам отправлять команды нашему боту
     */
    @Override
    public boolean filter(Message message)
    {
        return !message.getFrom().getIsBot();
    }
    
    /**
     * Обработчик сообщений бота, которые не являются командами (в т.ч. нажатия кнопок на встроенных клавиатурах)
     */
    @Override
    public void processNonCommandUpdate(Update update)
    {
        final User sender = update.hasCallbackQuery() ? update.getCallbackQuery().getFrom() : update.getMessage().getFrom();
        final long senderId = sender.getId();
        final String senderName = sender.getUserName();
        // запрещаем другим ботам писать нашему боту
        if (sender.getIsBot())
        {
            logger.warn("{}: сообщения от других ботов не поддерживаются! Отправитель \"{} ({})\"", classMame, senderName,
                    senderId);
            return;
        }
        final Message mes = update.hasMessage() ? update.getMessage() : update.getCallbackQuery().getMessage();
        final String chatId = mes.getChatId().toString();
        // Если сообщение является нажатием кнопки встроенной клавиатуры, то обрабатываем его отдельно (всегда без эхо-повтора)
        if (update.hasCallbackQuery())
        {
            final CallbackQuery callbackQuery = update.getCallbackQuery();
            // TODO: передавать в метод обработки меню ИД сообщения mes.getMessageId(), чтобы можно было, отправлять
            // ответы на конкретное сообщение, а не цитировать его тест вручную.
            botLogic.processMenu(this, senderId, senderName, chatId, callbackQuery.getData(), true);
            markCallbackQueryAsAnswered(callbackQuery);
        }
        else if (mes.hasText())
        {
            logger.debug("{}: (бот: {}) получено сообщение от юзера \"{}\"({})", classMame, getBotUsername(), senderName,
                    senderId);
            final String messageText = mes.getText().trim();
            if (useEchoRepeat)
            {
                SendMessage message = new SendMessage();
                message.setChatId(chatId);
                message.setText("Я вас услышал, Вы сказали: \"" + messageText + "\"");
                message.disableNotification();
                try
                {
                    execute(message);
                }
                catch (TelegramApiException e)
                {
                    String error = MessageFormat.format("{0}: Ошибка отправки сообщения от Телеграм бота \"{1}\" " +
                                    "для клиента с ИД: {2}", classMame, getBotUsername(), chatId);
                    logger.error(error, e);
                }
            }
            // вручную обрабатываем команду бота (/main_menu)
            if (messageText.equalsIgnoreCase(MAIN_MENU_COMMAND_NAME))
            {
                setUserStateTo(senderId, BotMenuState.MAIN_MENU);
                botLogic.processMenu(this, senderId, senderName, chatId);
                return;
            }
            botLogic.processMenu(this, senderId, senderName, chatId, messageText);
        }
    }
    
    /**
     * Снимает значок ожидания с кнопки встроенной клавиатуры - т.е. отправляем признак, что обработка запроса завершена
     */
    private void markCallbackQueryAsAnswered(CallbackQuery callbackQuery)
    {
        markCallbackQueryAsAnswered(callbackQuery, "", false);
    }
    /**
     * Снимает значок ожидания с кнопки встроенной клавиатуры - т.е. отправляем признак, что обработка запроса завершена
     *
     * @param popupText Текст появляющийся поверх окна с чатом
     * @param isAlert False - всплывающее уведомление по центру, True - сообщение в виде диалогового окна с кнопкой "ОК"
     */
    private void markCallbackQueryAsAnswered(CallbackQuery callbackQuery, String popupText, boolean isAlert)
    {
        try
        {
            AnswerCallbackQuery ans = AnswerCallbackQuery.builder().callbackQueryId(callbackQuery.getId()).build();
            ans.setShowAlert(true);
            if (StringUtils.isNotBlank(popupText))
            {
                ans.setText(popupText);
            }
            execute(ans);
        }
        catch (TelegramApiException e)
        {
            String error = MessageFormat.format("{0}: Ошибка отправки ответа для кнопки встроенной клавиатуры {1}" +
                            " от Телеграм бота \"{2}\" для клиента с ИД: {3}", classMame, callbackQuery.getData(),
                    getBotUsername(), callbackQuery.getFrom().getId());
            logger.error(error, e);
        }
    }
    
    /**
     * Устанавливает для указанного пользователя текущее положение в меню
     */
    @Override
    public void setUserStateTo(long userId, BotMenuState newState)
    {
        _userStates.compute(userId, (aLong, botMenuState) -> newState);
    }
    
    /**
     * Возвращает текущее положение юзера в меню
     */
    @Override
    public BotMenuState getUserState(long userId)
    {
        return _userStates.get(userId);
    }
    
    @Override
    public void sendYesNoInlineKeyboard(String title, String chatId, CallBackData yesButtonData)
    {
        sendYesNoInlineKeyboard(title, chatId, yesButtonData, null);
    }
    @Override
    public void sendYesNoInlineKeyboard(String title, String chatId, CallBackData yesButtonData, @Nullable CallBackData noButtonData)
    {
        Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>(2);
        buttons.put("Да", new InlineButtonPayload(yesButtonData.toString()));
        buttons.put("Нет", new InlineButtonPayload(Objects.toString(noButtonData, "no")));
        sendInlineKeyboard(title, chatId, buttons.entrySet());
    }
    /**
     * Метод отправки Встроенного меню в виде двух столбцов кнопок
     *
     * @implNote Порядок кнопок зависит от того, является ли {@link Set} экземпляром {@link LinkedHashSet}
     */
    @Override
    public void sendInlineKeyboard(String title, String chatId, Set<Map.Entry<String, InlineButtonPayload>> sourceButtons)
    {
        sendInlineKeyboard(title, chatId, sourceButtons, 2);
    }
    /**
     * Метод отправки Встроенного меню в виде нескольких столбцов кнопок
     *
     * @param columnsCount Желаемое кол-во столбцов
     *
     * @implNote Порядок кнопок зависит от того, является ли {@link Set} экземпляром {@link LinkedHashSet}
     */
    @Override
    public void sendInlineKeyboard(String title, String chatId, Set<Map.Entry<String, InlineButtonPayload>> sourceButtons,
                                   final int columnsCount)
    {
        if (sourceButtons.size() == 0)
        {
            throw new IllegalArgumentException("Список кнопок пуст!");
        }
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        message.enableHtml(true);
        message.setText(title);
        // Create InlineKeyboardMarkup object
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        // определяем кол-во строк необходимое, для того, чтобы все наши кнопки разместить в 2 столбца
        final int rowCount = (int)Math.ceil((double)sourceButtons.size() / columnsCount);
        // Create the rows (list of InlineKeyboardButton list)
        List<List<InlineKeyboardButton>> keyboardRows = new ArrayList<>(rowCount);
        int i = 0;
        List<InlineKeyboardButton> row = new ArrayList<>(columnsCount);
        for (Map.Entry<String, InlineButtonPayload> b : sourceButtons)
        {
            if (i % columnsCount == 0)         // если в строке уже две кнопки, то добавляем новую строку
            {
                row = new ArrayList<>(columnsCount);
                keyboardRows.add(row);
            }
            InlineKeyboardButton ib = new InlineKeyboardButton(b.getKey());
            if (StringUtils.isNotBlank(b.getValue().callback_data))
            {
                ib.setCallbackData(b.getValue().toString());
            }
            else
            {
                ib.setUrl(b.getValue().toString());
            }
            row.add(ib);
            i++;
        }
        inlineKeyboardMarkup.setKeyboard(keyboardRows);
        // Add it to the message
        message.setReplyMarkup(inlineKeyboardMarkup);
        try
        {
            execute(message);
        }
        catch (TelegramApiException e)
        {
            String error = MessageFormat.format("{0}: (бот: {1}) Ошибка установки встроенной клавиатуры ответа " +
                    "для клиента с ИД: {2}", classMame, getBotUsername(), chatId);
            logger.error(error, e);
        }
    }
    
    /**
     * Метод отправки кастомной клавиатуры Ответе
     *
     * @param title Дополнительный текст сообщения
     */
    @Override
    public void sendCustomKeyboard(String title, String chatId, String... buttons)
    {
        sendCustomKeyboard(title, chatId, 2, buttons);
    }
    
    /**
     * Метод отправки кастомной клавиатуры Ответе
     *
     * @param title Дополнительный текст сообщения
     */
    @Override
    public void sendCustomKeyboard(String title, String chatId, int rowCount, String... buttons)
    {
        if (rowCount > buttons.length)
        {
            throw new IllegalArgumentException("Кол-во рядов кнопок не может превышать кол-во самих кнопок!");
        }
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        message.setText(title);
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setOneTimeKeyboard(true);
        keyboardMarkup.setResizeKeyboard(true);
        // Create the keyboard (list of keyboard rows)
        List<KeyboardRow> keyboard = new ArrayList<>();
        final int buttonsOnRowCount = buttons.length / rowCount;
        for (int i = 0; i < rowCount; i++)
        {
            KeyboardRow row = new KeyboardRow();
            for (int j = 0; j < buttonsOnRowCount; j++)
            {
                row.add(buttons[i + j]);
            }
            keyboard.add(row);
        }
        // Set the keyboard to the markup
        keyboardMarkup.setKeyboard(keyboard);
        // Add it to the message
        message.setReplyMarkup(keyboardMarkup);
        try
        {
            execute(message);
        }
        catch (TelegramApiException e)
        {
            String error = MessageFormat.format("{0}: (бот: {1}) Ошибка установки клавиатуры ответа для клиента с ИД: {2}",
                    classMame, getBotUsername(), chatId);
            logger.error(error, e);
        }
    }
    

    @Override
    public void sendMessage(long chatID, String mesText)
    {
        sendMessage(chatID, mesText, -1);
    }
    @Override
    public void sendMessage(long chatID, String format, Object ... arguments)
    {
        sendMessage(chatID, MessageFormat.format(format, arguments), -1);
    }
    @Override
    public void sendMessage(long chatID, String mesText, int replyToMesID)
    {
        try
        {
            logger.debug("{}: отправляю сообщение через бота {} для клиента с ИД \"{}\"...", classMame, getBotUsername(),
                    chatID);
            SendMessage message = new SendMessage();
            message.enableHtml(true);
            message.enableNotification();
            message.enableWebPagePreview();
            message.setAllowSendingWithoutReply(true);
            message.setChatId(chatID);
            if (replyToMesID >= 0)
            {
                message.setReplyToMessageId(replyToMesID);
            }
            if (mesText.length() < MAX_MESSAGE_LENGTH)
            {
                message.setText(mesText);
                execute(message);
            }
            else
            {
                String part = "";
                int startIndex = 0;
                do
                {
                    part = StringUtils.substring(mesText, startIndex, startIndex + MAX_MESSAGE_LENGTH);
                    startIndex += MAX_MESSAGE_LENGTH;
                    message.setText(part);
                    execute(message);
                }
                while (StringUtils.isNotBlank(part));
            }
            logger.debug("{}: сообщение успешно отправлено через бота {} для клиента с ИД \"{}\"...", classMame, getBotUsername(),
                    chatID);
        }
        catch (TelegramApiException e)
        {
            String error = MessageFormat.format("{0}: : Ошибка отправки сообщения от Телеграм бота \"{1}\" для клиента с ИД: {2}",
                    classMame, getBotUsername(), chatID);
            logger.error(error, e);
        }
    }
  
    
}
