package org.mayurmu.finalwork.gorsvet.component.DonEnergoParser;

import org.apache.commons.lang3.NotImplementedException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.mayurmu.finalwork.gorsvet.component.SiteParser;
import org.mayurmu.finalwork.gorsvet.event.SiteDataChangedEvent;
import org.mayurmu.finalwork.gorsvet.exceptions.SiteParseException;
import org.mayurmu.finalwork.gorsvet.model.PageUrlRefEntry;
import org.mayurmu.finalwork.gorsvet.model.utils.UUIDs;
import org.mayurmu.finalwork.gorsvet.model.utils.OutageRefEntryBuilder;
import org.mayurmu.finalwork.gorsvet.repository.OutageRefEntryRepository;
import org.mayurmu.finalwork.gorsvet.repository.PageUrlRefEntryRepository;
import org.mayurmu.finalwork.gorsvet.utils.LocalDateTimeUtils;
import org.mayurmu.finalwork.gorsvet.utils.UriUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.FastByteArrayOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Класс парсера, который умеет работать с сайтом <a href="https://www.donenergo.ru/grafik-otklyucheniy/">ДонЭнерго</a>
 *      (по крайней мере в его версии от сентября 2022 г.)
 *
 * @apiNote Генерирует событие {@link SiteDataChangedEvent}
 *
 * @implNote TODO: Проверил парсинг полностью работает, правда текущая политика такова, что если хотя бы одну строку
 *              веб-страницы распарсить не удалось, то вся страница исключается из разбора - возможно это стоит пересмотреть.
 */
@Component
@Lazy
@Transactional
public class SiteParserDonEnergoImpl implements SiteParser
{
    
    //region 'Поля'
    
    private URI siteRoot;
    private Map<String, URI> pageAddresses = new HashMap<>();
    private List<PageUrlRefEntry> activePageUrls;
    
    private final PageUrlRefEntryRepository pageUrlRefEntryRepository;
    private final OutageRefEntryRepository outageRefEntryRepository;
    private final Logger logger;
    private final int pageRelaxTimeout;
    private final String pageDumpsDir;
    private final ZoneId dbZoneId;
    
    //endregion 'Поля'
    
    
    
    /**
     * Конструктор со всеми параметрами
     *
     * @param siteRoot Корневой адрес сайта для парсинга
     *
     * @throws IllegalStateException Если в справочнике адресов страниц нет с флагом {@link PageUrlRefEntry#getIsActive()} = true;
     */
    @Autowired
    public SiteParserDonEnergoImpl(@Value("${site.url.root}") String siteRoot,
                                   @Value("${site.page.timeout}") int pageRelaxTimeout,
                                   @Value("${site.page.dumps.dir}") String pageDumpsDir,
                                   @Value("${spring.jpa.properties.hibernate.jdbc.time_zone}") String dbZoneCode,
                                   PageUrlRefEntryRepository pageUrlRefEntryRepository,
                                   OutageRefEntryRepository outageRefEntryRepository,
                                   Logger logger)
    {
        this.siteRoot = URI.create(siteRoot);
        this.pageUrlRefEntryRepository = pageUrlRefEntryRepository;
        this.pageRelaxTimeout = pageRelaxTimeout;
        this.pageDumpsDir = pageDumpsDir;
        this.dbZoneId = ZoneId.of(dbZoneCode);
        this.outageRefEntryRepository = outageRefEntryRepository;
        this.logger = logger;
        config();
    }
    
    
    
    //region 'Свойства'
    
    @Override
    public URI getSiteRoot()
    {
        return siteRoot;
    }
    
    @Override
    public void setSiteRoot(URI siteRoot)
    {
        this.siteRoot = siteRoot;
    }
    
    
    @Override
    public Map<String, URI> getPageAddresses()
    {
        return pageAddresses;
    }
    
    @Override
    public void setPageAddresses(Map<String, URI> pageAddresses)
    {
        this.pageAddresses = pageAddresses;
    }
    
    //endregion 'Свойства'
    
    
    
    
    //region 'Методы'
    
    /**
     * @apiNote  Реализация загружает адреса из справочника - этот метод нужен только, если нужно их переопределить.
     */
    @SafeVarargs
    @Override
    public final void config(URI baseAdr, Map.Entry<String, URI>... pageAdrs)
    {
        this.siteRoot = baseAdr;
        pageAddresses.clear();
        for (Map.Entry<String, URI> adr : pageAdrs)
        {
            pageAddresses.put(adr.getKey(), adr.getValue());
        }
    }
    
    /**
     * @apiNote Нужно вызывать для актуализации списка страниц для мониторинга (вызывается автоматически методами {@link #load()}
     *          и {@link #parse()})
     */
    @Override
    public void config()
    {
        activePageUrls = pageUrlRefEntryRepository.findByIsActive(true);
        if (activePageUrls.isEmpty())
        {
            throw new IllegalStateException("В репозитории адресов страниц нет активных записей - продолжение невозможно");
        }
        setPageAddresses(activePageUrls.stream().collect(Collectors.toMap(PageUrlRefEntry::getName, x -> URI.create(x.getUrl()))));
    }
    
    @Override
    @Transactional
    public boolean load()
    {
        // перечитываем список активных страниц при каждом запросе - иначе он статически "висит" после первоначальной загрузки Бина.
        config();
        boolean isAllOk = true;
        for (Map.Entry<String, URI> curPage : pageAddresses.entrySet())
        {
            try
            {
                logger.info("Охлаждение между загрузкой страниц {} секунд...", this.pageRelaxTimeout);
                Thread.sleep(TimeUnit.SECONDS.toMillis(this.pageRelaxTimeout));
                PageUrlRefEntry pageOption = (activePageUrls == null || activePageUrls.isEmpty()) ? null :
                        activePageUrls.stream().filter(p -> p.getName().equals(curPage.getKey())).findAny().
                                orElse(null);
                isAllOk &= loadPage(curPage.getKey(), curPage.getValue(), pageOption);
            }
            catch (Exception ex)
            {
                logger.error("Неожиданное прерывание потока загрузки страниц внешнего сайта", ex);
            }
        }
        //сохраняем в репозиторий результаты
        if (this.activePageUrls == null)
        {
            return isAllOk;
        }
        try
        {
            this.activePageUrls = this.pageUrlRefEntryRepository.saveAllAndFlush(this.activePageUrls);
        }
        catch (Exception ex)
        {
            logger.error("Ошибка сохранения в БД результатов проверки страниц внешнего сайта", ex);
        }
        return isAllOk;
    }
    
    /**
     * Загружает указанную страницу - Не выбрасывает исключения
     */
    @Transactional
    protected boolean loadPage(String name, URI url, PageUrlRefEntry pageUrlRefEntry)
    {
        URI tagerAddress = null;
        try
        {
            tagerAddress = url.isAbsolute() ? url : UriUtils.addPath(this.siteRoot, url);
            logger.info("Начинаю загрузку страницы {} по адресу {}...", name, tagerAddress);
            Path outFileName = Paths.get(this.pageDumpsDir, name + ".html");
            try (InputStream webStream = tagerAddress.toURL().openStream())
            {
                Files.createDirectories(outFileName.getParent());
                long bytesRead = Files.copy(webStream, outFileName, StandardCopyOption.REPLACE_EXISTING);
                logger.info("Успешно загружена Страница {} по адресу {} в каталог \"{}\", всего считано байт: {}",
                        name, tagerAddress, outFileName.toAbsolutePath(), bytesRead);
                if (pageUrlRefEntry != null)
                {
                    pageUrlRefEntry.setLast_status_message("200 : OK");
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            String errMes = "Ошибка загрузки страницы " + name + " по адресу " + Objects.toString(tagerAddress, url.toString());
            logger.error(errMes, ex);
            if (pageUrlRefEntry != null)
            {
                pageUrlRefEntry.setLast_status_message(errMes + System.lineSeparator() + ex.getMessage());
            }
            return false;
        }
        finally
        {
            if (pageUrlRefEntry != null)
            {
                pageUrlRefEntry.setCheckDateTime(LocalDateTime.now());
            }
        }
    }
    
    @Override
    public boolean load(Path... filePaths)
    {
        throw new NotImplementedException("Метод не определён");
    }
    
    @SafeVarargs
    @Override
    public final boolean load(Map.Entry<String, String>... pages)
    {
        throw new NotImplementedException("Метод не определён");
    }
    
    @Override
    public boolean parse()
    {
        // перечитываем список активных страниц
        config();
        boolean hasUpdates = false;
        for (String pageName : pageAddresses.keySet())
        {
            // прописываем обновление распарсенных данных в справочник отключений
            hasUpdates |= parse(pageName);
        }
        return hasUpdates;
    }
    
    
    @Override
    @Transactional
    public boolean parse(String pageName)
    {
        Path pagePath = null;
        try
        {
            pagePath = Paths.get(this.pageDumpsDir, pageName + ".html");
            if (!Files.exists(pagePath))
            {
                logger.warn("Не найден файл для страницы {}, путь поиска {}", pageName, pagePath);
                return false;
            }
            // пропускаем файлы скачанные не сегодня
            Optional<PageUrlRefEntry> curPageOption = activePageUrls == null || activePageUrls.isEmpty() ? Optional.empty() :
                    activePageUrls.stream().
                    filter(f -> f.getName().equals(pageName)).findAny();
            if (!curPageOption.isPresent())
            {
                return false;
            }
            LocalDateTime fileDateTime = LocalDateTime.ofInstant(Files.getLastModifiedTime(pagePath).toInstant(),
                    this.dbZoneId);
            LocalDateTime lastParseDateTime = curPageOption.get().getParseDateTime();
            // если дата изменения файла позже, чем дата последнего парсинга соот-щей страницы, то пытаемся его распарсить
            if (lastParseDateTime == null || fileDateTime.isAfter(lastParseDateTime))
            {
                logger.info("Начинаю парсинг страницы {} из файла {}...", pageName, pagePath.toAbsolutePath());
                // прописываем изменения в справочник отключений
                return parsePageFile(pageName, pagePath, curPageOption.get());
            }
            else
            {
                logger.info("Отменён парсинг страницы {} из файла \"{}\", т.к. файл вероятно уже распарсен (судя по его дате)",
                        pageName, pagePath.toAbsolutePath());
            }
            return false;
        }
        catch (Exception ex)
        {
            logger.error("Ошибка парсинга страницы " + pageName + " из файла " + ((pagePath != null) ? pagePath.toAbsolutePath() : ""), ex);
            return false;
        }
    }
    
    /**
     * Парсит указанный файл и сохраняет изменения в БД (если они были с момента последнего парсинга, а также удаляет
     *      из Справочника отключений {@link OutageRefEntryRepository} записи связанные с предыдущим парсингом этой страницы).
     *
     * @return True - данные были обновлены, False - данные совпадают
     * 
     * @apiNote Для получения уведомления об обнаруженных изменениях на сайте вызывающий код должен подписаться на события.
     *
     * @implSpec Предполагается, что страницы на сайте изменяют целиком (т.е. вносят новые данные сразу для всех адресов),
     *          если же там будут править только часть записей, то система всё равно будет рассылать изменения ВСЕМ клиентам!!
     *
     * @implNote Образец заголовка таблицы и соот-но структуры колонок:
     *              <table>
     *                 <thead>
     * 						<tr>
     * 							<th style="text-align: center;">№ пп</th>
     * 							<th style="text-align: center;">Наименование<br>населенного<br>пункта</th>
     * 							<th style="text-align: center;">Границы<br>отключения<br>(улицы, дома)</th>
     * 							<th colspan="2" style="text-align: center;">Дата<br>отключения</th>
     * 							<th colspan="2" style="text-align: center;">Время<br>отключения</th>
     * 							<th style="text-align: center;">Причина отключения<br>(вид работ)</th>
     * 							<th style="text-align: center;">Примечание</th>
     * 						</tr>
     * 					</thead>
     * 					<tbody>
     *  		 			<tr>
     * 								<td>1</td>
     * 								<td>г. Новочеркасск</td>
     * 								<td>ул. Пушкинская 1-11, 15-29, 4-12; ул. Кавказская 191, 168; ул. Аксайская 120; ул. Аксайская 88-118/2, 81-99;
     * 								ул. Пушкинская 2</td>
     * 								<td>23.09.22</td>
     * 								<td>23.09.22</td>
     * 								<td>13=00</td>
     * 								<td>17=00</td>
     * 								<td>Техническое обслуживание оборудования ЛЭП-0,4 кВ Л-1, Л-2 от ТП-190</td>
     * 								<td>-//-</td>
     * 							</tr>
     * 					</tbody>
     * 				</table>
     */
    // на уровне метода эта аннотация не помогает, вероятно, поэтому: https://stackoverflow.com/a/60063273/2323972
    @Transactional //- такой метод можно вызывать только из-за пределов класса, иначе нужно помечать весь класс.
    protected boolean parsePageFile(String name, Path pagePath, PageUrlRefEntry page)
    {
        // Сначала получаем хэш текущей записи
        String currentHash = Objects.toString(page != null ? page.getParsedDataHash() : "", "");
        // парсим данные (Первый этап парсинга - получение "сырых" строковых данных)
        logger.info("Запускаем разбор страницы {}, из файла \"{}\"...", name, pagePath.toAbsolutePath());
        List<SiteDataEntry> parsedRows = new ArrayList<>();
        fillSiteDataEntries(parsedRows, pagePath);
        // рассчитываем хэш распарсенных данных
        String parsedHash;
        logger.info("Рассчитываем хэш распарсенных данных для страницы {}, из файла \"{}\"", name, pagePath.toAbsolutePath());
        try (FastByteArrayOutputStream outByteStream = new FastByteArrayOutputStream())
        {
            for (SiteDataEntry row : parsedRows)
            {
                outByteStream.write(row.toString().getBytes());
            }
            parsedHash = DigestUtils.md5DigestAsHex(outByteStream.toByteArray());
        }
        catch (IOException e)
        {
            throw new RuntimeException("Ошибка расчёта хэша распарсенных данных страницы " + name, e);
        }
        // удаляем записи справочника отключений связанные с этим ИД, если данные страницы действительно были обновлены
        if (!parsedHash.equalsIgnoreCase(currentHash))
        {
            logger.info("Парсер: Обнаружено изменение разобранных данных для страницы {}, из файла \"{}\"", name,
                    pagePath.toAbsolutePath());
            if (page == null)
            {
                return false;
            }
            if (UUIDs.isNotEmpty(page.getId()))
            {
                logger.debug("Парсер: Удаляем из справочника отключений данные связанные со страницей: {}...", page);
                long count = this.outageRefEntryRepository.deleteByPageUrlRefEntry_Id(page.getId());
                logger.debug("Парсер: Из справочника отключений удалено записей {}, связанных со страницей: {}", count, page);
            }
            // записываем распарсенные данные в репозиторий (Второй этап парсинга - преобразование данных)
            logger.info("Парсер: Записываем в БД изменения справочника отключений для страницы {}, из файла \"{}\"",
                    name, pagePath.toAbsolutePath());
            for (SiteDataEntry rawEntry : parsedRows)
            {
                // пропускаем пустые записи (те, у которых нет дат отключения)
                if (rawEntry.startDate.isEmpty() || rawEntry.endDate.isEmpty())
                {
                    continue;
                }
                this.outageRefEntryRepository.save(OutageRefEntryBuilder.createBuilder()
                        .city(rawEntry.city)
                        .streetsAndHouses(rawEntry.addresses)
                        .reason(rawEntry.reason)
                        .comments(rawEntry.comments)
                        .startDateTime(LocalDateTimeUtils.parseSiteDateAndTime(rawEntry.startDate, rawEntry.startTime))
                        .endDateTime(LocalDateTimeUtils.parseSiteDateAndTime(rawEntry.endDate, rawEntry.endTime))
                        .pageUrlRefEntry(page)
                .build());
            }
            this.outageRefEntryRepository.flush();
            page.setParseDateTime(LocalDateTime.now());
            page.setParsedDataHash(parsedHash);
            this.pageUrlRefEntryRepository.saveAndFlush(page);
            return true;
        }
        else
        {
            logger.info("Парсер: Страница {} пропущена, т.к. не содержит изменений разобранных данных в файле \"{}\"",
                    name, pagePath.toAbsolutePath());
            return false;
        }
    }
    
    /**
     * Метод непосредственного парсинга данных
     *
     * @param parsedRows Список прокси-объектов, который нужно заполнить
     * @param sourcePath Путь к файлу-источнику данных
     */
    private void fillSiteDataEntries(List<SiteDataEntry> parsedRows, Path sourcePath)
    {
        parsedRows.clear();
        try
        {
            Document doc = Jsoup.parse(sourcePath.toFile());
            logger.info("Начинаю разбор страницы с заголовком \"{}\" из файла \"{}\"...", doc.title(), sourcePath.toAbsolutePath());
            Element table = doc.body().getElementsByClass(SiteDataEntry.TABLE_CSS_CLASS_NAME).first();
            if (table == null)
            {
                throw new SiteParseException("Ошибка парсинга страницы: Не найдена HTML-таблица с именем класса: " +
                        SiteDataEntry.TABLE_CSS_CLASS_NAME);
            }
            Elements rows = table.getElementsByTag("tr");
            final SiteDataEntry.Builder entryBuilder = SiteDataEntry.Builder.createBuilder();
            for (Element row : rows)
            {
                Elements cells = row.getElementsByTag("td");
                // вторая ячейка не должна быть цифрой, т.к. в таком случае это ряд цифр
                if (cells.isEmpty() || !cells.get(0).text().matches(SiteDataEntry.DATA_ROW_FIRST_CELL_PATTERN) ||
                        cells.get(1).text().matches(SiteDataEntry.DATA_ROW_FIRST_CELL_PATTERN))
                {
                    continue;
                }
                entryBuilder.clear();
                final String HTML_LIKE_CHARS_FILTER = "[<>]";
                parsedRows.add(entryBuilder
                        .city(cells.get(1).text().replaceAll(HTML_LIKE_CHARS_FILTER, ""))
                        .addresses(cells.get(2).text().replaceAll(HTML_LIKE_CHARS_FILTER, ""))
                        .startDate(cells.get(3).text().replaceAll(HTML_LIKE_CHARS_FILTER, ""))
                        .endDate(cells.get(4).text().replaceAll(HTML_LIKE_CHARS_FILTER, ""))
                        .startTime(cells.get(5).text().replaceAll(HTML_LIKE_CHARS_FILTER, ""))
                        .endTime(cells.get(6).text().replaceAll(HTML_LIKE_CHARS_FILTER, ""))
                        .reason(cells.get(7).text().replaceAll(HTML_LIKE_CHARS_FILTER, ""))
                        .comments(cells.get(8).text().replaceAll(HTML_LIKE_CHARS_FILTER, ""))
                .build());
            }
            logger.info("Страница \"{}\" из файла \"{}\" успешно разобрана.", doc.title(), sourcePath.toAbsolutePath());
        }
        catch (IOException e)
        {
            throw new RuntimeException("Ошибка доступа к файлу: " + sourcePath.toAbsolutePath(), e);
        }
        catch (Exception ex)
        {
            if (ex instanceof SiteParseException)
            {
                throw (SiteParseException)ex;
            }
            throw new SiteParseException("Ошибка парсинга страницы из файла " + sourcePath.toAbsolutePath(), ex);
        }
    }
    
    //endregion 'Методы'
    
}
