package org.mayurmu.finalwork.gorsvet.service;

import java.time.Instant;

public interface SiteParserService
{
    boolean isTaskStarted();
    
    Instant getStartTime();
    
    void loadAndParse();
    
    void loadAll();
    
    void parseAll();
}
