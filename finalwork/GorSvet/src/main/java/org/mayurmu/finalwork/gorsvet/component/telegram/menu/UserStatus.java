package org.mayurmu.finalwork.gorsvet.component.telegram.menu;

import org.mayurmu.finalwork.gorsvet.model.Bot;

/**
 * Виды пользователей по "Активности"
 *
 * @apiNote Используется как параметр метода {@code MenuMaster.showClientListMenu()}
 */
public enum UserStatus
{
    UNKNOWN(""),
    /**
     * Связанный с действующим ботом
     */
    ACTIVE("Активный"),
    /**
     * Связанный с НЕ-действующим ботом (кроме {@link Bot#BANNED_USERS_BOT})
     */
    INACTIVE("Неактивный"),
    /**
     * Отключённый Админом (т.е. связанный с ботом {@link Bot#BANNED_USERS_BOT})
     */
    BANNED("Отключённый");
    
    public final String title;
    
    UserStatus(String title)
    {
        this.title = title;
    }
}
