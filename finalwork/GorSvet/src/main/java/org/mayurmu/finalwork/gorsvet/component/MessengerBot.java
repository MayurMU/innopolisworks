package org.mayurmu.finalwork.gorsvet.component;

import org.mayurmu.finalwork.gorsvet.component.telegram.CallBackData;
import org.mayurmu.finalwork.gorsvet.component.telegram.InlineButtonPayload;
import org.mayurmu.finalwork.gorsvet.component.telegram.menu.BotMenuState;
import org.mayurmu.finalwork.gorsvet.model.Bot;

import java.util.Map;
import java.util.Set;

/**
 * Универсальный интерфейс управления ботами различных мессенджеров (теоретически, не только Телеграм)
 */
public interface MessengerBot
{
    
    Bot getLinkedBotModel();
    
    String getBotUsername();
    
    void setUserStateTo(long senderId, BotMenuState newState);
    
    BotMenuState getUserState(long userId);
    
    void sendYesNoInlineKeyboard(String title, String chatId, CallBackData yesButtonData, CallBackData noButtonData);
    
    void sendInlineKeyboard(String title, String chatId, Set<Map.Entry<String, InlineButtonPayload>> sourceButtons);
    
    void sendInlineKeyboard(String title, String chatId, Set<Map.Entry<String, InlineButtonPayload>> sourceButtons,
                            int columnsCount);
    
    void sendCustomKeyboard(String title, String chatId, String... buttons);
    
    void sendCustomKeyboard(String title, String chatId, int rowCount, String... buttons);
    
    /**
     * Отправляет простое текстовое сообщение (возможно с несколькими HTML-тегами)
     */
    void sendMessage(long chatID, String message);
    
    /**
     * Отправляет форматированную строку
     *
     * @param format строка формата в стиле {@link java.text.MessageFormat}
     */
    void sendMessage(long chatID, String format, Object... arguments);
    
    /**
     * Отправляет простое текстовое сообщение (возможно с несколькими HTML-тегами) в ответ на указанное сообщение
     *
     * @param replyToMesID Если отрицательное, то игнорируется
     */
    void sendMessage(long chatID, String mesText, int replyToMesID);
    
    void sendYesNoInlineKeyboard(String title, String chatId, CallBackData yesButtonData);
}
