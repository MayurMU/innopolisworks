package org.mayurmu.finalwork.gorsvet.dto.utils;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class EntityMapper
{
    private final ModelMapper modelMapper;
    
    @Autowired
    public EntityMapper(ModelMapper modelMapper)
    {
        this.modelMapper = modelMapper;
    }
    
    /**
     * Метод маппинга списка Объектов Модели в список Dto
     *
     * @see <a href="https://www.baeldung.com/java-modelmapper-lists#using-custom-type-mapping">всзято из примера на Baeldung.com</a>
     */
    public <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        return source
                .stream()
                .map(element -> modelMapper.map(element, targetClass))
                .collect(Collectors.toList());
    }
    
    public <S, T> T map(S source, Class<T> targetClass)
    {
        return Objects.isNull(source) ? null : modelMapper.map(source, targetClass);
    }
    
}
