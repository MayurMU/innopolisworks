package org.mayurmu.finalwork.gorsvet.event;

import org.mayurmu.finalwork.gorsvet.model.Client;
import org.mayurmu.finalwork.gorsvet.model.OutageRefEntry;
import org.mayurmu.finalwork.gorsvet.model.Task;
import org.mayurmu.finalwork.gorsvet.service.TaskService;
import org.springframework.context.ApplicationEvent;

import java.util.List;
import java.util.Map;

/**
 * Событие наличия изменений данных сайта для ряда задач одного клиента
 *
 * @apiNote Генерируется в {@link TaskService}
 */
public class ClientTaskUpdatesFoundEvent extends ApplicationEvent
{
    public final Map<Task, List<OutageRefEntry>> tasksWithUpdates;
    public final Client client;
    
    public ClientTaskUpdatesFoundEvent(Object source, Client client, Map<Task, List<OutageRefEntry>> taskUpdates)
    {
        super(source);
        tasksWithUpdates = taskUpdates;
        this.client = client;
    }
}
