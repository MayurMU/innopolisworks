package org.mayurmu.finalwork.gorsvet.component.DonEnergoParser;

import java.util.StringJoiner;

/**
 * Строка таблицы сайта ДонЭнерго - простой POJO-класс для временного хранения распарсенных данных
 *
 * @implNote первая строка таблицы может состоять из цифр:
 *                      <p>
 *                          <tr>
 * 								<td>1</td>
 * 								<td>2</td>
 * 								<td>3</td>
 * 								<td>4</td>
 * 								<td>5</td>
 * 								<td>6</td>
 * 								<td>7</td>
 * 								<td>8</td>
 * 								<td>9</td>
 * 							</tr>
 * 						</p>
 * 				А может и сразу начинаться с данных:
 * 			            <p>
 * 					        <tr>
 * 								<td>2</td>
 * 								<td>г. Новошахтинск</td>
 * 								<td>ул. Веселая 41; ул. Заводская 46; ул. Кирпичная 22-62, 17-49; ул. Кирпичный проезд 8А-14Б; ул. Лазо 1-21, 2-24А;
 * 								ул. Лебедева 24-34А, 1А, 15-17; ул. Короленко 50-62, 1, 3А-9Б, 23-27</td>
 * 								<td>23.09.22</td>
 * 								<td>22.09.22</td>
 * 								<td>08=00</td>
 * 								<td>17=00</td>
 * 								<td>Работы для исключения аварийных ситуаций на оборудовании ЛЭП-0,4 кВ Л-3 от ТП-106</td>
 * 								<td></td>
 * 							</tr>
 * 						</p>
 */
public class SiteDataEntry
{
    public static final String TABLE_CSS_CLASS_NAME = "table_site1";
    public static final String DATA_ROW_FIRST_CELL_PATTERN = "\\d+";
    
    public final String city;
    public final String addresses;
    public final String startDate;
    public final String endDate;
    public final String startTime;
    public final String endTime;
    public final String reason;
    public final String comments;
    
    public SiteDataEntry(String city, String addresses, String startDate, String endDate, String startTime, String endTime,
                         String reason,
                         String comments)
    {
        this.city = city;
        this.addresses = addresses;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.reason = reason;
        this.comments = comments;
    }
    
    /**
     * @implNote Экономичная реализация для экономии размера строки
     */
    @Override
    public String toString()
    {
        return new StringJoiner("\n")
                .add(city)
                .add(addresses)
                .add(startDate)
                .add(endDate)
                .add(startTime)
                .add(endTime)
                .add(reason)
                .add(comments)
                .toString();
    }
    
    public static final class Builder
    {
        private String city;
        private String addresses;
        private String startDate;
        private String endDate;
        private String startTime;
        private String endTime;
        private String reason;
        private String comments;
        
        private Builder()
        {
        }
        
        public static Builder createBuilder()
        {
            return new Builder();
        }
        
        public Builder city(String city)
        {
            this.city = city;
            return this;
        }
        
        public Builder addresses(String addresses)
        {
            this.addresses = addresses;
            return this;
        }
        
        public Builder startDate(String startDate)
        {
            this.startDate = startDate;
            return this;
        }
        
        public Builder endDate(String endDate)
        {
            this.endDate = endDate;
            return this;
        }
        
        public Builder startTime(String startTime)
        {
            this.startTime = startTime;
            return this;
        }
        
        public Builder endTime(String endTime)
        {
            this.endTime = endTime;
            return this;
        }
        
        public Builder reason(String reason)
        {
            this.reason = reason;
            return this;
        }
        
        public Builder comments(String comments)
        {
            this.comments = comments;
            return this;
        }
        
        public SiteDataEntry build()
        {
            return new SiteDataEntry(city, addresses, startDate, endDate, startTime, endTime, reason, comments);
        }
    
        public void clear()
        {
            String city = null;
            String addresses = null;
            String startDate = null;
            String endDate = null;
            String startTime = null;
            String endTime = null;
            String reason = null;
            String comments = null;
        }
    }
}
