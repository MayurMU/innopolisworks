package org.mayurmu.finalwork.gorsvet.config;

import org.mayurmu.finalwork.gorsvet.exceptions.UnhandledAsyncExceptionHandler;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Назначаем свой обработчик не перехваченных исключений фоновых потоков (что произойдёт без него - неизвестно - вероятно
 *      приложение "упадёт"?)
 *
 * @see <a href="https://stackoverflow.com/a/65490437/2323972">Stack Overflow Example</a>
 */
@Configuration
@EnableAsync
public class AsyncConfigurator implements AsyncConfigurer
{
    private UnhandledAsyncExceptionHandler customAsyncExceptionHandler;
    
    @Autowired
    public void setCustomAsyncExceptionHandler(UnhandledAsyncExceptionHandler customAsyncExceptionHandler)
    {
        this.customAsyncExceptionHandler = customAsyncExceptionHandler;
    }
    
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler()
    {
        return this.customAsyncExceptionHandler;
    }
}
