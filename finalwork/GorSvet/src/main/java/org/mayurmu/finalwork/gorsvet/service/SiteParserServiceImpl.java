package org.mayurmu.finalwork.gorsvet.service;

import org.mayurmu.finalwork.gorsvet.component.SiteParser;
import org.mayurmu.finalwork.gorsvet.event.SiteDataChangedEvent;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

/**
 * Этот сервис нужен для запуска планировщика парсера и генерации событий обнаружения изменений на сайте
 */
@Service
public class SiteParserServiceImpl implements SiteParserService
{
    private final SiteParser parser;
    private final Logger logger;
    private final ApplicationEventPublisher applicationEventPublisher;
    
    private volatile boolean isTaskStarted = false;
    private volatile Instant startTime;
    
    @Autowired
    public SiteParserServiceImpl(SiteParser parser, Logger logger, ApplicationEventPublisher applicationEventPublisher)
    {
        this.parser = parser;
        this.logger = logger;
        this.applicationEventPublisher = applicationEventPublisher;
    }
    
    @Override
    public boolean isTaskStarted()
    {
        return isTaskStarted;
    }
    
    @Override
    public Instant getStartTime()
    {
        return startTime;
    }
    
    
    /**
     * Метод запуска полного цикла работы парсера
     *
     * @implNote Текущий вариант планировщика использует реализацию по умолчанию, т.е. однопоточный парсер (в главном
     * потоке спринга?). Для более сложных вариантов нужно уточнять реализацию или вообще использовать пакет quartz.
     * @apiNote TODO: Вероятно для изменения интервалов проверки сайта потребуется перезапуск приложения, т.к. они берутся
     * из файла конфигурации.
     */
    @Override
    @Scheduled(fixedDelayString = "${site.parse.delay}", initialDelayString = "${site.parse.delay}", timeUnit = TimeUnit.HOURS)
    @Async
    public void loadAndParse()
    {
        final String className = this.getClass().getSimpleName();
        logger.info("**********************************************************************************************");
        try
        {
            synchronized (this)
            {
                if (isTaskStarted)
                {
                    logger.info("{}: Запуск парсинга отменён - Задача парсинга уже выполняется с {}", className, startTime);
                    return;
                }
                isTaskStarted = true;
                startTime = Instant.now();
            }
            logger.info("{}: Запускаю задачу парсинга сайта...", className);
            logger.debug("{}: Выполняю конфигурацию парсера", className);
            parser.config();
            logger.debug("{}: Запускаю загрузки страниц сайта", className);
            if (parser.load())
            {
                logger.debug("{}, Запускаю разбор загруженных страниц сайта", className);
                if (parser.parse())
                {
                    logger.info("{}: (!) Обнаружено изменение данных загруженных страниц, оповещаю внешний код (!)", className);
                    this.applicationEventPublisher.publishEvent(new SiteDataChangedEvent(this));
                }
            }
            else
            {
                logger.info("{}: Пропускаю парсинг страниц, т.к. ничего не было загружено", className);
            }
            logger.info("{}: Задача парсинга сайта завершена, потребовалось времени: {}", className, LocalTime.
                    ofSecondOfDay(Duration.between(startTime, Instant.now()).getSeconds()));
            isTaskStarted = false;
        }
        catch (Exception ex)
        {
            isTaskStarted = false;
            logger.error(className + ": Неожиданная ошибка парсинга", ex);
        }
        finally
        {
            logger.info("**********************************************************************************************");
        }
    }
    
    @Override
    @Async
    public void loadAll()
    {
        this.parser.load();
    }
    
    @Override
    @Async
    public void parseAll()
    {
        boolean isUpdated = this.parser.parse();
        if (isUpdated)
        {
            this.applicationEventPublisher.publishEvent(new SiteDataChangedEvent(this));
        }
    }
    
    
}
