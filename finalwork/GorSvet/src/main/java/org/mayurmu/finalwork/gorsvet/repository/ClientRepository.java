package org.mayurmu.finalwork.gorsvet.repository;

import org.mayurmu.finalwork.gorsvet.model.Bot;
import org.mayurmu.finalwork.gorsvet.model.Client;
import org.mayurmu.finalwork.gorsvet.model.enums.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ClientRepository extends JpaRepository<Client, UUID>
{
    /**
     * Возвращает всех клиентов, кроме "забаненных"
     */
    @Query("select c from Client c where c.bot.secretKey <> ''")
    @NonNull
    List<Client> findAllByIsNotBanned();
    
    /**
     * Возвращает всех "Забаненных" пользователей
     */
    @Query("select c from Client c where c.bot.secretKey = ''")
    List<Client> findAllByIsBanned_True();
    
    /**
     * Возвращает клиента по ИД юзера Телеграм
     *
     * @param messengerChatId ИД чата (он же ИД юзера, т.к. наши боты не могут участвовать в групповых чатах)
     *
     * @return ВНИМАНИЕ: может вернуть в т.ч. и Забаненного Юзера!
     */
    @Query("select c from Client c where c.messengerChatId = ?1")
    Optional<Client> findByMessengerChatId(@NonNull long messengerChatId);
    
    @Query("select c from Client c inner join c.tasks tasks where tasks.id = ?1 and c.bot.secretKey <> ''")
    List<Client> findAllByTasks_Id(@NonNull UUID id);
    
    @Query("select c from Client c where c.role.type = ?1 and c.bot.secretKey <> ''")
    List<Client> findByRole_Type(@NonNull UserRole type);
    
    @Query("select c from Client c where c.bot.isAlive = ?1  and c.bot.secretKey <> ''")
    List<Client> findByBot_IsAlive(boolean isAlive);
    
    @Transactional
    @Modifying
    @Query("update Client c set c.bot = :bot where c.id = :id")
    int updateBotById(@Param("bot") Bot bot, @Param("id") UUID id);

    
}