package org.mayurmu.finalwork.gorsvet.event;

import org.mayurmu.finalwork.gorsvet.component.SiteParser;
import org.springframework.context.ApplicationEvent;


/**
 * Событие для оповещения подписчиков о том, что парсер ({@link SiteParser}) обнаружил изменение данных хотя бы на одной
 *      странице сайта ДонЭнерго.
 *
 * @implSpec Наследовать этот класс от {@link ApplicationEvent} необязательно, начиная со Spring 4.2
 */
public class SiteDataChangedEvent extends ApplicationEvent
{
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     *
     * @throws IllegalArgumentException if source is null.
     */
    public SiteDataChangedEvent(Object source)
    {
        super(source);
    }
}
