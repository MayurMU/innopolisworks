package org.mayurmu.finalwork.gorsvet.repository;

import org.mayurmu.finalwork.gorsvet.model.Role;
import org.mayurmu.finalwork.gorsvet.model.enums.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRepository extends JpaRepository<Role, UUID>
{
    boolean existsByType(UserRole type);
    
}