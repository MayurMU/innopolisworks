package org.mayurmu.finalwork.gorsvet.service;

import org.mayurmu.finalwork.gorsvet.model.Client;

public interface ClientService
{
    void addClient(Client client);
    
    void updateClient(Client client);
}
