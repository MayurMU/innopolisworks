package org.mayurmu.finalwork.gorsvet.component.telegram.menu;

import org.mayurmu.finalwork.gorsvet.component.MessengerBot;
import org.mayurmu.finalwork.gorsvet.component.telegram.BotLogic;

/**
 * Объект для инкапсуляции параметров метода {@link BotLogic#processMenu(MessengerBot, long, String, String, String, boolean)}
 */
public class MessageInfo
{
    public final long senderId;
    public final String senderName;
    public final String chatId;
    public final int messageId;
    
    public MessageInfo(long senderId, String senderName, String chatId, int messageId)
    {
        this.senderId = senderId;
        this.senderName = senderName;
        this.chatId = chatId;
        this.messageId = messageId;
    }
}
