package org.mayurmu.finalwork.gorsvet.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;


public class LocalDateTimeUtils
{
    private static final DateTimeFormatter donEnergoFormatter = DateTimeFormatter.ofPattern("dd.MM.[yyyy][yy]");
    
    public static final DateTimeFormatter googleCalendarTimeFormatter = DateTimeFormatter.ofPattern("HHmm");
    
    // 26.09.2022 9:00  или   30.09.2022 13:00
    public static final DateTimeFormatter rusFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM,
            FormatStyle.SHORT).withLocale(Locale.forLanguageTag("ru")
    );
    /**
     * Парсит дату и время из формата сайта ДонЭнерго в типизированный формат
     *
     * @param startDate Строка даты вида: "23.09.22" или "23.09.22г.", или "23.09.2022г."
     * @param startTime Строка времени вида: "09=00" или "09 =00", или "09-00"
     */
    public static LocalDateTime parseSiteDateAndTime(String startDate, String startTime)
    {
        // избавляемся от всего, кроме цифр и точек из даты "23.09.22г.", чтобы получить "23.09.22"
        LocalDate resultDate = LocalDate.parse(startDate.replaceAll("[^0-9.]\\.$", ""), donEnergoFormatter);
        String[] timeParts = startTime.replaceAll("\\s", "").split("[=-]");
        LocalTime resultTime = LocalTime.of(Integer.parseInt(timeParts[0]), Integer.parseInt(timeParts[1]));
        return LocalDateTime.of(resultDate, resultTime);
    }
}
