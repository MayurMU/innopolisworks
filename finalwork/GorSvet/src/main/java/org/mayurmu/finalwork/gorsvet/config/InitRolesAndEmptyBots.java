package org.mayurmu.finalwork.gorsvet.config;

import org.mayurmu.finalwork.gorsvet.model.Role;
import org.mayurmu.finalwork.gorsvet.repository.BotRepository;
import org.mayurmu.finalwork.gorsvet.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import org.mayurmu.finalwork.gorsvet.model.Bot;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * Класс записывающий все известные программе Роли в хранилище; также добавляет специального пустого бота
 *      (см.: {@link Bot#BANNED_USERS_BOT}, связь с которым уводит пользователя в "Бан")
 */
@Configuration
@Transactional
public class InitRolesAndEmptyBots
{
    final RoleRepository roleRepository;
    final BotRepository botRepository;
    
    @Autowired
    public InitRolesAndEmptyBots(RoleRepository roleRepository, BotRepository botRepository)
    {
        this.roleRepository = roleRepository;
        this.botRepository = botRepository;
        // сначала ищем существующие роли, если не нашли записываем их в репозиторий
        final List<Role> foundRoles = roleRepository.findAll();
        foundRoles.forEach(role -> Arrays.stream(Role.ROLES).filter(role1 -> role1.equals(role))
                .findAny().ifPresent(role1 -> role1.setId(role.getId())));
        for (Role r : Role.ROLES)
        {
            if (!foundRoles.contains(r))
            {
                roleRepository.save(r);
            }
        }
        roleRepository.flush();
        // добавляем или обновляем бота с именем как у ПУСТОГО спец-бота
        this.botRepository.findByUserName(Bot.BANNED_USERS_BOT.getUserName()).ifPresent(bot -> {
                Bot.BANNED_USERS_BOT.setId(bot.getId());
        });
        this.botRepository.save(Bot.BANNED_USERS_BOT);
        // добавляем или обновляем бота с именем как у спец-бота для восстановленных аккаунтов
        this.botRepository.findByUserName(Bot.RESTORED_USERS_BOT.getUserName()).ifPresent(bot -> {
            Bot.RESTORED_USERS_BOT.setId(bot.getId());
        });
        this.botRepository.saveAndFlush(Bot.RESTORED_USERS_BOT);
    }
    
    
}
