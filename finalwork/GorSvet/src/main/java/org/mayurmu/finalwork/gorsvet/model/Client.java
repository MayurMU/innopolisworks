package org.mayurmu.finalwork.gorsvet.model;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.ColumnDefault;
import org.mayurmu.finalwork.gorsvet.model.utils.UUIDs;
import org.mayurmu.finalwork.gorsvet.utils.LocalDateTimeUtils;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Сущность Клиент - Пользователь Телеграмм (или другого мессенджера)
 *
 * @apiNote Сочетание ИД Бота и ИД чата должно быть уникальным, т.к. один бот может вести только один чат с Пользователем
 */
@Entity
@Table(name = "client")
public class Client
{
    
    //region 'Поля'
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    @ColumnDefault(value = "gen_random_uuid()")
    private UUID id = UUIDs.EMPTY;
    
    /**
     * Это поле нужно для того, чтобы знать, куда отправлять уведомления польз-лю, т.к. бот не может писать ему напрямую,
     *  но может писать в уже открытый чат.
     *
     * @see <a href="https://stackoverflow.com/questions/53916393/telegram-bot-does-chat-id-remain-same-for-all-the-time">
     *     Для приватных чатов (а у нас будут только такие), Chat_id всегда равен User_id, так что последний хранить не нужно</a>
     */
    @Column(name = "messenger_chat_id", nullable = false, unique = true)
    private long messengerChatId;
    
    /**
     * Имя пользователя в Телеграмм (может и не быть), но для наглядности полезно
     */
    @Column(name = "messenger_user_name", unique = true, length = 50)
    private String messengerUserName;
    
    /**
     * Почтовый ящик - на случае, если клиент захочет получать уведомления по почте
     */
    @Column(name = "email", unique = true, length = 50)
    private String email;
    
    /**
     * Признак того, хочет ли клиент использовать, указанный им почтовый ящик для оповещений ({@link #email})
     */
    @Column(name = "use_email", nullable = false)
    private boolean useEmail;
    
    /**
     * Дата регистрации польз-ля (т.е. его первой попытки написать боту - дата отправки команды /start)
     */
    @Column(name = "register_date_time", nullable = false)
    private LocalDateTime registerDateTime;
    
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;
    
    /**
     * @apiNote  Если тот же юзер напишет другому (известному системе) боту, то система должна обновить эту ссылку (если
     *      старый бот неактивен, иначе спросить Юзера, откуда он хочет получать уведомления)
     */
    @ManyToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "bot_id", nullable = false)
    private Bot bot;
    
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "client_tasks",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "tasks_id"))
    private Set<Task> tasks = new HashSet<>();
    
    //endregion 'Поля'
    
    
    
    
    //region 'Конструкторы'
    
    /**
     * Пустой конструктор - для соблюдения спецификации и JPA (POJO)
     * @see <a href="https://ru.wikipedia.org/wiki/Java_Persistence_API#Entity">Wikipedia</a>
     *
     * @apiNote  НЕ назначает ИД (оставляет его пустым {@link UUIDs#EMPTY})
     */
    protected Client()
    {
    }
    /**
     * Конструктор Клиента с опциональным списком связанных Задач
     */
    public Client(long messengerChatId, String messengerUserName, String email, boolean useEmail, LocalDateTime registerDateTime,
                  Role role, Bot bot, Task... linkedTasks)
    {
        this.messengerChatId = messengerChatId;
        this.messengerUserName = messengerUserName;
        this.email = email;
        this.useEmail = useEmail;
        this.registerDateTime = registerDateTime;
        this.role = role;
        this.bot = bot;
        if (linkedTasks.length > 0)
        {
            tasks.addAll(Arrays.asList(linkedTasks));
        }
    }
    /**
     * Конструктор Клиента с готовым списком связанных Задач
     */
    public Client(long messengerChatId, String messengerUserName, String email, boolean useEmail, LocalDateTime registerDateTime,
                  Role role, Bot bot,  List<Task> linkedTasks)
    {
        this(messengerChatId, messengerUserName, email, useEmail, registerDateTime, role, bot);
        if (linkedTasks != null && !linkedTasks.isEmpty())
        {
            tasks.addAll(linkedTasks);
        }
    }
    
    //endregion 'Конструкторы'
    
    
    
    
    //region 'Свойства'
    
    public String getMessengerUserName()
    {
        return messengerUserName;
    }
    
    public void setMessengerUserName(String messensgerUserName)
    {
        this.messengerUserName = messensgerUserName;
    }
    
    public Set<Task> getTasks()
    {
        return tasks;
    }
    
    public void setTasks(Set<Task> tasks)
    {
        this.tasks = tasks;
    }
    
    public Bot getBot()
    {
        return bot;
    }
    
    public void setBot(Bot bot)
    {
        this.bot = bot;
    }
    
    public Role getRole()
    {
        return role;
    }
    
    public void setRole(Role role)
    {
        this.role = role;
    }
    
    public LocalDateTime getRegisterDateTime()
    {
        return registerDateTime;
    }
    
    public boolean getUseEmail()
    {
        return useEmail;
    }
    
    public void setUseEmail(boolean useEmail)
    {
        this.useEmail = useEmail;
    }
    
    public long getMessengerChatId()
    {
        return messengerChatId;
    }
    
    public void setMessengerChatId(long telegramChatId)
    {
        this.messengerChatId = telegramChatId;
    }
    
    public String getEmail()
    {
        return email;
    }
    
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public UUID getId()
    {
        return id;
    }
    
    public void setId(UUID id)
    {
        this.id = id;
    }
    
    //endregion 'Свойства'
    
    
    
    
    //region 'Методы'
    
    /**
     * Равными считаем Клиентов с одинаковыми ИД Чата {@link #messengerChatId}
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        
        Client client = (Client) o;
    
        return messengerChatId == client.messengerChatId;
    }
    
    @Override
    public int hashCode()
    {
        return (int) (messengerChatId ^ (messengerChatId >>> 32));
    }
    
    @Override
    public String toString()
    {
        return toString("");
    }
    
    /**
     * Осуществляет более подробный вывод (с кол-влм связанных данных)
     *
     * @implNote Осторожно: при {@code format = "f"} вероятно будут выполняться подзапросы для выборки связей.
     *
     * @param format Поддерживаются форматы:
     *              <p>"f" | "F" = (full) полный (с кол-вом связанных сущностей), Если передана пустая строка,
     *               то вызов эквивалентен методу {@link #toString()}
     *              </p>
     *              <p>
     *               "r"|"R" = (readable) читаемый - без ИД, но с кол-вом задач.
     *              </p>
     */
    public String toString(String format)
    {
        if (format == null)
        {
            format = "";
        }
        StringJoiner res = null;
        if (format.trim().equalsIgnoreCase("r"))
        {
            res = new StringJoiner("\n")
                    .add("id = " + id)
                    .add("\tИД в Телеграмм = " + messengerChatId)
                    .add("\tИмя пользователя в телеграм = " + messengerUserName)
                    .add("\temail = '" + email + "'")
                    .add("\tПрисылать уведомления на e-Mail = " + useEmail)
                    .add("\tДата регистрации = " + registerDateTime.format(LocalDateTimeUtils.rusFormatter))
                    .add("\tРоль = " + role.getType())
                    .add("\tБот = " + bot.getUserName() + " [" + bot.getId() + "]")
                    .add("\tЧисло задач = " + tasks.size());;
        }
        else if (StringUtils.isBlank(format))
        {
            res = new StringJoiner("\n", Client.class.getSimpleName() + ": [", "]")
                    .add("id = " + id)
                    .add("\tmessengerChatId = " + messengerChatId)
                    .add("\tmessengerUserName = " + messengerUserName)
                    .add("\temail = '" + email + "'")
                    .add("\tuseEmail = " + useEmail)
                    .add("\tregisterDateTime = " + registerDateTime)
                    .add("\trole type = " + role.getType())
                    .add("\tbot id = " + bot.getId());
        }
        if (format.trim().equalsIgnoreCase("f"))
        {
            res.add("tasks count = " + tasks.size());
        }
        return res.toString();
    }
    
    /**
     * Проверяет, не является ли клиент отключённым
     *
     * @return
     */
    public boolean isBanned()
    {
        return StringUtils.isBlank(this.bot.getSecretKey());
    }
    
    //endregion 'Методы'
    
}