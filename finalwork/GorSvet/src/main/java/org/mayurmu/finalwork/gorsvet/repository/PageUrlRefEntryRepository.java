package org.mayurmu.finalwork.gorsvet.repository;

import org.mayurmu.finalwork.gorsvet.model.PageUrlRefEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface PageUrlRefEntryRepository extends JpaRepository<PageUrlRefEntry, UUID>
{
    @Query("select p from PageUrlRefEntry p where p.isActive = ?1")
    List<PageUrlRefEntry> findByIsActive(boolean isActive);
    
    
}