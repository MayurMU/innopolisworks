package org.mayurmu.finalwork.gorsvet.utils;

import java.net.URI;
import java.net.URISyntaxException;


public class UriUtils
{
    public static class MalformedURLRuntimeException extends RuntimeException
    {
        public MalformedURLRuntimeException(String msg, Throwable cause)
        {
            super("Malformed URL: " + msg, cause);
        }
    }
    /**
     * Метод "надёжной" конкатенации URL - нужен, т.к. стандартный {@link URI#resolve(URI)} имеет ряд недостатков (см. ссылку)
     *
     * @implNote Вероятно не справится со сложными URL, где есть параметры, якоря и т.д. - для этого есть более мощна версия:
     *          <a href="https://stackoverflow.com/a/62044362/2323972">Extension method on URL</a>
     */
    public static URI addPath(URI uri, URI second)
    {
        return addPath(uri, second.toString());
    }
    /**
     * Метод "надёжной" конкатенации URL - нужен, т.к. стандартный {@link URI#resolve(URI)} имеет ряд недостатков (см. ссылку)
     *
     * @see <a href="https://stackoverflow.com/a/62044362/2323972">From StackOverflow</a>
     */
    public static URI addPath(URI uri, String relPath)
    {
         /*
              foo://example.com:8042/over/there?name=ferret#nose
              \_/   \______________/\_________/ \_________/ \__/
               |           |            |            |        |
            scheme     authority       path        query   fragment
               |   _____________________|__
              / \ /                        \
              urn:example:animal:ferret:nose

            see https://en.wikipedia.org/wiki/Uniform_Resource_Identifier
            */
        try
        {
    
            // cut initial slash of relative path
            String relPathToAdd = relPath.startsWith("/") ? relPath.substring(1) : relPath;
        
            // cut trailing slash of present path
            String path = uri.getPath();
            String pathWithoutTrailingSlash = path.endsWith("/") ? path.substring(0, path.length() - 1) : path;
        
            return new URI(uri.getScheme(),
                    uri.getAuthority(),
                    pathWithoutTrailingSlash + "/" + relPathToAdd,
                    uri.getQuery(),
                    uri.getFragment());
        }
        catch (URISyntaxException e)
        {
            throw new MalformedURLRuntimeException("Error parsing URI.", e);
        }
    }
}
