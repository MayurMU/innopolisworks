package org.mayurmu.finalwork.gorsvet.repository;

import org.mayurmu.finalwork.gorsvet.model.OutageRefEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface OutageRefEntryRepository extends JpaRepository<OutageRefEntry, UUID>
{
    /**
     * @return Полный список записей справочника отключений объединённый со справочником страниц сайта
     *
     * @see <a href="https://www.logicbig.com/tutorials/java-ee-tutorial/jpa/fetch-join.html">Приммер JOIN FETCH на JPQL</a>
     */
    @Query("SELECT oe from OutageRefEntry oe JOIN FETCH oe.pageUrlRefEntry p")
    List<OutageRefEntry> findAllEager();
    
    /**
     * Метод удаления "устаревших" записей справочника, связанных с обновлёнными страницами сайта
     *
     * @apiNote Вызывается после обновления данных с сайта
     *
     * @return Кол-во удалённых записей
     */
    long deleteByPageUrlRefEntry_Id(UUID id);
}