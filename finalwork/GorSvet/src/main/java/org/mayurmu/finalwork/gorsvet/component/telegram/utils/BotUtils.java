package org.mayurmu.finalwork.gorsvet.component.telegram.utils;

import org.apache.commons.lang3.StringUtils;
import org.mayurmu.finalwork.gorsvet.component.MessengerBot;
import org.mayurmu.finalwork.gorsvet.model.Client;
import org.mayurmu.finalwork.gorsvet.model.Role;
import org.slf4j.Logger;

import java.util.Objects;
import java.util.regex.Pattern;


/**
 * Класс для хранения вспомогательных методов обработки логики Ботов
 */
public class BotUtils
{
    public static final int MIN_SEARCH_TEXT_LENGTH = 4;
    public static final int MAX_SEARCH_TEXT_LENGTH = 30;
    // Ввод юзера ограничен только знаками русского алфавита, а также символами (,-/)
    public static final Pattern DISALLOWED_INPUT_CHARS_PATTERN = Pattern.compile("[^,-/А-Яа-я 0-9]");
    
    /**
     * Метод проверки предельной длины ввода юзера
     *
     * @param botImpl Бот-исполнитель, который передаст сообщение об ошибке Юзеру
     */
    public static boolean checkInputLength(MessengerBot botImpl, long senderId, String replyText)
    {
        int inputLength = Objects.toString(replyText, "").length();
        if (inputLength < MIN_SEARCH_TEXT_LENGTH)
        {
            botImpl.sendMessage(senderId, "Вы ввели строку длиной {0} символов, однако мин. длина строки для поиска равна {1}",
                    inputLength, MIN_SEARCH_TEXT_LENGTH);
            return false;
        }
        else if (inputLength > MAX_SEARCH_TEXT_LENGTH)
        {
            botImpl.sendMessage(senderId, "Вы ввели строку длиной {0} символов, однако макс. длина строки для поиска равна {1}",
                    inputLength, MAX_SEARCH_TEXT_LENGTH);
            return false;
        }
        return true;
    }
    
    public static boolean checkLegalChars(MessengerBot botImpl, long senderId, String replyText)
    {
        return checkLegalChars(botImpl, senderId, replyText, -1);
    }
    
    public static boolean checkLegalChars(MessengerBot botImpl, long senderId, String replyText, int replyToMesId)
    {
        if (StringUtils.isBlank(replyText))
        {
            return false;
        }
        if (DISALLOWED_INPUT_CHARS_PATTERN.matcher(replyText).find())
        {
            botImpl.sendMessage(senderId, "Вы ввели недопустимые символы разрешены только русские буквы, цифры," +
                            " а также пробел и знаки '-' ',' '/'", replyToMesId);
            return false;
        }
        return true;
    }
    
    /**
     * Метод проверяет, обладает ли указанный пользователь правами Админа, если нет.
     *
     * @return False - у юзера не прав Админна - вызывающий код должен прекратить выполнение.
     */
    public static boolean hasAdminRights(MessengerBot botImpl, Client client, long senderId, Logger logger)
    {
        if (client.getRole().getType() != Role.ADMIN.getType())
        {
            logger.warn("{}: [{}] Обнаружена попытка несанкционированного доступа от юзера {} ({})", BotUtils.class.getSimpleName(),
                    botImpl.getBotUsername(), client.getMessengerUserName(), senderId);
            return true;
        }
        return false;
    }
    
    /**
     * Возвращает нечто вроде Имени пользователя
     */
    public static String getClientTitle(Client cl)
    {
        return Objects.toString(cl.getMessengerUserName(), Objects.toString(cl.getEmail(),
                cl.getId().toString().substring(0, 9)));
    }
}
