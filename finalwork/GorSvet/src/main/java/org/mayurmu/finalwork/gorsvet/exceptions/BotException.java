package org.mayurmu.finalwork.gorsvet.exceptions;

public class BotException extends RuntimeException
{
    public BotException(String mes, Throwable cause)
    {
        super(mes, cause);
    }
    
    public BotException(String mes)
    {
        super(mes);
    }
}
