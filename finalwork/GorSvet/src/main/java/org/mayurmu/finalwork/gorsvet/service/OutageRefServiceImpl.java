package org.mayurmu.finalwork.gorsvet.service;

import org.apache.commons.lang3.StringUtils;
import org.mayurmu.finalwork.gorsvet.model.OutageRefEntry;
import org.mayurmu.finalwork.gorsvet.model.Task;
import org.mayurmu.finalwork.gorsvet.repository.OutageRefEntryRepository;
import org.mayurmu.finalwork.gorsvet.utils.AddressUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class OutageRefServiceImpl implements OutageRefService
{
    private final OutageRefEntryRepository outageRefEntryRepository;
    
    @Autowired
    public OutageRefServiceImpl(OutageRefEntryRepository outageRefEntryRepository)
    {
        this.outageRefEntryRepository = outageRefEntryRepository;
    }
    
    /**
     * TODO: Для этого метода логичнее задействовать поиск в БД
     */
    @Override
    public List<OutageRefEntry> checkAddress(String city, String address)
    {
        List<OutageRefEntry> allOutageEntries = outageRefEntryRepository.findAllEager();
        return checkAddress(null, city, address, allOutageEntries);
    }
    /**
     * TODO: Для этого метода логичнее задействовать поиск в БД
     */
    @Override
    public List<OutageRefEntry> checkAddress(LocalDateTime lastCheckDateTime, String city, String address)
    {
        List<OutageRefEntry> allOutageEntries = outageRefEntryRepository.findAllEager();
        return checkAddress(lastCheckDateTime, city, address, allOutageEntries);
    }
    /**
     * Метод поиска на основе кешированных данных, переданных в последнем параметре
     */
    @Override
    public List<OutageRefEntry> checkAddress(@Nullable LocalDateTime lastCheckDateTime, String city, String address,
                                             List<OutageRefEntry> outageEntries)
    {
        return outageEntries.stream().filter(entry -> dateCityAndAddressMatches(entry, lastCheckDateTime, city, address)).
                collect(Collectors.toList());
    }
    
    /**
     * Метод фильтрации справочника отключений по указанной дате обновления, Городу и Названию улицы
     *
     * @param taskDateTime Дата проверки задачи - будут возвращены только те записи, даты обновления которых Позже этой даты.
     * @param city Целевой город
     * @param address Целевая улица (и возможно номер дома, см.: {@link Task#STREET_HOUSE_SEPARATOR})
     */
    private boolean dateCityAndAddressMatches(OutageRefEntry entry, @Nullable LocalDateTime taskDateTime, String city, String address)
    {
        boolean result;
        if (taskDateTime == null)
        {
            result = true;
        }
        else
        {
            LocalDateTime entryDate = entry.getPageUrlRefEntry().getParseDateTime();
            result = (entryDate == null) || entryDate.isAfter(taskDateTime);
        }
        if (!result)
        {
            return false;
        }
        // если дата подходящая, то проверяем остальное
        String taskCity = AddressUtils.cleanCity(city);
        // оказалось, что город может иметь следующий вид: "г. Новошахтинск, пос. Соколово-Кундрюченский"
        if (Arrays.stream(entry.getCity().split(Task.STREET_HOUSE_SEPARATOR)).map(AddressUtils::cleanCity).noneMatch(taskCity::equals))
        {
            return false;
        }
        // если города совпали, можно проверять улицу и номер дома (если есть)
        // получаем начальную часть адреса задачи - что-то вроде: "ул. Маяковского"
        final String taskStreet = AddressUtils.cleanStreet(StringUtils.substringBefore(address, Task.STREET_HOUSE_SEPARATOR));
        if (StringUtils.isBlank(taskStreet))
        {
            return result;
        }
        String outageAdr = entry.getStreetsAndHouses();
        // если в Задаче есть Улица, а в записи Справочника отключений её нет, то эта запись нам не подходит.
        if (StringUtils.isBlank(outageAdr))
        {
            return false;
        }
        if (Objects.toString(outageAdr, "").toLowerCase().contains(OutageRefEntry.ALL_STREETS_KEY_WORD))
        {
            return true;
        }
        // пример записи адреса из справочника отключений:
        // ул. 8 Марта 123А-157Б, 78-156А; ул. Бабушкина 79-157, 80-158; ул. Троицкая 75А, 75Б;
        // ул. Первомайская 132-152, 89, 99; ул. Просвещения 180-182; ул. Крупской 86-94, 93-101;
        // ул. 8 Марта 113-123; пер. Жлобы; ул. Просвещения 211-221, 184-188
        // пер. Мичурина 2/12-2/20, 1/1-1/15; пер. Мичурина 76-80А, 101-109; пер. Дружный 2-10, 1-3.
        String[] outageAdrParts = outageAdr.split(OutageRefEntry.STREETS_SEPARATOR);
        List<String> foundOutageAdrParts = Arrays.stream(outageAdrParts).       //пример: "ул. Просвещения 211-221, 184-188"
                map(AddressUtils::cleanStreet).                                         // пример: "просвещения 211-221, 184-188"
                filter(s -> s.startsWith(taskStreet)).
                collect(Collectors.toList());
        if (foundOutageAdrParts.isEmpty())
        {
            return false;
        }
        // если улица совпала, то проверяем дом(а), если они были указаны Клиентом
        // получаем конечную часть адреса задачи - примеры: "15", "24А", "25 Б корпус 3", "пос. Луговой"
        final String taskHouse = StringUtils.substringAfter(address, Task.STREET_HOUSE_SEPARATOR).trim();
        if (StringUtils.isNotBlank(taskHouse))
        {
            result = foundOutageAdrParts.stream().                           // ул. 8 Марта 123А-157Б, 78-156А; ул. Бабушкина 132-152, 89, 99Б;
                    map(s -> StringUtils.substringAfter(s, taskStreet)).     // что-то вроде: "132-152, 89, 99Б"
                    anyMatch(s ->                                            // нужно попадание в перечисление либо в диапазон
                    Arrays.stream(s.split(Task.STREET_HOUSE_SEPARATOR)).           // "132-152" или "89", или "99Б"
                            anyMatch(range -> range.trim().equals(taskHouse) ||    // совпадение с точной строкой номера дома
                            houseRangeContains(range, taskHouse)              // или попадание в диапазон
                    )
            );
        }
        return result;
    }

    /**
     * Метод проверки, содержится ли указанный номер дома в диапазоне домов
     *
     * @param houseRange строка вида "78-156А"
     * @param taskHouse номер дома вида "99Б"
     *
     * @return True, если номер дома попадает в диапазон, при этом буквы / литеры / строения
     */
    private boolean houseRangeContains(String houseRange, String taskHouse)
    {
        // грубая проверка на соответствие шаблону диапазона номеров, например вот такая строка тоже подойдёт "78 ghhjgjjgjh(((-15А"
        if (!houseRange.trim().matches("\\d+[^0-9]*-\\d+[^0-9]*"))
        {
            return false;
        }
        final String HOUSE_POSTFIX = "(корпус|корп|к|кв|квартира|квартиры|часть квартир|строение|стр|с|лит|литера|литер).*";
        // проверяем, диапазон домов нам подали на вход или что-то другое, если не диапазон, то выходим
        // один диапазон адресов имеет вид: "78-156А"
        String[] rangePats = houseRange.trim().replaceAll("[^0-9-]", "").split(OutageRefEntry.HOUSE_RANGE_SEPARATOR);
        if (rangePats.length < 2)
        {
            return false;
        }
        int startNumber = Integer.parseInt(rangePats[0]);
        int endNumber = Integer.parseInt(rangePats[1]);
        // удаляем из номера дома, всё что не сам номер, т.е., например, от строки "78А корпус 3, строение 1", оставляем "78"
        String cleanedTaskHouse = taskHouse.replaceAll(HOUSE_POSTFIX, "").replaceAll("[^0-9]", "").trim();
        int taskHouseNumber = Integer.parseInt(cleanedTaskHouse);
        return taskHouseNumber >= startNumber && taskHouseNumber <= endNumber;
    }

}
