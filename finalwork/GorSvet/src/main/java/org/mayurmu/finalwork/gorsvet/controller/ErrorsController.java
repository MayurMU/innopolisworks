package org.mayurmu.finalwork.gorsvet.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ErrorsController
{
    @GetMapping("/error/500")
    public String get500Error()
    {
        return "Ошибка сервера";
    }
}
