package org.mayurmu.finalwork.gorsvet.model;

import org.hibernate.annotations.ColumnDefault;
import org.mayurmu.finalwork.gorsvet.model.utils.UUIDs;
import org.mayurmu.finalwork.gorsvet.component.SiteParser;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.UUID;

/**
 * Сущность-справочник веб-адресов страниц для парсинга (см.: {@link SiteParser}), а также состояний их проверки
 */
@Entity
@Table(name = "page_url_reference")
public class PageUrlRefEntry
{
    
    //region 'Поля'
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    @ColumnDefault("gen_random_uuid()")
    private UUID id = UUIDs.EMPTY;
    
    /**
     * Название страницы
     */
    @Column(name = "name", nullable = false, unique = true, length = 50)
    private String name;
    
    /**
     * Адрес страницы (абсолютный или относительный (см. файл настроек: site.url.root))
     */
    @Column(name = "url", nullable = false, unique = true)
    private String url;
    
    /**
     * Описание страницы, что на ней, за какие Города отвечает
     */
    @Column(name = "description", length = 500)
    private String description;
    
    /**
     * Дата и время последней попытки загрузить страницу
     */
    @Column(name = "check_date_time")
    private LocalDateTime checkDateTime;
    
    /**
     * Дата и время последнего удачного парсинга страницы
     */
    @Column(name = "parse_date_time")
    private LocalDateTime parseDateTime;
    
    /**
     * Сообщение об ошибке (возможно стэктрейс?), либо статус HTTP вроде 200=OK
     */
    @Column(name = "last_status_message", length = 2000)
    private String last_status_message;
    
    /**
     * Текущее состояние данной записи: True - будет проверяться, False - архивная (временно отключена)
     */
    @Column(name = "is_active", nullable = false)
    private boolean isActive;
    
    /**
     * MD5-hash "сырого" строкового содержимого страницы (чтобы понять, были там изменения или нет)
     */
    @Column(name = "parsed_data_hash", nullable = true, unique = true)
    private String parsedDataHash;
    
    /**
     * Обратная ссылка на список связанных записей справочник отключений
     */
    @OneToMany(mappedBy = "pageUrlRefEntry", orphanRemoval = false)
    private List<OutageRefEntry> outageRefEntries = new ArrayList<>();
    
    //endregion 'Поля'
    
    
    
    
    //region 'Конструкторы'
    
    protected PageUrlRefEntry()
    {}
    
    //endregion 'Конструкторы'
    
    
    
    
    //region 'Свойства'
    
    public String getParsedDataHash()
    {
        return parsedDataHash;
    }
    
    public void setParsedDataHash(String parsedDataHash)
    {
        this.parsedDataHash = parsedDataHash;
    }
    
    public boolean getIsActive()
    {
        return isActive;
    }
    
    public void setIsActive(boolean isActive)
    {
        this.isActive = isActive;
    }
    
    public String getLast_status_message()
    {
        return last_status_message;
    }
    
    public void setLast_status_message(String last_status_message)
    {
        this.last_status_message = last_status_message;
    }
    
    public LocalDateTime getParseDateTime()
    {
        return parseDateTime;
    }
    
    public void setParseDateTime(LocalDateTime parseDateTime)
    {
        this.parseDateTime = parseDateTime;
    }
    
    public LocalDateTime getCheckDateTime()
    {
        return checkDateTime;
    }
    
    public void setCheckDateTime(LocalDateTime lastCheckDateTime)
    {
        this.checkDateTime = lastCheckDateTime;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public String getUrl()
    {
        return url;
    }
    
    public void setUrl(String url)
    {
        this.url = url;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public List<OutageRefEntry> getOutageRefEntries()
    {
        return outageRefEntries;
    }
    
    public void setOutageRefEntries(List<OutageRefEntry> outageRefEntries)
    {
        this.outageRefEntries = outageRefEntries;
    }
    
    public UUID getId()
    {
        return id;
    }
    
    public void setId(UUID id)
    {
        this.id = id;
    }
    
    //endregion 'Свойства'
    
    
    
    
    //region 'Методы'
    
    @Override
    public String toString()
    {
        return toString("");
    }
    
    /**
     * @param format "f"|"F" - дополненный вывод с кол-вом связанных объектов
     */
    public String toString(String format)
    {
        StringJoiner res = new StringJoiner("\n", PageUrlRefEntry.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("\tname='" + name + "'")
                .add("\turl='" + url + "'")
                .add("\tdescription='" + description + "'")
                .add("\tcheckDateTime=" + checkDateTime)
                .add("\tparseDateTime=" + parseDateTime)
                .add("\tlast_status_message='" + last_status_message + "'")
                .add("\tisActive=" + isActive)
                .add("\tparsedDataHash='" + parsedDataHash + "'");
        if (format != null && format.trim().equalsIgnoreCase("f"))
        {
            res.add("outageRefEntries_count = " + outageRefEntries.size());
        }
        return res.toString();
    }
    
    
    //endregion 'Методы'
    
}