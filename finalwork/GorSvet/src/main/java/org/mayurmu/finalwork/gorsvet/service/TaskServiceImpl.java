package org.mayurmu.finalwork.gorsvet.service;

import org.mayurmu.finalwork.gorsvet.event.ClientTaskUpdatesFoundEvent;
import org.mayurmu.finalwork.gorsvet.event.SiteDataChangedEvent;
import org.mayurmu.finalwork.gorsvet.event.TaskUpdatesFoundEvent;
import org.mayurmu.finalwork.gorsvet.model.Client;
import org.mayurmu.finalwork.gorsvet.model.OutageRefEntry;
import org.mayurmu.finalwork.gorsvet.model.Role;
import org.mayurmu.finalwork.gorsvet.model.Task;
import org.mayurmu.finalwork.gorsvet.model.enums.UserRole;
import org.mayurmu.finalwork.gorsvet.repository.ClientRepository;
import org.mayurmu.finalwork.gorsvet.repository.OutageRefEntryRepository;
import org.mayurmu.finalwork.gorsvet.repository.TaskRepository;
import org.mayurmu.finalwork.gorsvet.utils.AddressUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

/**
 * Сервис управляющий задачами напоминаний
 *
 * @implNote Можно не реализовывать интерфейс {@link ApplicationListener}, начиная со Spring 4.2 - просто помечать метод
 *          как {@link EventListener}
 */
@Service
public class TaskServiceImpl implements TaskService, ApplicationListener<SiteDataChangedEvent>
{
    private final Logger logger;
    private final String className = TaskServiceImpl.class.getSimpleName();
    private final OutageRefEntryRepository outageRefEntryRepository;
    private final TaskRepository taskRepository;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final ClientRepository clientRepository;
    
    private final int maxTaskCountPerClient;
    private final OutageRefService outageRefService;
    
    private volatile boolean isTaskStarted = false;
    private volatile Instant startTime;
    
    
    
    @Autowired
    public TaskServiceImpl(@Value("${client.max.task.count}") int maxTaskCountPerClient,
                           OutageRefEntryRepository outageRefEntryRepository,
                           TaskRepository taskRepository,
                           ApplicationEventPublisher applicationEventPublisher,
                           OutageRefService outageRefService,
                           Logger logger, ClientRepository clientRepository)
    {
        this.logger = logger;
        this.outageRefEntryRepository = outageRefEntryRepository;
        this.taskRepository = taskRepository;
        this.applicationEventPublisher = applicationEventPublisher;
        this.maxTaskCountPerClient = maxTaskCountPerClient;
        this.outageRefService = outageRefService;
        this.clientRepository = clientRepository;
    }
    
    
    @Override
    public Instant getStartTime()
    {
        return startTime;
    }
    
    @Override
    public int getMaxTaskCountPerClient()
    {
        return maxTaskCountPerClient;
    }
    
    @Override
    public boolean isTaskStarted()
    {
        return isTaskStarted;
    }
    
    /**
     * @implSpec  По умолчанию все события выполняются в синхронном режиме - значит их обработку нужно выносить в отдельный
     *          асинхронный метод
     */
    @Override
    public void onApplicationEvent(SiteDataChangedEvent event)
    {
        logger.info("{}: (!) Обнаружено изменение данных сайта, событие от {} (!)", className, event.getSource());
        checkAll();
    }
    
    /**
     * Запускает проверку всех задач по событию обнаружения изменений на сайте со стороны парсера (см. {@link SiteDataChangedEvent})
     */
    @Override
    @Async
    @Transactional
    public void checkAll()
    {
        logger.info("================================================================================================");
        try
        {
            synchronized (this)
            {
                if (isTaskStarted)
                {
                    logger.info("{}: Запуск проверки задач отменён - проверка уже выполняется", className);
                    return;
                }
                isTaskStarted = true;
                startTime = Instant.now();
            }
            logger.info("{}: начинаю проверку задач...", className);
            // Получаем список всех задач с "активными" ботами - теоретически их может быть очень много (так что
            // по идее тут нужна либо пагенация, либо какой-то комплексный Запрос, который исключит нижележащий код.
            final Collection<Task> activeTasks = taskRepository.findByClients_Bot_IsAliveTrue();
            //TODO: возможно тут можно сделать какой-то очень сложный SQL-запрос, чтобы не выбирать все записи Справочника отключений.
            notifyClientsFor(activeTasks);
            logger.info("{}: Проверка задач завершена, потребовалось времени: {}", className, LocalTime.ofSecondOfDay(
                    Duration.between(startTime, Instant.now()).getSeconds()));
            isTaskStarted = false;
        }
        catch (Exception ex)
        {
            isTaskStarted = false;
            logger.error(className + ": Неожиданная ошибка проверки списка всех задач", ex);
        }
        finally
        {
            logger.info("================================================================================================");
        }
    }
    
    @Override
    public void checkAllByClient(Client client)
    {
        if (!client.getBot().getIsAlive())
        {
            return;
        }
        final Set<Task> clientTasks = client.getTasks();
        if (clientTasks.isEmpty())
        {
            return;
        }
        notifyClientsFor(clientTasks, client);
    }
    
    private void notifyClientsFor(Collection<Task> clientTasks)
    {
        notifyClientsFor(clientTasks, null);
    }
    
    /**
     * Находит записи справочники отключений для указанных задач и отправляет уведомления одному или нескольким клиентам.
     */
    private void notifyClientsFor(Collection<Task> clientTasks, @Nullable Client targetClient)
    {
        final List<OutageRefEntry> allOutageEntries = outageRefEntryRepository.findAllEager();
        Map<Task, List<OutageRefEntry>> taskUpdates = checkTasks(clientTasks, allOutageEntries);
        if (taskUpdates.isEmpty() && targetClient == null)
        {
            logger.info("{}: нет обновлений ни по одной задаче!", className);
        }
        else
        {
            logger.info("{}: (!) Отправляю событие с оповещением для {} задач (!)", className, taskUpdates.size());
            if (targetClient != null)
            {
                this.applicationEventPublisher.publishEvent(new ClientTaskUpdatesFoundEvent(this, targetClient, taskUpdates));
            }
            else
            {
                this.applicationEventPublisher.publishEvent(new TaskUpdatesFoundEvent(this, taskUpdates));
            }
        }
    }
    
    /**
     * Добавляет новую задачу или возвращает существующую привязанную к указанному клиенту
     *
     * @apiNote Побочный эффект - Если клиент был Гостем {@link UserRole#GUEST}, то он повышается до Пользователя {@link UserRole#USER}
     *
     * @implNote TODO: Нужно переписать на запрос в БД, чтобы не выбирать каждый раз список всех задач
     */
    @Override
    @Transactional
    public Task addTask(String city, String address, Client client)
    {
        String cleanedCity = AddressUtils.cleanCity(city);
        String cleanedStreet = AddressUtils.cleanStreet(address);
        List<Task> allTasks = taskRepository.findAll();
        Optional<Task> foundTaskOption = allTasks.stream().filter(task -> AddressUtils.cleanCity(task.getCity()).equals(cleanedCity) &&
                AddressUtils.cleanStreet(task.getAddress()).equals(cleanedStreet)).findAny();
        Task result;
        if (foundTaskOption.isPresent())
        {
            foundTaskOption.get().setLastCheckDateTime(null);
            result = foundTaskOption.get();
        }
        else
        {
            Task tsk = new Task(city, address, LocalDateTime.now());
            result = tsk;
        }
        client.getTasks().add(result);
        if (client.getRole().getType() == UserRole.GUEST)
        {
            client.setRole(Role.USER);
        }
        clientRepository.saveAndFlush(client);
        Task finalResult = result;
        Optional<Task> resOpt = client.getTasks().stream().filter(task -> task.equals(finalResult)).findAny();
        // вручную добавляем связанного Клиента в Задачу, т.к. Hibernate не умеет обновлять обратные связи
        if (resOpt.isPresent())
        {
            resOpt.get().getClients().add(client);
            result = resOpt.get();
        }
        return result;
    }
    
    @Override
    // Так не работает - в любом случае идёт откат (UnexpectedRollbackException: Transaction rolled back because it has
    // been marked as rollback-only) - вероятно это из-за того, что вызов идёт из Сервис, который целиком помечен, как
    //@Transactional - см.: https://stackoverflow.com/a/19354463/2323972
    //@Transactional(noRollbackForClassName = "DataIntegrityViolationException")
    @Transactional
    public boolean deleteTaskByIdFromClient(UUID selectedEntity, Client client)
    {
        // каскад в данном случае действует только на таблицу связи, НО не на список задач!
        boolean result = client.getTasks().removeIf(task -> task.getId().equals(selectedEntity));
        clientRepository.saveAndFlush(client);
        try
        {
            // удаляем Задачу, если на неё больше нет ссылок
            taskRepository.findById(selectedEntity).ifPresent(task -> {
                if (task.getClients().isEmpty())
                {
                    taskRepository.deleteById(selectedEntity);
                    taskRepository.flush();
                }
            });
        }
        catch (DataIntegrityViolationException ex)
        {
            result = false;
            logger.debug("{}: Всё в порядке, удалить Задачу с ИД ({}) не удалось из-за ограничений ссылочной целостности: {}",
                    className, selectedEntity, ex.toString());
        }
        return result;
    }
    
    @Override
    public boolean checkTaskByIdForClient(UUID taskId, Client client)
    {
        Optional<Task> foundTaskOption = taskRepository.findById(taskId);
        if (!foundTaskOption.isPresent() || !client.getTasks().contains(foundTaskOption.get()))
        {
            return false;
        }
        // сбрасываем дату последней проверки задачи
        foundTaskOption.get().setLastCheckDateTime(null);
        notifyClientsFor(Collections.singletonList(foundTaskOption.get()), client);
        return true;
    }
    
    @Override
    public void resetAllCheckDates()
    {
        taskRepository.resetAllCheckDates();
    }
    
    @Override
    public Optional<Task> findTaskById(UUID id)
    {
        return taskRepository.findById(id);
    }
    
    /**
     * Проверят все указанные задачи по списку Отключений и отправляет клиенту уведомления, если есть новости.
     */
    @Transactional
    protected Map<Task, List<OutageRefEntry>> checkTasks(Collection<Task> activeTasks, List<OutageRefEntry> allOutageEntries)
    {
        final Map<Task, List<OutageRefEntry>> taskUpdates = new HashMap<>();
        for (Task tsk : activeTasks)
        {
            logger.debug("{}: Проверяю задачу [{} {} {}]...", className, tsk.getId(), tsk.getCity(), tsk.getAddress());
            List<OutageRefEntry> updatedOutageRefEntries = outageRefService.checkAddress(tsk.getLastCheckDateTime(),
                    tsk.getCity(), tsk.getAddress(), allOutageEntries);
            // если Клиент указал задаче номер(а) дома(ов), то проверяем их наличие - иначе отказываем в напоминании
            if (!updatedOutageRefEntries.isEmpty())
            {
                taskUpdates.put(tsk, updatedOutageRefEntries);
            }
            logger.debug("{}: Проверка задачи [{} {} {}] завершена, найдено {} обновлений сохраняю изменения...",
                    className, tsk.getId(), tsk.getCity(), tsk.getAddress(), updatedOutageRefEntries.size());
            tsk.setLastCheckDateTime(LocalDateTime.now());
            taskRepository.save(tsk);
            logger.debug("{}: Изменения для задачи [{} {} {}] записаны в репозиторий", className, tsk.getId(),
                    tsk.getCity(), tsk.getAddress());
        }
        taskRepository.flush();
        return taskUpdates;
    }

    

}
