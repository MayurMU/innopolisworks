package org.mayurmu.finalwork.gorsvet.service;

import org.mayurmu.finalwork.gorsvet.component.MessengerBot;
import org.mayurmu.finalwork.gorsvet.event.ClientTaskUpdatesFoundEvent;
import org.mayurmu.finalwork.gorsvet.event.TaskUpdatesFoundEvent;
import org.mayurmu.finalwork.gorsvet.model.Bot;
import org.mayurmu.finalwork.gorsvet.model.Client;
import org.mayurmu.finalwork.gorsvet.model.OutageRefEntry;
import org.mayurmu.finalwork.gorsvet.model.Task;
import org.mayurmu.finalwork.gorsvet.repository.ClientRepository;
import org.mayurmu.finalwork.gorsvet.utils.LocalDateTimeUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriUtils;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Сервис управления Ботами: реагирует на событие обновления сайта (по задачам {@link TaskUpdatesFoundEvent})
 */
@Service
public class BotServiceImpl implements BotService
{
    private final Logger logger;
    private final String className = BotServiceImpl.class.getSimpleName();
    private final long botSleepTimeount;
    private final List<MessengerBot> messengerBots;
    private final ClientRepository clientRepository;
    
    private volatile boolean isTaskStarted = false;
    private volatile Instant startTime;
    
    
    @Autowired
    public BotServiceImpl(Logger logger, @Value("${bot.message.timeout}") int botSleepTimeout, List<MessengerBot> messengerBots,
                          ClientRepository clientRepository)
    {
        this.logger = logger;
        this.botSleepTimeount = TimeUnit.SECONDS.toMillis(botSleepTimeout);
        this.messengerBots = messengerBots;
        this.clientRepository = clientRepository;
    }
    
    @Override
    public boolean isTaskStarted()
    {
        return isTaskStarted;
    }
    
    @Override
    public Instant getStartTime()
    {
        return startTime;
    }
    
    @Override
    @EventListener
    //@TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
    @Transactional      //(propagation = Propagation.REQUIRES_NEW)
    public void onApplicationEvent(TaskUpdatesFoundEvent event)
    {
        logger.info("{}: (!) Получен запрос на рассылку уведомлений от {} (!)", className, event.getSource());
        sendAllNotifications(event.tasksWithUpdates);
    }
    
    
    /**
     * Ради этого метода включена асинхронная обработка событий, т.к. отправка оповещений может затянутся
     */
    @Override
    @EventListener
    @Transactional
    public void onApplicationEvent(ClientTaskUpdatesFoundEvent event)
    {
        logger.info("{}: (!) Получен запрос на рассылку уведомлений от {} для клиента {} (!)", className, event.getSource(),
                event.client);
        if (event.tasksWithUpdates.isEmpty())
        {
            this.sendMessageToClient(event.client, "Нет обновлений ни по одной задаче");
        }
        event.tasksWithUpdates.forEach((task, outageRefEntries) ->
            sendNotificationForClient(event.client, task, outageRefEntries)
        );
    }
    
    @Override
    @Async
    @Transactional
    public void sendAllNotifications(Map<Task, List<OutageRefEntry>> tasksWithUpdates)
    {
        logger.info("-------------------------------------------------------------------------------------------------");
        try
        {
            synchronized (this)
            {
                if (isTaskStarted)
                {
                    logger.info("{}: Запуск оповещения клиентов отменён - задача уже выполняется", className);
                    return;
                }
                isTaskStarted = true;
                startTime = Instant.now();
            }
            logger.info("{}: Начинаю оповещение клиентов через Бот для {} задач...", className, tasksWithUpdates.size());
            for (Task tsk : tasksWithUpdates.keySet())
            {
                logger.debug("{}: Начинаю оповещение клиентов через Бот для задачи [{} {} {}]...", className, tsk.getId(),
                        tsk.getCity(), tsk.getAddress());
                // оповещаем всех клиентов данной задачи - оповещения становятся в очередь, а данный метод завершается.
                // почему здесь не срабатывает LazyLoad, даже если сверху стоит @Transactional (?!) вероятно из-за вызова
                // через событие (см.: https://blog.pragmatists.com/spring-events-and-transactions-be-cautious-bdb64cb49a95)?!
                clientRepository.findAllByTasks_Id(tsk.getId()).forEach(client ->
                //tsk.getClients().forEach(client ->
                        this.sendNotificationForClient(client, tsk, tasksWithUpdates.get(tsk)));
                
                logger.debug("{}: Оповещение клиентов через Бот для задачи [{} {} {}] ЗАВЕРШЕНО", className, tsk.getId(),
                        tsk.getCity(), tsk.getAddress());
            }
            logger.info("{}: Оповещение клиентов через Бот завершено, потребовалось времени: {}", className, LocalTime.
                    ofSecondOfDay(Duration.between(startTime, Instant.now()).getSeconds()));
            isTaskStarted = false;
        }
        catch (Exception ex)
        {
            isTaskStarted = false;
            logger.error(className + ": Неожиданная ошибка оповещения через Бот для всех задач с обновлениями на сайте", ex);
        }
        finally
        {
            logger.info("---------------------------------------------------------------------------------------------");
        }
    }
    
    /**
     * @implSpec Боты телеграм поддерживают очень ограниченный набор HTML (и Markdown) тэгов, а именно:
     * <pre>
     *      <b>bold</b>, <strong>bold</strong>
     *      <i>italic</i>, <em>italic</em>
     *      <u>underline</u>, <ins>underline</ins>
     *      <s>strikethrough</s>, <strike>strikethrough</strike>
     *      <a href="URL">inline URL</a>
     *      <code>inline fixed-width code</code>
     *      <pre>pre-formatted fixed-width code block</pre>
     *      <pre><code class="language-java">System.out.println()</code></pre>
     * </pre>
     *
     * @see <a href="https://core.telegram.org/type/MessageEntity">
     *     Форматирвание Телеграм: Возможно есть ещё варианты, но, видимо, не через разметку</a>
     * @see <a href="https://core.telegram.org/bots/api#formatting-options">Оф. документация Телеграм: formatting-options</a>
     *
     * @param outageRefEntries Список записей Справочника отключений, в которых обнаружено совпадение с критериями Клиента
     */
    @Override
    @Async
    @Transactional
    public void sendNotificationForClient(Client client, Task tsk, List<OutageRefEntry> outageRefEntries)
    {
        try
        {
            final Bot bot = client.getBot();
            if (!bot.getIsAlive())
            {
                throw new IllegalArgumentException("Данный клиент " + client + " Не имеет активных ботов - отправка невозможна!");
            }
            StringBuilder messageBuilder = new StringBuilder("<b>Новые данные по отключению электричества:</b>\n");
            messageBuilder.append("Для Вашей задачи \"<i>").append(tsk.getCity()).append(" ").append(tsk.getAddress()).
                    append("</i>\"\n");
            int i = 1;
            for (OutageRefEntry entry : outageRefEntries)
            {
                messageBuilder.append("\n\n").append("<i>Подробности:</i>\n<b>").append(i++).append(".</b> ")
                    .append(entry.toString("r"))
                        // без ссылки приходит больше ответов (например работает Батайск), но они всё равно глючат (т.е. Телеграмм ругается на
                        // текст сообщения вроде:  [400] Bad Request: can't parse entities: Unclosed start tag at byte offset 6223
                        // после кодирования в юникод русских символов стало вроде бы лучше, но теперь другая ошибка:
                        // Can't find end tag corresponding to start tag a
                    .append("\n").append(generateGoogleCalendarLink(tsk, entry))
                ;
            }
            // подключаем Bot-API и отправляем само сообщение
            // TODO: по идее нужно запрещать не любую параллельную отправку, а только на одном и том же боте
            synchronized (this)
            {
                sendMessageToClientByBot(client, bot, messageBuilder.toString());
                // TODO: возможно таймаут нужен только для сообщений одного бота, а не для всех сразу (или не нужен вообще)
                if (this.botSleepTimeount > 0)
                {
                    logger.debug("{}: Охлаждение ({} секунд) перед отправкой сообщения Ботом", className, TimeUnit.
                            MILLISECONDS.toSeconds(this.botSleepTimeount));
                    Thread.sleep(this.botSleepTimeount);
                }
            }
        }
        catch (Exception ex)
        {
            logger.error(className + ": Неожиданная ошибка отправки сообщения ботом", ex);
        }
    }
    
    private void sendMessageToClient(Client client, String message)
    {
        final Bot bot = client.getBot();
        if (!bot.getIsAlive())
        {
            throw new IllegalArgumentException("Данный клиент " + client + " Не имеет активных ботов - отправка невозможна!");
        }
        sendMessageToClientByBot(client, bot, message);
    }
    private void sendMessageToClientByBot(Client client, Bot bot, String message)
    {
        messengerBots.stream().filter(x -> x.getBotUsername().equalsIgnoreCase(bot.getUserName())).findAny().
                ifPresent(x -> x.sendMessage(client.getMessengerChatId(), message));
    }
    
    /**
     * Метод генерации ссылки для календаря Google
     *
     * @implNote Формат ссылки для Google Calendar:
     *           <a href="https://www.google.com/calendar/render?action=TEMPLATE
     *           &text=4654
     *           &dates=20220922T090700Z/20220914T090700Z
     *           &details=%D0%BA5435
     *           &location=45435
     *           &sprop=&sprop=name">...</a>
     *
     * @implSpec TODO: Попробовать оформить эту ссылку как MarkDown V2, т.к. в виде HTML-ссылки есть какие-то проблемы
     *          то ли у Telegram, то ли у библиотеки связи с ним - в половине случаев ругается на отсутствие закрывающего
     *          тега, хотя, даже если не добавлять её в сообщение - всё-равно есть проблемы - вероятно всё-таки глючит
     *          библиотека, так что как вариант, можно полностью отказаться от HTML в пользу Markdown.
     *          P.S. html-ссылка получается совершенно корректной во всех случаях - проходит валидатор W3C.
     *
     * @see <a href="https://stackoverflow.com/a/21653600/2323972">Разъяснение примера для ссылки на Календарь</a>
     * @see <a href="https://en.wikipedia.org/wiki/List_of_tz_database_time_zones">List of tz database time zones</a>
     * @see <a href="https://github.com/InteractionDesignFoundation/add-event-to-calendar-docs/blob/main/services/google.md">
     *     Official documentation</a>
     */
    private String generateGoogleCalendarLink(Task tsk, OutageRefEntry entry)
    {
        final String aTagStart = "<a href=\"";
        final String aTagEnd = "\" target=\"_blank\" rel=\"nofollow\">[link_text]</a>";
        final String GOOGLE_CALENDAR_LINK_TEMPLATE =
                "http://www.google.com/calendar/render?" +
                "action=TEMPLATE" +
                "&text=[event-title]" +
                //"&dates=[start-custom format='Ymd\\\\THi00\\\\Z']/[end-custom format='Ymd\\\\THi00\\\\Z']\n" +
                "&dates=[start-custom format]/[end-custom format]" +
                "&details=[description]" +
                "&location=[location]" +
                "&trp=false" +
                //TODO: хорошо бы тут добавить ссылку на бота, но для этого её нужно хранить в БД
                //"&sprop= " +
                //"&sprop=name:\"\n" +
                // Please note that since I've specified a timezone with the "ctz" parameter, I used the local times for
                // the start and end dates.
                // Alternatively, you can use UTC dates and exclude the timezone parameter
                "&ctz=Europe/Moscow";
        String title = String.format("Отключение света по адресу %s [%s]", tsk.getAddress(), tsk.getCity());
        // Требуемый формат даты: 20220922T090700Z/20220914T090700Z
        // "Z" в конце вроде как не нужна, чтобы Час. пояс брался из параметра "&ctz=Europe/Moscow"
        final LocalDateTime outageStartDate = entry.getStartDateTime();
        
        String startDate = outageStartDate.format(DateTimeFormatter.BASIC_ISO_DATE) + "T" + outageStartDate.
                format(LocalDateTimeUtils.googleCalendarTimeFormatter) + "00";
        final LocalDateTime outageEndDate = entry.getEndDateTime();
        String endDate = outageEndDate.format(DateTimeFormatter.BASIC_ISO_DATE) + "T" + outageEndDate.format(
                LocalDateTimeUtils.googleCalendarTimeFormatter) + "00";
        String entryDescription = entry.toString("r");
        
        // Так работает Аксай, но не Азов
        String res = GOOGLE_CALENDAR_LINK_TEMPLATE.
                replace("[event-title]", UriUtils.encodeQueryParam(title, StandardCharsets.UTF_8)).
                replace("[description]", UriUtils.encodeQueryParam(entryDescription, StandardCharsets.UTF_8)).
                //replace("[link_text]", "Добавить напоминание в календарь Google").
                //  Make sure it's an address google maps can read easily.
                replace("[location]", UriUtils.encodeQueryParam(tsk.getCity() + " " + tsk.getAddress(),
                                StandardCharsets.UTF_8)).
                replace("[start-custom format]", UriUtils.encodeQueryParam(startDate, StandardCharsets.UTF_8)).
                replace("[end-custom format]", UriUtils.encodeQueryParam(endDate, StandardCharsets.UTF_8));

        // Так работает Азов, Аксай, Рассвет, но НЕ Батайск
//        String res = GOOGLE_CALENDAR_LINK_TEMPLATE.
//                replace("[event-title]", title).
//                replace("[description]", entry.toString("r")).
//                //replace("[link_text]", "Добавить напоминание в календарь Google").
//                replace("[location]", tsk.getCity() + " " + tsk.getAddress()).
//                replace("[start-custom format]", startDate).
//                replace("[end-custom format]", endDate);
        //res = UriUtils.encode(res, StandardCharsets.UTF_8);
        
        // так всё-равно не работает Батайск...
        return aTagStart + res + aTagEnd.replace("[link_text]", "Добавить напоминание в календарь Google");
    }
    
    
    
}
