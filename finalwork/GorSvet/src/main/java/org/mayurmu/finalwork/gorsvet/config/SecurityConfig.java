package org.mayurmu.finalwork.gorsvet.config;

import org.mayurmu.finalwork.gorsvet.model.Role;
import org.mayurmu.finalwork.gorsvet.model.enums.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

/**
 * TODO: Идея для Security - ограничить доступ к контроллерам только для Админа и аутентифицировать его через Телеграм!
 *      Второй вариант - ограничить видимость сервера только для localhost, но тогда Телеграм будет работать только через Полинг.
 *
 * @see <a href="https://habr.com/ru/post/321682/">Habr о регистрации на сайте через Телеграм</a>
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig
{
    private final String adminPassword;
    
    @Autowired
    public SecurityConfig(@Value("${admin.password}") String adminPassword)
    {
        this.adminPassword = adminPassword;
    }
    
    
    /**
     * Пока что разрешаем все запросы - в дальнейшем контролеры будут доступны только для Админа
     */
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/**")
//                .antMatchers("/bots/**")
                .permitAll()
//                .and()
//                .authorizeRequests()
//                .antMatchers("/parser/**")
//////                //.antMatchers("/**")
//                .hasRole(UserRole.ADMIN.name())
//                .anyRequest()
//                .authenticated()
                .and()
                .httpBasic();
        return http.build();
    }
    
    @Bean
    public UserDetailsService userDetailsService()
    {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        UserDetails admin = User
                .withUsername(UserRole.ADMIN.name().toLowerCase())
                .passwordEncoder(encoder::encode)
                //.password(encoder.encode(this.adminPassword))
                .password(this.adminPassword)
                .roles(UserRole.ADMIN.name())
                .build();
        return new InMemoryUserDetailsManager(admin);
    }
}
