package org.mayurmu.finalwork.gorsvet.component.telegram;

import org.apache.commons.lang3.StringUtils;

/**
 * Инкапсулирует строку с очень ограниченной длиной - всего 64 байта (см. спецификации Телеграмм для встроенных клавиатур)
 */
public class CallBackData
{
    public static final byte MAX_TELEGRAM_INLINE_KEYBOARD_CALLBACK_DATA_LENGTH = 64;
    private static final String BUTTON_NAME_ID_SEPARATOR = "=";
    
    public final String callback_data;
    
    public CallBackData(String name, String value)
    {
        this(name + BUTTON_NAME_ID_SEPARATOR + value);
    }
    
    public CallBackData(String data)
    {
        if (data.getBytes().length > MAX_TELEGRAM_INLINE_KEYBOARD_CALLBACK_DATA_LENGTH)
        {
            throw new IllegalArgumentException("Превышена макс длина callback_data");
        }
        callback_data = data;
    }
    
    public static String getValuePart(String replyData)
    {
        return StringUtils.substringAfter(replyData, BUTTON_NAME_ID_SEPARATOR);
    }
    
    public static String getNamePart(String replyData)
    {
        return StringUtils.substringBefore(replyData, BUTTON_NAME_ID_SEPARATOR);
    }
    
    @Override
    public String toString()
    {
        return callback_data;
    }
}

