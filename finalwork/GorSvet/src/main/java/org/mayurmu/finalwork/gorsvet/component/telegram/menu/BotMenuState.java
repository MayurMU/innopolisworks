package org.mayurmu.finalwork.gorsvet.component.telegram.menu;

/**
 * Текущее положение пользователя в меню
 */
public enum BotMenuState
{
    /**
     * Неизвестное состояние - когда юзер отправляет первое сообщение в чат в т.ч. /start
     */
    UNKNOWN,
    /**
     * Юзер находится в главом Меню
     */
    MAIN_MENU,
    /**
     * Юзер в Списке своих задач
     */
    MY_TASKS_MENU,
    /**
     * Юзер внутри редактирования своей задачи
     */
    TASK_MENU,
    /**
     * Админ в меню управления справочником отключений
     */
    OUTAGE_REFERENCE,
    /**
     * Админ в меню управления ботами
     */
    BOTS_MENU,
    /**
     * Меню управления клиентами
     */
    CLIENTS_MENU,
    /**
     * Режим поиска адреса
     */
    SEARCH_ADDRESS,
    /**
     * Добавить Задачу
     */
    ADD_TASK_BUTTON,
    /**
     * Проверить все задачи Клиента
     */
    CHECK_MY_TASKS,
    /**
     * Сбросить дату последней проверки для всех задач в БД
     */
    RESET_LAST_CHECK_DATE,
    /**
     * Режим ввода новой задачи
     */
    TASK_INPUT_MODE,
    /**
     * Виртуальный код, который подставляется в обработчике клика на конкретную задачу
     */
    SELECT_TASK_BUTTON,
    TASK_EDIT_MENU,
    CHECK_TASK_BUTTON,
    DELETE_TASK_BUTTON,
    DELETE_TASK_DIALOG,
    /**
     * Перейти Назад (на уровень вверх)
     */
    GO_BACK_BUTTON,
    /**
     * Добавить нового бота в систему
     */
    ADD_BOT_BUTTON,
    /**
     * Добавить нового бота в систему
     */
    SHOW_ALL_BOTS,
    SHOW_ACTIVE_BOTS,
    SHOW_INACTIVE_BOTS,
    SHOW_ALL_CLIENTS,
    SHOW_BANNED_CLIENTS,
    /**
     * Меню-список ботов
     */
    BOT_LIST_MENU,
    /**
     * Режим редактирования Бота
     */
    BOT_EDIT_MODE,
    DEACTIVATE_BOT,
    ACTIVATE_BOT,
    CHANGE_BOT_KEY_BUTTON,
    CHANGE_BOT_DESCRIPTION_BUTTON,
    CHANGE_BOT_NAME_BUTTON,
    /**
     * Режим создания нового Бота (задание имени)
     */
    BOT_INPUT_NAME_MODE,
    /**
     * Показать клиентов с Активными ботами
     */
    SHOW_ACTIVE_CLIENTS,
    SHOW_INACTIVE_CLIENTS,
    SHOW_ADMINS,
    SHOW_USERS,
    SHOW_GUESTS,
    /**
     * Меню Редактирования клиента
     */
    CLIENT_EDIT_MENU,
    CHANGE_CLIENT_ROLE_BUTTON,
    DELETE_CLIENT_BUTTON,
    DELETE_CLIENT_DIALOG,
    RESTORE_CLIENT_BUTTON,
    RESTORE_CLIENT_DIALOG,
    /**
     * Меню выбора новой Роли для Клиента
     */
    CLIENT_ROLE_EDIT_DIALOG,
    /**
     * Меню списка клиентов
     */
    CLIENT_LIST_MENU,
    /**
     * Управление справочником Отключений
     */
    MANAGE_URLS,
    UPDATE_REFERENCE,
    DOWNLOAD_PAGES,
    PARSE_PAGES,
    TRUNCATE_REFERENCE
}

