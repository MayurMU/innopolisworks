package org.mayurmu.finalwork.gorsvet.controller;

import org.mayurmu.finalwork.gorsvet.dto.BotDto;
import org.mayurmu.finalwork.gorsvet.dto.utils.EntityMapper;
import org.mayurmu.finalwork.gorsvet.repository.BotRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/bots")
public class BotController
{
    private final BotRepository botRepository;
    private final EntityMapper mapper;
    
    public BotController(BotRepository botRepository, EntityMapper mapper)
    {
        this.botRepository = botRepository;
        this.mapper = mapper;
    }
    
    /**
     * Возвращает всех ботов через разделитель строки
     */
    @GetMapping
    public String getAllAsString()
    {
        return mapper.mapList(botRepository.findAll(), BotDto.class).stream().map(BotDto::toString).collect(
                Collectors.joining("\n\n"));
    }
    
    /**
     * Возвращает всех ботов как JSON-массив
     */
    @GetMapping("/json")
    public Collection<BotDto> getAll()
    {
        return mapper.mapList(botRepository.findAll(), BotDto.class);
    }
}
