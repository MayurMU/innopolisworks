package org.mayurmu.finalwork.gorsvet.config;

import org.mayurmu.finalwork.gorsvet.component.MessengerBot;
import org.mayurmu.finalwork.gorsvet.component.telegram.BotLogic;
import org.mayurmu.finalwork.gorsvet.component.telegram.MessengerBotTelegramImpl;
import org.mayurmu.finalwork.gorsvet.exceptions.BotException;
import org.mayurmu.finalwork.gorsvet.exceptions.NoActiveBotsFoundException;
import org.mayurmu.finalwork.gorsvet.model.Bot;
import org.mayurmu.finalwork.gorsvet.repository.BotRepository;
import org.mayurmu.finalwork.gorsvet.repository.ClientRepository;
import org.mayurmu.finalwork.gorsvet.service.ClientService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import java.util.ArrayList;
import java.util.List;

/**
 * @see <a href="https://core.telegram.org/bots/webhooks#the-longer-version">Настройка работы через WebHook - The longer version</a>
 */
@Configuration
public class TelegramConfig
{
    private static String PREDEFINED_ADMIN_USER_NAME;
    private static String SITE_URL_ROOT;
    
    private final List<MessengerBot> registeredBots = new ArrayList<>();
    private final String className = TelegramConfig.class.getSimpleName();
    private final BotLogic botLogic;
    
    private TelegramBotsApi botsApi;
    
    
    
    /**
     * Класс Настройки простого бота в режиме полинга
     */
    @Autowired
    public TelegramConfig(@Value("${bot.use.echo}") boolean useEchoRepeat,
                          @Value("${bot.admin.messenger.username}") String predefinedAdminUserName,
                          @Value("${site.url.root}") String siteRoot,
                          Logger logger,
                          BotRepository botRepository,
                          ClientService clientService,
                          ClientRepository clientRepository,
                          BotLogic botLogic)
    {
        this.botLogic = botLogic;
        PREDEFINED_ADMIN_USER_NAME = predefinedAdminUserName;
        SITE_URL_ROOT = siteRoot;
        this.registerBots(useEchoRepeat, botRepository, logger, clientService, clientRepository);
    }
    
    public static String getPREDEFINED_ADMIN_USER_NAME()
    {
        return PREDEFINED_ADMIN_USER_NAME;
    }
    
    public static String getSiteUrlRoot()
    {
        return SITE_URL_ROOT;
    }
    
    @Bean
    public List<MessengerBot> getRegisteredBots()
    {
        return registeredBots;
    }
    
    /**
     * Единственный экземпляр подключения к Telegram API - может использоваться несколькими ботами
     *
     * @throws BotException Пробрасываем исключение наружу, т.к. без ботов работать всё равно нельзя
     */
    public TelegramBotsApi getTelegramBotsApi()
    {
        try
        {
            if (botsApi == null)
            {
                botsApi = new TelegramBotsApi(DefaultBotSession.class);
            }
            return botsApi;
        }
        catch (TelegramApiException tex)
        {
            throw new BotException("Ошибка подключения к API Telegram", tex);
        }
    }
    
    private void registerBots(boolean useEchoRepeat, BotRepository botRepository, Logger logger, ClientService clientService,
                             ClientRepository clientRepository)
    {
        List<Bot> aliveBots = botRepository.findAllByIsAliveTrue();
        if (aliveBots.isEmpty())
        {
            throw new NoActiveBotsFoundException(className + ": Запуск невозможен: Нет активных ботов - проверьте хранилище");
        }
        else
        {
            aliveBots.forEach(bot -> {
                try
                {
                    MessengerBot regBot = new MessengerBotTelegramImpl(bot.getUserName(), bot.getSecretKey(), useEchoRepeat,
                            botLogic, bot, logger
                    );
                    getTelegramBotsApi().registerBot((TelegramLongPollingBot)regBot);
                    registeredBots.add(regBot);
                    logger.info("(!) {}: Зарегистрирован бот {} (!)", className, bot.getUserName());
                }
                catch (TelegramApiException e)
                {
                    logger.error(className + ": Ошибка регистрации бота " + bot.getUserName(), e);
                }
            });
        }
    }
}
