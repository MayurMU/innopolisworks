package org.mayurmu.finalwork.gorsvet.service;

import org.mayurmu.finalwork.gorsvet.model.Client;
import org.mayurmu.finalwork.gorsvet.model.Task;
import org.springframework.scheduling.annotation.Async;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

public interface TaskService
{
    Instant getStartTime();
    
    int getMaxTaskCountPerClient();
    
    boolean isTaskStarted();
    
    /**
     * Запускает асинхронную проверку всех задач
     */
    @Async
    void checkAll();
    
    void checkAllByClient(Client client);
    
    Task addTask(String city, String address, Client client);
    
    void resetAllCheckDates();
    
    Optional<Task> findTaskById(UUID id);
    
    boolean deleteTaskByIdFromClient(UUID selectedEntity, Client client);
    
    /**
     * Запускает принудительную проверку Задачи с указанным ИД (без учёта даты последней проверки), высылает уведомление
     *  указанному Клиенту.
     */
    boolean checkTaskByIdForClient(UUID selectedEntity, Client client);
}
