package org.mayurmu.finalwork.gorsvet.component.telegram;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.mayurmu.finalwork.gorsvet.component.MessengerBot;
import org.mayurmu.finalwork.gorsvet.component.telegram.menu.BotMenuState;
import org.mayurmu.finalwork.gorsvet.component.telegram.menu.MenuButtons;
import org.mayurmu.finalwork.gorsvet.component.telegram.menu.UserStatus;
import org.mayurmu.finalwork.gorsvet.component.telegram.utils.BotUtils;
import org.mayurmu.finalwork.gorsvet.config.TelegramConfig;
import org.mayurmu.finalwork.gorsvet.model.*;
import org.mayurmu.finalwork.gorsvet.model.enums.UserRole;
import org.mayurmu.finalwork.gorsvet.model.utils.UUIDs;
import org.mayurmu.finalwork.gorsvet.repository.BotRepository;
import org.mayurmu.finalwork.gorsvet.repository.ClientRepository;
import org.mayurmu.finalwork.gorsvet.service.OutageRefService;
import org.mayurmu.finalwork.gorsvet.service.TaskService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.mayurmu.finalwork.gorsvet.component.telegram.menu.MessageInfo;

import javax.annotation.Nullable;
import java.net.URI;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Класс общей бизнес логики Телеграм Ботов
 */
@Component
@Transactional
@Lazy
public class BotLogic
{
    private final ClientRepository clientRepository;
    private final Logger logger;
    private final String classMame = BotLogic.class.getSimpleName();
    private final OutageRefService outageRefService;
    private final BotRepository botRepository;
    private final TaskService taskService;
    private final MenuMaster _menuMaster = new MenuMaster();
    
    /**
     * Зарегистрировался ли Администратор в системе или ещё не успел (т.е. в базе нет ни одного Юзера с правами Админа)
     */
    private boolean hasAdminUser;
    
    @Autowired
    public BotLogic(ClientRepository clientRepository, Logger logger, OutageRefService outageRefService,
                    BotRepository botRepository, TaskService taskService)
    {
        this.clientRepository = clientRepository;
        this.logger = logger;
        this.outageRefService = outageRefService;
        this.botRepository = botRepository;
        this.taskService = taskService;
    }
    
    
    /**
     * Вспомогательный вложенный класс показа различных меню Бота
     */
    protected class MenuMaster
    {
        /**
         * Метод показа основного меню
         */
        private void showMainMenu(MessengerBot botImpl, String chatId, long senderId, UserRole userRole)
        {
            Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>();
            buttons.put("Поиск адреса", new InlineButtonPayload(BotMenuState.SEARCH_ADDRESS.name()));
            buttons.put("Мои Задачи", new InlineButtonPayload(BotMenuState.MY_TASKS_MENU.name()));
            if (userRole == UserRole.ADMIN)
            {
                buttons.put("Боты", new InlineButtonPayload(BotMenuState.BOTS_MENU.name()));
                buttons.put("Клиенты", new InlineButtonPayload(BotMenuState.CLIENTS_MENU.name()));
                buttons.put("Справочник", new InlineButtonPayload(BotMenuState.OUTAGE_REFERENCE.name()));
            }
            buttons.put("Сайт ДонЭнерго", new InlineButtonPayload(URI.create(TelegramConfig.getSiteUrlRoot())));
            botImpl.sendInlineKeyboard("Выберите действие:", chatId, buttons.entrySet());
            botImpl.setUserStateTo(senderId, BotMenuState.MAIN_MENU);
        }
        
        /**
         * Метод показа меню Управления отдельной Задачей
         */
        private void showTaskEditMenu(MessengerBot botImpl, Client client, String chatId, long senderId, UUID selectedEntity)
        {
            Optional<Task> foundTaskOption = taskService.findTaskById(selectedEntity);
            if (!foundTaskOption.isPresent())
            {
                botImpl.sendMessage(senderId, "Ошибка: Такой задачи не существует?!");
                return;
            }
            String taskText = foundTaskOption.get().toString("s");
            
            Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>();
            buttons.put("Проверить", new InlineButtonPayload(BotMenuState.CHECK_TASK_BUTTON.name(), selectedEntity.toString()));
            buttons.put("(-) Удалить", new InlineButtonPayload(BotMenuState.DELETE_TASK_BUTTON.name(), selectedEntity.toString()));
            buttons.put("Вернуться Назад", new InlineButtonPayload(BotMenuState.GO_BACK_BUTTON.name()));
            
            botImpl.sendInlineKeyboard("Меню задачи \"" + taskText + "\":", chatId, buttons.entrySet());
            botImpl.setUserStateTo(senderId, BotMenuState.TASK_EDIT_MENU);
        }
        
        /**
         * Метод показа меню Редактирования Клиента
         */
        @Transactional
        protected void showClientEditMenu(MessengerBot botImpl, long senderId, String chatId, UUID selectedEntity)
        {
            Optional<Client> foundClientOption = clientRepository.findById(selectedEntity);
            if (!foundClientOption.isPresent())
            {
                botImpl.sendMessage(senderId, "Ошибка: Такого клиента не существует?!");
                processMenu(botImpl, senderId, "", chatId, BotMenuState.CLIENT_LIST_MENU.name());
                return;
            }
            String clientData = foundClientOption.get().toString("r");
            
            Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>();
            buttons.put("Изменить роль", new InlineButtonPayload(BotMenuState.CHANGE_CLIENT_ROLE_BUTTON.name(), selectedEntity.toString()));
            if (foundClientOption.get().isBanned())
            {
                buttons.put("(!) Восстановить", new InlineButtonPayload(BotMenuState.RESTORE_CLIENT_BUTTON.name(), selectedEntity.toString()));
            }
            else
            {
                buttons.put("(-) Отключить", new InlineButtonPayload(BotMenuState.DELETE_CLIENT_BUTTON.name(), selectedEntity.toString()));
            }
            buttons.put("Вернуться в меню управления Клиентами", new InlineButtonPayload(BotMenuState.GO_BACK_BUTTON.name()));
            
            botImpl.sendInlineKeyboard("<b>Меню клиента</b>\n \"" + clientData + "\":", chatId, buttons.entrySet());
            botImpl.setUserStateTo(senderId, BotMenuState.CLIENT_EDIT_MENU);
        }
        
        /**
         * Метод показа меню управления Клиентами
         */
        private void showClientsMenu(MessengerBot botImpl, Client client, String chatId, long senderId)
        {
            if (BotUtils.hasAdminRights(botImpl, client, senderId, logger))
            {
                return;
            }
            Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>();
            buttons.put("Все клиенты", new InlineButtonPayload(BotMenuState.SHOW_ALL_CLIENTS.name()));
            buttons.put("Только активные", new InlineButtonPayload(BotMenuState.SHOW_ACTIVE_CLIENTS.name()));
            buttons.put("Только Неактивные", new InlineButtonPayload(BotMenuState.SHOW_INACTIVE_CLIENTS.name()));
            buttons.put("Администраторы", new InlineButtonPayload(BotMenuState.SHOW_ADMINS.name()));
            buttons.put("Пользователи", new InlineButtonPayload(BotMenuState.SHOW_USERS.name()));
            buttons.put("Гости", new InlineButtonPayload(BotMenuState.SHOW_GUESTS.name()));
            buttons.put("Отключённые", new InlineButtonPayload(BotMenuState.SHOW_BANNED_CLIENTS.name()));
            buttons.put("Вернуться Назад", new InlineButtonPayload(BotMenuState.GO_BACK_BUTTON.name()));
            
            botImpl.sendInlineKeyboard("Меню управления клиентами:", chatId, buttons.entrySet());
            botImpl.setUserStateTo(senderId, BotMenuState.CLIENTS_MENU);
        }
        
        /**
         * Метод показа меню управления Справочником отключений
         */
        private void showOutageReferenceMenu(MessengerBot botImpl, Client client, String chatId, long senderId)
        {
            if (BotUtils.hasAdminRights(botImpl, client, senderId, logger))
            {
                return;
            }
            Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>();
            buttons.put("URL страниц для парсинга", new InlineButtonPayload(BotMenuState.MANAGE_URLS.name()));
            buttons.put("Обновить всё", new InlineButtonPayload(BotMenuState.UPDATE_REFERENCE.name()));
            buttons.put("Только загрузить", new InlineButtonPayload(BotMenuState.DOWNLOAD_PAGES.name()));
            buttons.put("Только распарсить", new InlineButtonPayload(BotMenuState.PARSE_PAGES.name()));
            buttons.put("Очистить справочник", new InlineButtonPayload(BotMenuState.TRUNCATE_REFERENCE.name()));
            buttons.put("Вернуться Назад", new InlineButtonPayload(BotMenuState.GO_BACK_BUTTON.name()));
            
            botImpl.sendInlineKeyboard("Меню управления Справочником Отключений:", chatId, buttons.entrySet());
            botImpl.setUserStateTo(senderId, BotMenuState.OUTAGE_REFERENCE);
        }
        
        /**
         * Метод показа меню управления Ботами
         */
        protected void showBotsMenu(MessengerBot botImpl, Client client, String chatId, long senderId)
        {
            if (BotUtils.hasAdminRights(botImpl, client, senderId, logger))
            {
                return;
            }
            Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>();
            buttons.put("Все боты", new InlineButtonPayload(BotMenuState.SHOW_ALL_BOTS.name()));
            buttons.put("Только активные боты", new InlineButtonPayload(BotMenuState.SHOW_ACTIVE_BOTS.name()));
            buttons.put("Только Неактивные боты", new InlineButtonPayload(BotMenuState.SHOW_INACTIVE_BOTS.name()));
            buttons.put("Вернуться Назад", new InlineButtonPayload(BotMenuState.GO_BACK_BUTTON.name()));
            botImpl.sendInlineKeyboard("Меню управления ботами:", chatId, buttons.entrySet());
            botImpl.setUserStateTo(senderId, BotMenuState.BOTS_MENU);
        }
    
        /**
         * Метод показа меню список-Ботов
         */
        public void showBotListMenu(MessengerBot botImpl, Client client, String chatId, long senderId, Boolean isAlive)
        {
            if (BotUtils.hasAdminRights(botImpl, client, senderId, logger))
            {
                return;
            }
            Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>();
            if (isAlive == null || isAlive)
            {
                buttons.put("(+) Добавить бота", new InlineButtonPayload(BotMenuState.ADD_BOT_BUTTON.name()));
            }
            List<Bot> foundBots = isAlive == null ? botRepository.findAllWithSecretKey() : botRepository.findAllByIsAlive(isAlive);
            for (Bot bot : foundBots)
            {
                buttons.put(bot.getUserName(), new InlineButtonPayload(bot.getId().toString()));
            }
            buttons.put("Вернуться Назад", new InlineButtonPayload(BotMenuState.GO_BACK_BUTTON.name()));
            botImpl.sendInlineKeyboard("Список ботов:", chatId, buttons.entrySet());
            botImpl.setUserStateTo(senderId, BotMenuState.BOT_LIST_MENU);
        }
        
        /**
         * Показывает меню-список клиентов возможно отфильтрованный по роли, но БЕЗ учёта статуса (есть активный бот/нет активного бота)
         *
         * @param desiredRole   {@link UserRole#UNKNOWN} для вывода всех клиентов, без учёта роли
         */
        public void showClientListMenu(MessengerBot botImpl, String chatId, long senderId, UserRole desiredRole)
        {
            showClientListMenu(botImpl, chatId, senderId, desiredRole, UserStatus.UNKNOWN);
        }
        /**
         * Показывает меню-список клиентов возможно отфильтрованный по роли или по статусу (есть активный бот/нет активного бота)
         *
         * @param desiredRole   {@link UserRole#UNKNOWN} для вывода всех клиентов, без учёта роли
         * @param userStatus    Каких пользователей показывать
         */
        public void showClientListMenu(MessengerBot botImpl, String chatId, long senderId, UserRole desiredRole, UserStatus userStatus)
        {
            List<Client> foundClients = null;
            if (desiredRole != UserRole.UNKNOWN && userStatus != UserStatus.UNKNOWN)
            {
                throw new IllegalArgumentException("Оба параметра (Роль и Статус) не могут быть заданы одновременно!");
            }
            if ((desiredRole == UserRole.UNKNOWN) && (userStatus == UserStatus.UNKNOWN))
            {
                foundClients = clientRepository.findAllByIsNotBanned();
            }
            else if (desiredRole == UserRole.UNKNOWN)
            {
                if (userStatus == UserStatus.ACTIVE)
                {
                    foundClients = clientRepository.findByBot_IsAlive(true);
                }
                else if (userStatus == UserStatus.INACTIVE)
                {
                    foundClients = clientRepository.findByBot_IsAlive(false);
                }
                else if (userStatus == UserStatus.BANNED)
                {
                    foundClients = clientRepository.findAllByIsBanned_True();
                }
            }
            else
            {
                foundClients = clientRepository.findByRole_Type(desiredRole);
            }
            Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>();
            // не показываем в списке самого себя
            assert foundClients != null;
            foundClients.removeIf(client -> client.getMessengerChatId() == senderId);
            for (Client cl : foundClients)
            {
                String clTitle = BotUtils.getClientTitle(cl);
                buttons.put(Strings.dquote(clTitle), new InlineButtonPayload(cl.getId().toString()));
            }
            buttons.put("Вернуться в меню управления Клиентами", new InlineButtonPayload(BotMenuState.GO_BACK_BUTTON.name()));
            String clRole = (desiredRole == UserRole.UNKNOWN ? "" : " с ролью " + desiredRole.name());
            String clStatus = (userStatus == UserStatus.UNKNOWN ? "" : " (статус: " + userStatus.title + ")");
            
            botImpl.sendInlineKeyboard("Список клиентов" + clRole + clStatus + ":", chatId, buttons.entrySet());
            botImpl.setUserStateTo(senderId, BotMenuState.CLIENT_LIST_MENU);
        }
    
        /**
         * Метод показа Списка задач Юзера
         */
        @Transactional
        protected void showMyTasksMenu(MessengerBot botImpl, Client client, String chatId, long senderId, UserRole clientRole)
        {
            Set<Task> clientTasks = client.getTasks();
            Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>();
            buttons.put("(+) Добавить задачу", new InlineButtonPayload(BotMenuState.ADD_TASK_BUTTON.name()));
            if (clientRole == UserRole.ADMIN)
            {
                buttons.put("(!) Сбросить дату проверки для ВСЕХ клиентов", new InlineButtonPayload(
                        BotMenuState.RESET_LAST_CHECK_DATE.name()));
            }
            if (!clientTasks.isEmpty())
            {
                buttons.put("Проверить все задачи", new InlineButtonPayload(BotMenuState.CHECK_MY_TASKS.name()));
            }
            for (Task t : clientTasks)
            {
                // TODO: по идее во внешнюю среду лучше не отдавать ИД (заменить на MD5 Hash его или текста кнопки?)
                // но тогда, чтобы понять, какую кнопку меню нажал юзер придётся посчитать Хеш для всех записей таблицы!
                buttons.put(Strings.dquote(t.toString("s")), new InlineButtonPayload(t.getId().toString()));
            }
            buttons.put("Вернуться Назад", new InlineButtonPayload(BotMenuState.GO_BACK_BUTTON.name()));
            botImpl.sendInlineKeyboard(clientTasks.isEmpty() ? "У Вас пока нет задач:" : "Ваши текущие задачи:", chatId,
                    buttons.entrySet());
            botImpl.setUserStateTo(senderId, BotMenuState.MY_TASKS_MENU);
        }
    
        /**
         * Отображает меню редактирования Бота и информацию о нём
         */
        public void showBotEditMenu(MessengerBot botImpl, long senderId, String chatId, Client client, UUID selectedEntity)
        {
            Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>();
            Optional<Bot> foundBot = botRepository.findById(selectedEntity);
            if (!foundBot.isPresent())
            {
                botImpl.sendMessage(senderId, "Что-то пошло не так - бот не найден?!");
                _menuMaster.showBotsMenu(botImpl, client, chatId, senderId);
                return;
            }
            botImpl.sendMessage(senderId,"<b>Информация о выбранном боте:</b>\n\n" + foundBot.get().toString("r"));
            buttons.put("Сменить ключ", new InlineButtonPayload(BotMenuState.CHANGE_BOT_KEY_BUTTON.name()));
            buttons.put("Изменить описание", new InlineButtonPayload(BotMenuState.CHANGE_BOT_DESCRIPTION_BUTTON.name()));
            buttons.put("Изменить Имя бота", new InlineButtonPayload(BotMenuState.CHANGE_BOT_NAME_BUTTON.name()));
            if (foundBot.get().getIsAlive())
            {
                buttons.put("(-) Отключить", new InlineButtonPayload(BotMenuState.DEACTIVATE_BOT.name(),
                        selectedEntity.toString()));
            }
            else
            {
                buttons.put("(+) Активировать", new InlineButtonPayload(BotMenuState.ACTIVATE_BOT.name(),
                        selectedEntity.toString()));
            }
            buttons.put("Вернуться Назад", new InlineButtonPayload(BotMenuState.GO_BACK_BUTTON.name()));
            botImpl.sendInlineKeyboard("Возможные действия:", chatId, buttons.entrySet());
            botImpl.setUserStateTo(senderId, BotMenuState.BOT_EDIT_MODE);
        }

    }
    
    
    
    /**
     * Метод проверки пользователя - добавляет новых пользователей как гостей и назначает админа (если его нет, см.:
     * {@link TelegramConfig#getPREDEFINED_ADMIN_USER_NAME()}), а также обновляет ссылку на бота для существующих
     * пользователей, если они зашли с нового бота.
     *
     * @return юзер телеграмм превращённый в нашего Клиента
     */
    @Transactional
    public Client getClient(MessengerBot botImpl, @Nullable String senderName, long senderId)
    {
        final Bot linkedBot = botImpl.getLinkedBotModel();
        // TODO: возможно стоит рассмотреть удаление роли Гость, т.к. это по сути просто засоряет базу
        Client foundClient = clientRepository.findByMessengerChatId(senderId).orElse(
                new Client(senderId, senderName, null, false, LocalDateTime.now(), Role.GUEST, linkedBot)
        );
        // если пользователь существует, то мы должны проверить его бота в БД и не является ли он Админом (зашедшим впервые)
        if (!hasAdminUser && TelegramConfig.getPREDEFINED_ADMIN_USER_NAME().equalsIgnoreCase(senderName))
        {
            foundClient.setRole(Role.ADMIN);
            hasAdminUser = true;
        }
        // Если Юзер не забанен, переназначаем в аккаунте юзера его бота на того, с которого он зашёл в последний раз.
        if (!foundClient.isBanned() && !foundClient.getBot().getId().equals(linkedBot.getId()))
        {
            foundClient.setBot(linkedBot);
        }
        // обновляем имя юзера в БД, т.к. с последнего захода онно могло измениться
        foundClient.setMessengerUserName(senderName);
        clientRepository.saveAndFlush(foundClient);
        // создаём что-то вроде сессии для клиента - добавляем его в список текущих состояний
        if (botImpl.getUserState(senderId) == null)
        {
            botImpl.setUserStateTo(senderId, BotMenuState.UNKNOWN);
        }
        return foundClient;
    }
    
    /**
     * Метод показа меню и реакции на текущее состояние
     */
    public void processMenu(MessengerBot botImpl, long senderId, String senderName, String chatId)
    {
        processMenu(botImpl, senderId, senderName, chatId, "", false);
    }
    /**
     * Метод показа меню и реакции на текущее состояние (ответ - простой текст)
     */
    public void processMenu(MessengerBot botImpl, long senderId, String senderName, String chatId, String replyText)
    {
        processMenu(botImpl, senderId, senderName, chatId, replyText, false);
    }
    /**
     * Метод показа меню и реакции на текущее состояние пользователя (его положения в меню)
     *
     * @apiNote Точка входа в бизнес-логику ботов - вызывается из движка Телеграм-ботов (см: {@link MessengerBotTelegramImpl})
     *
     * @param isCallback Определяет, как будет интерпретироваться параметр {@code replyData}: True - код кнопки, False - простой текст.
     *                   Влияет на то, будет ли проводиться предварительный парсинг параметра replyData или он сразу поступит
     *                   на вход метода соот-щего текущему положению юзера в меню.
     *
     * @implNote TODO: Рефакторинг - переписать внутренние вызовы на использование в качестве параметра объекта {@link MessageInfo}
     */
    public void processMenu(MessengerBot botImpl, long senderId, String senderName, String chatId, String replyData, boolean isCallback)
    {
        final Client client = this.getClient(botImpl, senderName, senderId);
        if (client.isBanned())
        {
            botImpl.sendMessage(senderId, "Ваш аккаунт отключён Администратором, обращайтесь по адресу support@gorsvet.ru");
            return;
        }
        final UserRole clientRole = client.getRole().getType();
        UUID selectedEntity = UUIDs.EMPTY;
        BotMenuState currentState = botImpl.getUserState(senderId);
        BotMenuState pushedButton = BotMenuState.UNKNOWN;
        // парсим ответ из кода кнопки нажатой юзером
        if (isCallback && StringUtils.isNotBlank(replyData))
        {
            // TODO: избавиться здесь от логики разбора replyData построенной на обработке исключений
            try
            {
                pushedButton = BotMenuState.valueOf(replyData);
                // поправляем текущее состояние, если юзер продолжил старый диалог
                // TODO: доделать для остальных меню - выделить в отдельный метод
                if (MenuButtons.TASKS_MENU.contains(pushedButton))
                {
                    currentState = MenuButtons.TASKS_MENU.menuButton;
                }
                else if (MenuButtons.MAIN_MENU.contains(pushedButton))
                {
                    currentState = MenuButtons.MAIN_MENU.menuButton;
                }
            }
            catch (IllegalArgumentException ex)
            {
                try
                {
                    selectedEntity = UUID.fromString(replyData);
                }
                catch (IllegalArgumentException x)
                {
                    try
                    {
                        selectedEntity = UUID.fromString(CallBackData.getValuePart(replyData));
                        pushedButton = BotMenuState.valueOf(CallBackData.getNamePart(replyData));
                    }
                    catch (IllegalArgumentException e)
                    {
                        String mes = MessageFormat.format("{0}: [{1}] предварительный парсинг параметра меню не удался" +
                                        " - пользователь {2} ({3}) отправил неизвестный тип параметра {4}, передаю сырые данные" +
                                        " в обработчик меню {5}", classMame, botImpl.getBotUsername(), senderName,
                                senderId, replyData, currentState);
                        logger.debug(mes);
                    }
                }
            }
            // логируем только коды кнопок, т.к. простой текст логируется в самом движке бота
            logger.debug("{}: [{}] от пользователя {} ({}) получен запрос на переход в меню {}", classMame,
                    botImpl.getBotUsername(),
                    senderName,
                    senderId,
                    replyData);
        }
        handleCurrentState(currentState, botImpl, senderId, chatId, client, replyData, selectedEntity, pushedButton);
    }
    
    /**
     * Главный метод-обработчик текущего состояния Диалога с Ботом
     */
    private void handleCurrentState(BotMenuState currentState, MessengerBot botImpl, long senderId, String chatId,
                                    Client client, String replyData,
                                    UUID selectedEntity, BotMenuState pushedButton)
    {
        switch (currentState)
        {
            // нажатие на кнопку в главном меню
            case MAIN_MENU:
            {
                handleMainMenuButton(botImpl, senderId, chatId, client, pushedButton);
                break;
            }
            case MY_TASKS_MENU:
            {
                handleMyTasksMenuButton(botImpl, senderId, chatId, client, pushedButton, selectedEntity);
                break;
            }
            // Пользователь перешёл в режим поиска Адреса
            case SEARCH_ADDRESS:
            {
                handleAddressSearchMode(botImpl, senderId, chatId, client, replyData);
                break;
            }
            // режим создания новой задачи
            case TASK_INPUT_MODE:
            {
                handleTaskInputMode(botImpl, senderId, chatId, client, replyData);
                break;
            }
            // меню конкретной Задачи
            case TASK_EDIT_MENU:
            {
                handleTaskEditMenuButton(botImpl, senderId, chatId, client, pushedButton, selectedEntity);
                break;
            }
            // диалог подтверждения Удаления Задачи пользователем
            case DELETE_TASK_DIALOG:
            {
                handleTaskDeleteDialogUserResponse(botImpl, senderId, chatId, client, pushedButton, selectedEntity);
                break;
            }
            // меню фильтрации клиентов
            case CLIENTS_MENU:
            {
                handleClientsMenuButton(botImpl, senderId, chatId, client, pushedButton, selectedEntity);
                break;
            }
            // меню выбора клиента из списка
            case CLIENT_LIST_MENU:
            {
                handleClientListMenuButton(botImpl, senderId, chatId, client, pushedButton, selectedEntity);
                break;
            }
            // меню редактирования выбранного Клиента
            case CLIENT_EDIT_MENU:
            {
                handleClientEditMenuButton(botImpl, senderId, chatId, client, pushedButton, selectedEntity);
                break;
            }
            // реакция на кнопки диалога Выбора роли Юзера
            case CLIENT_ROLE_EDIT_DIALOG:
            {
                handleChangeRoleDialogUserResponse(botImpl, senderId, chatId, replyData, selectedEntity);
                break;
            }
            // диалог Отключения клиента
            case DELETE_CLIENT_DIALOG:
            {
                handleClientDeleteDialogUserResponse(botImpl, senderId, chatId, pushedButton, selectedEntity);
                break;
            }
            // диалог Восстановления клиента
            case RESTORE_CLIENT_DIALOG:
            {
                handleClientRestoreDialogUserResponse(botImpl, senderId, chatId, pushedButton, selectedEntity);
                break;
            }
            // Меню фильтрации Ботов
            case BOTS_MENU:
            {
                handleBotsMenuButton(botImpl, senderId, chatId, client, pushedButton);
                break;
            }
            // Меню списка Ботов
            case BOT_LIST_MENU:
            {
                handleBotListMenuButton(botImpl, senderId, chatId, client, pushedButton, selectedEntity);
                break;
            }
            // Меню редактирования Бота
            case BOT_EDIT_MODE:
            {
                handleBotEditMenuButton(botImpl, senderId, chatId, client, pushedButton, selectedEntity);
                break;
            }
            // приход Юзера из неизвестного состояния
            case UNKNOWN:
            default:
            {
                _menuMaster.showMainMenu(botImpl, chatId, senderId, client.getRole().getType());
                break;
            }
        }
    }
    
    /**
     * Обработчик кнопок меню Редактирования Бота
     */
    private void handleBotEditMenuButton(MessengerBot botImpl, long senderId, String chatId, Client client,
                                         BotMenuState pushedButton, UUID selectedEntity)
    {
        switch (pushedButton)
        {
            case ACTIVATE_BOT:
            {
                break;
            }
            case DEACTIVATE_BOT:
            {
                break;
            }
            case CHANGE_BOT_KEY_BUTTON:
            {
                break;
            }
            case CHANGE_BOT_DESCRIPTION_BUTTON:
            {
                break;
            }
            case CHANGE_BOT_NAME_BUTTON:
            {
                break;
            }
            case GO_BACK_BUTTON:
            {
                _menuMaster.showBotsMenu(botImpl, client, chatId, senderId);
                break;
            }
            default:
            {
                _menuMaster.showMainMenu(botImpl, chatId, senderId, client.getRole().getType());
                break;
            }
        }
    }
    
    
    /**
     * Обработчик кнопок меню Фильтрации Ботов
     */
    private void handleBotsMenuButton(MessengerBot botImpl, long senderId, String chatId, Client client,
                                      BotMenuState pushedButton)
    {
        switch (pushedButton)
        {
            case SHOW_ALL_BOTS:
            {
                _menuMaster.showBotListMenu(botImpl, client, chatId, senderId, null);
                break;
            }
            case SHOW_ACTIVE_BOTS:
            {
                _menuMaster.showBotListMenu(botImpl, client, chatId, senderId, true);
                break;
            }
            case SHOW_INACTIVE_BOTS:
            {
                _menuMaster.showBotListMenu(botImpl, client, chatId, senderId, false);
                break;
            }
            case GO_BACK_BUTTON:
            default:
            {
                _menuMaster.showMainMenu(botImpl, chatId, senderId, client.getRole().getType());
                break;
            }
        }
    }
    
    
    /**
     * Обработчик кнопки "Изменить роль Юзера"
     *
     * @param replyData      Строка в формате: "UUID=ROLE_NAME"
     * @param selectedEntity    если передан просто ИД, то нужно вернуться в меню редактирования этого Клиента
     */
    @Transactional
    protected void handleChangeRoleDialogUserResponse(MessengerBot botImpl, long senderId, String chatId, String replyData,
                                                      UUID selectedEntity)
    {
        if (UUIDs.isNotEmpty(selectedEntity))
        {
            _menuMaster.showClientEditMenu(botImpl, senderId, chatId, selectedEntity);
            return;
        }
        try
        {
            UserRole newRole = UserRole.valueOf(CallBackData.getValuePart(replyData));
            UUID clientID = UUID.fromString(CallBackData.getNamePart(replyData));
            Client foundClient = clientRepository.findById(clientID).orElse(null);
            assert foundClient != null;
            Role r = Arrays.stream(Role.ROLES).filter(role -> role.getType() == newRole).findAny().orElse(foundClient.getRole());
            foundClient.setRole(r);
            clientRepository.saveAndFlush(foundClient);
            _menuMaster.showClientEditMenu(botImpl, senderId, chatId, clientID);
        }
        catch (Exception ex)
        {
            botImpl.sendMessage(senderId, "Что-то пошло не так, возможно, такого клиента не существует!");
            _menuMaster.showClientsMenu(botImpl, getClient(botImpl, "", senderId), chatId, senderId);
        }
    }
    
    /**
     * Обработчик ответа пользователя на запрос о подтверждении удаления Задачи
     */
    private void handleTaskDeleteDialogUserResponse(MessengerBot botImpl, long senderId, String chatId, Client client,
                                                    BotMenuState pushedButton, UUID selectedEntity)
    {
        if (pushedButton != BotMenuState.DELETE_TASK_BUTTON)
        {
            _menuMaster.showTaskEditMenu(botImpl, client, chatId, senderId, selectedEntity);
        }
        else if (UUIDs.isEmpty(selectedEntity))
        {
            logger.warn("{}: [{}] попытка удалить Задачу с пустым ИД", classMame, botImpl.getBotUsername());
            botImpl.sendMessage(senderId, "Что-то пошло не так(");
            botImpl.setUserStateTo(senderId, BotMenuState.UNKNOWN);
        }
        // юзер подтвердил удаление Задачи
        else
        {
            if (taskService.deleteTaskByIdFromClient(selectedEntity, client))
            {
                botImpl.sendMessage(senderId, "Задача успешно удалена");
            }
            else
            {
                botImpl.sendMessage(senderId, "Не удалось удалить Задачу!");
            }
            _menuMaster.showMyTasksMenu(botImpl, client, chatId, senderId, client.getRole().getType());
        }
    }
    
    /**
     * Обработчик ответа в Диалоге Удаления Клиента
     */
    private void handleClientDeleteDialogUserResponse(MessengerBot botImpl, long senderId, String chatId,
                                                      BotMenuState pushedButton, UUID selectedEntity)
    {
        if (pushedButton != BotMenuState.DELETE_CLIENT_BUTTON)
        {
            _menuMaster.showClientEditMenu(botImpl, senderId, chatId, selectedEntity);
        }
        else if (UUIDs.isEmpty(selectedEntity))
        {
            logger.warn("{}: [{}] попытка удалить Клиента с пустым ИД", classMame, botImpl.getBotUsername());
            botImpl.sendMessage(senderId, "Что-то пошло не так(");
            botImpl.setUserStateTo(senderId, BotMenuState.UNKNOWN);
        }
        // юзер подтвердил отключение Клиента
        else
        {
            if (clientRepository.updateBotById(Bot.BANNED_USERS_BOT, selectedEntity) == 1)
            {
                botImpl.sendMessage(senderId, "Клиент успешно отключён");
            }
            else
            {
                botImpl.sendMessage(senderId, "Что-то пошло не так!");
            }
            _menuMaster.showClientsMenu(botImpl, getClient(botImpl, "", senderId), chatId, senderId);
        }
    }
    
    /**
     * Метод-обработчик Диалога Восстановления Клиента
     */
    private void handleClientRestoreDialogUserResponse(MessengerBot botImpl, long senderId, String chatId,
                                                       BotMenuState pushedButton, UUID selectedEntity)
    {
        if (pushedButton != BotMenuState.RESTORE_CLIENT_BUTTON)
        {
            _menuMaster.showClientEditMenu(botImpl, senderId, chatId, selectedEntity);
        }
        else if (UUIDs.isEmpty(selectedEntity))
        {
            logger.warn("{}: [{}] попытка Восстановить Клиента с пустым ИД", classMame, botImpl.getBotUsername());
            botImpl.sendMessage(senderId, "Что-то пошло не так(");
            botImpl.setUserStateTo(senderId, BotMenuState.UNKNOWN);
        }
        // юзер подтвердил восстановление Клиента
        else
        {
            if (clientRepository.updateBotById(Bot.RESTORED_USERS_BOT, selectedEntity) == 1)
            {
                botImpl.sendMessage(senderId, "Клиент успешно Восстановлен, теперь он должен написать Боту!");
            }
            else
            {
                botImpl.sendMessage(senderId, "Что-то пошло не так!");
            }
            _menuMaster.showClientsMenu(botImpl, getClient(botImpl, "", senderId), chatId, senderId);
        }
    }
    
    /**
     * Метод-обработчик кнопок меню Редактирования отдельного Клиента
     */
    private void handleClientEditMenuButton(MessengerBot botImpl, long senderId, String chatId, Client client,
                                            BotMenuState pushedButton, UUID selectedEntity)
    {
        if (pushedButton == BotMenuState.CHANGE_CLIENT_ROLE_BUTTON ||
                pushedButton == BotMenuState.DELETE_CLIENT_BUTTON ||
                pushedButton == BotMenuState.RESTORE_CLIENT_BUTTON)
        {
            if (UUIDs.isEmpty(selectedEntity))
            {
                logger.warn("{}: [{}] попытка изменить Клиента с пустым ИД", classMame, botImpl.getBotUsername());
                botImpl.sendMessage(senderId, "Что-то пошло не так(");
                return;
            }
        }
        switch (pushedButton)
        {
            case CHANGE_CLIENT_ROLE_BUTTON:
            {
                Optional<Client> clientOptional = clientRepository.findById(selectedEntity);
                if (!clientOptional.isPresent())
                {
                    _menuMaster.showClientsMenu(botImpl, getClient(botImpl, "", senderId), chatId, senderId);
                    return;
                }
                Map<String, InlineButtonPayload> buttons = new LinkedHashMap<>();
                Arrays.stream(UserRole.values()).skip(1).forEach(r -> buttons.put(r.name(),
                        new InlineButtonPayload(selectedEntity.toString(), r.name()))
                );
                final String curRoleName = clientOptional.get().getRole().getType().name();
                buttons.put("Оставить как есть - (" + curRoleName + ")", new InlineButtonPayload(selectedEntity.toString()));
                botImpl.sendInlineKeyboard("Выберите новую роль для клиента " + BotUtils.getClientTitle(clientOptional.get()),
                        chatId, buttons.entrySet());
                botImpl.setUserStateTo(senderId, BotMenuState.CLIENT_ROLE_EDIT_DIALOG);
                break;
            }
            case DELETE_CLIENT_BUTTON:
            {
                botImpl.sendYesNoInlineKeyboard("Вы уверены, что хотите отключить этого клиента?", chatId,
                        new CallBackData(BotMenuState.DELETE_CLIENT_BUTTON.name(), selectedEntity.toString()),
                        new CallBackData(BotMenuState.CLIENT_EDIT_MENU.name(), selectedEntity.toString())
                );
                botImpl.setUserStateTo(senderId, BotMenuState.DELETE_CLIENT_DIALOG);
                break;
            }
            case RESTORE_CLIENT_BUTTON:
            {
                botImpl.sendYesNoInlineKeyboard("Вы уверены, что хотите Восстановить этого клиента?", chatId,
                        new CallBackData(BotMenuState.RESTORE_CLIENT_BUTTON.name(), selectedEntity.toString()),
                        new CallBackData(BotMenuState.CLIENT_EDIT_MENU.name(), selectedEntity.toString())
                );
                botImpl.setUserStateTo(senderId, BotMenuState.RESTORE_CLIENT_DIALOG);
                break;
            }
            case GO_BACK_BUTTON:
            {
                _menuMaster.showClientsMenu(botImpl, client, chatId, senderId);
                break;
            }
            default:
            {
                _menuMaster.showMainMenu(botImpl, chatId, senderId, client.getRole().getType());
                break;
            }
        }
    }
    
    /**
     * Обработчик кнопок меню Редактирования Задачи
     *
     * @param pushedButton      Кнопка меню
     * @param selectedEntity    Выбранная сущность
     */
    private void handleTaskEditMenuButton(MessengerBot botImpl, long senderId, String chatId, Client client,
                                          BotMenuState pushedButton, UUID selectedEntity)
    {
        final UserRole clientRole = client.getRole().getType();
        switch (pushedButton)
        {
            case DELETE_TASK_BUTTON:
            {
                if (UUIDs.isEmpty(selectedEntity))
                {
                    logger.warn("{}: [{}] попытка удалить Задачу с пустым ИД", classMame, botImpl.getBotUsername());
                    botImpl.sendMessage(senderId, "Что-то пошло не так(");
                    return;
                }
                botImpl.sendYesNoInlineKeyboard("Вы уверены, что хотите удалить эту задачу?", chatId, 
                        new CallBackData(BotMenuState.DELETE_TASK_BUTTON.name(), selectedEntity.toString()),
                        new CallBackData(BotMenuState.TASK_EDIT_MENU.name(), selectedEntity.toString())
                );
                botImpl.setUserStateTo(senderId, BotMenuState.DELETE_TASK_DIALOG);
                break;
            }
            // команда принудительной проверки Задачи (без учёта даты её последней проверки) 
            case CHECK_TASK_BUTTON:
            {
                botImpl.sendMessage(senderId, "Запущена проверка вашей задачи.\n При наличии обновлений вам придёт сообщение");
                if (!taskService.checkTaskByIdForClient(selectedEntity, client))
                {
                    botImpl.sendMessage(senderId, "Что-то пошло не так, возможно такой задачи не существует {0}", selectedEntity);
                }
                _menuMaster.showTaskEditMenu(botImpl, client, chatId, senderId, selectedEntity);
                break;
            }
            case GO_BACK_BUTTON:
            {
                _menuMaster.showMyTasksMenu(botImpl, client, chatId, senderId, clientRole);
                break;
            }
            default:
            {
                _menuMaster.showMainMenu(botImpl, chatId, senderId, clientRole);
                break;
            }
        }
    }
    
    /**
     * Обработчик меню Задач юзера
     *
     * @param selectedEntity Если задан игнорирует значение параметра {@code pushedButton}
     */
    private void handleMyTasksMenuButton(MessengerBot botImpl, long senderId, String chatId, Client client,
                                         BotMenuState pushedButton, UUID selectedEntity)
    {
        final UserRole clientRole = client.getRole().getType();
        if (UUIDs.isNotEmpty(selectedEntity))
        {
            pushedButton = BotMenuState.SELECT_TASK_BUTTON;
        }
        switch (pushedButton)
        {
            case ADD_TASK_BUTTON:
            {
                // у гостя не может быть задач, так что мы сразу переводим его в режим ввода Новой Задачи
                if (clientRole == UserRole.GUEST)
                {
                    this.gotoTaskInput(botImpl, client, chatId, senderId);
                }
                else
                {
                    final int maxTaskCount = taskService.getMaxTaskCountPerClient();
                    if (client.getTasks().size() >= maxTaskCount)
                    {
                        botImpl.sendMessage(senderId, "Превышено макс. кол-во задач для вашего аккаунта: {0}",
                                maxTaskCount);
                    }
                    else
                    {
                        this.gotoTaskInput(botImpl, client, chatId, senderId);
                    }
                }
                break;
            }
            case CHECK_MY_TASKS:
            {
                botImpl.sendMessage(senderId, "Запущена проверка всех ваших задач.\n При наличии обновлений вам придёт сообщение");
                taskService.checkAllByClient(client);
                _menuMaster.showMyTasksMenu(botImpl, client, chatId, senderId, clientRole);
                break;
            }
            case RESET_LAST_CHECK_DATE:
            {
                taskService.resetAllCheckDates();
                botImpl.sendMessage(senderId, "Даты последней проверки для всех задач сброшены");
                _menuMaster.showMyTasksMenu(botImpl, client, chatId, senderId, clientRole);
                break;
            }
            case SELECT_TASK_BUTTON:
            {
                _menuMaster.showTaskEditMenu(botImpl, client, chatId, senderId, selectedEntity);
                break;
            }
            case GO_BACK_BUTTON:
            {
                _menuMaster.showMainMenu(botImpl, chatId, senderId, clientRole);
                break;
            }
            default:
            {
                _menuMaster.showMyTasksMenu(botImpl, client, chatId, senderId, clientRole);
                break;
            }
        }
    }
    
    /**
     * Метод-обработчик кнопок меню "Список клиентов"
     */
    private void handleClientListMenuButton(MessengerBot botImpl, long senderId, String chatId, Client client,
                                            BotMenuState pushedButton, UUID selectedEntity)
    {
        if (pushedButton == BotMenuState.GO_BACK_BUTTON)
        {
            _menuMaster.showClientsMenu(botImpl, client, chatId, senderId);
            return;
        }
        else if (UUIDs.isEmpty(selectedEntity))
        {
            botImpl.sendMessage(senderId, "Что-то пошло не так - передан пустой ИД клиента");
            processMenu(botImpl, senderId, "", chatId, BotMenuState.CLIENT_LIST_MENU.name(), true);
            return;
        }
        _menuMaster.showClientEditMenu(botImpl, senderId, chatId, selectedEntity);
    }
    
    /**
     * Обработчик нажатия на кнопку в списке Ботов
     */
    private void handleBotListMenuButton(MessengerBot botImpl, long senderId, String chatId, Client client,
                                         BotMenuState pushedButton, UUID selectedEntity)
    {
        if (pushedButton == BotMenuState.GO_BACK_BUTTON)
        {
            _menuMaster.showBotsMenu(botImpl, client, chatId, senderId);
        }
        else if (pushedButton == BotMenuState.ADD_BOT_BUTTON)
        {
            gotoAddBotNameMode(botImpl, senderId, chatId, selectedEntity);
        }
        else if (UUIDs.isEmpty(selectedEntity))
        {
            botImpl.sendMessage(senderId, "Что-то пошло не так - передан пустой ИД бота");
            processMenu(botImpl, senderId, "", chatId, BotMenuState.BOT_LIST_MENU.name(), true);
        }
        else
        {
            _menuMaster.showBotEditMenu(botImpl, senderId, chatId, client, selectedEntity);
        }
    }
    
    /**
     * Метод обработчик кнопок меню "Управление клиентами"
     */
    private void handleClientsMenuButton(MessengerBot botImpl, long senderId, String chatId, Client client,
                                         BotMenuState pushedButton, UUID selectedEntity)
    {
        final UserRole clientRole = client.getRole().getType();
        if (UUIDs.isNotEmpty(selectedEntity))
        {
            pushedButton = BotMenuState.SELECT_TASK_BUTTON;
        }
        switch (pushedButton)
        {
            case SHOW_GUESTS:
            {
                _menuMaster.showClientListMenu(botImpl, chatId, senderId, UserRole.GUEST);
                break;
            }
            case SHOW_USERS:
            {
                _menuMaster.showClientListMenu(botImpl, chatId, senderId, UserRole.USER);
                break;
            }
            case SHOW_ADMINS:
            {
                _menuMaster.showClientListMenu(botImpl, chatId, senderId, UserRole.ADMIN);
                break;
            }
            case SHOW_ALL_CLIENTS:
            {
                _menuMaster.showClientListMenu(botImpl, chatId, senderId, UserRole.UNKNOWN);
                break;
            }
            // TODO: по идее фильтр Активные/Неактивные лучше всего было бы разместить на втором уровне и возможно на
            // на ходу обновлять список показанных клиентов (редактируя сообщение)
            case SHOW_ACTIVE_CLIENTS:
            {
                _menuMaster.showClientListMenu(botImpl, chatId, senderId, UserRole.UNKNOWN, UserStatus.ACTIVE);
                break;
            }
            case SHOW_INACTIVE_CLIENTS:
            {
                _menuMaster.showClientListMenu(botImpl, chatId, senderId, UserRole.UNKNOWN, UserStatus.INACTIVE);
                break;
            }
            case SHOW_BANNED_CLIENTS:
            {
                _menuMaster.showClientListMenu(botImpl, chatId, senderId, UserRole.UNKNOWN, UserStatus.BANNED);
                break;
            }
            case GO_BACK_BUTTON:
            default:
            {
                _menuMaster.showMainMenu(botImpl, chatId, senderId, clientRole);
                break;
            }
        }
    }
    
    
    /**
     * Обработчик кнопок Главного меню
     */
    private void handleMainMenuButton(MessengerBot botImpl, long senderId, String chatId, Client client, BotMenuState pushedButton)
    {
        switch (pushedButton)
        {
            case MY_TASKS_MENU:
            {
                _menuMaster.showMyTasksMenu(botImpl, client, chatId, senderId, client.getRole().getType());
                break;
            }
            case BOTS_MENU:
            {
                _menuMaster.showBotsMenu(botImpl, client, chatId, senderId);
                break;
            }
            case CLIENTS_MENU:
            {
                _menuMaster.showClientsMenu(botImpl, client, chatId, senderId);
                break;
            }
            case OUTAGE_REFERENCE:
            {
                _menuMaster.showOutageReferenceMenu(botImpl, client, chatId, senderId);
                break;
            }
            case SEARCH_ADDRESS:
            {
                this.gotoAddressSearch(botImpl, client, chatId, senderId);
                break;
            }
            default:
            {
                _menuMaster.showMainMenu(botImpl, chatId, senderId, client.getRole().getType());
                break;
            }
        }
    }
    
    
    /**
     * Переходим в режим поиска адресов по справочнику Отключений
     */
    private void gotoAddressSearch(MessengerBot botImpl, Client client, String chatId, long senderId)
    {
        botImpl.sendMessage(Long.parseLong(chatId), "Введите строку для поиска\n " +
                "(Город и Номер дома должны быть отделены запятой)," +
                " например,\n \"г. Новочеркасск, ул. Высоковольтная\" или \"Ростов-на-Дону, Молодежная, 30\":");
        botImpl.setUserStateTo(senderId, BotMenuState.SEARCH_ADDRESS);
    }
    
    /**
     * Обработчик перехода в режим Ввода новой Задачи
     */
    private void gotoTaskInput(MessengerBot botImpl, Client client, String chatId, long senderId)
    {
        botImpl.sendMessage(senderId, "Введите новую задачу в виде: \n\n" +
                "<i>Город</i><b>,</b> <i>Улица</i><b>,</b> <i>номер дома</i>\n\n " +
                "(Разделитель - запятая \"<b>,</b>\".\n Обязательным является, только Город)" );
        botImpl.setUserStateTo(senderId, BotMenuState.TASK_INPUT_MODE);
    }
    
    /**
     * Обработчик перехода в режим Добавления нового Бота
     */
    public void gotoAddBotNameMode(MessengerBot botImpl, long senderId, String chatId, UUID selectedEntity)
    {
        botImpl.sendMessage(senderId, "<b>Режим регистрации нового Бота</b>\n\n" +
                "Введите имя Бота в Телеграм (то, что с окончанием Bot):");
        botImpl.setUserStateTo(senderId, BotMenuState.BOT_INPUT_NAME_MODE);
    }
    
    /**
     * Метод управление режимом Ввода новой Задачи
     *
     * @implNote Добавляем задачи по одной - после ввода каждой задачи возвращаем Юзера в меню Задач
     */
    private void handleTaskInputMode(MessengerBot botImpl, long senderId, String chatId, Client client, String replyText)
    {
        if (!BotUtils.checkInputLength(botImpl, senderId, replyText) || !BotUtils.checkLegalChars(botImpl, senderId, replyText))
        {
            return;
        }
        String city =  StringUtils.substringBefore(replyText, Task.STREET_HOUSE_SEPARATOR);
        String address = StringUtils.substringAfter(replyText, Task.STREET_HOUSE_SEPARATOR);
        Task tsk = taskService.addTask(city, address, client);
        logger.debug(tsk.getClients().stream().map(Client::toString).collect(Collectors.joining("\n")));
        botImpl.sendMessage(senderId, "Поздравляем, задача успешно добавлена!");
        _menuMaster.showMyTasksMenu(botImpl, client, chatId, senderId, client.getRole().getType());
    }
    
    /**
     * Метод управление режимом поиска Адреса
     */
    private void handleAddressSearchMode(MessengerBot botImpl, long senderId, String chatId, Client client, String replyText)
    {
        if (!BotUtils.checkInputLength(botImpl, senderId, replyText) || !BotUtils.checkLegalChars(botImpl, senderId, replyText))
        {
            return;
        }
        String city =  StringUtils.substringBefore(replyText, Task.STREET_HOUSE_SEPARATOR);
        String address = StringUtils.substringAfter(replyText, Task.STREET_HOUSE_SEPARATOR);
        botImpl.sendMessage(senderId, "Выполняю поиск, ожидайте...");
        List<OutageRefEntry> foundEntries = outageRefService.checkAddress(city, address);
        int count = foundEntries.size();
        // Пример: "азов, мичурина, 21"
        if (count <= 0)
        {
            botImpl.sendMessage(senderId, "По вашему запросу ничего не найдено");
            return;
        }
        StringBuilder result = new StringBuilder(foundEntries.size() + 1);
        result.append("<b>По вашему запросу найдено <i>").append(count).append("</i> записей:</b>");
        foundEntries.forEach(entry -> result.append("\n\n").append(entry.toString("s")));
        String answer = result.toString();
        botImpl.sendMessage(senderId, answer);
    }

    
    
    
    
    

}
