package org.mayurmu.finalwork.gorsvet.exceptions;

import org.mayurmu.finalwork.gorsvet.service.TaskService;

/**
 * Выбрасывается некоторыми методами {@link TaskService} при попытке добавить для клиента больше задач, чем разрешено.
 */
public class MaxClientTasksCountExceededExeption extends RuntimeException
{
    public MaxClientTasksCountExceededExeption(String mes, Throwable cause)
    {
        super(mes, cause);
    }
    
    public MaxClientTasksCountExceededExeption(String mes)
    {
        super(mes);
    }
}
