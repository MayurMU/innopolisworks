package org.mayurmu.finalwork.gorsvet;

import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * Приложение парсер сайта ДонЭнерго с уведомлением о результатах клиентов через Телеграм (по указанным ими критериям)
 *
 * @author @MayurMU (Маюров Михаил)
 */
@SpringBootApplication
@EntityScan("org.mayurmu.finalwork.gorsvet.model")
@EnableScheduling
public class GorSvetApplication
{
    /**
     * Главная точка входа приложения - при запуске создаёт все синглтон-бины (они все такие по умолчанию).
     */
    public static void main(String[] args)
    {
        SpringApplication.run(GorSvetApplication.class, args);
    }
    
    /**
     * Аннотация @Bean - это по сути аналог синглтона (если не указаны доп. аннотации, вроде @Scope)
     */
    @Bean
    public ModelMapper getModelMapper()
    {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setFieldMatchingEnabled(true).setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);
        return mapper;
    }
    
    @Bean
    public Logger getAppLogger()
    {
        return LoggerFactory.getLogger(GorSvetApplication.class);
    }
    
    /**
     * Этот бин разрешает асинхронное выполнение обработчиков событий (к сожалению сразу всех)
     *
     * @see <a href="https://www.baeldung.com/spring-events#anonymous-events">Справка</a>
     */
    @Bean
    public ApplicationEventMulticaster applicationEventMulticaster()
    {
        SimpleApplicationEventMulticaster eventMulticaster = new SimpleApplicationEventMulticaster();
        eventMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return eventMulticaster;
    }
}
