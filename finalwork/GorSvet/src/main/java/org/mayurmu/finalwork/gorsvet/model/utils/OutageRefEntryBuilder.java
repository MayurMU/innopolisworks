package org.mayurmu.finalwork.gorsvet.model.utils;

import org.mayurmu.finalwork.gorsvet.model.OutageRefEntry;
import org.mayurmu.finalwork.gorsvet.model.PageUrlRefEntry;

import java.time.LocalDateTime;


public final class OutageRefEntryBuilder
{
    private String city = "";
    private String streetsAndHouses;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private String reason;
    private String comments;
    private PageUrlRefEntry pageUrlRefEntry;
    
    private OutageRefEntryBuilder()
    {
    }
    
    public static OutageRefEntryBuilder createBuilder()
    {
        return new OutageRefEntryBuilder();
    }
    
    public OutageRefEntryBuilder city(String city)
    {
        this.city = city;
        return this;
    }
    
    public OutageRefEntryBuilder streetsAndHouses(String streetsAndHouses)
    {
        this.streetsAndHouses = streetsAndHouses;
        return this;
    }
    
    public OutageRefEntryBuilder startDateTime(LocalDateTime startDateTime)
    {
        this.startDateTime = startDateTime;
        return this;
    }
    
    public OutageRefEntryBuilder endDateTime(LocalDateTime endDateTime)
    {
        this.endDateTime = endDateTime;
        return this;
    }
    
    public OutageRefEntryBuilder reason(String reason)
    {
        this.reason = reason;
        return this;
    }
    
    public OutageRefEntryBuilder comments(String comments)
    {
        this.comments = comments;
        return this;
    }
    
    public OutageRefEntryBuilder pageUrlRefEntry(PageUrlRefEntry pageUrlRefEntry)
    {
        this.pageUrlRefEntry = pageUrlRefEntry;
        return this;
    }
    
    /**
     * @exception IllegalStateException Когда не задана ссылка на справочник URL'ов {@link PageUrlRefEntry} или когда не
     *          задана ни одна из Дат.
     *
     * @implNote Пример несоответствия дат Начала и Окончания отключения:
     *          		<tr>
     * 						<td>2</td>
     * 						<td>г. Новошахтинск</td>
     * 						<td>ул. Веселая 41; ул. Заводская 46; ул. Кирпичная 22-62, 17-49; ул. Кирпичный проезд 8А-14Б; ул. Лазо 1-21,
     * 						2-24А; ул. Лебедева 24-34А, 1А, 15-17; ул. Короленко 50-62, 1, 3А-9Б, 23-27</td>
     * 						<td>23.09.22</td>
     * 						<td>22.09.22</td>
     * 						<td>08=00</td>
     * 						<td>17=00</td>
     * 						<td>Работы для исключения аварийных ситуаций на оборудовании ЛЭП-0,4 кВ Л-3 от ТП-106</td>
     * 						<td></td>
     * 					</tr>
     */
    public OutageRefEntry build()
    {
        if (startDateTime == null || endDateTime == null)
        {
            throw new IllegalStateException("Обе даты, и Дата начала и Дата окончания отключения должны быть заданы!");
        }
        if (pageUrlRefEntry == null)
        {
            throw new IllegalStateException("Ссылка на запись справочника URL-ов должна быть задана - данные НЕ могли быть получены из воздуха!");
        }
        return new OutageRefEntry(city, streetsAndHouses, startDateTime, endDateTime, reason, comments, pageUrlRefEntry);
    }
}
