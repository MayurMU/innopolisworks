package org.mayurmu.finalwork.gorsvet.repository;

import org.mayurmu.finalwork.gorsvet.model.Bot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface BotRepository extends JpaRepository<Bot, UUID>
{
    /**
     * @return Возвращает первого попавшегося Живого бота
     */
    @Query("select b from Bot b where b.isAlive = true")
    List<Bot> findAllByIsAliveTrue();
    
    @Query("select b from Bot b where b.userName = ?1")
    Optional<Bot> findByUserName(String userName);
    
    @Query("select b from Bot b where b.isAlive = ?1 and length(b.secretKey) > 10")
    List<Bot> findAllByIsAlive(boolean isAlive);
    
    @Query("select b from Bot b where length(b.secretKey) > 10")
    List<Bot> findAllWithSecretKey();
}