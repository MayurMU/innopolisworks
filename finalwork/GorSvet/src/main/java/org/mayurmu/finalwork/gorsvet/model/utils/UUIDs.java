package org.mayurmu.finalwork.gorsvet.model.utils;

import java.util.UUID;

public class UUIDs
{
    public static final UUID EMPTY = new UUID(0L, 0L);
    
    public static boolean isNotEmpty(UUID id)
    {
        return !isEmpty(id);
    }
    
    public static boolean isEmpty(UUID id)
    {
        return id == null || id == EMPTY;
    }
}
