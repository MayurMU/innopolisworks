package org.mayurmu.finalwork.gorsvet.event;

import org.mayurmu.finalwork.gorsvet.model.OutageRefEntry;
import org.mayurmu.finalwork.gorsvet.model.Task;
import org.mayurmu.finalwork.gorsvet.service.TaskService;

import org.springframework.context.ApplicationEvent;

import java.util.List;
import java.util.Map;

/**
 * Событие наличия изменений данных сайта для ряда задач
 *
 * @apiNote Генерируется в {@link TaskService}
 */
public class TaskUpdatesFoundEvent extends ApplicationEvent
{
    public final Map<Task, List<OutageRefEntry>> tasksWithUpdates;
    
    public TaskUpdatesFoundEvent(Object source, Map<Task, List<OutageRefEntry>> taskUpdates)
    {
        super(source);
        tasksWithUpdates = taskUpdates;
    }
}
