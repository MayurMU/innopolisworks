package org.mayurmu.finalwork.gorsvet.component.telegram.menu;

import java.util.Arrays;

/**
 * Содержит наборы кнопок, принадлежащих разным меню (первого уровня)
 */
public class MenuButtons
{
    /**
     * Меню Задач
     */
    public static class TasksMenu
    {
        public final BotMenuState menuButton = BotMenuState.MY_TASKS_MENU;
        public final BotMenuState[] buttons = { BotMenuState.ADD_TASK_BUTTON,
                                                BotMenuState.CHECK_MY_TASKS,
                                                BotMenuState.SELECT_TASK_BUTTON};
        
        public boolean contains(BotMenuState menuState)
        {
            return Arrays.asList(MenuButtons.TASKS_MENU.buttons).contains(menuState);
        }
    }
    public static final TasksMenu TASKS_MENU = new TasksMenu();
    
    /**
     * Меню Ботов
     */
    public static class BotsMenu
    {
        public final BotMenuState menuButton = BotMenuState.BOTS_MENU;
        public final BotMenuState[] buttons = {
                                                BotMenuState.SHOW_ALL_BOTS,
                                                BotMenuState.SHOW_ACTIVE_BOTS,
                                                BotMenuState.SHOW_INACTIVE_BOTS};
        
        public boolean contains(BotMenuState menuState)
        {
            return Arrays.asList(MenuButtons.BOTS_MENU.buttons).contains(menuState);
        }
    }
    public static final BotsMenu BOTS_MENU = new BotsMenu();
    
    /**
     * Меню Клиентов
     */
    public static class ClientsMenu
    {
        public final BotMenuState menuButton = BotMenuState.CLIENTS_MENU;
        public final BotMenuState[] buttons = { BotMenuState.SHOW_ACTIVE_CLIENTS,
                                                BotMenuState.SHOW_INACTIVE_CLIENTS,
                                                BotMenuState.SHOW_ADMINS,
                                                BotMenuState.SHOW_USERS,
                                                BotMenuState.SHOW_GUESTS};
        
        public boolean contains(BotMenuState menuState)
        {
            return Arrays.asList(MenuButtons.CLIENTS_MENU.buttons).contains(menuState);
        }
    }
    public static final ClientsMenu CLIENTS_MENU = new ClientsMenu();
    
    /**
     * Меню Справочника
     */
    public static class OutageReferenceMenu
    {
        public final BotMenuState menuButton = BotMenuState.OUTAGE_REFERENCE;
        public final BotMenuState[] buttons = { BotMenuState.MANAGE_URLS,
                                                BotMenuState.UPDATE_REFERENCE,
                                                BotMenuState.DOWNLOAD_PAGES,
                                                BotMenuState.PARSE_PAGES,
                                                BotMenuState.TRUNCATE_REFERENCE};
        
        public boolean contains(BotMenuState menuState)
        {
            return Arrays.asList(MenuButtons.OUTAGE_MENU.buttons).contains(menuState);
        }
    }
    public static final OutageReferenceMenu OUTAGE_MENU = new OutageReferenceMenu();
    

    /**
     * Главное меню
     */
    public static class MainMenu
    {
        public final BotMenuState menuButton = BotMenuState.MAIN_MENU;
        public final BotMenuState[] buttons = { BotMenuState.BOTS_MENU,
                                                BotMenuState.MY_TASKS_MENU,
                                                BotMenuState.SEARCH_ADDRESS,
                                                BotMenuState.CLIENTS_MENU,
                                                BotMenuState.OUTAGE_REFERENCE};
        
        public boolean contains(BotMenuState menuState)
        {
            return Arrays.asList(MenuButtons.MAIN_MENU.buttons).contains(menuState);
        }
    }
    public static final MainMenu MAIN_MENU = new MainMenu();
}
