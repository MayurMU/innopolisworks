package org.mayurmu.finalwork.gorsvet.exceptions;

public class SiteParseException extends RuntimeException
{
    public SiteParseException(String mes, Throwable cause)
    {
        super(mes, cause);
    }
    
    public SiteParseException(String mes)
    {
        super(mes);
    }
}
