package org.mayurmu.finalwork.gorsvet.exceptions;

import org.slf4j.Logger;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

/**
 * Кастомный класс обработки не перехваченных исключений фоновых потоков - просто записывает ошибку в лог
 *
 * @implNote TODO: в идеале, надо бы уведомлять админа по email\Telegram
 *
 * @see <a href="https://www.baeldung.com/spring-async#exception-handling">Baeldung: Async Exception Handling</a>
 */
@Component
public class UnhandledAsyncExceptionHandler implements AsyncUncaughtExceptionHandler
{
    private final Logger logger;
    
    @Autowired
    public UnhandledAsyncExceptionHandler(Logger logger)
    {
        this.logger = logger;
    }
    
    @Override
    public void handleUncaughtException(Throwable throwable, Method method, Object... obj)
    {
        StringBuilder exParamsBuilder = new StringBuilder();
        for (Object param : obj)
        {
            exParamsBuilder.append("\tParameter value = ").append(param).append(System.lineSeparator());
        }
        try (StringWriter sw = new StringWriter();
             PrintWriter pw = new PrintWriter(sw))
        {
            throwable.printStackTrace(pw);
            exParamsBuilder.append(throwable).append(sw.toString());
        }
        catch (Exception ex)
        {
            throwable.printStackTrace();
        }
        logger.error("Unhandled Async Exception \"{}\" was thrown in Method \"{}()\", with params: \n{}",
                 throwable, method.getName(), exParamsBuilder);
    }
}


