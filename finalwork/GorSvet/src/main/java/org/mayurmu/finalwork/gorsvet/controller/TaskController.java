package org.mayurmu.finalwork.gorsvet.controller;

import org.mayurmu.finalwork.gorsvet.service.TaskService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Отладочный REST-контроллер задач
 */
@RestController
@RequestMapping("/tasks")
public class TaskController
{
    private final Logger logger;
    private final TaskService taskService;
    
    @Autowired
    public TaskController(TaskService taskService, Logger logger)
    {
        this.logger = logger;
        this.taskService = taskService;
    }
    
    
    @GetMapping("/start")
    public String startCheckAll()
    {
        String mes = "Запущена проверка всех Задач";
        logger.info("{}: {}", TaskController.class.getSimpleName(), mes);
        taskService.checkAll();
        return mes;
    }
}
