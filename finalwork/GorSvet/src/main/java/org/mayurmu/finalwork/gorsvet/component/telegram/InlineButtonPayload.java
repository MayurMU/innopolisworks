package org.mayurmu.finalwork.gorsvet.component.telegram;

import java.net.URI;
import java.util.Objects;

/**
 * Расширяет класс {@link CallBackData} так, чтобы он мог хранить URL, а не только строку
 */
public class InlineButtonPayload extends CallBackData
{
    public final URI url;
    
    public InlineButtonPayload(String name, String value)
    {
        super(name, value);
        url = null;
    }
    
    public InlineButtonPayload(String callBackData)
    {
        super(callBackData);
        url = null;
    }
    
    public InlineButtonPayload(URI url)
    {
        super("");
        this.url = url;
    }
    
    @Override
    public String toString()
    {
        return Objects.toString(url, super.callback_data);
    }
}
