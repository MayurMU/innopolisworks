package org.mayurmu.finalwork.gorsvet.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A DTO for the {@link org.mayurmu.finalwork.gorsvet.model.Bot} entity
 */
public class BotDto implements Serializable
{
    private String userName;
    private String comment;
    private boolean isAlive;
    private LocalDateTime keyChangeDateTime;
    private LocalDateTime createDateTime;
    
    public BotDto()
    {
    }
    
    public BotDto(String userName, String comment, boolean isAlive, LocalDateTime keyChangeDateTime, LocalDateTime createDateTime)
    {
        this.userName = userName;
        this.comment = comment;
        this.isAlive = isAlive;
        this.keyChangeDateTime = keyChangeDateTime;
        this.createDateTime = createDateTime;
    }
    
    public String getUserName()
    {
        return userName;
    }
    
    public String getComment()
    {
        return comment;
    }
    
    public boolean getIsAlive()
    {
        return isAlive;
    }
    
    public LocalDateTime getKeyChangeDateTime()
    {
        return keyChangeDateTime;
    }
    
    public LocalDateTime getCreateDateTime()
    {
        return createDateTime;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        BotDto entity = (BotDto) o;
        return Objects.equals(this.userName, entity.userName) &&
                Objects.equals(this.comment, entity.comment) &&
                Objects.equals(this.isAlive, entity.isAlive) &&
                Objects.equals(this.keyChangeDateTime, entity.keyChangeDateTime) &&
                Objects.equals(this.createDateTime, entity.createDateTime);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(userName, comment, isAlive, keyChangeDateTime, createDateTime);
    }
    
    @Override
    public String toString()
    {
        return getClass().getSimpleName() + "(" +
                "userName = " + userName + ", " +
                "comment = " + comment + ", " +
                "isAlive = " + isAlive + ", " +
                "keyChangeDateTime = " + keyChangeDateTime + ", " +
                "createDateTime = " + createDateTime + ")";
    }
}