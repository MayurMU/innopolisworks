package org.mayurmu.finalwork.gorsvet.repository;

import org.mayurmu.finalwork.gorsvet.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
public interface TaskRepository extends JpaRepository<Task, UUID>
{
    /**
     * @apiNote Тут по идее нужен страничный подход, т.к. теоретически Задач может быть очень много.
     *
     * @return Все задачи с активными ботами
     */
    List<Task> findByClients_Bot_IsAliveTrue();
    
    @Transactional
    @Modifying
    @Query("update Task t set t.lastCheckDateTime = null")
    int resetAllCheckDates();
    
}