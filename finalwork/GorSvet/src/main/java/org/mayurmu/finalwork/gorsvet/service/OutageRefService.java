package org.mayurmu.finalwork.gorsvet.service;

import org.mayurmu.finalwork.gorsvet.model.OutageRefEntry;

import java.time.LocalDateTime;
import java.util.List;

public interface OutageRefService
{
    List<OutageRefEntry> checkAddress(String city, String address);
    
    List<OutageRefEntry> checkAddress(LocalDateTime lastCheckDateTime, String city, String address);
    
    List<OutageRefEntry> checkAddress(LocalDateTime lastCheckDateTime, String city, String address, List<OutageRefEntry> allOutageEntries);
}
