package org.mayurmu.finalwork.gorsvet.controller;

import org.mayurmu.finalwork.gorsvet.service.SiteParserService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


/**
 * @apiNote Используем здесь ленивую загрузку бина, чтобы дождать иниц-ии данных Спрингом из data.sql
 */
@RestController
@RequestMapping("/parser")
@Lazy
public class SiteParserController
{
    private final Logger logger;
    private final SiteParserService parserService;
    
    @Autowired
    public SiteParserController(SiteParserService parserService, Logger logger)
    {
        this.logger = logger;
        this.parserService = parserService;
    }
    
    /**
     * Выполняет загрузку и парсинг всех страниц
     */
    @GetMapping("/start")
    public String startParser()
    {
        parserService.loadAndParse();
        return "Запущен (или уже выполняется) полный цикл задачи загрузки и парсинга сайта";
    }
    
    @GetMapping("/load")
    public String loadPages()
    {
        String mes = "Запущена загрузка страниц";
        logger.info(mes);
        parserService.loadAll();
        return mes;
    }
    
    @GetMapping("/parse")
    public String parsePages()
    {
        String mes = "Запущен парсинг страниц";
        logger.info(mes);
        parserService.parseAll();
        return mes;
    }
    
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String onServerError(RuntimeException exception)
    {
        return "Неожиданная ошибка сервера: " + exception.getClass().getSimpleName() + " - " + exception.getMessage();
    }
}
