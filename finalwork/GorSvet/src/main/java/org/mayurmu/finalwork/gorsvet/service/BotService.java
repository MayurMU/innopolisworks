package org.mayurmu.finalwork.gorsvet.service;

import org.mayurmu.finalwork.gorsvet.event.ClientTaskUpdatesFoundEvent;
import org.mayurmu.finalwork.gorsvet.event.TaskUpdatesFoundEvent;
import org.mayurmu.finalwork.gorsvet.model.Client;
import org.mayurmu.finalwork.gorsvet.model.OutageRefEntry;
import org.mayurmu.finalwork.gorsvet.model.Task;
import org.mayurmu.finalwork.gorsvet.repository.BotRepository;
import org.mayurmu.finalwork.gorsvet.repository.ClientRepository;
import org.slf4j.Logger;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;

import java.time.Instant;
import java.util.List;
import java.util.Map;

public interface BotService
{
    boolean isTaskStarted();
    
    Instant getStartTime();
    
    @EventListener
    void onApplicationEvent(TaskUpdatesFoundEvent event);
    
    @EventListener
    void onApplicationEvent(ClientTaskUpdatesFoundEvent event);
    
    @Async
    void sendAllNotifications(Map<Task, List<OutageRefEntry>> tasksWithUpdates);
    
    /**
     * Отправляет оповещение конкретному клиенту по конкретной задаче
     *
     * @param outageRefEntries Список записей Справочника отключений, в которых обнаружено совпадение с критериями Клиента
     */
    @Async
    void sendNotificationForClient(Client client, Task tsk, List<OutageRefEntry> outageRefEntries);

}
