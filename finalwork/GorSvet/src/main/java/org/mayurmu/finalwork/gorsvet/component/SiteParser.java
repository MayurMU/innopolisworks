package org.mayurmu.finalwork.gorsvet.component;


import java.net.URI;
import java.nio.file.Path;
import java.util.Map;

public interface SiteParser
{
    URI getSiteRoot();
    
    void setSiteRoot(URI siteRoot);
    
    Map<String, URI> getPageAddresses();
    
    void setPageAddresses(Map<String, URI> pageAddresses);
    
    /**
     * Назначает адреса для мониторинга
     *
     * @param baseAdr   корневой адрес сайта
     * @param pageAdrs  адреса страниц относительно корня (но могут быть и абсолютными, тогда корень будет игнорироваться)
     */
    @SuppressWarnings("unchecked")
    void config(URI baseAdr, Map.Entry<String, URI> ... pageAdrs);
    
    /**
     * Выполняет конфигурацию по умолчанию, предоставляемую реализацией
     */
    void config();
    
    /**
     * Загружает страницы в память по адресам указанным в процессе настройки (см.: {@link #config})
     */
    boolean load();
    
    /**
     * Альтернативный метод загрузки данных для парсинга из указанных файлов (полученных из какого-то другого источника)
     *
     * @param filePaths имена файлов определяют имена страниц
     */
    boolean load(Path ... filePaths);
    
    /**
     * Альтернативный метод прямой загрузки данных для парсинга из указанных строк включающих содержимое нужных страниц.
     *
     * @param pages массив содержимого страниц для парсинга, здесь Ключ - имя страницы, Значение - само содержимое
     */
    @SuppressWarnings("unchecked")
    boolean load(Map.Entry<String, String> ... pages);
    
    /**
     * Вызывает парсинг всех загруженных страниц
     *
     * @return true - были обновления, false - нет обновлений или ошибка
     */
    boolean parse();
    
    /**
     * Вызывает парсинг конкретной загруженной ранее страницы
     */
    boolean parse(String pageName);

}
