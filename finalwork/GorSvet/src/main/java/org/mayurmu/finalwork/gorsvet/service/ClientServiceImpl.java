package org.mayurmu.finalwork.gorsvet.service;

import org.mayurmu.finalwork.gorsvet.model.Client;
import org.mayurmu.finalwork.gorsvet.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientServiceImpl implements ClientService
{
    private final ClientRepository clientRepository;
    
    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository)
    {
        this.clientRepository = clientRepository;
    }
    
    
    @Override
    @Transactional
    public void addClient(Client client)
    {
        clientRepository.saveAndFlush(client);
    }
    
    @Override
    @Transactional
    public void updateClient(Client client)
    {
        clientRepository.saveAndFlush(client);
    }
}
