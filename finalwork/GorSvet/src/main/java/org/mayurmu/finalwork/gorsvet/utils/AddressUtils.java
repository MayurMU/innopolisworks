package org.mayurmu.finalwork.gorsvet.utils;

import org.apache.logging.log4j.util.Strings;

public class AddressUtils
{
    private static final String CITY_PREFIXES = "(г|гор|город|п|пос|поселок|посёлок|х|хут|хутор|с|село|ст|станица|пгт|сл|слобода)(\\.|\\s)";
    // "Юфимцева; пер. Учебный; СТ Лесная поляна; СТ Солнечное; рынок Орёл; пер. Дальний; Самарское шоссе; ул. 1-я, 2-я левая"
    private static final String STREET_PREFIXES = "(проспект|просп|пр|переулок|пер|площадь|пл|улица|ул|линия|л|бульвар|бул|ст|шоссе|)(\\.|\\s)";
    
    public static String cleanStreet(String rawStreet)
    {
        return Strings.isBlank(rawStreet) ? "" : rawStreet.trim().toLowerCase().replaceAll(STREET_PREFIXES, "").trim();
    }
    
    public static String cleanCity(String rawCity)
    {
        return Strings.isBlank(rawCity) ? "" : rawCity.trim().toLowerCase().replaceAll(CITY_PREFIXES, "").trim();
    }
}
