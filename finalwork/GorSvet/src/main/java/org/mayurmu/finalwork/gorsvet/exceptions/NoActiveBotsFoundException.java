package org.mayurmu.finalwork.gorsvet.exceptions;

public class NoActiveBotsFoundException extends RuntimeException
{
    public NoActiveBotsFoundException(String mes, Throwable cause)
    {
        super(mes, cause);
    }
    
    public NoActiveBotsFoundException(String mes)
    {
        super(mes);
    }
}
