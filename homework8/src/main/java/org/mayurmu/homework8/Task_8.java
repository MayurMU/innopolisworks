package org.mayurmu.homework8;

import com.github.javafaker.Faker;
import org.mayurmu.common.Utils;

import java.util.Locale;
import java.util.Scanner;

/**
 * Класс реализующий ДЗ №8 - "Наследование и полиморфизм"
 * */
public class Task_8
{
    
    private static final Scanner _scn = new Scanner(System.in);
    
    static final Faker _rujfaker = new Faker(new Locale("ru"));
    
    enum Profession
    {
        NONE("н/д"),
        PROGRAMMER("Программист"),
        TESTER("Тестировщик"),
        PROJECT_MANAGER("Менеджер проекта"),
        SYSTEM_ADMINISTRATOR("Системный администратор");
    
        /**
         *   Название профессии
         */
        final String Name;
    
        Profession(String profName)
        {
            Name = profName;
        }
    }
    
    /**
     * Программа генерирует случайный массив профессионалов, заставляет их работать, а затем идти в отпуск
     * */
    public static void main(String[] args)
    {
        try
        {
            do
            {
                System.out.println("Генерация случайных работников...");
                Worker[] workers = new Worker[_rujfaker.random().nextInt(10, 15)];
                fillRandomWorkersArray(_rujfaker, workers);
                System.out.println("Случайный массив людей:");
                System.out.println();
                Utils.printArray(workers);
                System.out.println();
                System.out.println("Отправляем всех людей на работу...");
                System.out.println();
                for (Worker worker : workers)
                {
                    worker.goToWork();
                }
                System.out.println();
                System.out.println("Отправляем всех людей в отпуск...");
                System.out.println();
                for (Worker worker : workers)
                {
                    worker.goToVacation(_rujfaker.random().nextInt(5, 35));
                }
                System.out.println();
                System.out.println("Введите любую строку, чтобы повторить или ПРОБЕЛ, чтобы выйти:");
            }
            while (!_scn.nextLine().equalsIgnoreCase(" "));
        }
        catch (Exception ex)
        {
            System.err.append("Непредвиденная ошибка работы программы:").append(System.lineSeparator()).println(ex);
            ex.printStackTrace();
        }
        finally
        {
            _scn.close();
            System.out.println("Программа завершена");
        }
    }
    
    /**
     * Метод заполняет массив работников случайными данными и случайными наследниками класса {@link Worker} на основе
     *  известных профессий из {@link Profession}
     * @param jfaker - генератор русских имён
     * @param workers - список сотрудников, который нужно заполнить
     */
    private static void fillRandomWorkersArray(Faker jfaker, Worker[] workers)
    {
        final Profession[] professions = Profession.values();
        final int profLen = professions.length;
    
        for (int i = 0; i < workers.length; i++)
        {
            String fisrtName = jfaker.name().firstName();
            String lastName = Utils.getCorrectGenderLastName(fisrtName, jfaker.name().lastName());
            Profession randomProfession = professions[jfaker.random().nextInt(profLen)];
            int randomAge = jfaker.random().nextInt(Worker.MIN_WORKER_AGE, 100);
            
            switch (randomProfession)
            {
                case NONE:
                {
                    workers[i] = new Worker(fisrtName, lastName, randomAge, randomProfession.Name);
                    break;
                }
                case PROGRAMMER:
                {
                    workers[i] = new Programmer(fisrtName, lastName, randomAge);
                    break;
                }
                case TESTER:
                {
                    workers[i] = new Tester(fisrtName, lastName, randomAge);
                    break;
                }
                case PROJECT_MANAGER:
                {
                    workers[i] = new ProjectManager(fisrtName, lastName, randomAge);
                    break;
                }
                case SYSTEM_ADMINISTRATOR:
                {
                    workers[i] = new SystemAdministrator(fisrtName, lastName, randomAge);
                    break;
                }
            }
        }
    }
    
    
    
}

