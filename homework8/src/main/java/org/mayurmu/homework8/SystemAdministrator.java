package org.mayurmu.homework8;

public class SystemAdministrator extends Worker
{
    
    public SystemAdministrator(String fisrtName, String lastName, int age)
    {
        super(fisrtName, lastName, age, Task_8.Profession.SYSTEM_ADMINISTRATOR.Name);
    }
    
    @Override
    public void goToWork()
    {
        super.goToWork();
        System.out.println("\t - настраивает инфраструктуру");
    }
    
    @Override
    public void goToVacation(int days)
    {
        super.goToVacation(days);
        System.out.println("\t (едет за границу)");
    }
    
    
}
