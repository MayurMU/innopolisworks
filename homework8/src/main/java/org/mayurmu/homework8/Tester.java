package org.mayurmu.homework8;

public class Tester extends Worker
{
    public Tester(String fisrtName, String lastName, int age)
    {
        super(fisrtName, lastName, age, Task_8.Profession.TESTER.Name);
    }
    
    @Override
    public void goToWork()
    {
        super.goToWork();
        System.out.println("\t - тестирует программу");
    }
    
    @Override
    public void goToVacation(int days)
    {
        super.goToVacation(days);
        System.out.println("\t (едет на море)");
    }
    
    
}
