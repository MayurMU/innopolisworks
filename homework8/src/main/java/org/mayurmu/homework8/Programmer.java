package org.mayurmu.homework8;

public class Programmer extends Worker
{
    
    Programmer(String name, String lastName, int age)
    {
        super(name, lastName, age, Task_8.Profession.PROGRAMMER.Name);
    }
    
    
    @Override
    public void goToWork()
    {
        super.goToWork();
        System.out.println("\t - пишет код");
    }
    
    @Override
    public void goToVacation(int days)
    {
        super.goToVacation(days);
        System.out.println("\t (едет в горы)");
    }
    
    
}
