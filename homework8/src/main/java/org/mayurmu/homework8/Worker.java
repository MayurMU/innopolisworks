package org.mayurmu.homework8;

import org.mayurmu.homework7.Human;

import java.util.StringJoiner;

/**
 * Класс - работник
 * */
public class Worker extends Human
{
    String profession;  // профессия
    
    /**
     * Мин. возраст работника по т.к. РФ
     */
    public static final int MIN_WORKER_AGE = 15;
    
    /**
     * Создаёт работника с минимально допустимым возрастом {@link #MIN_WORKER_AGE}
     */
    public Worker()
    {
        this("", "", MIN_WORKER_AGE, "");
    }
    
    /**
     * Конструктор с параметрами
     * @param name      Имя
     * @param lastName  Фамилия
     * @param prof      Профессия (одна из известных)
     * @param age       Возрраст
     * @exception IllegalArgumentException При параметре {@code age} < {@link #MIN_WORKER_AGE}
     */
    public Worker(String name, String lastName, int age, String prof)
    {
        if (age < MIN_WORKER_AGE)
        {
            throw new IllegalArgumentException("Возраст сотрудника должен быть больше " + MIN_WORKER_AGE);
        }
        super.setName(name);
        super.setLastName(lastName);
        super.setAge(age);
        profession = prof != null ? prof : "";
    }
    
    /**
     * Метод выполняющий поход на работу - выводит, кто работает, какая у него профессия и как он работает.
     * */
    public void goToWork()
    {
        System.out.printf("%s \"%s\" работает%n", profession, getFullName());
    }
    
    /**
     * Метод в котором описан отпуск. На вход принимает количество дней отпуска. Метод выводит, кто уходит в отпуск,
     *  какая у него профессия и на сколько дней уходит в отпуск.
     * @param days Кол-во дней отпуска
     * @exception IllegalArgumentException При кол-ве дней отпуска {@code days <= 0}
     */
    public void goToVacation(int days)
    {
        if (days <= 0)
        {
            throw new IllegalArgumentException("Кол-во дней отпуска должно быть положительным! Однако было передано: " + days);
        }
        System.out.printf("%s \"%s\" - Уходит в ОТПУСК на следующее количество дней %d!%n", profession, getFullName(), days);
    }
    
    
    @Override
    public String toString()
    {
        return new StringJoiner(", ",  "[", "]")
                .add(super.toString())
                .add("profession = " + profession)
                .toString();
    }
    
    
    public String getProfession()
    {
        return profession;
    }
    
    public void setProfession(String profession)
    {
        this.profession = profession != null ? profession : "";
    }
    
    /**
     * Метод установки возраста сотрудника
     * @implSpec  Налагает доп. ограничение на возраст человека и получается нарушает принцип подстановки Лисков:
     * "Предусловия не могут быть усилены в подклассе.", но полагаю это очень частая ситуация - стараются ли этого избегать, если нет явных контрактов?
     * @exception IllegalArgumentException При попытке установить возраст меньше {@link #MIN_WORKER_AGE}
     */
    @Override
    public void setAge(int age)
    {
        if (age > 0 && age < MIN_WORKER_AGE)
        {
            throw new IllegalArgumentException(String.format("Трудоустройство разрешено с %d лет, однако было передано число %d", MIN_WORKER_AGE, age));
        }
        else
        {
            super.setAge(age);
        }
    }

}
