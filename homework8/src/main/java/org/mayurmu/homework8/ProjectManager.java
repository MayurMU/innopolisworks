package org.mayurmu.homework8;

public class ProjectManager extends Worker
{
    public ProjectManager(String fisrtName, String lastName, int age)
    {
        super(fisrtName, lastName, age, Task_8.Profession.PROJECT_MANAGER.Name);
    }
    
    @Override
    public void goToWork()
    {
        super.goToWork();
        System.out.println("\t - распределяет работу");
    }
    
    @Override
    public void goToVacation(int days)
    {
        super.goToVacation(days);
        System.out.println("\t (отдыхает на даче)");
    }
    
    
}
