package org.mayurmu.Task_4;

import java.util.*;

/**
 * Бинарное сложение
 * */
public class Task_4
{
    private static final Random _rnd = new Random();
    private static final Scanner _scn = new Scanner(System.in);
    
    public static void main(String[] args)
    {
        boolean wantRepeat = false;
        try
        {
            // генерируем два случайных целых числа
            do
            {
                System.out.println("Сейчас будут сгенерированы два случайных числа - введите любой символ, чтобы продолжить - ПРОБЕЛ, для выхода:");
                String str = _scn.nextLine();
                wantRepeat = !str.equalsIgnoreCase(" ");
                if (!wantRepeat)
                {
                    break;
                }
                int firstNum = _rnd.nextInt();
                boolean genNegative = _rnd.nextBoolean();
                if (genNegative)
                {
                    firstNum = -firstNum;
                }
                int secondNum = _rnd.nextInt();
                genNegative = _rnd.nextBoolean();
                if (genNegative)
                {
                    secondNum = -secondNum;
                }
                System.out.printf("Выполняю двоичное сложение чисел %d и %d...%n", firstNum, secondNum);
                
                // проводим сложение этих чисел в двоичном виде
                // если оба числа отрицательны, пользуемся правилом "выноса минуса за скобки": -A-B = -(A+B)
                int res = 0;
                if (firstNum < 0 && secondNum < 0)
                {
                    res = -binaryAdd(-firstNum, -secondNum);
                }
                else
                {
                    res = binaryAdd(firstNum, secondNum);
                }
                int naturalRes = firstNum + secondNum;
                
                // переводим результат в 10-ю систему счисления (СС)
                System.out.printf("Результат: %d%n", res);
                System.out.printf("Проверка: %d%n", naturalRes);
                System.out.printf("Вывод: %s%n", res == naturalRes ? "ПРОЙДЕНА" : "НЕ ПРОЙДЕНА");
            }
            while (true);
        }
        catch (Exception ex)
        {
            System.err.println("Необработанная ошибка: " + System.lineSeparator() + ex);
            ex.printStackTrace();
        }
        finally
        {
            _scn.close();
            System.out.println("Программа завершена");
        }
    }
    
    /**
     * Метод двоичного сложения чисел - переводит числа во внутреннее двоичное представление, складывает их и выводит результат в 10-й СС
     * */
    private static int binaryAdd(int first, int second)
    {
        if (first == 0 && second == 0)
        {
            return 0;
        }
        else if (first == 0)
        {
            return second;
        }
        else if (second == 0)
        {
            return first;
        }
        boolean isFirstNegative = first < 0;
        boolean isSecondNegative = second < 0;
        boolean[] ba1 = convertToUnsignedBoolArray(Math.abs(first));
        boolean[] ba2 = convertToUnsignedBoolArray(Math.abs(second));
        // получаем дополнительный код
        if (isFirstNegative)
        {
            transformToFromAuxCode(ba1);

        }
        if (isSecondNegative)
        {
            transformToFromAuxCode(ba2);
        }
        // мы не можем залезать знаковый разряд
        boolean[] sum = new boolean[Integer.SIZE];
        boolean haveShift = false, result = false;
        for (int i = 0, j = 0; i < ba1.length; i++)
        {
            if (i < ba2.length)
            {
                result = ba1[i] ^ ba2[i] ^ haveShift;
                // перенос в старший разряд будет только в том случае, если до этого Переноса не было и обе 1 или Перенос был и хотя бы один не равен 0
                haveShift = haveShift ? (ba1[i] || ba2[i]) : (ba1[i] && ba2[i]);
            }
            else    // если меньший массив кончился дальше используем только Перенос и большой массив
            {
                result = ba1[i] ^ haveShift;
                haveShift = ba1[i] && haveShift;
            }
            sum[i] = result;
        }
        if (sum[sum.length - 1])
        {
            transformToFromAuxCode(sum);
            return -convertFromUnsignedBoolArray(sum);
        }
        else
        {
            return convertFromUnsignedBoolArray(sum);
        }
    }
    
    /**
     * Метод преобразования двоичного числа из массива бит в десятичное число
     * @param bitArray Массив бит числа в "неестественном" порядке - старший бит справа
     * @return Десятичное число
     */
    private static int convertFromUnsignedBoolArray(boolean[] bitArray)
    {
        int result = 0;
        for (int i = 0; i < bitArray.length; i++)
        {
            result += bitArray[i] ? Math.pow(2, i) : 0;
        }
        return result;
    }
    
    /**
    * Метод трансформации прямого кода двоичного числа в дополнительный код и наоборот
    *
     * @param code - Массив с прямым код числа, где номера битов должны быть расположены "неестественно" - старший бит справа
    *
    * */
    private static void transformToFromAuxCode(boolean[] code)
    {
        for (int i = 0; i < code.length; i++)
        {
            code[i] = !code[i];
        }
        boolean haveShift = true;
        for (int i = 0; i < code.length; i++)
        {
            boolean bit = code[i];
            code[i] = haveShift ^ code[i];
            haveShift = bit && haveShift;
        }
    }
    
    /**
     * Метод преобразования целого неотрицательного числа в набор бит
     * @param number Неотрицательное число
     * @return Набор бит (прямой код!), в котором биты расположены "неестественно" старший бит справа
     */
    private static boolean[] convertToUnsignedBoolArray(int number)
    {
        if (number <= 0)
        {
            throw new IllegalArgumentException("Аргумент должен быть положительным!");
        }
        boolean[] result = new boolean[Integer.SIZE];
        int i = 0;
        while (number > 0)
        {
            int remainder = number % 2;
            result[i++] = remainder > 0;
            number /= 2;
        }
        return result;
    }
}
