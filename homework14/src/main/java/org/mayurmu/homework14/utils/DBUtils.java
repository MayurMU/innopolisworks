package org.mayurmu.homework14.utils;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.model.naming.ImplicitNamingStrategy;
import org.hibernate.cfg.Configuration;
import org.mayurmu.homework14.app.Main;
import org.mayurmu.homework14.model.utils.StackOverflowImplicitNamingStrategy;

import java.io.IOException;
import java.util.Properties;
import java.util.TimeZone;


public class DBUtils
{
    private static final Configuration _cfg = new Configuration();
    private static final ImplicitNamingStrategy _noRandomTextNamingStrategy = new StackOverflowImplicitNamingStrategy();
    
    public static SessionFactory hiberFactory;
    private static TimeZone _dbTimeZone;
    
    
    /**
     * Конфигурирует фабрику сессий
     *
     * @throws RuntimeException При проблемах с файлами настроек
     */
    public static void loadConfig()
    {
        try
        {
            Properties props = new Properties();
            props.load(Main.class.getClassLoader().getResourceAsStream("app.properties"));
            // полагаю в этом может быть смысл, если использовать тип LocalDate, когда на сервере время отличается от МСК
            _dbTimeZone = TimeZone.getTimeZone(props.getProperty("jdbc.timezone"));
            // К сожалению стратегия выдачи имён работает только для Unique Keys объявленных на уровне таблицы, но не на
            // уровне колонки - это известный баг Hibernate 5.x (см.: https://stackoverflow.com/a/58425868/2323972)
            _cfg.setImplicitNamingStrategy(_noRandomTextNamingStrategy);
            _cfg.configure("hibernate.cfg.xml");
        }
        catch (IOException ex)
        {
            throw new RuntimeException("Ошибка загрузки файлов конфигурации", ex);
        }
    }
    
    /**
     * Создаёт новую Сессию Hibernate через встроенную фабрику и Часовой пояс полученный из файла настроек
     */
    public static Session getHibernateSession()
    {
        if (hiberFactory == null || hiberFactory.isClosed())
        {
            hiberFactory = _cfg.buildSessionFactory();
        }
        return getHibernateSession(hiberFactory, _dbTimeZone);
    }
    
    /**
     * Создаёт новую Сессию Hibernate через указанную фабрику
     *
     * @param timeZoneForDB Желаемый часовой пояс для БД (полагаю это имеет смысл, если Время сервера отличается от времени клиентов)
     *
     * @return Новый экземпляр сессии
     *
     * @apiNote Рекомендуется получать новый экземпляр после каждой неудачной ТА, т.к. сессия в этом случае оказывается в
     * неопределённом состоянии.
     */
    public static Session getHibernateSession(SessionFactory hibernateFactory, TimeZone timeZoneForDB)
    {
        return hibernateFactory.withOptions().jdbcTimeZone(timeZoneForDB).openSession();
    }
    
}