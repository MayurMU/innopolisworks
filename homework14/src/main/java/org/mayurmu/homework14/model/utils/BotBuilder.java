package org.mayurmu.homework14.model.utils;

import org.mayurmu.homework14.model.Bot;

import java.time.LocalDateTime;

public final class BotBuilder
{
    private String secretKey = "";
    private String userName = "";
    private String comment = "";
    private boolean isAlive = true;
    private LocalDateTime createDateTime = LocalDateTime.now();
    private LocalDateTime keyChangeDateTime = LocalDateTime.now();
    
    private BotBuilder()
    {
    }
    
    public static BotBuilder createBuilder()
    {
        return new BotBuilder();
    }
    
    /**
     * Обязательное поле
     */
    public BotBuilder secretKey(String secretKey)
    {
        this.secretKey = secretKey;
        return this;
    }
    
    public BotBuilder userName(String userName)
    {
        this.userName = userName;
        return this;
    }
    
    public BotBuilder comment(String comment)
    {
        this.comment = comment;
        return this;
    }
    
    public BotBuilder isAlive(boolean isAlive)
    {
        this.isAlive = isAlive;
        return this;
    }
    
    public BotBuilder keyChangeDateTime(LocalDateTime keyChangeDateTime)
    {
        this.keyChangeDateTime = keyChangeDateTime;
        return this;
    }
    
    public BotBuilder createDateTime(LocalDateTime createDateTime)
    {
        this.createDateTime = createDateTime;
        return this;
    }
    
    /**
     * @exception IllegalStateException Когда у бота нет {@link #secretKey}
     */
    public Bot build()
    {
        if (secretKey == null || secretKey.trim().isEmpty())
        {
            throw new IllegalStateException("У бота обязательно должен быть секретный ключ - иначе бот бесполезен, " +
                    "однако был передан: " + secretKey);
        }
        return new Bot(secretKey, userName, comment, isAlive, keyChangeDateTime, createDateTime);
    }
}
