package org.mayurmu.homework14.model;

import org.mayurmu.homework14.model.types.UUIDs;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.UUID;

@Entity
@Table(name = "bot")
public class Bot
{
    
    //region 'Поля'
    
    @Transient
    public static final Bot EMPTY = new Bot();
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id = UUIDs.EMPTY;
    
    /**
     * Ключ API бота (например, Телеграм)
     */
    @Column(name = "secret_key", nullable = false, unique = true)
    private String secretKey;
    
    /**
     * Имя пользователя бота во внешней системе (например, Телеграм) - скорее всего не нужно - больше для информации.
     *
     * @implNote Может повторяться, если бота придётся пересоздать (старая запись хранится для исторических целей)
     */
    @Column(name = "user_name", nullable = false, length = 30)
    private String userName;
    
    /**
     * Произвольная заметка о назначении бота
     */
    @Column(name = "comment")
    private String comment;
    
    /**
     * Активна ли запись бота (архивный статус)
     */
    @Column(name = "is_alive", nullable = false)
    private boolean isAlive;
    
    /**
     * Последняя дата смены ключа API (по умолчанию будет совпадать с {@link #createDateTime}
     */
    @Column(name = "key_change_date_time", nullable = false)
    private LocalDateTime keyChangeDateTime;
    
    /**
     * Дата создания записи о Боте
     */
    @Column(nullable = false)
    private LocalDateTime createDateTime;
    
    /**
     * Список связанных клиентов, которые последний раз заходили через этот бот
     */
    @OneToMany(mappedBy = "bot", cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private List<Client> clients = new ArrayList<>();
    
    //endregion 'Поля'
    
    
    
    
    //region 'Конструкторы'
    
    protected Bot()
    {
        createDateTime = keyChangeDateTime = LocalDateTime.now();
        secretKey = userName = "";
    }
    
    public Bot(String secretKey, String userName, String comment, boolean isAlive, LocalDateTime keyChangeDateTime,
               LocalDateTime createDateTime)
    {
        this.secretKey = secretKey;
        this.userName = userName;
        this.comment = comment;
        this.isAlive = isAlive;
        this.keyChangeDateTime = keyChangeDateTime;
        this.createDateTime = createDateTime;
    }
    
    //endregion 'Конструкторы'
    
    
    
    
    //region 'Свойства'
    
    public boolean getIsAlive()
    {
        return isAlive;
    }
    
    public void setIsAlive(boolean isAlive)
    {
        this.isAlive = isAlive;
    }
    
    public String getComment()
    {
        return comment;
    }
    
    public void setComment(String comment)
    {
        this.comment = comment;
    }
    
    public List<Client> getClients()
    {
        return clients;
    }
    
    public void setClients(List<Client> clients)
    {
        this.clients = clients;
    }
    
    public LocalDateTime getKeyChangeDateTime()
    {
        return keyChangeDateTime;
    }
    
    public void setKeyChangeDateTime(LocalDateTime keyChangeDate)
    {
        this.keyChangeDateTime = keyChangeDate;
    }
    
    public String getUserName()
    {
        return userName;
    }
    
    public String getSecretKey()
    {
        return secretKey;
    }
    
    public void setSecretKey(String secretKey)
    {
        this.secretKey = secretKey;
    }
    
    public UUID getId()
    {
        return id;
    }
    
    public void setId(UUID id)
    {
        this.id = id;
    }
    
    public LocalDateTime getCreateDateTime()
    {
        return createDateTime;
    }
    
    public void setCreateDateTime(LocalDateTime createDateTime)
    {
        this.createDateTime = createDateTime;
    }
    
    //endregion 'Свойства'
    
    
    
    
    //region 'Методы'
    
    @Override
    public String toString()
    {
        return new StringJoiner("\n", Bot.class.getSimpleName() + ": [", "]")
                .add("id=" + id)
                .add("secretKey='" + secretKey + "'")
                .add("userName='" + userName + "'")
                .add("comment='" + comment + "'")
                .add("isAlive=" + isAlive)
                .add("keyChangeDateTime=" + keyChangeDateTime)
                .add("createDateTime=" + createDateTime)
                .toString();
    }
    
    
    //endregion 'Методы'
    
}