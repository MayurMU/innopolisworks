package org.mayurmu.homework14.model;

import org.mayurmu.homework14.model.types.UUIDs;
import org.mayurmu.homework14.model.types.UserRole;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.UUID;

/**
 * Роль пользователя системы, на основе {@link UserRole}
 */
@Entity
@Table(name = "role")
public class Role
{
    
    //region 'Поля'
    
    @Transient
    public static final Role ADMIN = new Role("Администратор системы (управление Ботами и Клиентами, настройка системы)",
            UserRole.ADMIN);
    @Transient
    public static final Role GUEST = new Role("Гость (только поисковые запросы)", UserRole.GUEST);
    @Transient
    public static final Role USER = new Role("Пользователь (Поиск и Создание задач-напоминаний)", UserRole.USER);
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    private UUID id = UUIDs.EMPTY;
    
    /**
     * Краткое описание роли
     */
    @Column(name = "description")
    private String description;
    
    /**
     * Константное наименование роли
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, unique = true)
    private UserRole type;
    
    /**
     * Список Юзеров с этой ролью
     *
     * @implNote Не уверен, нужна ли эта обратная связь, разве что для Админа - показать список юзеров
     */
    @OneToMany(mappedBy = "role", cascade = CascadeType.REFRESH, orphanRemoval = false, fetch = FetchType.LAZY)
    private List<Client> clients = new ArrayList<>();
    
    //endregion 'Поля'
    
    
    
    
    //region 'Конструкторы'
    
    /**
     * Пустой конструктор по умолчанию
     */
    protected Role()
    {}
    
    /**
     * Конструктор с параметрами
     */
    public Role(String description, UserRole type)
    {
        this.description = description;
        this.type = type;
    }
    
    //endregion 'Конструкторы'
    
    
    
    
    //region 'Свойства'
    
    public UUID getId()
    {
        return id;
    }
    
    public void setId(UUID id)
    {
        this.id = id;
    }
    
    public List<Client> getClients()
    {
        return clients;
    }
    
    public void setClients(List<Client> clients)
    {
        this.clients = clients;
    }
    
    public UserRole getType()
    {
        return type;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    //endregion 'Свойства'
    
    
    
    
    //region 'Методы'
    
    
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        
        Role role = (Role) o;
    
        return type == role.type;
    }
    
    @Override
    public int hashCode()
    {
        return type.hashCode();
    }
    
    @Override
    public String toString()
    {
        return new StringJoiner(", ", this.getClass().getSimpleName() + ": [", "]")
                .add("ID=" + id)
                .add("type=" + type)
                .add("description='" + description + "'")
                .toString();
    }
    
    //endregion 'Методы'
    
}