package org.mayurmu.homework14.app;

import org.hibernate.Session;
import org.mayurmu.homework14.model.Client;
import org.mayurmu.homework14.model.Task;
import org.mayurmu.homework14.utils.DBUtils;
import org.mayurmu.homework14.utils.TestDataGenerator;

import javax.persistence.EntityTransaction;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс реализующий 14-ю Задачу курса Иннополис Java (Hibernate/JPA)
 */
public class Main
{
    private static Set<Client> _fakeClients;
    private static Set<Task> _fakeTasks;
    
    /**
     * Программа выводит на консоль всех Клиентов, подписанных на конкретную Задачу, и все Задачи, конкретного Клиента
     */
    public static void main(String[] args)
    {
        // Сколько генерировать клиентов
        final int TEST_DATA_COUNT = 100;
        // Сколько выводить на экран "случайных" сущностей
        final int PRINT_COUNT = 5;
        int exitCode = 0;
        try
        {
            DBUtils.loadConfig();
            
            System.out.println("Генерация тестовых данных...");
            final TestDataGenerator dataGenerator = new TestDataGenerator(TEST_DATA_COUNT);
            dataGenerator.regenerateClientsData();
            _fakeClients = dataGenerator.clientCollection;
            // создаём список случайных задач, которые случайным образом связываются с нашими клиентами
            dataGenerator.regenerateTasksData(_fakeClients.size(), TestDataGenerator.TaskGenPolicy.ONE_OR_MORE);
            _fakeTasks = dataGenerator.taskCollection;
            // настраиваем обратные связи клиентов с созданными задачами
            _fakeTasks.forEach(t -> t.getClients().forEach(cl -> cl.getTasks().add(t)));
            System.out.printf("Готово, сгенерировано %d Клиентов и %d Задач.%n", _fakeClients.size(), _fakeTasks.size());
            System.out.println("Запись данных в БД...");
            // как я понял при любых исключениях сессию использовать более нельзя и она должна быть закрыта
            try (Session session = DBUtils.getHibernateSession())
            {
                writeDataToDB(session, _fakeTasks);
                writeDataToDB(session, _fakeClients);
            }
            System.out.println("Данные записаны успешно, начинаю их проверку...");
            // пересоздаём сессию, чтобы убедиться, что данные мы получаем именно из БД
            try (Session session = DBUtils.getHibernateSession())
            {
                //TODO: возможно здесь достаточно вызвать session.refresh() для каждого элемента записанных коллекций?
                verifyDataFromDB(session);
                System.out.println("Проверка данных завершена.");
                System.out.println();
                System.out.printf("Выводим на экран только те Клиенты и Задачи (но не более %d), что имеют по нескольку связей:%n",
                        PRINT_COUNT);
                // для наглядности, сначала найдём среди тестовых данных такие, которые явно реализуют отношение М:М и
                // будем выводить на экран только их
                List<Client> clientsWithSeveralTasks = _fakeClients.stream().filter(client -> client.getTasks().size() > 1).
                        limit(PRINT_COUNT).collect(Collectors.toList());
                System.out.println();
                if (clientsWithSeveralTasks.isEmpty())
                {
                    System.out.printf("К сожалению, таких клиентов не нашлось - будет выведено %d случайных клиентов: %n",
                            PRINT_COUNT);
                    for (int i = 0; i < Math.min(PRINT_COUNT, _fakeClients.size()); i++)
                    {
                        clientsWithSeveralTasks.add(_fakeClients.iterator().next());
                    }
                }
                // Ищем в БД клиентов с указанными характеристиками по ИД
                List<Client> foundClients = session.byMultipleIds(Client.class).multiLoad(clientsWithSeveralTasks.stream().
                        map(Client::getId).collect(Collectors.toList()));
                foundClients.forEach(cl -> {
                    System.out.append(cl.toString("f")).println("\n\nСвязанные Задачи:\n");
                    cl.getTasks().forEach(System.out::println);
                    System.out.println("**************************************************************************");
                });
                List<Task> tasksWithSeveralClients = _fakeTasks.stream().filter(client -> client.getClients().size() > 1).
                        limit(PRINT_COUNT).collect(Collectors.toList());
                if (tasksWithSeveralClients.isEmpty())
                {
                    System.out.printf("К сожалению, таких задач не нашлось - будет выведено %d случайных задач: %n", PRINT_COUNT);
                    for (int i = 0; i < Math.min(PRINT_COUNT, _fakeTasks.size()); i++)
                    {
                        tasksWithSeveralClients.add(_fakeTasks.iterator().next());
                    }
                }
                // Ищем в БД задачи с указанными характеристиками по ИД
                List<Task> foundTasks = session.byMultipleIds(Task.class).multiLoad(tasksWithSeveralClients.stream().
                        map(Task::getId).collect(Collectors.toList()));
                foundTasks.forEach(tsk -> {
                    System.out.append(tsk.toString("f")).println("\n\t Связанные Клиенты:\n");
                    tsk.getClients().forEach(x -> System.out.append(x.toString()).println(System.lineSeparator()));
                    System.out.println("======================================================================");
                });
            }
        }
        catch (Exception ex)
        {
            exitCode = 1;
            System.err.printf("Неожиданная ошибка работы программы: %n%s", ex);
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                DBUtils.hiberFactory.close();
            }
            catch (Exception exception)
            {
                System.err.println(exception.toString());
                exception.printStackTrace();
            }
            System.out.println("Программа завершена");
            System.exit(exitCode);
        }
    }
    
    
    /**
     * Метод проверки данных считанных из базы на совпадение с тем, что мы записывали в неё
     *
     * @exception IllegalStateException Данные в базе не совпадают с тем, что было записано ранее
     */
    private static void verifyDataFromDB(Session session)
    {
        // Запрос на JPQL (как я понял, Criteria является более гибким вариантом, а JPQL имеет смысл, только когда нужно
        // выполнить Native SQL-запрос (см.: https://www.baeldung.com/jpql-hql-criteria-query#advantages-of-criteria-queries-over-hql-and-jpql-queries)
        // Однако JPQL очень близок к SQL, а Criteria ещё нудно изучать).
        List<Client> loadedClients = session.createQuery("SELECT cl FROM Client cl", Client.class).getResultList();
        if (loadedClients.size() != _fakeClients.size() || !new HashSet<>(loadedClients).containsAll(_fakeClients))
        {
            throw new IllegalStateException("Списки клиентов не совпадают!");
        }
        List<Task> loadedTasks = session.createQuery("SELECT t FROM Task t", Task.class).getResultList();
        if (loadedTasks.size() != _fakeTasks.size() || !new HashSet<>(loadedTasks).containsAll(_fakeTasks))
        {
            throw new IllegalStateException("Списки задач не совпадают!");
        }
        // проверяем связи Клиентов с Задачами
        for (Client cl : loadedClients)
        {
            Client fakeClient = _fakeClients.stream().filter(x -> x.equals(cl)).findAny().orElseThrow(IllegalStateException::new);
                    //_fakeClients.get(_fakeClients.indexOf(cl));
            Set<Task> clientTasks = cl.getTasks();
            Set<Task> fakeClientTasks = fakeClient.getTasks();
            if (!clientTasks.equals(fakeClientTasks))
            {
                throw new IllegalStateException("Списки связанных задач не совпадают! Обнаружено на клиенте: " + cl);
            }
        }
        // проверяем обратные связи Задач с Клиентами
        for (Task tsk : loadedTasks)
        {
            Task fakeTask = _fakeTasks.stream().filter(x -> x.equals(tsk)).findAny().orElseThrow(IllegalStateException::new);
            Set<Client> taskClients = tsk.getClients();
            Set<Client> fakeTaskClients = fakeTask.getClients();
            if (!taskClients.equals(fakeTaskClients))
            {
                throw new IllegalStateException("Списки связанных клиентов не совпадают для Задачи: " + tsk);
            }
        }
    }
    
    /**
     * Метод записи данных в БД
     *
     * @apiNote Побочный эффект - в случае выброса исключения объект сессии будет закрыт
     *
     * @exception IllegalArgumentException Когда {@code sourceCollections.length == 0}
     * @exception RuntimeException При непредвиденных ошибках работы
     */
    private static void writeDataToDB(Session session, Collection<?> sourceCollection)
    {
        if (sourceCollection == null || sourceCollection.size() == 0)
        {
            throw new IllegalArgumentException("Нечего сохранять в БД - передан пустой список данных");
        }
        EntityTransaction ta = session.getTransaction();
        try
        {
            ta.begin();
            sourceCollection.forEach(session::save);
            // Это по идее не обязательно, т.к. Hibernate и так периодически производит сброс данных в БД (если только
            //      FlushMode не установлен в MANUAL).
            // Вроде как тут уже должна производиться запись в БД, но т.к. автокоммита ТА в JPA по умолчанию нет, то
            // данных в БД не будет пока не зафиксирована ТА (commit) или точнее данные будут отброшены самой СУБД, если ТА не зафиксировать.
            session.flush();
            ta.commit();
        }
        catch (Exception ex)
        {
            try
            {
                ta.rollback();
                session.close();
            }
            catch (Exception e)
            {
                // обработка не требуется
            }
            throw new RuntimeException("Ошибка записи данных в БД: \n", ex);
        }
    }
}