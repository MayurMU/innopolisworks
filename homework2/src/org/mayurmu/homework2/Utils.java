package org.mayurmu.homework2;

import java.util.InputMismatchException;
import java.util.Scanner;


public class Utils
{
    /**
     * Метод гарантированного ввода целого числа - не даёт пользователю ввести ничего другого
     * */
    public static int inputNumber(Scanner scn)
    {
        boolean incorrectInput = false;
        int a = -1;
        do
        {
            try
            {
                System.out.println("Введите целое число, (-1) для выхода:");
                a = scn.nextInt();
                incorrectInput = false;
            }
            catch (InputMismatchException ime)
            {
                System.err.format("Вы должны вводить числа!%n").println(ime.toString());
                scn.skip(".*\n");  // пропускаем некорректный ввод, иначе он снова будет прочитан при следующем вызове
                incorrectInput = true;
            }
        }
        while (incorrectInput);
        return a;
    }
}
