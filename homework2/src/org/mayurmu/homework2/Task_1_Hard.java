package org.mayurmu.homework2;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Задача 1 - повышенная сложность
 * */
public class Task_1_Hard
{
    static final int MAX_LENGTH = 10;
    static final int[] OrderedNumbers = new int[MAX_LENGTH];
    
    static final Scanner _scn = new Scanner(System.in);
    
    public static void main(String[] args)
    {
        int A = 0, i = 0, j = 0, k = 0;
        boolean mustInsert = false;
        
        Arrays.fill(OrderedNumbers, 0);
        A = Utils.inputNumber(_scn);
        
        while (A != -1 && i < MAX_LENGTH)
        {
            j = i;
            mustInsert = false;
            // Ищем позицию, на которую нужно вставить "Ai-oe" - для этого по очереди сравниваем его с каждым элементом, начиная с конца,
            // пока не найдём такой, который меньше либо равен "Ai-oe" - вставлять нужно на его место.
            // Если же все эл-ты больше "Ai-oe", то вставлять "Ai-oe" нужно после всех элементов - в конец массива.
            // При этом вставка элемента в середину предполагает, что все эл-ты справа от него должны сдвинуться на один шаг вправо
            // (последний эл-т может быть отброшен, если он выйдет за длину массива)
            while (j > 0 && OrderedNumbers[j - 1] > A)
            {
                mustInsert = true;
                j--;
            }
            if (mustInsert)
            {
                k = i;
                while (k > j && k > 0)                           // пока мы не дошли до искомого индекса - сдвигаем эл-ты вправо
                {
                    if (k < MAX_LENGTH)           // защита от выхода за пределы массива при сдвиге - отбрасываем последний эл-т, если вышел.
                    {
                        OrderedNumbers[k] = OrderedNumbers[k - 1];
                    }
                    k--;
                }
                OrderedNumbers[j] = A;
            }
            else
            {
                OrderedNumbers[i] = A;
            }
            A = Utils.inputNumber(_scn);
            i++;
        }
    
        // обрабатываем исключительную ситуацию, т.к. число (-1) у нас незначащее
        if (OrderedNumbers[0] == -1)
        {
            System.out.println("Коллекция пуста - негде искать минимум!");
        }
        else
        {
            // Если массив заполнен не полностью, то добавляем в него признак
            // конца последовательности (-1)
            if (i < MAX_LENGTH)
            {
                OrderedNumbers[i] = -1;
            }
            System.out.append("Минимум найден: ").println(OrderedNumbers[0]);
            //System.out.append("Упорядоченный массив: ").println(Arrays.toString(OrderedNumbers));
            System.out.println("Упорядоченный массив: ");
            j = 0;
            do
            {
                System.out.append(String.valueOf(OrderedNumbers[j])).append(" ");
                j++;
            }
            while (j < MAX_LENGTH && OrderedNumbers[j - 1] != -1);
            System.out.println();
        }
        System.out.println("Программа завершена");
        _scn.close();
    }
    
}
