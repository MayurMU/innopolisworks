package org.mayurmu.homework2;

import java.util.Scanner;

/**
 * Задача 2 - обычная сложность
 * */
public class Task_2
{
    static int A = 0, evenCount = 0, oddCount = 0;
   
    static final Scanner _scn = new Scanner(System.in);
    
    public static void main(String[] args)
    {
        A = Utils.inputNumber(_scn);
        
        while (A != -1)
        {
            if (A % 2 == 0)
            {
                evenCount++;
            }
            else
            {
                oddCount++;
            }
            A = Utils.inputNumber(_scn);
        }
        
        System.out.append("Кол-во чётных: ").println(evenCount);
        System.out.append("Кол-во НЕчётных:").println(oddCount);
        System.out.println("Программа завершена");
        _scn.close();
    }
    
    
}
