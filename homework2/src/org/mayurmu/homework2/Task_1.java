package org.mayurmu.homework2;

import java.util.Scanner;

/**
 * Задача 1 - обычная сложность
 * */
public class Task_1
{
    static final Scanner _scn = new Scanner(System.in);
    
    public static void main(String[] args)
    {
        int A = 0, min = 0;
        
        min = A = Utils.inputNumber(_scn);
        
        while (A != -1)
        {
            if (A < min)
            {
                min = A;
            }
            A = Utils.inputNumber(_scn);
        }
        
        if (min == -1)
        {
            System.out.println("Коллекция пуста - негде искать минимум!");
        }
        else
        {
            System.out.append("Минимум найден: ").println(min);
        }
        System.out.println("Программа завершена");
        _scn.close();
    }
    
}
