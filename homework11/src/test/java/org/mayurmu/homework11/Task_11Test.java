package org.mayurmu.homework11;

import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

@Order(2)
class Task_11Test
{
    
    //region 'Исключения'
    
    @Test
    @DisplayName("Метод упадёт, если среди элементов входного массива будет null, при числе элементов больше 1")
    void bubbleSort_ExceptionOnNullElementInNonEmptyArray()
    {
        HumanEx[] testData1 = new HumanEx[] { null };
        HumanEx[] testData2 = new HumanEx[] { new HumanEx(), null };
        assertAll(() -> Task_11.bubbleSort(testData1, Task_11.ascFIOLengthComparator), () -> Task_11.shakerSort(testData1,
                Task_11.ascFIOLengthComparator));
        assertArrayEquals(testData1, Task_11.bubbleSort(testData1, Task_11.descFIOLengthComparator));
        assertArrayEquals(testData1, Task_11.shakerSort(testData1, Task_11.descFIOLengthComparator));
        assertThrows(NullPointerException.class, () -> Task_11.bubbleSort(testData2, Task_11.ascFIOLengthComparator));
        assertThrows(NullPointerException.class, () -> Task_11.shakerSort(testData2, Task_11.descFIOLengthComparator));
    }
    
    //endregion 'Исключения'
    
    
    
    //region 'Негативные сценарии'
    
    @Test
    @DisplayName("Метод не должен падать при массиве 0-вой длины")
    void bubbleSort_ZeroLengthArray()
    {
        HumanEx[] testData = new HumanEx[0];
        assertAll(() -> Task_11.bubbleSort(testData, Task_11.ascFIOLengthComparator), () -> Task_11.shakerSort(testData,
                Task_11.descFIOLengthComparator));
    }
    
    @Test
    @DisplayName("Метод не должен падать на массиве из единственного элемента")
    void bubbleSort_SingleElementArray()
    {
        HumanEx[] testData = new HumanEx[1];
        assertArrayEquals(testData, Task_11.bubbleSort(testData, Task_11.descFIOLengthComparator));
        assertArrayEquals(testData, Task_11.shakerSort(testData, Task_11.ascFIOLengthComparator));
    }
    
    @Test
    @DisplayName("Метод не должен падать на массиве только из пустых объектов")
    void bubbleSort_StubElementsArray()
    {
        HumanEx[] testData = {new HumanEx(), new HumanEx(), new HumanEx()};
        assertArrayEquals(testData, Task_11.bubbleSort(testData, Task_11.descFIOLengthComparator));
        assertArrayEquals(testData, Task_11.shakerSort(testData, Task_11.ascFIOLengthComparator));
    }
    
    //endregion 'Негативные сценарии'
    
    
    
    //region 'Позитивные сценарии'
    
    @RepeatedTest(value = 20)   // число повторений должно быть достаточно большим, чтобы перебрать все возможные сочетания Компаратора и Сортировщика
    @DisplayName("Пузырёк - Многократное сравнение с Array.sort()")
    void sort_Normal()
    {
        Comparator<HumanEx> comp = TestDataGenerator.getRandomComparator();
        HumanSorter randomSorter = TestDataGenerator.getRandomSorter();
        TestDataGenerator.regenerateData();
        HumanEx[] expectedResult = Arrays.copyOf(TestDataGenerator.humans, TestDataGenerator.humans.length);
        HumanEx[] result = randomSorter.sort(TestDataGenerator.humans, comp);
        assert !Arrays.equals(TestDataGenerator.humans, result) : "Обнаружено изменение входного массива!";
        Arrays.sort(expectedResult, comp);
        assertArrayEquals(expectedResult, result);
    }
    
    //endregion 'Позитивные сценарии'
    
}