package org.mayurmu.homework11;

import org.junit.jupiter.api.*;
import org.mayurmu.common.Utils;

import java.util.Arrays;
import java.util.Objects;

import static org.mayurmu.homework11.TestDataGenerator.humans;

@Order(1)
class HumanExTest
{
    @AfterAll
    static void printTestData()
    {
        System.out.println();
        System.out.println("=========================================================");
        System.out.printf("===  Тестовый массив (кол-во непустых элементов %d): ====%n",
                Arrays.stream(humans).filter(Objects::nonNull).count());
        System.out.println("=========================================================");
        Utils.printArray(humans, "\n\n");
        System.out.println("=========================================================");
    }
    
    /**
     * null - для любого заданного значения x, вызов x.equals(null) всегда должен возвращать false
     */
    @Test
    @DisplayName("Проверка равенства с Null")
    void testEquals_Null()
    {
        for (HumanEx h : humans)
        {
            if (h != null)
            {
                Assertions.assertNotEquals(null, h, h.toString());
            }
        }
    }
    
    /**
     * Рефлексивность - сколько бы раз мы не вызывали x.equals(x), результат должен быть всегда true.
     */
    @Test
    @DisplayName("Рефлексивность равенства")
    void testEquals_Reflexivity()
    {
        for (HumanEx h : humans)
        {
            if (h != null)
            {
                Assertions.assertEquals(h, h, h.toString());
            }
        }
    }
    
    /**
     * Симметричность - для объектов x и y, если x.equals(y) возвращает true, то тогда и y.equals(x) тоже должен вернуть true.
     * Согласованность - для любых x и y, должен возвращаться один и тот же результат сравнения, при условии, что между
     *      сравнениями ни один из объектов не изменял свое состояние (в полях, по которым происходит сравнение).
     */
    @Test
    @DisplayName("Симметричность и Согласованность равенства")
    void testEquals_SymmetryAndConsistency()
    {
        HumanEx h1, h2;
        for (int i = 0; i < humans.length - 2; i++)
        {
            h1 = humans[i];
            h2 = humans[i + 1];
            if (h1 == null || h2 == null)
            {
                continue;
            }
            // случайные данные не должны быть равны
            Assertions.assertNotEquals(h1, h2, h1 + "\n\n" + h2);
            // меняем "значащие" поля
            h2.setPassportSeries(h1.getPassportSeries());
            h2.setPassportNumber(h1.getPassportNumber());
            Assertions.assertTrue(h1.equals(h2) && h2.equals(h1), h1 + "\n\n" + h2);
            // меняем "незначащие" поля
            h2.setAge(TestDataGenerator.jfaker.random().nextInt(HumanEx.MIN_AGE, 99));
            h1.setLastName(Utils.getCorrectGenderLastName(h1.getName(), TestDataGenerator.jfaker.name().lastName()));
            Assertions.assertEquals(h1, h2, h1 + "\n\n" + h2);
        }
    }
    
    /**
     * Транзитивность - для объектов x, y и z, x.equals(y) возвращает true, а x.equals(z) тоже возвращает true,
     *      y.equals(z) тоже должен вернуть true
     */
    @Test
    @DisplayName("Транзитивность равенства")
    void testEquals_Transitivity()
    {
        HumanEx x, y, z;
        for (int i = 0; i < humans.length - 3; i++)
        {
            x = humans[i];
            y = humans[i + 1];
            z = humans[i + 2];
            if (x == null || y == null || z == null)
            {
                continue;
            }
            y.setPassportSeries(x.getPassportSeries());
            y.setPassportNumber(x.getPassportNumber());
            z.setPassportSeries(y.getPassportSeries());
            z.setPassportNumber(y.getPassportNumber());
            Assertions.assertTrue(x.equals(y) && y.equals(z) && x.equals(z), x + "\n\n" + y + "\n\n" + z);
        }
    }
    
    /**
     * Вызов метода .hashCode() над одним и тем же объектом, должен всегда возвращать одно и то же число, при условии,
     *      что объект между вызовам hashCode(), не изменялся (в полях, по которым происходит вычисление хеша).
     */
    @Test
    @DisplayName("HashCode - Внутренняя согласованность")
    void testHashCode_InternalConsistency()
    {
        for (HumanEx human : humans)
        {
            if (human != null)
            {
                int expectedHash = human.hashCode();
                String[] fi = TestDataGenerator.jfaker.name().fullName().split(" ");
                human.setName(fi[0]);
                human.setLastName(fi[1]);
                human.setPatronymic("");
                Assertions.assertEquals(expectedHash, human.hashCode());
                human.setPassportNumber(TestDataGenerator.jfaker.numerify("######"));
                Assertions.assertNotEquals(expectedHash, human.hashCode());
            }
        }
    }
    
    /**
     * Вызов метода .hashCode() над двумя объектами должен возвращать одно и то же число, если данные два объекта равны (.equals() вернул true).
     */
    @Test
    @DisplayName("HashCode - согласованность c Equals")
    void testHashCode_EqualsConsistency()
    {
        HumanEx h1, h2;
        for (int i = 0; i < humans.length - 2; i++)
        {
            h1 = humans[i];
            h2 = humans[i + 1];
            if (h1 == null || h2 == null)
            {
                continue;
            }
            // делаем объекты равными
            h2.setPassportSeries(h1.getPassportSeries());
            h2.setPassportNumber(h1.getPassportNumber());
            Assertions.assertEquals(h1, h2, h1 + "\n\n" + h2);
            // проверяем равенство hash-кодов
            Assertions.assertEquals(h1.hashCode(), h2.hashCode(), h1 + "\n\n" + h2);
        }
    }
    
}