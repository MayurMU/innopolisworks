package org.mayurmu.homework11;

import com.github.javafaker.Faker;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.mayurmu.common.Utils;

import org.joda.time.Instant;
import java.util.Comparator;

/**
 * Класс реализующий задачу 11 - "классы String и Object"
 */
public class Task_11
{
    static final Comparator<HumanEx> ascFIOLengthComparator = Comparator.comparingInt(x -> x.getFullName().length());
    static final Comparator<HumanEx> descFIOLengthComparator = Comparator.comparingInt(x -> -x.getFullName().length());
    
    /**
     * Задача 11 - Создаёт массив из нескольких случайных людей и выводит его на экран
     */
    public static void main(String[] args)
    {
        try
        {
            System.out.println("Случайный массив людей:");
            // различия времени сортировки можно увидеть при кол-ве эл-тов порядка 1000
            HumanEx[] humans = new HumanEx[TestDataGenerator.jfaker.random().nextInt(5, 10)];
            TestDataGenerator.fillRandomHumansArray(humans);
            System.out.println();
            Utils.printArray(humans, "\n\n");
            System.out.println();
            System.out.println("======================================================================================");
            System.out.println("Отсортированный массив людей (по количеству букв в их ФИО - восходящий порядок - ASC):");
            System.out.println();
            sortAndPrintArray(humans, ascFIOLengthComparator, Task_11::bubbleSort, "Пузырёк", Task_11::shakerSort, "Шейкер");
            System.out.println("======================================================================================");
            System.out.println("Отсортированный массив людей (по количеству букв в их ФИО - нисходящий порядок - DESC):");
            System.out.println();
            sortAndPrintArray(humans, descFIOLengthComparator, Task_11::shakerSort, "Шейкер", Task_11::bubbleSort, "Пузырёк");
        }
        catch (Exception ex)
        {
            System.err.append("Непредвиденная ошибка работы программы:").append(System.lineSeparator()).println(ex);
            ex.printStackTrace();
        }
        finally
        {
            System.out.println();
            System.out.println("Программа завершена");
        }
    }
    
    /**
     * Вспомогательный метод сравнения времени сортировки двух функций с выводом результата второй сортировки на консоль
     * @param sorter1 Первый алгоритм сортировки
     * @param sorter2 Второй алгоритм сортировки
     * @param name1 Имя первого алг.
     * @param name2 Имя второго алг.
     *
     * @implNote На экран выводится результат полученный от {@code sorter2}
     *
     * @implSpec От Joda-Time похоже тут нет особого прока, т.к. она тоже не умеет измерять наносекундные интервалы,
     *      кроме того, на её сайте рекомендуют переходить на стандартные средства JDK 8 (java.time (JSR-310)).
     * @see <a href="https://www.joda.org/joda-time/faq.html#submilli">What about sub-millisecond accuracy?</a>
     */
    static void sortAndPrintArray(HumanEx[] array, Comparator<HumanEx> comp, HumanSorter sorter1, String name1, HumanSorter sorter2, String name2)
    {
        Instant startTime = Instant.now();
        sorter1.sort(array, comp);
        Period timeOfSort = new Interval(startTime, Instant.now()).toPeriod();
        System.out.println("------------------------------------------");
        System.out.printf("Время выполнения (\"%s\": %s:%s:%s)%n", name1, timeOfSort.getMinutes(), timeOfSort.getSeconds(), timeOfSort.getMillis());
        startTime = Instant.now();
        HumanEx[] sortedAsc2 = sorter2.sort(array, comp);
        timeOfSort = new Interval(startTime, Instant.now()).toPeriod();
        System.out.println("------------------------------------------");
        System.out.printf("Время выполнения (\"%s\": %s:%s:%s)%n", name2, timeOfSort.getMinutes(), timeOfSort.getSeconds(), timeOfSort.getMillis());
        System.out.println("------------------------------------------");
        System.out.println();
        Utils.printArray(sortedAsc2, "\n\n");
        System.out.println();
    }
    
    /**
     * Сортировка Пузырьком с поддержкой выбора направления сортировки
     *
     * @implNote Сложность O(n^2). Метод из лекции переделан на функцию вместо процедуры. Этот алгоритм считается учебным
     *      и почти не применяется на практике из-за низкой эффективности: он медленно работает на тестах, в которых
     *      маленькие элементы (их называют «черепахами») стоят в конце массива.
     * @see <a href="https://academy.yandex.ru/posts/osnovnye-vidy-sortirovok-i-primery-ikh-realizatsii">Академия Яндекса</a>
     *
     * @param array входной неотсортированный массив
     * @param comp Средство сравнения объектов (и выбора направления сортировки)
     *
     * @return Отсортированный массив
     */
    public static HumanEx[] bubbleSort(HumanEx[] array, Comparator<HumanEx> comp)
    {
        if (array.length == 0)
        {
            return new HumanEx[0];
        }
        else if (array.length == 1)
        {
            return new HumanEx[] { array[0] };
        }
        HumanEx[] result = new HumanEx[array.length];
        System.arraycopy(array, 0, result, 0, array.length);
        
        for (int i = 0; i < result.length; i++)
        {
            for (int j = 0; j < result.length - 1; j++)
            {
                if (comp.compare(result[j], result[j + 1]) > 0)
                {
                    HumanEx tempHuman = result[j];
                    result[j] = result[j + 1];
                    result[j + 1] = tempHuman;
                }
            }
        }
        return result;
    }
    
    /**
     * Сортировка перемешиванием (шейкерная сортировка)
     *
     * @implNote Сложность O(n^2), Шейкерная сортировка отличается от пузырьковой тем, что она двунаправленная: алгоритм
     *      перемещается не строго слева направо, а сначала слева направо, затем справа налево.
     *
     * @see <a href="https://academy.yandex.ru/posts/osnovnye-vidy-sortirovok-i-primery-ikh-realizatsii">Академия Яндекса</a>
     * @param array Входной массив
     * @return Отсортированный массив
     */
    public static HumanEx[] shakerSort(HumanEx[] array, Comparator<HumanEx> comp)
    {
        if (array.length == 0)
        {
            return new HumanEx[0];
        }
        else if (array.length == 1)
        {
            return new HumanEx[] { array[0] };
        }
        HumanEx[] result = new HumanEx[array.length];
        System.arraycopy(array, 0, result, 0, array.length);
        
        int left = 0;
        int right = result.length - 1;
        HumanEx tempHuman;
        
        while (left <= right)
        {
            for (int i = right; i > left; --i)
            {
                if (comp.compare(result[i - 1], result[i]) > 0)
                {
                    tempHuman = result[i - 1];
                    result[i - 1] = result[i];
                    result[i] = tempHuman;
                }
            }
            ++left;
            for (int i = left; i < right; ++i)
            {
                if (comp.compare(result[i], result[i + 1]) > 0)
                {
                    tempHuman = result[i + 1];
                    result[i + 1] = result[i];
                    result[i] = tempHuman;
                }
            }
            --right;
        }
        return result;
    }
    
}