package org.mayurmu.homework11;

import com.github.javafaker.Faker;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;

/**
 * Вспомогательный класс генерации тестовых данных
 *
 * @apiNote Почему-то не удалось разместить его в каталоге /test - тогда он становится не видим классами в каталоге /main
 */
public class TestDataGenerator
{
    public static final int TEST_DATA_LENGTH = 20;
    /**
     * Кешированная копия данных полученная в ходе последнего заполнения массива
     */
    public static final HumanEx[] humans = new HumanEx[TEST_DATA_LENGTH];
    public static final Faker jfaker = new Faker(new Locale("ru"));
    @SuppressWarnings("unchecked")
    private static final Comparator<HumanEx>[] comparators = new Comparator[] { Task_11.descFIOLengthComparator,
                                                                                Task_11.ascFIOLengthComparator };
    private static final HumanSorter[] sorters = new HumanSorter[] { Task_11::bubbleSort, Task_11::shakerSort };
    
    /*
     * Первоначально данные будут включать несколько Null-элементов
     */
    static
    {
        fillRandomHumansArray(humans, true);
    }
    
    static Comparator<HumanEx> getRandomComparator()
    {
        return comparators[jfaker.random().nextInt(comparators.length)];
    }
    
    /**
     * Перезаполняет массив {@link #humans} валидными данными (без Null)
     */
    static void regenerateData()
    {
        fillRandomHumansArray(humans, false);
    }
    
    /**
     * То же, что и {@link #regenerateData()}, но возвращает копию массива ссылок на тестовые объекты
     */
    static HumanEx[] getNewData()
    {
        regenerateData();
        return Arrays.copyOf(humans, humans.length);
    }
    
    /**
     * Метод заполнения массива людей случайными значениями
     * @param humans    Массив людей ненулевой длины
     */
    public static void fillRandomHumansArray(HumanEx[] humans)
    {
        fillRandomHumansArray(humans, false);
    }
    /**
     * Метод заполнения массива людей случайными значениями (в т.ч. может рандомно вставлять null вместо некоторых эл-в)
     * @param humans    Массив людей ненулевой длины
     * @param isInsertNulls     Добавлять ли случайное кол-во Null объектов (от 1 до половины длины массива)
     * @exception IllegalArgumentException При пустом массиве людей ({@code humans == null || humans.length == 0})
     */
    public static void fillRandomHumansArray(HumanEx[] humans, boolean isInsertNulls)
    {
        class RussianFIOGenerator
        {
            private static final String PATRONYMIC_ENDING_1 = "вич";
            private static final String PATRONYMIC_ENDING_2 = "вна";
            
            private boolean isPatronymic(String namePart)
            {
                return namePart.endsWith(PATRONYMIC_ENDING_1) || namePart.endsWith(PATRONYMIC_ENDING_2);
            }
            
            /**
             * Метод гарантировано возвращает ФИО с отчеством
             */
            String[] getFIO()
            {
                String[] result;
                do
                {
                    // Наблюдение: Здесь мы можем случайно получить два варианта: ФИО или ИОФ, а также вообще не получить Отчество
                    result = jfaker.name().fullName().split(" ");
                }
                while (result.length == 2 || !isPatronymic(result[result.length - 1]));
                return result;
            }
        }
        
        class RussianStreetNameGenerator
        {
            private static final String MALE_STREET_PREFIXES_PATTERN = "проспект|пр.|переулок|пер.";
            private static final String FEMALE_STREET_PREFIXES_PATTERN = "площадь|пл.|улица|ул.";
            private final String[] FEMALE_STREET_PREFIXES = FEMALE_STREET_PREFIXES_PATTERN.split("\\|");
            private final String[] MALE_STREET_PREFIXES = MALE_STREET_PREFIXES_PATTERN.split("\\|");
            /**
             * Метод возвращает согласованное название улицы вида "{пл.|площадь Юбилейная}"
             */
            String getStreetName()
            {
                String temp = jfaker.address().streetName().replaceAll(MALE_STREET_PREFIXES_PATTERN + "|" +
                        FEMALE_STREET_PREFIXES_PATTERN, "").trim();
                String prefix = temp.endsWith("я") ? FEMALE_STREET_PREFIXES[jfaker.random().nextInt(FEMALE_STREET_PREFIXES.length)] :
                        MALE_STREET_PREFIXES[jfaker.random().nextInt(MALE_STREET_PREFIXES.length)];
                return prefix + " " + temp;
            }
        }
        
        if (humans == null || humans.length == 0)
        {
            throw new IllegalArgumentException("Нечего заполнять - Передан пустой массив!");
        }
        RussianFIOGenerator fioGen = new RussianFIOGenerator();
        RussianStreetNameGenerator sGen = new RussianStreetNameGenerator();
        
        final int nullsCount = jfaker.number().numberBetween(1, humans.length / 2);
        
        for (int i = 0, n = 0; i < humans.length; i++)
        {
            if (isInsertNulls && (n < nullsCount && jfaker.bool().bool()))
            {
                humans[i] = null;
                n++;
                continue;
            }
            String[] fio = fioGen.getFIO();
            Boolean hasPatronymic = jfaker.random().nextBoolean();
            HumanEx human = new HumanEx(fio[1], fio[0], hasPatronymic ? fio[2] : "", jfaker.number().numberBetween(HumanEx.MIN_AGE, 100),
                    jfaker.number().digits(4), jfaker.number().digits(6));
            
            Boolean hasAddress = jfaker.random().nextBoolean();
            if (hasAddress)
            {
                String houseNumber = String.valueOf(jfaker.number().numberBetween(1, 100));
                String flatNumber = String.valueOf(jfaker.number().numberBetween(1, 999));
                human.setAddress(new HumanEx.Address(jfaker.address().city(), sGen.getStreetName(), houseNumber, flatNumber));
            }
            humans[i] = human;
        }
    }
    
    public static HumanSorter getRandomSorter()
    {
        return sorters[jfaker.random().nextInt(sorters.length)];
    }
}
