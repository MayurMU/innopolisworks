package org.mayurmu.homework11;

import java.util.Comparator;

@FunctionalInterface
public interface HumanSorter
{
    HumanEx[] sort(HumanEx[] array, Comparator<HumanEx> comp);
}
