package org.mayurmu.homework11;

import java.util.StringJoiner;

import org.mayurmu.homework7.Human;

/**
 * Класс расширенного человека (человек с паспортом)
 */
public class HumanEx extends Human
{
    public static class Address
    {
        public final String city;
        public final String street;
        public final String house;
        public final String flat;
        public final boolean isEmpty;
    
        protected Address(String city, String street, String house, String flat)
        {
            this.city = city != null ? city.trim() : "";
            this.street = street != null ? street.trim() : "";
            this.house = house != null ? house.trim() : "";
            this.flat = flat != null ? flat.trim() : "";
            isEmpty = (city + street + house + flat).isEmpty();
        }
    
        @Override
        public String toString()
        {
            return new StringJoiner(", ")
                    .add(city.isEmpty() ? "" : "Город " + city)
                    .add(street)
                    .add(house.isEmpty() ? "" : "дом " + house)
                    .add(flat.isEmpty() ? "" : "квартира " + flat)
                    .toString();
        }
    }
    
    /**
     * Минимальный возраст выдачи паспорта
     */
    public static final int MIN_AGE = 14;
    
    private String patronymic;
    private String passportNumber;
    private String passportSeries;
    private Address address;
    
    
    
    //region 'Конструкторы'
    
    public HumanEx()
    {
        this("", "" , MIN_AGE, "0000", "000000");
    }
    
    /**
     * Упрощённый конструктор без Отчества и адреса
     */
    public HumanEx(String name, String lastName, int age, String passportSeries, String passportNumber)
    {
        this(name, lastName, "", age, passportSeries, passportNumber, "", "", "", "");
    }
    
    /**
     * Упрощённый конструктор без адреса
     */
    public HumanEx(String name, String lastName, String patronymic, int age, String passportSeries, String passportNumber)
    {
        this(name, lastName, patronymic, age, passportSeries, passportNumber, "", "", "", "");
    }
    
    /**
     * Конструктор человека с паспортом
     * @exception IllegalArgumentException Когда не задан паспорт или возраст его получения не достигнут (см.: {@link #MIN_AGE})
     */
    public HumanEx(String name, String lastName, String patronymic, int age, String passportSeries, String passportNumber,
                   String city, String street, String house, String flat)
    {
        super();
        if (passportSeries == null || passportSeries.isEmpty() || passportNumber == null ||
            passportNumber.isEmpty())
        {
            throw new IllegalArgumentException("Серия и номер Паспорта обязательно должны быть заданы!");
        }
        setName(name);
        setLastName(lastName);
        setAge(age);
        setPassportNumber(passportNumber);
        setPassportSeries(passportSeries);
        this.patronymic = patronymic != null ? patronymic.trim() : "";
        this.address = new Address(city, street, house, flat);
    }
    
    //endregion 'Конструкторы'
    
    
    
    
    //region 'Свойства'
    
    @Override
    public void setAge(int age)
    {
        if (age < MIN_AGE)
        {
            throw new IllegalArgumentException("Возраст человека с паспортом должен быть не менее " + MIN_AGE + " лет!, однако было передано:" + age);
        }
        super.setAge(age);
    }
    
    /**
     * @return Возвращает полное имя человека (ФИО) через пробел
     */
    @Override
    public String getFullName()
    {
        return patronymic.isEmpty() ? super.getFullName() : patronymic + " " + super.getFullName();
    }
    
    public Address getAddress()
    {
        return address;
    }
    
    public void setAddress(Address address)
    {
        this.address = address;
    }
    
    public String getPatronymic()
    {
        return patronymic;
    }
    
    public void setPatronymic(String patronymic)
    {
        this.patronymic = patronymic != null ? patronymic.trim() : "";
    }
    
    /**
     * Номер паспорта
     */
    public String getPassportNumber()
    {
        return passportNumber;
    }
    
    /**
     * Номер паспорта
     * @param number Допускаются только цифры (ровно 6)
     */
    public void setPassportNumber(String number)
    {
        if (number == null || !number.trim().matches("^\\d{6}$"))
        {
            throw new IllegalArgumentException("Номер Паспорта гражданина РФ должен состоять строго из 6 цифр! Однако было передано: " + number);
        }
        this.passportNumber = number.trim();
    }
    
    /**
     * Серия паспорта
     */
    public String getPassportSeries()
    {
        return passportSeries;
    }
    
    /**
     * Серия и паспорта (через пробел)
     */
    public String getPassportSeriesAndNumber()
    {
        return passportSeries + ' ' + passportNumber;
    }
    
    /**
     * Устанавливает серию паспорта
     * @param series Допускаются только цифры (ровно 4)
     */
    public void setPassportSeries(String series)
    {
        if (series == null || !series.trim().matches("^\\d{4}$"))
        {
            throw new IllegalArgumentException("Серия Паспорта гражданина РФ должна состоять строго из 4 цифр! Однако было передано: " + series);
        }
        this.passportSeries = series.trim();
    }
    
    //endregion 'Свойства'
    
    
    
    
    //region 'Методы'
    
    /**
     * Метод получения строкового представления человека.
     * Возвращает строку вида:
     * <pre>
     * Пупкин Вася Варфаламеевич
     * Паспорт:
     * Серия: 98 22 Номер: 897643
     * Город Джава, ул. Программистов, дом 15, квартира 54
     * </pre>
     */
    @Override
    public String toString()
    {
        return new StringJoiner("\n")
                .add(super.getFullName() + " " + patronymic)
                .add("Возраст: " + getAge())
                .add("Паспорт:")
                .add("Серия: " + passportSeries.substring(0, 2) + ' ' + passportSeries.substring(2, 4) + " Номер: " + passportNumber)
                .add(address == null || address.isEmpty ? "-Адрес отсутствует-" : address.toString())
                .toString();
    }
    
    @Override
    public int hashCode()
    {
        return passportSeries.toLowerCase().hashCode() + passportNumber.toLowerCase().hashCode();
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null || obj.getClass() != this.getClass())
        {
            return false;
        }
        if (obj == this)    // равенство ссылок
        {
            return true;
        }
        HumanEx other = (HumanEx) obj;
        return passportNumber.equalsIgnoreCase((other).passportNumber) && passportSeries.equalsIgnoreCase((other).passportSeries);
    }
    
    //endregion 'Методы'
    
}
