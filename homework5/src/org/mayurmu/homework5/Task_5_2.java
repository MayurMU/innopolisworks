package org.mayurmu.homework5;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Задача 5 пункт 2 - обычная сложность -
 * Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые.
 * */
public class Task_5_2
{
    private static final Random _rnd = new Random();
    private static final Scanner _scn = new Scanner(System.in);
    
    public static void main(String[] args)
    {
        boolean wantRepeat;
        int[] randomArray = new int[Short.SIZE];      //Integer.SIZE * 10 - на таком размере у меня устойчива заметна разница скорости алгоритмов
        try
        {
            do
            {
                // генерируем массив случайных целых чисел с заданным кол-вом нулей (от 0 до 100%)
                System.out.println("Случайный массив со случайным кол-вом нулей:");
                // генерируем случайное кол-во индексов нулей в пределах длины массива
                int zerosCount = _rnd.nextInt(randomArray.length + 1);
                // генерируем сам массив индексов
                int[] zeroIndexes = _rnd.ints(zerosCount, 0, randomArray.length).toArray();
                for (int i = 0; i < randomArray.length; i++)
                {
                    randomArray[i] = Task_5_1.getNumberPosInArray(i, zeroIndexes) != -1 ? 0 : _rnd.nextInt(Byte.MAX_VALUE + 1);
                }
                System.out.append('\t').println(Arrays.toString(randomArray));
    
                System.out.println("Уплотняем массив функцией:");
                // такой вариант замера времени подходит только для очень больших массив, т.е. оч. длительного времени выполнения
                //long startTime = System.currentTimeMillis();
                long startTime = System.nanoTime();
                int[] compArray = getCompactedArray(randomArray);
                long endTime = System.nanoTime();
                System.out.append('\t').println(Arrays.toString(compArray));
                // такой вариант замера времени подходит только для очень больших массив, т.е. оч. длительного времени выполнения
                //System.out.printf("Затраченное время: %1$tM:%1$tS:%1$tL", endTime - startTime);
                System.out.printf("Затраченное время (наносек): %d", endTime - startTime);
                System.out.println();
                System.out.println();
                
                System.out.println("Уплотняем массив процедурой:");
                startTime = System.nanoTime();
                compactArray(randomArray);
                endTime = System.nanoTime();
                System.out.append('\t').println(Arrays.toString(randomArray));
                System.out.printf("Затраченное время (наносек): %d", endTime - startTime);
                System.out.println();
                System.out.println();
                
                System.out.println("Введите любой символ для новой попытки - для выхода введите ПРОБЕЛ");
                wantRepeat = !_scn.nextLine().equalsIgnoreCase(" ");
            }
            while (wantRepeat);
        }
        catch (Exception ex)
        {
            System.err.println("Необработанная ошибка: " + System.lineSeparator() + ex);
            ex.printStackTrace();
        }
        finally
        {
            _scn.close();
            System.out.println("Программа завершена");
        }
    }
    
    /**
     * Метод сжатия массива - размещает нулевые элементы справа
     * @param array Входной массив
     * @implNote Эта версия должна работать медленнее O(n^2), но меньше расходовать память - не создавать мусор
     */
    private static void compactArray(int[] array)
    {
        //N.B. вижу здесь две реализации - с экономией памяти (перемещать элементы по очереди, просматривая массив с конца)
        // и второй вариант с экономией времени ЦП (создать второй массив и поместить туда эл-ты в два прохода - сначала 0-ли,
        //  потом всё остальное - но, если будет второй массив, тогда логично возвращать его из метода, а по заданию у нас Процедура)
        // просматриваем массив с конца
        for (int i = array.length - 1; i >= 0; i--)
        {
            if (array[i] != 0)
            {
                // продолжаем просмотр "влево", начиная с предыдущего эл-та, в поисках Нулевого эл-та
                for (int j = i - 1; j >= 0 && array[i] != 0; j--)
                {
                    if (array[j] == 0)
                    {
                        array[j] = array[i];
                        array[i] = 0;
                    }
                }
                // если Нулевые эл-ты слева не нашлись, значит уплотнение окончено
                if (array[i] != 0)
                {
                    return;
                }
            }
        }
    }
    
    /**
     * Метод уплотнения массива - реализация в виде функции - сложность O(n)
     * @implNote Должно работать быстрее {@link #compactArray(int[])}, но с большим расходом памяти и с образованием мусора
     * */
    private static int[] getCompactedArray(int[] array)
    {
        int[] result = new int[array.length];
        for (int i = 0, k = 0, j = array.length - 1; i < array.length; i++)
        {
            // заполняем массив с двух сторон: нули - справа, остальное - слева
            if (array[i] == 0)
            {
                result[j--] = 0;
            }
            else
            {
                result[k++] = array[i];
            }
        }
        return result;
    }
    
}
