package org.mayurmu.homework5;

import org.mayurmu.common.Utils;
import org.mayurmu.exceptions.UserInputCancelledException;

import java.util.*;

/**
 * Задача 5 пункт 1 - обычная сложность -
 * Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть индекс этого числа в массиве.
 * Если число в массиве отсутствует - вернуть -1.
 */
public class Task_5_1
{
    private static final Random _rnd = new Random();
    private static final Scanner _scn = new Scanner(System.in);
    
    public static void main(String[] args)
    {
        try
        {
            System.out.println("Случайный массив:");
            // генерируем массив случайных целых чисел
            int[] randomArray = _rnd.ints(Byte.SIZE, Byte.MIN_VALUE, Byte.MAX_VALUE + 1).toArray();
            System.out.println(Arrays.toString(randomArray));
            boolean wantRepeat = true;
            do
            {
                int number = Utils.inputIntNumber(_scn, "Введите число, которое нужно найти в массиве", true);
                int pos = getNumberPosInArray(number, randomArray);
                if (pos < 0)
                {
                    System.out.println("Такого числа в массиве нет!");
                }
                else
                {
                    System.out.printf("Найдена позиция числа в массиве: %d%n", pos);
                }
            }
            while (true);
        }
        catch (UserInputCancelledException ux)
        {
            System.out.println("Пользователь отказался от ввода");
        }
        catch (Exception ex)
        {
            System.err.println("Необработанная ошибка: " + System.lineSeparator() + ex);
            ex.printStackTrace();
        }
        finally
        {
            _scn.close();
            System.out.println("Программа завершена");
        }
    }
    
    static int getNumberPosInArray(int number, int[] array)
    {
        int pos = -1;
        for (int i = 0; i < array.length && pos < 0; i++)
        {
            if (number == array[i])
            {
                pos = i;
            }
        }
        return pos;
    }
}
