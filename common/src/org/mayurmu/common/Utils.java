package org.mayurmu.common;

import java.util.InputMismatchException;
import java.util.Scanner;

import org.mayurmu.exceptions.UserInputCancelledException;


public class Utils
{
    /**
     * Метод гарантированного ввода целого числа - не даёт пользователю ввести ничего другого, использует приветствие по умолчанию
     * */
    public static int inputIntNumber(Scanner scn)
    {
        return inputIntNumber(scn, "Введите целое число: ");
    }
    
    /**
     * Метод гарантированного ввода целого числа, использует приветствие по умолчанию - позволяет поль-лю отказаться от ввода.
     *
     * @implNote Использует стандартную строку приглашения ввода
     *
     **/
    public static int inputIntNumber(Scanner scn, boolean allowExitOnEmptyString) throws UserInputCancelledException
    {
        if (allowExitOnEmptyString)
        {
            return inputIntNumber(scn, "Ведите целое число - пустая строка для выхода", true);
        }
        else
        {
            return inputIntNumber(scn);
        }
    }
    public static int inputIntNumber(Scanner scn, String helloText, boolean allowExitOnEmptyString) throws UserInputCancelledException
    {
        return inputIntNumber(scn, helloText, 10, allowExitOnEmptyString);
    }
    /**
     * Метод гарантированного ввода целого числа, использует приветствие по умолчанию - позволяет поль-лю отказаться от ввода.
     *
     * @param allowExitOnEmptyString Выходит по при вводе пустой строки - уведомляет поль-ля о существовании такой возможности
     * @param helloText Приветствие для пользователя - может быть пустой строкой
     * @param numberSystemBase Основание СИ
     *
     * @return Число введённое пользователем, если поль-ль не отказался от ввода (см.: {@code allowExitOnEmptyString})
     *
     * @apiNote при {@code allowExitOnEmptyString == false} логичнее сразу вызывать {@link #inputIntNumber(Scanner)},
     *  чтобы не приходилось обрабатывать исключение {@link UserInputCancelledException}.
     *
     * @exception UserInputCancelledException Выбрасывается при вводе пользователем пустой строки
     * */
    public static int inputIntNumber(Scanner scn, String helloText, int numberSystemBase, boolean allowExitOnEmptyString) throws UserInputCancelledException
    {
        final String HELLO_TEXT_END = " - введите ПРОБЕЛ для выхода:";
        String userInput = "";
        int res = -1;
        if (allowExitOnEmptyString)
        {
            boolean incorrectInput = false;
            do
            {
                System.out.println(helloText.endsWith(":") ? helloText.replace(":", HELLO_TEXT_END) : helloText +
                        HELLO_TEXT_END);
                userInput = scn.nextLine().trim();
                if (userInput.isEmpty())
                {
                    throw new UserInputCancelledException();
                }
                try
                {
                    res = Integer.parseInt(userInput, numberSystemBase);
                    incorrectInput = false;
                }
                catch (NumberFormatException nfe)
                {
                    incorrectInput = true;
                    System.err.format("Вы должны вводить только целые числа!%n").println(nfe.toString());
                }
            }
            while (incorrectInput);
        }
        else
        {
            res = inputIntNumber(scn);
        }
        return res;
    }
    
    /**
     * Метод гарантированного ввода целого числа - не даёт пользователю ввести ничего другого
     *
     * @param helloText     Текст приглашения для ввода (может быть пустым)
     *
     * @implNote    При неправильном вводе выводит текст ошибки в консоль (поток ошибок)
     * */
    public static int inputIntNumber(Scanner scn, String helloText)
    {
        boolean incorrectInput = false;
        int a = -1;
        do
        {
            try
            {
                if (helloText != null && !helloText.isEmpty())
                {
                    System.out.println(helloText);
                }
                a = scn.nextInt();
                incorrectInput = false;
            }
            catch (InputMismatchException ime)
            {
                System.err.format("Вы должны вводить только целые числа!%n").println(ime.toString());
                scn.skip(".*\n");  // пропускаем некорректный ввод, иначе он снова будет прочитан при следующем вызове
                incorrectInput = true;
            }
        }
        while (incorrectInput);
        return a;
    }
}
