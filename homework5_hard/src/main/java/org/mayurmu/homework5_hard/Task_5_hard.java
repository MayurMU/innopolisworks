package org.mayurmu.homework5_hard;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import org.mayurmu.common.Utils;
import org.mayurmu.exceptions.UserInputCancelledException;

/**
 * Задача 5 повышенной сложности - поиск всех вхождений элемента в массиве
 * @implSpec проект использует библиотеку из другого Maven-проекта в каталоге /common-code, соот-но сначала эту библиотеку
 *  нужно установить в локальный репозиторий.
 *
 */
public class Task_5_hard
{
    private static final Random _rnd = new Random();
    private static final Scanner _scn = new Scanner(System.in);
    
    /*
    * Программа ищет все вхождения элемента в массиве, пользователь может регулировать какой индекс элемента выводить,
    *  если элемент встречается в массиве несколько раз.
    **/
    public static void main(String[] args)
    {
        try
        {
            System.out.println("Случайный массив:");
            // генерируем массив случайных целых чисел так, чтобы в нём с большой вероятностью были дубликаты
            int[] randomArray = _rnd.ints(Short.SIZE, 0, Short.SIZE).toArray();
            System.out.println(Arrays.toString(randomArray));
            do
            {
                int number = Utils.inputIntNumber(_scn, "Введите число, которое нужно найти в массиве", true);
                int[] positions = getAllNumberPosInArray(number, randomArray);
                if (positions == null)
                {
                    System.out.println("Такого числа в массиве нет!");
                }
                else
                {
                    int i = 0;
                    while (i < positions.length && positions[i] != -1)
                    {
                        i++;
                    }
                    if (i == 1)
                    {
                        System.out.printf("Найдена позиция числа в массиве: %d%n", positions[0]);
                    }
                    else
                    {
                        String mes = String.format("Количество позиций числа (%d) в массиве - %d (шт.), индекс какого совпадения, Вы хотите " +
                                        "увидеть: ", number, i);
                        boolean isIndexOutOfBounds;
                        int pos;
                        do
                        {
                            pos = Utils.inputIntNumber(_scn, mes, true);
                            isIndexOutOfBounds = pos < 1 || pos > i;
                            if (isIndexOutOfBounds)
                            {
                                System.err.println("Вы должны ввести число от 1 до " + i);
                            }
                        }
                        while (isIndexOutOfBounds);
                        System.out.printf("Вы выбрали %d-e совпадение в массиве, его индекс в исходном массиве: %d %n",
                                pos, positions[pos - 1]);
                    }
                }
            }
            while (true);
        }
        catch (UserInputCancelledException ux)
        {
            System.out.println("Пользователь отказался от ввода");
        }
        catch (Exception ex)
        {
            System.err.println("Необработанная ошибка: " + System.lineSeparator() + ex);
            ex.printStackTrace();
        }
        finally
        {
            _scn.close();
            System.out.println("Программа завершена");
        }
    }
    
    /**
     * Метод получения всех индексов указанного элемента в массиве
     *
     * @param number Искомое число
     * @param array  Массив-источник
     *
     * @return Массив найденных индексов длинной равной {@code array.length} и изначально заполненный значением (-1)
     * или Null, если ничего найти не удалось или на вход был передан пустой массив.
     *
     */
    static int[] getAllNumberPosInArray(int number, int[] array)
    {
        if (array.length == 0)
        {
            return null;
        }
        int[] foundIndices = new int[array.length];
        Arrays.fill(foundIndices, -1);
        int lastIndex = -1;
        int i = 0;
        do
        {
            lastIndex = getNumberPosInArray(number, array, lastIndex + 1);
            if (lastIndex >= 0)
            {
                foundIndices[i++] = lastIndex;
            }
        }
        while ((lastIndex != -1) && (lastIndex < array.length - 1));
        return i == 0 ? null : foundIndices;
    }
    
    /**
     * Метод получения первого попавшегося индекса указанного элемента в массиве, начиная с указанной позиции
     *
     * @param number     Искомое число
     * @param array      Массив-источник
     * @param startIndex Номер элемента, с которого надо начинать поиск (включительно)
     *
     * @return Индекс первого найденного элемента, начиная с {@code startIndex}
     *
     * @throws IllegalArgumentException когда {@code startIndex} за пределами массива
     */
    private static int getNumberPosInArray(int number, int[] array, int startIndex)
    {
        if (startIndex < 0 || startIndex >= array.length)
        {
            throw new IllegalArgumentException(String.format("Параметр startIndex должен быть >= 0 < array.length (%d); был передан: %d",
                    array.length, startIndex));
        }
        int pos = -1;
        for (int i = startIndex; i < array.length && pos < 0; i++)
        {
            if (number == array[i])
            {
                pos = i;
            }
        }
        return pos;
    }
}