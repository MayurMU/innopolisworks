package org.mayurmu.homework5_hard;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;


class Task_5_hardTest
{
    
    //region 'Исключения'
    
    @Test
    @DisplayName("Проверка NullPointerException при пустой ссылке на массив")
    void testGetAllNumberPosInArray_NULL_Array()
    {
        Assertions.assertThrows(NullPointerException.class, () -> Task_5_hard.getAllNumberPosInArray(1, null));
    }
    
    //endregion 'Исключения'
    
    
    
    
    //region 'Негативные сценарии'
    
    @Test
    @DisplayName("Проверка возвраиа NULL значения при пустом массиве")
    void testGetAllNumberPosInArray_ZeroLengthArray()
    {
        Assertions.assertNull(Task_5_hard.getAllNumberPosInArray(1, new int[0]));
    }
    
    //endregion 'Негативные сценарии'
    
    
    
    
    //region 'Позитивные сценарии'
    
    @Test
    @DisplayName("Проверка нормальной работы, когда числа нет в массиве")
    void getAllNumberPosInArray_Regular_NoSuchElement()
    {
        Assertions.assertNull(Task_5_hard.getAllNumberPosInArray(1, new int[] {0, 9, 5}));
    }
    
    @Test
    @DisplayName("Проверка нормальной работы, когда число есть в единственном экземпляре")
    void getAllNumberPosInArray_Regular_SingleElementFound()
    {
        int numberToFind = 15;
        int[] arr = new int[] {5, 9, 15, 6, 3, 0, -5};
        int[] expectedArr = new int[arr.length];
        Arrays.fill(expectedArr, -1);
        expectedArr[0] = 2;
        Assertions.assertArrayEquals(expectedArr, Task_5_hard.getAllNumberPosInArray(numberToFind, arr));
    }
    
    @Test
    @DisplayName("Проверка нормальной работы, когда весь массив заполнен одним и тем же числом")
    void testGetAllNumberPosInArray_OnlyOneNumberInArrayButMultipleTimes()
    {
        int numberToFind = 1;
        int[] arr = new int[] {1, 1, 1, 1, 1};
        int[] expectedArr = new int[] {0, 1, 2, 3, 4};
        Assertions.assertArrayEquals(expectedArr, Task_5_hard.getAllNumberPosInArray(numberToFind, arr));
    }
    
    @Test
    @DisplayName("Проверка нормальной работы, число встречается несколько раз")
    void testGetAllNumberPosInArray_Regular_DuplicatesInArray()
    {
        int numberToFind = 9;
        int[] arr = new int[] {5, 9, 15, 6, 9, 0, 9};
        int[] expectedArr = new int[arr.length];
        Arrays.fill(expectedArr, -1);
        expectedArr[0] = 1;
        expectedArr[1] = 4;
        expectedArr[2] = 6;
        Assertions.assertArrayEquals(expectedArr, Task_5_hard.getAllNumberPosInArray(numberToFind, arr));
    }
    
    //endregion 'Позитивные сценарии'
    
}