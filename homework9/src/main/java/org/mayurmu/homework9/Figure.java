package org.mayurmu.homework9;

import com.indvd00m.ascii.render.Render;
import com.indvd00m.ascii.render.api.ICanvas;
import com.indvd00m.ascii.render.api.IContextBuilder;
import com.indvd00m.ascii.render.api.IRender;


/**
 * Класс абстрактная фигура
 *
 * @apiNote Предполагается, что координаты располагаются как на экране - в верхнем левом углу (0; 0), т.е. отрицательных к-т быть НЕ может
 */
public abstract class Figure
{
    protected int x;
    protected int y;
    private IRender renderer;
    
    /*
     * Возвращает объект занимающийся отрисовкой фигуры
     */
    private IRender getTextRenderer()
    {
        if (renderer == null)
        {
            renderer = new Render();
        }
        return renderer;
    }
    
    abstract double getPerimeter();
    
    abstract void Draw(int fieldSizeX, int fieldSizeY);
    
    public Figure(int x, int y)
    {
        if (x < 0 || y < 0)
        {
            throw new IllegalArgumentException("Координаты фигуры не могут быть отрицательными!");
        }
        this.x = x;
        this.y = y;
    }
    
    /**
     * Вспомогательный метод для подготовки поля отрисовки c очерченными границами
     *
     * @param fieldSizeX Предельные размер поля по оси Х
     * @param fieldSizeY Предельные размер поля по оси Y
     *
     * @return Контекст отрисовки, в котором накапливаются фигуры для вывода на экран
     */
    protected IContextBuilder getTextBuilder(int fieldSizeX, int fieldSizeY)
    {
        IContextBuilder builder = this.getTextRenderer().newBuilder();
        return builder.width(fieldSizeX).height(fieldSizeY).element(new com.indvd00m.ascii.render.elements.Rectangle());
    }
    
    /**
     * Вспомогательный метод выполняющий отрисовку фигуры в виде текста
     */
    protected void drawTextFigure(IContextBuilder builder)
    {
        ICanvas canvas = this.getTextRenderer().render(builder.build());
        String s = canvas.getText();
        System.out.println(s);
    }
}

