package org.mayurmu.homework9;

import com.indvd00m.ascii.render.api.IContextBuilder;

import java.util.StringJoiner;

/**
 * Класс Окружность
 *
 * @apiNote Предполагается, что координаты располагаются как на экране - в верхнем левом углу (0; 0), т.е. отрицательных к-т быть НЕ может
 */
public class Circle extends Ellipse implements Movable
{
    // радиус окружности - постоянная величина
    public final int radius;
    
    /**
     * Конструктор Окружности
     *
     * @param x Координата центра по оси X
     * @param y Координата центра по оси Y
     * @param r Радиус круга
     *
     * @throws IllegalArgumentException При {@code r <= 0}
     */
    public Circle(int x, int y, int r)
    {
        super(x, y, r * 2, r * 2);
        if (r < 1)
        {
            throw new IllegalArgumentException("Радиус должен быть положительным числом, однако было передано: " + r);
        }
        radius = r;
    }
    
    @Override
    public void move(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Метод расчёта периметра Окружности
     *
     * @implNote Т.к. Окружность является частным случаем Эллипса, то периметр можно было бы считать и по его формуле,
     * но это будет дольше (Формула для расчёта периметра окружности: L = 2*r*π)
     */
    @Override
    public double getPerimeter()
    {
        return radius * 2 * Math.PI;
    }
    
    @Override
    void Draw(int fieldSizeX, int fieldSizeY)
    {
        IContextBuilder builder = super.getTextBuilder(fieldSizeX, fieldSizeY);
        builder.element(new com.indvd00m.ascii.render.elements.Circle(x, y, radius));
        super.drawTextFigure(builder);
    }
    
    @Override
    public String toString()
    {
        return new StringJoiner(", ", "[", "]")
                .add("radius = " + radius)
                .add("x = " + x)
                .add("y = " + y)
                .toString();
    }
}
