package org.mayurmu.homework9;

import com.indvd00m.ascii.render.api.IContextBuilder;

import java.util.StringJoiner;

/**
 * Класс Прямоугольник
 *
 * @apiNote Предполагается, что координаты располагаются как на экране - в верхнем левом углу (0; 0), т.е. отрицательных к-т быть НЕ может
 * @implNote Для определения прямоугольника нужно знать две его вершины (к-ты левого верхнего и правого нижнего угла)
 */
public class Rectangle extends Figure
{
    protected int x2;
    protected int y2;
    
    private int getSideA()
    {
        return (this.x2 - this.x);
    }
    
    private int getSideB()
    {
        return (this.y2 - this.y);
    }
    
    /**
     * Конструктор Прямоугольника, который принимает к-ты левой верхней и нижней правой вершин
     *
     * @param x1 К-та X левой верхней вершины
     * @param y1 К-та Y левой верхней вершины
     * @param x2 К-та X правой нижней вершины
     * @param y2 К-та Y правой нижней вершины
     *
     * @throws IllegalArgumentException Когда к-ты правой нижней вершины, меньше к-т левой верхней
     */
    public Rectangle(int x1, int y1, int x2, int y2)
    {
        super(x1, y1);
        if (x2 < x1 || y2 < y1)
        {
            throw new IllegalArgumentException("Координаты второй вершины должны быть больше к-т первой: (x2 > x1 && y2 > y1)!");
        }
        this.x2 = x2;
        this.y2 = y2;
    }
    
    /**
     * Конструктор для наследников - не позволяет полностью задать Прямоугольник
     */
    protected Rectangle(int x, int y)
    {
        super(x, y);
    }
    
    /**
     * Метод получения периметра Прямоугольника
     *
     * @implNote P = (A + B) * 2
     */
    @Override
    public double getPerimeter()
    {
        return 2 * (getSideA() + getSideB());
    }
    
    @Override
    void Draw(int fieldSizeX, int fieldSizeY)
    {
        IContextBuilder builder = super.getTextBuilder(fieldSizeX, fieldSizeY);
        builder.element(new com.indvd00m.ascii.render.elements.Rectangle(x, y, getSideA(), getSideB()));
        super.drawTextFigure(builder);
    }
    
    @Override
    public String toString()
    {
        return new StringJoiner(", ", "[", "]")
                .add("x1 = " + x)
                .add("y1 = " + y)
                .add("x2 = " + x2)
                .add("y2 = " + y2)
                .toString();
    }
}
