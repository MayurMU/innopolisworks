package org.mayurmu.homework9;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


/**
 * Класс реализующий задание 9 "Абстрактные классы и интерфейсы"
 */
public class Task_9
{
    private static final Scanner _scn = new Scanner(System.in);
    private static final Random _rnd = new Random();
    // предельные размеры поля отрисовки псевдографики
    private static final int FIELD_SIZE_X = 150;
    private static final int FIELD_SIZE_Y = 30;

    /**
     * Программа создаёт внеэкранные образы фигур и при выходе сохраняет работу в указанный файл
     * @param args - путь к файлу, куда нужно сохранить работу программы, если не указан, будет запрошен у польз-ля
     */
    public static void main(String[] args)
    {
        String userChoice;
        try
        {
            do
            {
                System.out.println("Выберите, какую фигуру Вы хотите нарисовать (ПРОБЕЛ для выхода), варианты: ");
                System.out.append('\t').println(Arrays.toString(Arrays.stream(FigureType.values()).skip(1).toArray()));
                System.out.print(">_: ");
                userChoice = _scn.nextLine();
                if (userChoice.equalsIgnoreCase(" "))
                {
                    return;
                }
                FigureType figType = FigureType.getFigureTypeByShortCut(userChoice.trim());
                if (figType == FigureType.NONE)
                {
                    System.out.println("Такой фигуры нет: " + userChoice);
                    continue;
                }
                System.out.append("Генерирую случайный ").append(figType.name).println("...");
                
                Figure fig = null;
                int randX = _rnd.nextInt(FIELD_SIZE_X / 2);
                int randY = _rnd.nextInt(FIELD_SIZE_Y / 2);
                
                switch (figType)
                {
                    case CIRCLE:
                    {
                        fig = new Circle(randX, randY, _rnd.nextInt(Byte.SIZE));
                        break;
                    }
                    case ELLIPSE:
                    {
                        fig = new Ellipse(randX, randY, _rnd.nextInt(Short.SIZE) + Byte.SIZE, _rnd.nextInt(Byte.SIZE) + 1);
                        break;
                    }
                    case RECTANGLE:
                    {
                        fig = new Rectangle(randX, randY, _rnd.nextInt(Short.SIZE) + randX + 1, _rnd.nextInt(Short.SIZE) + randY + 1);
                        break;
                    }
                    case SQUARE:
                    {
                        fig = new Square(randX, randY, _rnd.nextInt(Short.SIZE) + 1);
                        break;
                    }
                }
                System.out.println("Создан " +  figType.name + ": " + fig.toString());
                System.out.println();
                System.out.printf("Его периметр: %g%n", fig.getPerimeter());
                System.out.println();
                System.out.println("Рисую...");
                System.out.println();
                fig.Draw(FIELD_SIZE_X, FIELD_SIZE_Y);
                if (fig instanceof Movable)
                {
                    System.out.println("Данный объект можно двигать - введите любую строку, чтобы выполнить случайное перемещение (ПРОБЕЛ - выход):");
                    if (_scn.nextLine().equalsIgnoreCase(" "))
                    {
                        return;
                    }
                    ((Movable)fig).move(_rnd.nextInt(FIELD_SIZE_X / 2), _rnd.nextInt(FIELD_SIZE_Y / 2));
                    System.out.println("Объект перемещён:");
                    System.out.println(fig);
                    System.out.println();
                    System.out.println("Рисую...");
                    System.out.println();
                    fig.Draw(FIELD_SIZE_X, FIELD_SIZE_Y);
                }
                else
                {
                    System.out.println("Данный объект нельзя двигать!");
                }
            }
            while (!userChoice.equalsIgnoreCase(" "));
        }
        catch (Exception ex)
        {
            System.err.append("Непредвиденная ошибка работы программы:").append(System.lineSeparator()).println(ex);
            ex.printStackTrace();
        }
        finally
        {
            _scn.close();
            System.out.println("Программа завершена");
        }
    }

    
}

