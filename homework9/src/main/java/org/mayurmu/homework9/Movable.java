package org.mayurmu.homework9;

/**
 * Интерфейс для перемещаемых фигур
 */
interface Movable
{
    /**
     * Метод перемещения фигуры в указанную точку
     *
     * @param x - новая к-та X
     * @param y - новая к-иа Y
     */
    void move(int x, int y);
}
