package org.mayurmu.homework9;

import com.indvd00m.ascii.render.api.IContextBuilder;

import java.util.StringJoiner;

/**
 * Класс Эллипс
 *
 * @apiNote Предполагается, что координаты располагаются как на экране - в верхнем левом углу (0; 0), т.е. отрицательных к-т быть НЕ может
 * @implNote Эллипс можно задать через Две полуоси и к-ты центра (т.е. точки их пересечения), т.к. уравнение кривой
 * эллипса всегда описывается формулой: x²/a² + y²/b² = 1, а Фокусное расстояние C и полуоси эллипса связаны соотношением:
 * (a² = b² + c²), откуда можно получить к-ты фокусов Эллипса, т.к. фокусное расстояние С - это половина расстояния между F1 и F2.
 * Эксцентриситет (вытянутость фигуры): E = C / A;
 * (<a href=https://www.mathelp.spb.ru/book1/ellips.htm>Источник</a>)
 */
public class Ellipse extends Figure
{
    // полуоси эллипса - постоянные величины
    private final int bigSemiAxisLength;
    private final int smallSemiAxisLength;
    
    /**
     * Конструктор Эллипса
     *
     * @param x                   К-та X центра эллипса
     * @param y                   К-та Y центра эллипса
     * @param bigSemiAxisLength   Длина большой полуоси эллипса
     * @param smallSemiAxisLength Длина малой полуоси эллипса
     *
     * @throws IllegalArgumentException Когда полуоси не являются положительными числами или не соблюдается {@code bigSemiAxis >= smallSemiAxis}
     */
    public Ellipse(int x, int y, int bigSemiAxisLength, int smallSemiAxisLength)
    {
        super(x, y);
        if (bigSemiAxisLength < 1 || smallSemiAxisLength < 1)
        {
            String mes = String.format("Большая и малая полуоси Эллипса должны быть положительными числами, однако было передано (%d; %d)",
                    bigSemiAxisLength, smallSemiAxisLength);
            throw new IllegalArgumentException(mes);
        }
        else if (bigSemiAxisLength < smallSemiAxisLength)
        {
            throw new IllegalArgumentException("Не соблюдён принцип bigSemiAxisLength >= smallSemiAxisLength");
        }
        this.bigSemiAxisLength = bigSemiAxisLength;
        this.smallSemiAxisLength = smallSemiAxisLength;
    }
    
    /**
     * Метод приближённого расчёта периметра эллипса
     *
     * @implNote Формула для расчёта периметра Эллипса: L = 4 * ((Pi * a * b) + (a - b)^2) / (a + b)
     * <a href=https://microexcel.ru/dlina-ellipsa-kalkulyator/>Формула взята отсюда</a>
     */
    @Override
    public double getPerimeter()
    {
        int semiAxisDifference = bigSemiAxisLength - smallSemiAxisLength;
        double numerator = (Math.PI * bigSemiAxisLength * smallSemiAxisLength) + (semiAxisDifference * semiAxisDifference);
        return 4 * numerator / (bigSemiAxisLength + smallSemiAxisLength);
    }
    
    @Override
    void Draw(int fieldSizeX, int fieldSizeY)
    {
        IContextBuilder builder = super.getTextBuilder(fieldSizeX, fieldSizeY);
        builder.element(new com.indvd00m.ascii.render.elements.Ellipse(x, y, bigSemiAxisLength, smallSemiAxisLength));
        super.drawTextFigure(builder);
    }
    
    @Override
    public String toString()
    {
        return new StringJoiner(", ", "[", "]")
                .add("Большая полуось = " + bigSemiAxisLength)
                .add("Малая полуось = " + smallSemiAxisLength)
                .add("x = " + x)
                .add("y = " + y)
                .toString();
    }
}
