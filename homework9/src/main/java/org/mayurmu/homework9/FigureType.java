package org.mayurmu.homework9;

/**
 * Перечисление фигур для меню
 */
public enum FigureType
{
    NONE("", ""),
    CIRCLE("Окружность", "О"),
    SQUARE("Квадрат", "К"),
    ELLIPSE("Эллипс", "Э"),
    RECTANGLE("Прямоугольник", "П");
    
    private static final FigureType[] figureTypes = FigureType.values();
    
    public final String name;
    public final String shortcut;
    
    FigureType(String name, String shortcut)
    {
        this.name = name;
        this.shortcut = shortcut;
    }
    
    @Override
    public String toString()
    {
        return String.format("%s (%s)", name, shortcut);
    }
    
    /**
     * Метод получения эл-та перечисления фигуры ({@link FigureType}) по её сокращённому имени
     *
     * @param shortCut Короткое название фигуры
     *
     * @return Фигуру с соот-щим кор. именем или {@code FigureType.NONE}, если такой фигуры нет
     */
    public static FigureType getFigureTypeByShortCut(String shortCut)
    {
        for (FigureType ft : figureTypes)
        {
            if (ft.shortcut.equalsIgnoreCase(shortCut))
            {
                return ft;
            }
        }
        return FigureType.NONE;
    }
}
