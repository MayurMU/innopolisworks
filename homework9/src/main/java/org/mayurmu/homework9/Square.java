package org.mayurmu.homework9;

import java.util.StringJoiner;

/**
 * Класс Квадрат
 *
 * @apiNote Предполагается, что координаты располагаются как на экране - в верхнем левом углу (0; 0), т.е. отрицательных к-т быть НЕ может
 * @implNote Для определения квадрата достаточно знать одну вершину (к-ты левого верхнего угла) и длину стороны
 */
public class Square extends Rectangle implements Movable
{
    /**
     * Длина стороны квадрата - постоянная величина
     */
    public final int sideLength;
    
    /**
     * Конструктор Квадрата
     *
     * @param x1         - к-та X верхнего левого угла
     * @param y1         - к-та Y верхнего левого угла
     * @param sideLength - длина стороны
     *
     * @throws IllegalArgumentException При {@code sideLength <= 0}
     */
    public Square(int x1, int y1, int sideLength)
    {
        super(x1, y1);
        if (sideLength < 1)
        {
            throw new IllegalArgumentException("Длина стороны Квадрата должна быть положительной, однако было передано: " + sideLength);
        }
        this.sideLength = sideLength;
        super.x2 = x1 + sideLength;
        super.y2 = y1 + sideLength;
    }
    
    /**
     * Метод перемещения Квадрата в указанную точку
     *
     * @param x - новая к-та X верхнего левого угла
     * @param y - новая к-та Y верхнего левого угла
     */
    @Override
    public void move(int x, int y)
    {
        this.x = x;
        this.y = y;
        super.x2 = x + sideLength;
        super.y2 = y + sideLength;
    }
    
    /**
     * Метод получения периметра Квадрата
     *
     * @implNote Т.к. Квадрат является частным случаем Прямоугольника, то периметр можно было бы считать и по его формуле,
     * но это будет дольше (Формула для расчёта периметра окружности: P = A * 4)
     */
    @Override
    public double getPerimeter()
    {
        return sideLength * 4;
    }
    
    @Override
    public String toString()
    {
        return new StringJoiner(", ", "[", "]")
                .add("Длина стороны = " + sideLength)
                .add("Вершина x = " + x)
                .add("Вершина y = " + y)
                .toString();
    }
}
