--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: bot; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.bot (id, comment, create_date_time, is_alive, key_change_date_time, secret_key, user_name) VALUES ('8046c46e-d907-4f9b-a7ad-bff784511be2', 'Rerum rerum quia ut odit rerum.', '2022-05-05 20:42:48.309', false, '2022-05-07 20:42:48.309', '1642523290:CRWrx2cobx9KpMovQD317uZ1-SEcoOubPQX', 'Marenjohnson_bot');
INSERT INTO public.bot (id, comment, create_date_time, is_alive, key_change_date_time, secret_key, user_name) VALUES ('2d26ebb3-bedf-4eb6-987b-54a2690f51d2', 'Et ratione voluptatibus fugit velit aliquam at dolore.', '2022-06-19 20:42:48.31', true, '2022-09-21 20:42:48.31', '2591716918:MNIof0dwgs3KpOyjWW843lT8-EQmaGfsJYT', 'Trinhreynolds_bot');
INSERT INTO public.bot (id, comment, create_date_time, is_alive, key_change_date_time, secret_key, user_name) VALUES ('fd9e0a00-7bc9-4db3-aea7-6549aa433734', '', '2022-04-16 20:42:48.309', true, '2022-06-14 20:42:48.309', '3479831202:QUEbb3ojjm5ScAsiQW682bG2-SEulRifTHG', 'Dolliegrant_bot');


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.role (id, description, type) VALUES ('7fc46305-aa61-4d87-954b-7be9f7ccfdae', 'Администратор системы (управление Ботами и Клиентами, настройка системы)', 'ADMIN');
INSERT INTO public.role (id, description, type) VALUES ('eb902649-f6d7-4189-9b13-22d89985fa27', 'Пользователь (Поиск и Создание задач-напоминаний)', 'USER');
INSERT INTO public.role (id, description, type) VALUES ('32de4169-a275-48fb-bd60-4da0cee18c7a', 'Гость (только поисковые запросы)', 'GUEST');


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.client (id, email, messenger_chat_id, messenger_user_name, register_date_time, use_email, bot_id, role_id) VALUES ('a9732ffc-d301-49fd-b83f-40bcbe57ecec', 'susann.kohler@hotmail.com', -7158979897477385216, NULL, '2022-05-12 20:42:48.369', false, '8046c46e-d907-4f9b-a7ad-bff784511be2', '7fc46305-aa61-4d87-954b-7be9f7ccfdae');
INSERT INTO public.client (id, email, messenger_chat_id, messenger_user_name, register_date_time, use_email, bot_id, role_id) VALUES ('890f1ac4-be56-464c-b067-4ba92bc52027', 'gregg.hamill@gmail.com', -7009262657704885248, NULL, '2022-01-28 20:42:48.369', true, '8046c46e-d907-4f9b-a7ad-bff784511be2', '7fc46305-aa61-4d87-954b-7be9f7ccfdae');
INSERT INTO public.client (id, email, messenger_chat_id, messenger_user_name, register_date_time, use_email, bot_id, role_id) VALUES ('79e42bd7-1162-4d60-bc11-55cdf81b6633', 'reyes.mccullough@hotmail.com', -4932578135675860992, NULL, '2022-07-11 20:42:48.369', false, '8046c46e-d907-4f9b-a7ad-bff784511be2', 'eb902649-f6d7-4189-9b13-22d89985fa27');
INSERT INTO public.client (id, email, messenger_chat_id, messenger_user_name, register_date_time, use_email, bot_id, role_id) VALUES ('1f76a3f7-987b-4678-8001-307c8cfbe6f0', NULL, -452534756522088960, 'BurtonDickens', '2022-02-24 20:42:48.355', false, '2d26ebb3-bedf-4eb6-987b-54a2690f51d2', '7fc46305-aa61-4d87-954b-7be9f7ccfdae');
INSERT INTO public.client (id, email, messenger_chat_id, messenger_user_name, register_date_time, use_email, bot_id, role_id) VALUES ('11a71f89-78e7-40ee-bebf-915c29fc6237', NULL, -4679533036160733184, NULL, '2022-08-16 20:42:48.355', false, 'fd9e0a00-7bc9-4db3-aea7-6549aa433734', '7fc46305-aa61-4d87-954b-7be9f7ccfdae');


--
-- Data for Name: task; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.task (id, address, city, create_date_time) VALUES ('38907c1c-0ae0-4598-80d2-ef465b10a831', 'переулок Ломоносова', 'Владивосток', '2022-02-13 01:40:33.61');
INSERT INTO public.task (id, address, city, create_date_time) VALUES ('a41d7b2b-957e-4cbe-96fb-d67814dedbdf', 'улица Гоголя', 'Владивосток', '2022-02-28 02:42:05.508');
INSERT INTO public.task (id, address, city, create_date_time) VALUES ('d722432a-94ee-4558-b3d5-45c65634dc26', 'ул. Березовая', 'Владивосток', '2022-06-06 23:09:27.092');
INSERT INTO public.task (id, address, city, create_date_time) VALUES ('225e0e85-9e04-4c7e-bb9b-e5821a1c97df', 'переулок Труда', 'Владивосток', '2022-03-24 05:44:21.459');
INSERT INTO public.task (id, address, city, create_date_time) VALUES ('56a3fd6e-d815-4386-abaa-c230b8a90b6e', 'проспект Победы', 'Владивосток', '2022-02-15 02:21:40.685');


--
-- Data for Name: client_tasks; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.client_tasks (client_id, tasks_id) VALUES ('a9732ffc-d301-49fd-b83f-40bcbe57ecec', 'a41d7b2b-957e-4cbe-96fb-d67814dedbdf');
INSERT INTO public.client_tasks (client_id, tasks_id) VALUES ('a9732ffc-d301-49fd-b83f-40bcbe57ecec', 'd722432a-94ee-4558-b3d5-45c65634dc26');
INSERT INTO public.client_tasks (client_id, tasks_id) VALUES ('890f1ac4-be56-464c-b067-4ba92bc52027', 'd722432a-94ee-4558-b3d5-45c65634dc26');
INSERT INTO public.client_tasks (client_id, tasks_id) VALUES ('890f1ac4-be56-464c-b067-4ba92bc52027', '225e0e85-9e04-4c7e-bb9b-e5821a1c97df');
INSERT INTO public.client_tasks (client_id, tasks_id) VALUES ('79e42bd7-1162-4d60-bc11-55cdf81b6633', 'd722432a-94ee-4558-b3d5-45c65634dc26');
INSERT INTO public.client_tasks (client_id, tasks_id) VALUES ('79e42bd7-1162-4d60-bc11-55cdf81b6633', '56a3fd6e-d815-4386-abaa-c230b8a90b6e');
INSERT INTO public.client_tasks (client_id, tasks_id) VALUES ('1f76a3f7-987b-4678-8001-307c8cfbe6f0', 'd722432a-94ee-4558-b3d5-45c65634dc26');
INSERT INTO public.client_tasks (client_id, tasks_id) VALUES ('11a71f89-78e7-40ee-bebf-915c29fc6237', '38907c1c-0ae0-4598-80d2-ef465b10a831');


--
-- Data for Name: outage_reference; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('e3db962e-375d-4e0d-b0cb-a9b64eb3de0f', 'Магнитогорск', 'Est incidunt qui iure in.', '2022-09-24 15:00:00', 'Техническое обслуживание ЛЭП 7,0 кВ Л-0 от ТП-512', '2022-09-24 09:00:00', 'улица Железнодорожная 3-37');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('5c0877cd-ce6b-496b-a713-a0f9c01a9cbe', 'Иркутск', '-//-', '2022-09-20 20:00:00', 'Техническое обслуживание ЛЭП 6,3 кВ Л-4 от ТП-292', '2022-09-20 12:00:00', 'проспект Энергетиков 922, 431, 866, 416, 208; пр. Шевченко 86-130; пер. Труда 13-75; ул. Октябрьская 856, 465; ул. Зелёная 105-195');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('5bc78802-4573-4536-a516-ef8bfcd41d03', 'Сочи', '-//-', '2022-09-23 20:00:00', 'Техническое обслуживание ЛЭП 9,1 кВ Л-8 от ТП-121', '2022-09-23 12:00:00', 'переулок Свердлова 670');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('12813890-ecf6-4b7c-a1b4-78a75933f1eb', 'Санкт-Петербург', 'Quia vel quo hic amet et repudiandae ipsam.', '2022-09-20 17:00:00', 'Техническое обслуживание ЛЭП 3,0 кВ Л-7 от ТП-817', '2022-09-20 12:00:00', 'ул. Парковая 6-66, 71-82, 87-166, 173-175, 177-207');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('4d335647-fe86-492b-959f-9091d36ac3a2', 'Краснодар', 'Itaque sint nemo fuga et.', '2022-09-24 13:00:00', 'Техническое обслуживание ЛЭП 8,5 кВ Л-1 от ТП-011', '2022-09-24 09:00:00', 'улица Горная 1-25, 27-57, 62-90');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('5f0021a9-48ca-4351-ab88-f9a830775371', 'Саратов', '-//-', '2022-09-22 13:00:00', 'Техническое обслуживание ЛЭП 8,7 кВ Л-9 от ТП-356', '2022-09-22 09:00:00', 'переулок Победы');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('f34f2594-9c35-4efd-9f39-73fba33d0365', 'Екатеринбург', '-//-', '2022-09-24 16:00:00', 'Техническое обслуживание ЛЭП 6,4 кВ Л-4 от ТП-159', '2022-09-24 09:00:00', 'ул. Колхозная 214');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('bef00a2d-6abb-44a8-ae8a-5d6dbd3fa20a', 'Ижевск', '-//-', '2022-09-25 18:00:00', 'Техническое обслуживание ЛЭП 9,6 кВ Л-4 от ТП-801', '2022-09-25 11:00:00', 'улица Трактовая');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('240564af-1ec1-40e4-9261-acb4c790c89c', 'Краснодар', '-//-', '2022-09-20 16:00:00', 'Техническое обслуживание ЛЭП 3,1 кВ Л-2 от ТП-967', '2022-09-20 11:00:00', 'пл. Дорожная 4-59, 64-83, 84-111');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('5670bee6-37ba-4af0-91c3-5e3ebf9d9323', 'Кемерово', '-//-', '2022-09-19 20:00:00', 'Техническое обслуживание ЛЭП 5,8 кВ Л-0 от ТП-364', '2022-09-19 12:00:00', 'площадь Центральная');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('6fd5deaf-d01b-41a3-ac0a-78bf2fbc8504', 'Астрахань', 'Veritatis non assumenda.', '2022-09-18 14:00:00', 'Техническое обслуживание ЛЭП 6,6 кВ Л-1 от ТП-418', '2022-09-18 09:00:00', 'улица Красная');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('c5e29d7e-86ff-49dd-8153-7f3fcc7742e4', 'Тольятти', 'Aspernatur natus placeat.', '2022-09-19 18:00:00', 'Техническое обслуживание ЛЭП 6,5 кВ Л-7 от ТП-549', '2022-09-19 12:00:00', 'улица Трудовая 651, 339, 918');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('2035a93f-eac9-4935-9cf7-4a2034070b3c', 'Иркутск', 'Rerum totam ut quis vero molestiae tenetur.', '2022-09-18 14:00:00', 'Техническое обслуживание ЛЭП 8,9 кВ Л-4 от ТП-127', '2022-09-18 09:00:00', 'пр. Школьный 6-127, 130-133, 133-134');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('140f504f-c95c-4f24-99de-7ddc111b363a', 'Москва', '-//-', '2022-09-23 18:00:00', 'Техническое обслуживание ЛЭП 7,8 кВ Л-2 от ТП-682', '2022-09-23 11:00:00', 'проспект Дзержинского 132, 741, 379, 323');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('2a1a029f-d6f6-4a94-ab15-b6bbac617c63', 'Иваново', '-//-', '2022-09-19 15:00:00', 'Техническое обслуживание ЛЭП 4,8 кВ Л-8 от ТП-326', '2022-09-19 09:00:00', 'пр. Дзержинского 270, 382, 390; ул. Школьная 2-106, 111-216, 220-293; пер. Куйбышева 731, 26, 852, 652, 820; пл. Строительная 8-18, 23-26, 31-114, 116-118, 122-167; улица Степная 8-89, 96-169, 175-297, 299-314');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('f6a04c1e-ae8d-4645-ac37-10e80bfc94d2', 'Тула', '-//-', '2022-09-24 18:00:00', 'Техническое обслуживание ЛЭП 7,5 кВ Л-1 от ТП-791', '2022-09-24 11:00:00', 'Весь населённый пункт');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('5aa433e3-f84f-4ba1-bb40-892d5db895e2', 'Рязань', '-//-', '2022-09-19 15:00:00', 'Техническое обслуживание ЛЭП 4,4 кВ Л-3 от ТП-240', '2022-09-19 11:00:00', 'ул. Цветочная 700');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('72382fb0-0c09-4a93-b217-26280a3b3364', 'Чебоксары', 'Et voluptas praesentium ut consectetur ea.', '2022-09-21 14:00:00', 'Техническое обслуживание ЛЭП 4,7 кВ Л-7 от ТП-641', '2022-09-21 10:00:00', 'пл. Партизанская 4-33, 33-48');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('011e8eb2-e6d4-4d0b-b73b-a14791af702b', 'Ставрополь', '-//-', '2022-09-24 16:00:00', 'Техническое обслуживание ЛЭП 0,6 кВ Л-5 от ТП-363', '2022-09-24 09:00:00', 'улица Луговая 44-83');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('f5caa5b8-6e8b-4cef-89b5-79b8c3914a6b', 'Рязань', 'Praesentium repellendus fugiat sed.', '2022-09-21 16:00:00', 'Техническое обслуживание ЛЭП 1,6 кВ Л-9 от ТП-730', '2022-09-21 11:00:00', 'пер. Некрасова 137');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('09c330b0-e728-4f00-aed6-106e5535e6b5', 'Тула', 'Asperiores hic voluptate magni quis in.', '2022-09-20 15:00:00', 'Техническое обслуживание ЛЭП 2,9 кВ Л-8 от ТП-386', '2022-09-20 09:00:00', 'пл. Северная 4-103, 105-135, 140-173, 174-262, 262-274; улица Клубная 104-158; пл. Подгорная 459, 273, 518; пл. Степная 5-41, 46-170, 177-249; площадь Горная 3-28, 33-140; площадь Рабочая 55, 239, 405, 680; пл. Южная 61-180; пер. Мичурина 94-214');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('712d3e3e-39e1-40c8-9960-85f9649f08da', 'Оренбург', '-//-', '2022-09-18 18:00:00', 'Техническое обслуживание ЛЭП 5,0 кВ Л-6 от ТП-446', '2022-09-18 12:00:00', 'площадь Коммунистическая 52-107');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('c3a0814b-6851-4ff5-89f6-59b7bc134f68', 'Томск', 'Corporis ipsum minus reiciendis.', '2022-09-22 16:00:00', 'Техническое обслуживание ЛЭП 1,1 кВ Л-1 от ТП-398', '2022-09-22 12:00:00', 'пл. Первомайская');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('7ccffed3-7da4-4c76-916e-4132c4b9ee1b', 'Воронеж', 'Aut odio fugiat molestiae hic quia facilis.', '2022-09-20 16:00:00', 'Техническое обслуживание ЛЭП 6,6 кВ Л-3 от ТП-409', '2022-09-20 10:00:00', 'площадь Партизанская 54-140');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('0a28854b-02f6-441e-be1c-488e4bd6431a', 'Тула', 'Fugit facere doloremque rerum repellendus sint labore ipsum.', '2022-09-19 17:00:00', 'Техническое обслуживание ЛЭП 6,8 кВ Л-8 от ТП-419', '2022-09-19 10:00:00', 'пр. Чехова 504');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('23618e72-3467-40a2-ab1d-f6f1b085e84b', 'Тверь', '-//-', '2022-09-25 19:00:00', 'Техническое обслуживание ЛЭП 5,5 кВ Л-0 от ТП-054', '2022-09-25 12:00:00', 'пос. Газовиков');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('dd0d30a9-59c4-482e-a389-56508efada96', 'Нижний Новгород', 'Voluptatem ex laudantium inventore.', '2022-09-23 19:00:00', 'Техническое обслуживание ЛЭП 0,2 кВ Л-9 от ТП-602', '2022-09-23 12:00:00', 'проспект Чапаева');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('8afd0f0a-85df-4055-86d8-8dc9215a2bb9', 'Новосибирск', 'Dolor sit quo eos.', '2022-09-23 20:00:00', 'Техническое обслуживание ЛЭП 9,6 кВ Л-0 от ТП-976', '2022-09-23 12:00:00', 'улица Степная 116-131; площадь Цветочная 575, 683, 595, 175; улица Береговая 50, 869; пл. Шоссейная 97, 997, 426, 376');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('a8a5d6c3-0547-4c07-b805-d9f9283f30a3', 'Нижний Новгород', 'Ipsum architecto distinctio aut alias ut.', '2022-09-21 16:00:00', 'Техническое обслуживание ЛЭП 6,1 кВ Л-9 от ТП-254', '2022-09-21 11:00:00', 'пер. Калинина 86-164');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('e0a17df0-82e7-4ef6-951e-95c48af9a0b6', 'Челябинск', 'Quibusdam blanditiis consequatur.', '2022-09-22 17:00:00', 'Техническое обслуживание ЛЭП 8,0 кВ Л-0 от ТП-486', '2022-09-22 11:00:00', 'площадь Клубная 111-192; улица Озерная 906; переулок Победы 73-87');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('af45f5cc-a5c7-4ffb-b30b-ba6ce907677a', 'Челябинск', 'Voluptas vero odio veniam minus.', '2022-09-25 18:00:00', 'Техническое обслуживание ЛЭП 8,3 кВ Л-4 от ТП-666', '2022-09-25 12:00:00', 'площадь Береговая 504');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('ef502fff-6687-4ab0-8aaf-54a65ac35975', 'Тула', 'Ut dolorem provident.', '2022-09-25 17:00:00', 'Техническое обслуживание ЛЭП 5,7 кВ Л-4 от ТП-273', '2022-09-25 09:00:00', 'площадь Центральная 652');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('b233a2a5-4062-4bb9-a061-e0ea3122442b', 'Омск', '-//-', '2022-09-23 13:00:00', 'Техническое обслуживание ЛЭП 6,9 кВ Л-8 от ТП-039', '2022-09-23 09:00:00', 'пр. Маяковского 132, 413, 252, 867, 89');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('d88308f0-950f-45e9-82ca-33ee11cc7241', 'Махачкала', 'Numquam est aut vel.', '2022-09-21 18:00:00', 'Техническое обслуживание ЛЭП 7,3 кВ Л-8 от ТП-357', '2022-09-21 11:00:00', 'улица Рабочая');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('532965ba-29a1-4731-ba70-a37ec5277a49', 'Тольятти', 'Suscipit debitis rerum ab eos omnis.', '2022-09-24 16:00:00', 'Техническое обслуживание ЛЭП 4,5 кВ Л-2 от ТП-745', '2022-09-24 09:00:00', 'Весь населённый пункт');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('be6a4bf5-6387-4ea0-ac78-83d731712993', 'Владимир', '-//-', '2022-09-22 14:00:00', 'Техническое обслуживание ЛЭП 1,7 кВ Л-2 от ТП-619', '2022-09-22 09:00:00', 'улица Пионерская 404, 389, 639, 898; улица Клубная 16-104; пр. Матросова 561, 150, 991, 649; проспект Труда 64-173');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('47c28b40-4f65-49b6-935b-464f29b4a6ce', 'Екатеринбург', '-//-', '2022-09-19 15:00:00', 'Техническое обслуживание ЛЭП 0,0 кВ Л-2 от ТП-617', '2022-09-19 09:00:00', 'пр. Комарова 559, 605, 265, 949; площадь Почтовая 107-199; улица Молодежная 83-85; площадь Комсомольская 97-151; пл. Коммунистическая 124-134; пр. Суворова 29; площадь Школьная 8-20, 22-104, 105-211, 211-335, 337-370; площадь Луговая 551');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('8e6885d0-3a75-4fb7-aaf8-44b8c1e49062', 'Краснодар', '-//-', '2022-09-19 18:00:00', 'Техническое обслуживание ЛЭП 3,2 кВ Л-6 от ТП-367', '2022-09-19 10:00:00', 'улица Клубная 503, 790, 574');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('92711013-95aa-4ddb-9029-9cf484983e9f', 'Махачкала', '-//-', '2022-09-21 17:00:00', 'Техническое обслуживание ЛЭП 5,3 кВ Л-5 от ТП-383', '2022-09-21 10:00:00', 'пр. Островского 731');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('6ec34178-9c64-42dc-985e-9f4995cb4bde', 'Чебоксары', '-//-', '2022-09-19 15:00:00', 'Техническое обслуживание ЛЭП 8,4 кВ Л-6 от ТП-779', '2022-09-19 11:00:00', 'переулок Крупской 6-97, 99-171, 178-192, 194-222');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('92913730-da53-44af-af75-7f503e3bb6fe', 'Пермь', 'Illum quibusdam harum cum ea repellendus.', '2022-09-18 17:00:00', 'Техническое обслуживание ЛЭП 9,1 кВ Л-1 от ТП-319', '2022-09-18 12:00:00', 'ул. Шоссейная 823, 29, 663');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('d984b87f-206b-472d-ade2-c551f2f5837b', 'Барнаул', 'Aut harum exercitationem voluptate voluptatem molestiae id.', '2022-09-24 17:00:00', 'Техническое обслуживание ЛЭП 9,6 кВ Л-6 от ТП-239', '2022-09-24 11:00:00', 'площадь Красноармейская 43-137; ул. Майская 3-91, 91-112, 119-164, 170-211; ул. Красноармейская 984, 629, 623; пл. Заводская 2-48, 54-63, 69-93; переулок Горького 46-95; улица Октябрьская 3-116, 117-176, 178-222; пер. Чехова 72-132; пл. Нагорная 3-71, 76-143, 150-204, 205-247');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('97e46a65-4daa-40e8-a367-e90180d0cc60', 'Тула', '-//-', '2022-09-20 18:00:00', 'Техническое обслуживание ЛЭП 0,7 кВ Л-3 от ТП-629', '2022-09-20 11:00:00', 'пер. Матросова 648, 23, 571, 833, 979');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('bc8f99a3-b5dc-4e33-95de-f0844e37cacd', 'Тольятти', 'Non sunt repellat illum consequatur quia exercitationem.', '2022-09-19 20:00:00', 'Техническое обслуживание ЛЭП 2,3 кВ Л-9 от ТП-102', '2022-09-19 12:00:00', 'переулок Дружбы');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('2dc23cf7-456a-4b37-b3d7-76dd2c450ced', 'Воронеж', '-//-', '2022-09-20 14:00:00', 'Техническое обслуживание ЛЭП 1,1 кВ Л-4 от ТП-960', '2022-09-20 10:00:00', 'площадь Юбилейная 178, 996');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('5dc78909-1b27-4898-989d-e75e424d31d4', 'Краснодар', '-//-', '2022-09-25 14:00:00', 'Техническое обслуживание ЛЭП 6,8 кВ Л-9 от ТП-474', '2022-09-25 10:00:00', 'пер. Свердлова 102-122');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('861121b3-a79e-45ac-a790-cb52b4b9eff4', 'Краснодар', 'Quo velit numquam.', '2022-09-24 18:00:00', 'Техническое обслуживание ЛЭП 0,9 кВ Л-7 от ТП-529', '2022-09-24 10:00:00', 'пл. Сосновая 4-43');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('e387dcb6-02e4-401f-896f-93edbca4f8f6', 'Тула', '-//-', '2022-09-21 18:00:00', 'Техническое обслуживание ЛЭП 1,5 кВ Л-5 от ТП-172', '2022-09-21 11:00:00', 'пр. Суворова 84-203');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('13ff86f0-7d64-44d2-b66c-30e1e241b71f', 'Екатеринбург', '-//-', '2022-09-24 13:00:00', 'Техническое обслуживание ЛЭП 4,9 кВ Л-2 от ТП-448', '2022-09-24 09:00:00', 'пр. Гагарина 10-92');
INSERT INTO public.outage_reference (id, city, comments, end_date_time, reason, start_date_time, streets_and_houses) VALUES ('2860942a-6868-4b42-bf2a-94810a313168', 'Ульяновск', '-//-', '2022-09-19 16:00:00', 'Техническое обслуживание ЛЭП 3,9 кВ Л-7 от ТП-712', '2022-09-19 10:00:00', 'пос. Газовиков');


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.hibernate_sequence', 1, true);


--
-- PostgreSQL database dump complete
--

