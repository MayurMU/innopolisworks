--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET CLIENT_ENCODING TO 'WIN866';
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: gor_svet_test; Type: DATABASE; Schema: -; Owner: -
--
DROP DATABASE IF EXISTS gor_svet_test;

CREATE DATABASE gor_svet_test WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Russian_Russia.1251';


\connect gor_svet_test

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE gor_svet_test; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON DATABASE gor_svet_test IS 'База под бота Телеграм по плановым отключениям света.';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: bot; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bot (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    comment character varying(255),
    create_date_time timestamp without time zone NOT NULL,
    is_alive boolean NOT NULL,
    key_change_date_time timestamp without time zone NOT NULL,
    secret_key character varying(255) NOT NULL,
    user_name character varying(30) NOT NULL
);


--
-- Name: client; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.client (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    email character varying(50),
    messenger_chat_id bigint NOT NULL,
    messenger_user_name character varying(50),
    register_date_time timestamp without time zone NOT NULL,
    use_email boolean NOT NULL,
    bot_id uuid NOT NULL,
    role_id uuid NOT NULL
);


--
-- Name: client_tasks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.client_tasks (
    client_id uuid NOT NULL,
    tasks_id uuid NOT NULL
);


--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: outage_reference; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.outage_reference (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    city character varying(255) NOT NULL,
    comments character varying(255),
    end_date_time timestamp without time zone NOT NULL,
    reason character varying(255),
    start_date_time timestamp without time zone NOT NULL,
    streets_and_houses character varying(2000)
);


--
-- Name: role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.role (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    description character varying(255),
    type character varying(255) NOT NULL
);


--
-- Name: task; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.task (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    address character varying(255) NOT NULL,
    city character varying(50) NOT NULL,
    create_date_time timestamp without time zone NOT NULL
);


--
-- Name: bot bot_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bot
    ADD CONSTRAINT bot_pkey PRIMARY KEY (id);


--
-- Name: client client_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- Name: client_tasks client_tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client_tasks
    ADD CONSTRAINT client_tasks_pkey PRIMARY KEY (client_id, tasks_id);


--
-- Name: outage_reference outage_reference_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.outage_reference
    ADD CONSTRAINT outage_reference_pkey PRIMARY KEY (id);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: task task_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);


--
-- Name: bot uk_2eh1dwwywk7wryixfsc30d3gi; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bot
    ADD CONSTRAINT uk_2eh1dwwywk7wryixfsc30d3gi UNIQUE (secret_key);


--
-- Name: role uk_93vn3jeavtylq20tjdx2p2kkd; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT uk_93vn3jeavtylq20tjdx2p2kkd UNIQUE (type);


--
-- Name: client uk_bfgjs3fem0hmjhvih80158x29; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT uk_bfgjs3fem0hmjhvih80158x29 UNIQUE (email);


--
-- Name: client uk_mk6q60b5c7vbytwf9kp644ypc; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT uk_mk6q60b5c7vbytwf9kp644ypc UNIQUE (messenger_user_name);


--
-- Name: task uk_task_city_address; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT uk_task_city_address UNIQUE (city, address);


--
-- Name: client uk_vmtcx42xe7r9omm4dddr8ibg; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT uk_vmtcx42xe7r9omm4dddr8ibg UNIQUE (messenger_chat_id);


--
-- Name: ix_outage_reference_city; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_outage_reference_city ON public.outage_reference USING btree (city);


--
-- Name: client fk_client_bot_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT fk_client_bot_id FOREIGN KEY (bot_id) REFERENCES public.bot(id);


--
-- Name: client fk_client_role_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT fk_client_role_id FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- Name: client_tasks fk_client_tasks_client_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client_tasks
    ADD CONSTRAINT fk_client_tasks_client_id FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_tasks fk_client_tasks_tasks_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client_tasks
    ADD CONSTRAINT fk_client_tasks_tasks_id FOREIGN KEY (tasks_id) REFERENCES public.task(id);


--
-- PostgreSQL database dump complete
--

