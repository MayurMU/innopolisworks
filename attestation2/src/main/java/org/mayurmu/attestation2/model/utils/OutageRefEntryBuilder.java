package org.mayurmu.attestation2.model.utils;

import org.mayurmu.attestation2.model.OutageRefEntry;

import java.time.LocalDateTime;


public final class OutageRefEntryBuilder
{
    private String city = "";
    private String streetsAndHouses;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private String reason;
    private String comments;
    
    private OutageRefEntryBuilder()
    {
    }
    
    public static OutageRefEntryBuilder createBuilder()
    {
        return new OutageRefEntryBuilder();
    }
    
    public OutageRefEntryBuilder city(String city)
    {
        this.city = city;
        return this;
    }
    
    public OutageRefEntryBuilder streetsAndHouses(String streetsAndHouses)
    {
        this.streetsAndHouses = streetsAndHouses;
        return this;
    }
    
    public OutageRefEntryBuilder startDateTime(LocalDateTime startDateTime)
    {
        this.startDateTime = startDateTime;
        return this;
    }
    
    public OutageRefEntryBuilder endDateTime(LocalDateTime endDateTime)
    {
        this.endDateTime = endDateTime;
        return this;
    }
    
    public OutageRefEntryBuilder reason(String reason)
    {
        this.reason = reason;
        return this;
    }
    
    public OutageRefEntryBuilder comments(String comments)
    {
        this.comments = comments;
        return this;
    }
    
    /**
     * @exception IllegalStateException Когда Дата начала > Даты окончания
     */
    public OutageRefEntry build()
    {
        if (startDateTime == null || endDateTime == null)
        {
            throw new IllegalStateException("Обе даты, и Дата начала и Дата окончания отключения должны быть заданы!");
        }
        if (endDateTime.isBefore(startDateTime))
        {
            throw new IllegalStateException("Дата начала отключения не может быть позже Даты окончания, проверьте данные!");
        }
        return new OutageRefEntry(city, streetsAndHouses, startDateTime, endDateTime, reason, comments);
    }
}
