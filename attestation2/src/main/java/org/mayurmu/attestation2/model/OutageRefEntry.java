package org.mayurmu.attestation2.model;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.UUID;

/**
 * Справочник отключений - приходит из внешней системы в результате парсинга сайта ДонЭнерго.
 *
 * @implNote Возможно таблица плохо нормализована, т.к. Город присутствует по нескольку раз, но выносить его смысла не вижу.
 *          Поэтому сделал на него индекс.
 */
@Entity
@Table(name = "outage_reference", indexes = @Index(name = "ix_outage_city", columnList = "city"))
public class OutageRefEntry
{
    
    //region 'Поля'
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    @ColumnDefault(value = "gen_random_uuid()")
    private UUID id;
    
    /**
     * Населённый пункт
     */
    @Column(name = "city", nullable = false)
    private String city;
    
    /**
     * Список диапазонов адресов отключений
     *
     * @apiNote Видно, что диапазоны по разным улицам разделены (;).
     *          <p>Примеры:</p>
     *          <ul>
     *              <li>{@link #city} = х. Калининский, {@code #streetsAndHouses} = пос. Газовиков</li>
     *              <li>{@link #city} = ст. Казанская, {@code #streetsAndHouses} = Весь населённый пункт</li>
     *              <li>просп. Баклановский 142/1</li>
     *              <li>ул. Б. Хмельницкого 56 кв. 3</li>
     *              <li>ул. Набережная 5, 23, 33</li>
     *              <li>ул. Высоковольтная 4-27</li>
     *              <li>
     *                  ул. Б. Хмельницкого 57-часть квартир, 59-79, 83-85, 87-часть квартир; ул. Комитетская 164-188,
     *                      157-177; ул. Красноармейская 111
     *              </li>
     *              <li>ул. Седова 165-247, 34 А-40</li>
     *              <li>ул. 13-я линия 12-32, 5-27;</li>
     *          </ul>
     */
    @Column(name = "streets_and_houses", length = 2000)
    private String streetsAndHouses;
    
    /**
     * Дата начала отключения электроэнергии
     */
    @Column(name = "start_date_time", nullable = false)
    private LocalDateTime startDateTime;
    
    /**
     * Дата окончания отключения электроэнергии
     *
     * @implNote Соответствие дате Начала ({@link #startDateTime}) не проверяется, т.к. данные приходят через парсинг.
     */
    @Column(name = "end_date_time", nullable = false)
    private LocalDateTime endDateTime;
    
    /**
     * Причина отключения, как правило что-то вроде:
     * <pre>
     *     Техническое обслуживание ЛЭП 0,4 кВ Л-6 от ТП-87
     * </pre>
     */
    @Column(name = "reason")
    private String reason;
    
    /**
     * Почти всегда пустой (-), но бывает и так:
     *  <pre>
     *      {@link #city} = г. Гуково
     *      Заявок на 17.09.22-18.09.22 нет
     *  </pre>
     */
    @Column(name = "comments")
    private String comments;
    
    //endregion 'Поля'
    
    
    
    
    //region 'Конструкторы'
    
    protected OutageRefEntry()
    {
        city = "";
        startDateTime = LocalDateTime.now();
        endDateTime = startDateTime.plusHours(1);
    }
    
    public OutageRefEntry(String city, String streetsAndHouses, LocalDateTime startDateTime, LocalDateTime endDateTime,
                          String reason,
                          String comments)
    {
        this.city = city;
        this.streetsAndHouses = streetsAndHouses;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.reason = reason;
        this.comments = comments;
    }
    
    //endregion 'Конструкторы'
    
    
    
    
    //region 'Свойства'
    
    public String getReason()
    {
        return reason;
    }
    
    public void setReason(String reason)
    {
        this.reason = reason;
    }
    
    public String getComments()
    {
        return comments;
    }
    
    public void setComments(String comments)
    {
        this.comments = comments;
    }
    
    public LocalDateTime getEndDateTime()
    {
        return endDateTime;
    }
    
    public void setEndDateTime(LocalDateTime endDateAndTime)
    {
        this.endDateTime = endDateAndTime;
    }
    
    public String getStreetsAndHouses()
    {
        return streetsAndHouses;
    }
    
    public void setStreetsAndHouses(String streetsAndHouses)
    {
        this.streetsAndHouses = streetsAndHouses;
    }
    
    public UUID getId()
    {
        return id;
    }
    
    public void setId(UUID id)
    {
        this.id = id;
    }
    
    public String getCity()
    {
        return city;
    }
    
    public void setCity(String city)
    {
        this.city = city;
    }
    
    public LocalDateTime getStartDateTime()
    {
        return startDateTime;
    }
    
    public void setStartDateTime(LocalDateTime startDateAndTime)
    {
        this.startDateTime = startDateAndTime;
    }
    
    //endregion 'Свойства'
    
    
    
    
    //region 'Методы'
    
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        
        OutageRefEntry that = (OutageRefEntry) o;
        
        if (!city.equals(that.city))
        {
            return false;
        }
        if (!Objects.equals(streetsAndHouses, that.streetsAndHouses))
        {
            return false;
        }
        if (!startDateTime.equals(that.startDateTime))
        {
            return false;
        }
        return endDateTime.equals(that.endDateTime);
    }
    
    @Override
    public int hashCode()
    {
        int result = city.hashCode();
        result = 31 * result + (streetsAndHouses != null ? streetsAndHouses.hashCode() : 0);
        result = 31 * result + startDateTime.hashCode();
        result = 31 * result + endDateTime.hashCode();
        return result;
    }
    
    @Override
    public String toString()
    {
        return new StringJoiner("\n", OutageRefEntry.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("\tcity='" + city + "'")
                .add("\tstreetsAndHouses='" + streetsAndHouses + "'")
                .add("\tstartDateTime=" + startDateTime)
                .add("\tendDateTime=" + endDateTime)
                .add("\treason='" + reason + "'")
                .add("\tcomments='" + comments + "'")
                .toString();
    }
    
    //endregion 'Методы'
}