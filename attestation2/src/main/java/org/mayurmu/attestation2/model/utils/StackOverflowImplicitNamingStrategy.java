package org.mayurmu.attestation2.model.utils;


import org.hibernate.boot.model.naming.*;
import javax.persistence.*;

import java.util.stream.Collectors;

/**
 * Класс реализующий Кастомную стратегию именования ограничений БД создаваемых Hibernate (вместо стандартной, вроде:
 *      {@code UK_88x4vo88ix1k9jnovcylc55t9})
 *
 * @apiNote Вроде как должна работать только для аннотации {@link UniqueConstraint}, но не для {@link Column#unique()}
 *      из-за бага в Hibernate.
 *
 * @implNote Пример:
 *     <pre>
 *         alter table ASSET add constraint UK_ASSET_SYSTEM_NODE_ID unique (SYSTEM_NODE_ID)
 *     </pre>
 *
 * @see <a href="https://hibernate.atlassian.net/browse/HHH-11586">На основе кода отсюда</a>
 */
public class StackOverflowImplicitNamingStrategy extends ImplicitNamingStrategyJpaCompliantImpl
{
    private static final long serialVersionUID = 1L;
    
    @Override
    public Identifier determineForeignKeyName(ImplicitForeignKeyNameSource source)
    {
        String name = "FK_" + source.getTableName() + "_" + source.getColumnNames()
                .stream()
                .map(Identifier::toString)
                .collect(Collectors.joining("_"));
        
        return toIdentifier(name, source.getBuildingContext());
    }
    
    @Override
    public Identifier determineIndexName(ImplicitIndexNameSource source)
    {
        String name = "IX_" + source.getTableName() + "_" + source.getColumnNames()
                .stream()
                .map(Identifier::toString)
                .collect(Collectors.joining("_"));
        
        return toIdentifier(name, source.getBuildingContext());
    }
    
    @Override
    public Identifier determineUniqueKeyName(ImplicitUniqueKeyNameSource source)
    {
        String name = "UK_" + source.getTableName() + "_" + source.getColumnNames()
                .stream()
                .map(Identifier::toString)
                .collect(Collectors.joining("_"));
        
        return toIdentifier(name, source.getBuildingContext());
    }
}
