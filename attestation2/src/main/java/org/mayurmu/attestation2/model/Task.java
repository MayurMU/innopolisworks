package org.mayurmu.attestation2.model;

import org.hibernate.annotations.ColumnDefault;
import org.mayurmu.attestation2.model.types.UUIDs;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Задача по отслеживанию отключений света по конкретному адресу (для оповещения Юзера)
 */
@Entity
@Table(name = "task", uniqueConstraints = @UniqueConstraint(name = "UK_city_and_address", columnNames = {"city", "address"}))
public class Task
{
    
    //region 'Поля'
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    @ColumnDefault(value = "gen_random_uuid()")
    private UUID id = UUIDs.EMPTY;
    
    @Column(name = "city", nullable = false, length = 50)
    private String city;
    
    /**
     * Строка для поиска в списке отключений
     *
     * @apiNote По умолчанию в произвольном виде, хотя, конечно, лучше бы это как-то валидировать или давать Юзеру только
     *          выбор на основе БД Адресов, чтобы он не мог создавать бесполезные задачи для заведомо несуществующих адресов.
     */
    @Column(name = "address", nullable = false)
    private String address;
    
    /**
     * Дата создания задачи (больше для исторических целей или для статистики).
     */
    @Column(name = "create_date_time", nullable = false)
    private LocalDateTime createDateTime;
    
    /**
     * Список клиентов подписанных на эту задачу
     */
    @ManyToMany(mappedBy = "tasks", cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private Set<Client> clients = new HashSet<>();
    
    //endregion 'Поля'
    
    
    
    
    //region 'Конструкторы'
    
    protected Task()
    {
    }
    
    public Task(String city, String address, LocalDateTime createDateTime)
    {
        this.city = city;
        this.address = address;
        this.createDateTime = createDateTime;
    }
    
    //endregion 'Конструкторы'
    
    
    
    
    //region 'Свойства'
    
    public String getCity()
    {
        return city;
    }
    
    public void setCity(String city)
    {
        this.city = city;
    }
    
    public Set<Client> getClients()
    {
        return clients;
    }
    
    public void setClients(Set<Client> clients)
    {
        this.clients = clients;
    }
    
    public LocalDateTime getCreateDateTime()
    {
        return createDateTime;
    }
    
    public void setCreateDateTime(LocalDateTime createDateTime)
    {
        this.createDateTime = createDateTime;
    }
    
    public String getAddress()
    {
        return address;
    }
    
    public void setAddress(String targetAddress)
    {
        this.address = targetAddress;
    }
    
    public UUID getId()
    {
        return id;
    }
    
    public void setId(UUID id)
    {
        this.id = id;
    }
    
    //endregion 'Свойства'
    
    
    
    
    //region 'Методы'
    
    /**
     * Равными считаем задачи с совпадающей комбинацией полей {@link #city} {@link #address} (с учётом всех знаков)
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        
        Task task = (Task) o;
        
        if (!city.equals(task.city))
        {
            return false;
        }
        return Objects.equals(address, task.address);
    }
    
    @Override
    public int hashCode()
    {
        int result = city.hashCode();
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }
    
    @Override
    public String toString()
    {
        return toString("");
    }
    
    /**
     * Осуществляет более подробный вывод (с кол-влм связанных данных)
     *
     * @implNote Осторожно: при {@code format = "f"} вероятно будут выполняться подзапросы для выборки связей.
     *
     * @param format Поддерживается только "f" | "F" = полный (с кол-вом связанных сущностей), Если передана пустая строка,
     *               то вызов эквивалентен методу {@link #toString()}
     */
    public String toString(String format)
    {
        StringJoiner res = new StringJoiner(", ", Task.class.getSimpleName() + ": [", "]")
                .add("id=" + id)
                .add("city='" + city + "'")
                .add("address='" + address + "'")
                .add("createDateTime=" + createDateTime);
        if (format.trim().equalsIgnoreCase("f"))
        {
            res.add("clients count = " + clients.size());
        }
        return res.toString();
    }
    
    //endregion 'Методы'
    
}