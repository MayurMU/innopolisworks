package org.mayurmu.attestation2.utils;

import net.datafaker.Faker;
import org.mayurmu.attestation2.model.*;
import org.mayurmu.attestation2.model.utils.BotBuilder;
import org.mayurmu.attestation2.model.utils.OutageRefEntryBuilder;
import org.mayurmu.common.Utils;
import org.mayurmu.attestation2.model.types.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Вспомогательный класс генерации коллекций тестовых данных для 14-й Задачи
 */
public class TestDataGenerator
{
    /**
     * Сколько максимум задач может быть сгенерировано для клиента
     */
    public static final int MAX_TASK_COUNT_PER_CLIENT = 10;
    
    
    /**
     * Политика генерации Задач для Клиентов
     *
     * @implNote Предполагается что, пересечения и повторения могут допускаться, т.к. у нас связь Многие-ко-Многим
     */
    public enum TaskGenPolicy
    {
        UNKNOWN,
        /**
         * Может иметь, а может и не иметь одну и более связанных Задач
         */
        VARYING,
        /**
         * Наличие связанных задач обязательно (1 или больше)
         */
        ONE_OR_MORE,
        /**
         * По одной Задаче на Клиента обязательно
         */
        ONLY_ONE,
        /**
         * Связи отсутствуют
         */
        NO_LINKS
    }
    
    
    // кол-во тестовых данных для генерации
    public final int dataLength;
    public final Set<Client> clientCollection = new HashSet<>();
    public final Set<Task> taskCollection = new HashSet<>();
    public final Set<Bot> botCollection = new HashSet<>();
    public final Set<OutageRefEntry> outageRefEntriesCollection = new HashSet<>();
    // Свежий порт библиотеки Perl Data::Faker, по-сравнению с JavaFaker не имеет уязвимых зависимостей, а также имеет
    // ряд полезных доработок, см.: https://www.datafaker.net/documentation/getting-started/
    public static final Faker ruFaker = new Faker(new Locale("ru"));
    public static final Faker nFaker = new Faker();
    
    /**
     * Конструктор сразу генерирующий внутреннюю коллекцию {@link #clientCollection}
     *
     * @param dataLength Кол-во тестовых записей для списка Людей
     */
    public TestDataGenerator(int dataLength)
    {
        this.dataLength = dataLength;
    }
    
    /**
     * Перезаполняет массив {@link #clientCollection} Клиентов, без связей с задачами, роль у всех выставляется как {@link UserRole}
     *
     * @apiNote Чтобы клиенты оказались связаны со случайными ботами, Боты должны генерировать ДО клиентов {@link #regenerateBotsData(int)}
     */
    public void regenerateClientsData()
    {
        this.regenerateClientsData(TaskGenPolicy.NO_LINKS);
    }
    
    /**
     * Перезаполняет массив {@link #clientCollection} валидными данными (без Null)
     */
    public void regenerateClientsData(TaskGenPolicy taskGenPolicy)
    {
        this.fillRandomClients(clientCollection, botCollection, taskGenPolicy, false, dataLength);
    }
    
    /**
     * Перезаполняет массив {@link #taskCollection} Клиентов, без связей с задачами, роль у всех выставляется как {@link UserRole}
     *
     * @param maxCount Предельное кол-во Задач, которые нужно создать (может быть меньше, если {@code taskGenPolicy !=}
     *                 {@link TaskGenPolicy#ONLY_ONE})
     */
    public void regenerateTasksData(int maxCount, TaskGenPolicy taskGenPolicy)
    {
        regenerateTasksData(maxCount, taskGenPolicy, false);
    }
    /**
     * Перезаполняет массив {@link #taskCollection} Клиентов, без связей с задачами, роль у всех выставляется как {@link UserRole}
     *
     * @param maxCount Предельное кол-во Задач, которые нужно создать (может быть меньше, если {@code taskGenPolicy !=}
     *                 {@link TaskGenPolicy#ONLY_ONE})
     * @param onlyStreetAddressInTheSameCity True - все адреса будут принадлежать единственному городу, различаться
     *                                       будет только улица.
     */
    public void regenerateTasksData(int maxCount, TaskGenPolicy taskGenPolicy, boolean onlyStreetAddressInTheSameCity)
    {
        if (maxCount <= 0)
        {
            throw new IllegalArgumentException("Нечего заполнять - maxCount должен быть > 0!");
        }
        fillRandomTasks(taskCollection, clientCollection, taskGenPolicy, maxCount, onlyStreetAddressInTheSameCity);
    }
    
    public void regenerateBotsData(int maxCount)
    {
        if (maxCount <= 0)
        {
            throw new IllegalArgumentException("Нечего заполнять - maxCount должен быть > 0!");
        }
        fillRandomBots(botCollection, maxCount);
    }
    
    public void regenerateOutageRefEntriesCollection(int maxCount)
    {
        if (maxCount <= 0)
        {
            throw new IllegalArgumentException("Нечего заполнять - maxCount должен быть > 0!");
        }
        fillRandomOutageReference(outageRefEntriesCollection, maxCount);
    }
    
    
    private void fillRandomOutageReference(Set<OutageRefEntry> outageRefEntriesCollection, int maxCount)
    {
        if (outageRefEntriesCollection == null)
        {
            throw new IllegalArgumentException("Нечего заполнять - список не может быть Null!");
        }
        else
        {
            outageRefEntriesCollection.clear();
        }
        StringBuilder adrBuilder = new StringBuilder();
        final AddressGenerator adrGen = new AddressGenerator(ruFaker);
        final String REASON_PATTERN = "Техническое обслуживание ЛЭП #,# кВ Л-# от ТП-###";
        
        for (int i = 0; i < maxCount; i++)
        {
            String address = adrGen.getRandomAddress(adrBuilder);
            LocalDateTime startDate = LocalDateTime.now().plusDays(nFaker.random().nextInt(Byte.SIZE)).
                    withHour(nFaker.random().nextInt(9, 12)).withMinute(0).withSecond(0).withNano(0);
            outageRefEntriesCollection.add(OutageRefEntryBuilder.createBuilder().
                    startDateTime(startDate).
                    endDateTime(startDate.plusHours(nFaker.random().nextInt(4, 8))).
                    city(ruFaker.address().city()).
                    streetsAndHouses(address).
                    comments(nFaker.random().nextBoolean() ? "-//-" : ruFaker.lorem().sentence()).
                    reason(ruFaker.numerify(REASON_PATTERN)).
                    build());
        }
    }
    
    
    
    private void fillRandomBots(Set<Bot> bots, int maxCount)
    {
        if (bots == null)
        {
            throw new IllegalArgumentException("Нечего заполнять - список не может быть Null!");
        }
        else
        {
            bots.clear();
        }
        final String BOT_KEY_EXAMPLE = "5687000396:AAHwq6wcim5DbAhqDW240lL3-UMdrGlgEWE";
        for (int i = 0; i < maxCount; i++)
        {
            LocalDateTime creationDate = LocalDateTime.now().minusDays(nFaker.random().nextInt(270));
            bots.add(BotBuilder.createBuilder().
                    userName(StringUtils.capitalize(nFaker.name().username()).replace(".", "") + "_bot").
                    createDateTime(creationDate).
                    keyChangeDateTime(creationDate.plusDays(nFaker.random().nextInt(Byte.MAX_VALUE))).
                    comment(nFaker.random().nextBoolean() ? ruFaker.lorem().sentence() : "").
                    isAlive(nFaker.random().nextBoolean()).
                    secretKey(nFaker.examplify(BOT_KEY_EXAMPLE)).
                    build());
        }
    }
    
    /**
     * Метод заполнения массива юзеров случайными значениями (в т.ч. может рандомно вставлять null вместо некоторых эл-в).
     *
     * @param clients       Список людей, который нужно заполнить
     * @param bots          Список ботов, которых нужно назначать клиентам, если пустой или Null, то будет назначен {@link Bot#EMPTY}
     * @param isInsertNulls Добавлять ли случайное кол-во Null объектов (от 1 до половины длины массива)
     * @param tskGenPolicy  Политика генерации адресов (все, некоторые, никаких,...)
     * @param maxCount      Предельное кол-во теоретически может быть и меньше, если случайно будет создан дубликат Клиента.
     *
     * @throws IllegalArgumentException При {@code clients == null} или неизвестной политике назначения адресов.
     * @throws IllegalStateException    Если в {@link UserRole} есть роли неизвестные данному методу
     *
     * @apiNote Внимание: если tskGenPolicy != {@link TaskGenPolicy#NO_LINKS} : очищает список связанных клиентов для
     *          {@link #taskCollection}
     */
    public void fillRandomClients(Set<Client> clients, Set<Bot> bots, TaskGenPolicy tskGenPolicy, boolean isInsertNulls,
                                  int maxCount)
    {
        if (clients == null)
        {
            throw new IllegalArgumentException("Нечего заполнять - список не может быть Null!");
        }
        else
        {
            clients.clear();
        }
        if (tskGenPolicy == TaskGenPolicy.UNKNOWN)
        {
            throw new IllegalArgumentException("Неизвестное значение перечисления: " + tskGenPolicy.name());
        }
        else if (tskGenPolicy != TaskGenPolicy.NO_LINKS)
        {
            if (taskCollection.isEmpty())
            {
                throw new IllegalStateException("Список задач не может быть пустым, если указана политика связывания: " +
                        tskGenPolicy.name());
            }
            taskCollection.forEach(task -> task.getClients().clear());
        }
        final int nullsCount = ruFaker.number().numberBetween(1, maxCount) / 2;
    
        for (int i = 0, n = 0; i < maxCount; i++)
        {
            if (isInsertNulls && (n < nullsCount && ruFaker.bool().bool()))
            {
                clients.add(null);
                n++;
                continue;
            }
            boolean hasEmail = ruFaker.random().nextBoolean();
            UserRole randomRoleType = UserRole.values()[ruFaker.random().nextInt(UserRole.values().length)];
            Role randomRole;
            switch (randomRoleType)
            {
                case USER:
                {
                    randomRole = Role.USER;
                    break;
                }
                case ADMIN:
                {
                    randomRole = Role.ADMIN;
                    break;
                }
                case UNKNOWN:
                case GUEST:
                {
                    randomRole = Role.GUEST;
                    break;
                }
                default:
                {
                    throw new IllegalStateException("Обнаружена неизвестная роль: " + randomRoleType.name());
                }
            }
            Bot randomBot = bots == null || bots.isEmpty() ? Bot.EMPTY : (Bot)bots.toArray()[nFaker.random().nextInt(bots.size())];
            
            Client client = new Client(ruFaker.random().nextLong(Integer.MIN_VALUE, Long.MAX_VALUE),
                    // приводим Имя пользователя к более читаемому виду: из "peter.stones" в "PeterStones"
                    nFaker.random().nextBoolean() ? Arrays.stream(nFaker.name().username().split("\\.")).map(
                            StringUtils::capitalize).collect(Collectors.joining()) : null,
                    hasEmail ? nFaker.internet().emailAddress() : null, hasEmail && ruFaker.random().nextBoolean(),
                    LocalDateTime.now().minusDays(ruFaker.random().nextInt(365)),
                    randomRole, randomBot);
            
            clients.add(client);
    
            if (tskGenPolicy != TaskGenPolicy.NO_LINKS)
            {
                Set<Task> linkedTasks = this.getRandomLinkedEntities(taskCollection, tskGenPolicy, MAX_TASK_COUNT_PER_CLIENT);
                if (!linkedTasks.isEmpty())
                {
                    client.getTasks().addAll(linkedTasks);
                }
            }
        }
    }
    
    /**
     * Заполняет указанный список Задач, в соответствии с указанной политикой связи этих задач и указанных Клиентов
     *
     * @param onlyStreetAddressInTheSameCity True - все адреса будут принадлежать единственному городу, различаться
     *                                       будет только улица.
     */
    public void fillRandomTasks(Set<Task> tasks, Set<Client> clients, TaskGenPolicy taskGenPolicy, int maxCount,
                                boolean onlyStreetAddressInTheSameCity)
    {
        if (taskGenPolicy == TaskGenPolicy.UNKNOWN)
        {
            throw new IllegalArgumentException("Неизвестное значение перечисления: " + taskGenPolicy.name());
        }
        if (tasks == null)
        {
            throw new IllegalArgumentException("Нечего заполнять - список Задач не может быть Null!");
        }
        else
        {
            tasks.clear();
        }
        final LocalDateTime defaultMinTaskDate = LocalDateTime.now().minusDays(180);
        LocalDateTime minTaskDate = defaultMinTaskDate;
        if (taskGenPolicy != TaskGenPolicy.NO_LINKS)
        {
            if (clients == null || clients.isEmpty())
            {
                throw new IllegalArgumentException("Нечего заполнять - список связанных Клиентов не может быть пустым, " +
                        "если задана политика связи отличная от " + TaskGenPolicy.NO_LINKS);
            }
            clients.forEach(cl -> cl.getTasks().clear());
            minTaskDate = clients.stream().
                    map(Client::getRegisterDateTime).min(LocalDateTime::compareTo).
                    orElse(defaultMinTaskDate);
        }
        final int taskCount = taskGenPolicy == TaskGenPolicy.ONLY_ONE ? clients.size() : maxCount;
        
        for (int i = 0; i < taskCount; i++)
        {
            String adr = Utils.getCorrectRussianStreetName(ruFaker.address().streetName());
            String city;
            if (onlyStreetAddressInTheSameCity)
            {
                city = tasks.stream().findAny().map(Task::getCity).orElse(ruFaker.address().city());
            }
            else
            {
                city = ruFaker.address().city();
                // делаем так, чтобы задач только по Городу было не больше 30%
                boolean onlyCity = ruFaker.random().nextInt(3) == 0;
                boolean hasHouseNumber = onlyCity || (ruFaker.random().nextInt(3) > 0);
                if (!onlyCity && hasHouseNumber)
                {
                    adr += " " + ruFaker.address().buildingNumber();
                }
            }
            // как конвертировать Date в LocalDateTime и обратно см здесь: https://www.baeldung.com/java-date-to-localdate-and-localdatetime
            LocalDateTime createDate = ruFaker.date().between(java.sql.Timestamp.valueOf(minTaskDate),
                    Timestamp.valueOf(LocalDateTime.now())).toLocalDateTime();
            Task tsk = new Task(city, adr, createDate);
            // будут добавлены только неповторяющиеся Задачи
            tasks.add(tsk);
            
            if (taskGenPolicy != TaskGenPolicy.NO_LINKS)
            {
                Set<Client> linkedClients = this.getRandomLinkedEntities(clients, taskGenPolicy, MAX_TASK_COUNT_PER_CLIENT / 2);
                if (!linkedClients.isEmpty())
                {
                    tsk.getClients().addAll(linkedClients);
                }
            }
        }
    }
    
    /**
     * Получает список случайных сущностей из указанной коллекции в соот-ии с указанными правилами связывания
     *
     * @param linkedEntitiesSource        Список сущностей, которые потенциально могут быть связаны с желаемой сущностью.
     *                                    Предполагается, что список не упорядочен, иначе эл-ты будут выбираться подряд!
     * @param maxLinkedEntities   Макс. кол-во связанных сущностей (если <= 0, то ограничено linkedEntitiesSource.size()
     *
     * @return Случайный или пустой список в зависимости от значения {@code genPolicy}
     */
    public <E> Set<E> getRandomLinkedEntities(Set<E> linkedEntitiesSource, TaskGenPolicy genPolicy, int maxLinkedEntities)
    {
        if (genPolicy == TaskGenPolicy.NO_LINKS)
        {
            return Collections.emptySet();
        }
        boolean hasLinkedEntities = genPolicy == TaskGenPolicy.ONLY_ONE || genPolicy == TaskGenPolicy.ONE_OR_MORE ||
                ruFaker.random().nextBoolean();
        if (!hasLinkedEntities)
        {
            return Collections.emptySet();
        }
        int linkedEntitiesCount = genPolicy == TaskGenPolicy.ONLY_ONE ? 1 : ruFaker.number().numberBetween(1,
                maxLinkedEntities > 0 ? maxLinkedEntities : linkedEntitiesSource.size());
        Set<E> result = new HashSet<>(linkedEntitiesCount);

        result.addAll(linkedEntitiesSource.stream().unordered().skip(nFaker.random().nextInt(linkedEntitiesSource.size())).
                limit(linkedEntitiesCount).
                collect(Collectors.toSet()));
        return result;
    }
}

