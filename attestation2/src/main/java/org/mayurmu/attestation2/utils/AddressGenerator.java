package org.mayurmu.attestation2.utils;

import org.mayurmu.common.Utils;
import net.datafaker.Faker;

public class AddressGenerator
{
    public enum AddressEntryType
    {
        NONE,
        /**
         * Например: "Весь населённый пункт" или "пос. Газовиков"
         */
        ONLY_CITY,
        /**
         * Например: "ул. Б. Хмельницкого"
         *
         * @implNote Вообще, таких случаев не встречал.
         */
        ONLY_STREET,
        /**
         * Например: "просп. Баклановский 142/1"
         */
        EXACT_HOUSE,
        /**
         * Например: ул. Набережная 5, 23, 33
         */
        HOUSE_RANGE_COMMA,
        /**
         * Например: "ул. Высоковольтная 4-27"
         */
        HOUSE_RANGE_HORIZONTAL_SLASH,
        /**
         * Например: "ул. Седова 165-247, 34 А-40"
         */
        SEVERAL_HOUSE_RANGES,
        /**
         * Объединение других вариантов, кроме {@link #ONLY_CITY}, {@link #ONLY_STREET}
         *
         * Например: "ул. Б. Хмельницкого 57-часть квартир, 59-79; ул. Комитетская 164-188, 157-177; ул. Красноармейская 111"
         */
        SEVERAL_STREETS_AND_HOUSE_RANGES
    }
    
    private final Faker faker;
    
    AddressGenerator(Faker someFaker)
    {
        this.faker = someFaker;
    }
    
    public String getRandomAddress()
    {
        return getRandomAddress(AddressEntryType.values(),null);
    }
    public String getRandomAddress(AddressEntryType[] allowedAdrTypes)
    {
        return getRandomAddress(allowedAdrTypes, null);
    }
    public String getRandomAddress(StringBuilder adrBuilder)
    {
        return getRandomAddress(AddressEntryType.values(), adrBuilder);
    }
    /**
     * @param adrBuilder Созданный снаружи StringBuilder - передаём для экономии, чтобы не создавать его при каждом вызове.
     */
    public String getRandomAddress(AddressEntryType[] allowedAdrTypes, StringBuilder adrBuilder)
    {
        if (adrBuilder == null)
        {
            adrBuilder = new StringBuilder();
        }
        else
        {
            adrBuilder.setLength(0);
        }
        AddressEntryType adrType = allowedAdrTypes[faker.random().nextInt(1, allowedAdrTypes.length - 1)];
        String address = "";
        switch (adrType)
        {
            case ONLY_CITY:
            {
                address = faker.random().nextBoolean() ? "Весь населённый пункт" : "пос. Газовиков";
                break;
            }
            case EXACT_HOUSE:
            {
                appendStreetName(adrBuilder);
                adrBuilder.append(" ").append(faker.address().buildingNumber());
                address = adrBuilder.toString();
                break;
            }
            case HOUSE_RANGE_COMMA:
            {
                appendStreetName(adrBuilder);
                adrBuilder.append(" ");
                appendHouseRangeViaComma(faker, adrBuilder);
                address = adrBuilder.toString();
                break;
            }
            case HOUSE_RANGE_HORIZONTAL_SLASH:
            {
                appendStreetName(adrBuilder);
                adrBuilder.append(" ");
                appendHouseRangeViaSlash(faker, adrBuilder);
                address = adrBuilder.toString();
                break;
            }
            case ONLY_STREET:
            {
                address = Utils.getCorrectRussianStreetName(faker.address().streetName());
                break;
            }
            case SEVERAL_HOUSE_RANGES:
            {
                appendStreetName(adrBuilder);
                adrBuilder.append(" ");
                appendSeveralHouseRanges(adrBuilder);
                address = adrBuilder.toString();
                break;
            }
            case SEVERAL_STREETS_AND_HOUSE_RANGES:
            {
                final AddressEntryType[] variants = {AddressEntryType.EXACT_HOUSE,
                                                     AddressEntryType.HOUSE_RANGE_COMMA,
                                                     AddressEntryType.HOUSE_RANGE_HORIZONTAL_SLASH,
                                                     AddressEntryType.SEVERAL_HOUSE_RANGES};
                
                final int streetsCount = faker.random().nextInt(1, Byte.SIZE);
                for (int i = 0; i < streetsCount; i++)
                {
                    adrBuilder.append(getRandomAddress(variants));
                    if (i != streetsCount - 1)
                    {
                        adrBuilder.append("; ");
                    }
                }
                address = adrBuilder.toString();
                break;
            }
            default:
            {
                throw new IllegalArgumentException("Неизвестный параметр перечисления: " + adrType.name() + "\n");
            }
        }
        return address;
    }
    
    private void appendSeveralHouseRanges(StringBuilder adrBuilder)
    {
        appendSeveralHouseRanges(adrBuilder, 0);
    }
    private void appendSeveralHouseRanges(StringBuilder adrBuilder, int count)
    {
        int rangesCount = count > 0 ? count : faker.random().nextInt(2, 5);
        for (int i = 0, lastNumber = 1; i < rangesCount; i++)
        {
            lastNumber = appendHouseRangeViaSlash(faker, adrBuilder, lastNumber + faker.random().nextInt(Byte.SIZE));
            if (i != rangesCount - 1)
            {
                adrBuilder.append(", ");
            }
        }
    }
    
    /**
     * @return Возвращает последний номер добавленного дома
     */
    private static int appendHouseRangeViaComma(Faker faker, StringBuilder adrBuilder)
    {
        int houseCount = faker.random().nextInt(1, 5);
        int num = 0;
        for (int j = 0; j < houseCount; j++)
        {
            String number = faker.address().buildingNumber();
            num = Integer.parseInt(number);
            adrBuilder.append(num);
            if (j != houseCount - 1)
            {
                adrBuilder.append(", ");
            }
        }
        return num;
    }
    
    /**
     * @return Возвращает последний номер добавленного дома
     */
    private static int appendHouseRangeViaSlash(Faker faker, StringBuilder adrBuilder)
    {
        return appendHouseRangeViaSlash(faker, adrBuilder, 0);
    }
    /**
     * @return Возвращает последний номер добавленного дома
     */
    private static int appendHouseRangeViaSlash(Faker faker, StringBuilder adrBuilder, int startHouseNumber)
    {
        int startNumber = startHouseNumber > 0 ? startHouseNumber : faker.number().numberBetween(1, Byte.MAX_VALUE);
        int endNumber = startNumber + faker.random().nextInt(1, Byte.MAX_VALUE);
        adrBuilder.append(startNumber).append("-").append(endNumber);
        return endNumber;
    }
    
    private void appendStreetName(StringBuilder adrBuilder)
    {
        adrBuilder.append(Utils.getCorrectRussianStreetName(faker.address().streetName()));
    }
}
