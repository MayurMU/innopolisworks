package org.mayurmu.attestation2.app;

import org.hibernate.Session;
import org.mayurmu.attestation2.model.*;
import org.mayurmu.attestation2.model.types.UserRole;
import org.mayurmu.attestation2.utils.DBUtils;
import org.mayurmu.attestation2.utils.TestDataGenerator;

import javax.persistence.EntityTransaction;
import java.util.*;
import java.util.stream.Stream;

/**
 * Класс создающий структуру таблиц финальной работы курса Иннополис Java и заполняющий её тестовыми данными
 */
public class Attestation_2
{
    /**
     * Программа реализует задание для 2-й Аттестации:
     *  "Создать schema.sql файл, который содержит описание таблиц и данных для этих таблиц".
     *  "Написать 3-4 запроса на <b>заполнение</b> каждой из этих таблиц."
     */
    public static void main(String[] args)
    {
        // Сколько генерировать клиентов
        final int TEST_DATA_COUNT = 5;
        final int REF_DATA_COUNT = 50;
        int exitCode = 0;
        try
        {
            DBUtils.loadConfig();
            System.out.println("Генерация тестовых данных...");
            final TestDataGenerator dataGenerator = new TestDataGenerator(TEST_DATA_COUNT);
            dataGenerator.regenerateBotsData(TEST_DATA_COUNT);
            dataGenerator.regenerateOutageRefEntriesCollection(REF_DATA_COUNT);
            Set<OutageRefEntry> _fakeOutageRefEntries = dataGenerator.outageRefEntriesCollection;
            dataGenerator.regenerateClientsData();
            Set<Client> _fakeClients = dataGenerator.clientCollection;
            // создаём список случайных задач, которые случайным образом связываются с нашими клиентами
            dataGenerator.regenerateTasksData(_fakeClients.size(), TestDataGenerator.TaskGenPolicy.ONE_OR_MORE);
            Set<Task> _fakeTasks = dataGenerator.taskCollection;
            // настраиваем обратные связи клиентов с созданными задачами
            _fakeTasks.forEach(t -> t.getClients().forEach(cl -> cl.getTasks().add(t)));
            System.out.printf("Готово, сгенерировано %d Клиентов и %d Задач.%n", _fakeClients.size(), _fakeTasks.size());
            System.out.println("Запись данных в БД...");
            // как я понял при любых исключениях сессию использовать более нельзя и она должна быть закрыта
            try (Session session = DBUtils.getHibernateSession())
            {
                writeDataToDB(session, Arrays.asList(Role.ADMIN, Role.USER, Role.GUEST));
                writeDataToDB(session, _fakeTasks);
                // по идее в системе должен быть хотя бы один Админ, поэтому, если его не удалось сгенерировать, то назначаем его вручную.
                if (_fakeClients.stream().anyMatch(client -> client.getRole().getType() == UserRole.ADMIN))
                {
                    _fakeClients.iterator().next().setRole(Role.ADMIN);
                }
                // при записи клиентов каскадом пишутся и боты
                writeDataToDB(session, _fakeClients);
                writeDataToDB(session, _fakeOutageRefEntries);
                System.out.println("Данные записаны успешно.");
                
                System.out.println("Выводим записанные данные:");
                
                System.out.println("\n++++++++++++++++++++ Роли ++++++++++++++++++++");
                System.out.println();
                Stream<Role> loadedRoles = session.createQuery("SELECT r FROM Role r", Role.class).getResultStream();
                loadedRoles.forEach(System.out::println);
                
                System.out.println("\n____________________ Боты _____________________");
                System.out.println();
                Stream<Bot> loadedBots = session.createQuery("SELECT b FROM Bot b", Bot.class).getResultStream();
                loadedBots.forEach(bot -> System.out.append(bot.toString()).println("\n"));
                
                System.out.println("\n********************** Клиенты ***************************************");
                System.out.println();
                Stream<Client> loadedClients = session.createQuery("SELECT cl FROM Client cl", Client.class).
                        getResultStream();
                loadedClients.forEach(client -> System.out.append(client.toString("f")).println("\n"));
                
                System.out.println("\n====================== Задачи ========================================");
                Stream<Task> loadedTasks = session.createQuery("SELECT t FROM Task t", Task.class).getResultStream();
                loadedTasks.forEach(task -> System.out.println(task.toString("f")));
                
                System.out.println("\n-------------------- Записи справочника отключений (первые 10)---------------------------");
                Stream<OutageRefEntry> loadedOutageRefEntries  = session.createQuery("SELECT oe FROM OutageRefEntry oe",
                        OutageRefEntry.class).setMaxResults(10).getResultStream();
                loadedOutageRefEntries.forEach(x -> System.out.append(x.toString()).println("\n"));
            }
        }
        catch (Exception ex)
        {
            exitCode = 1;
            System.err.printf("Неожиданная ошибка работы программы: %n%s", ex);
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                DBUtils.hiberFactory.close();
            }
            catch (Exception exception)
            {
                System.err.println(exception.toString());
                exception.printStackTrace();
            }
            System.out.println("Программа завершена");
            System.exit(exitCode);
        }
    }
    
    /**
     * Метод записи данных в БД
     *
     * @apiNote Побочный эффект - в случае выброса исключения объект сессии будет закрыт
     *
     * @exception IllegalArgumentException Когда {@code sourceCollections.length == 0}
     * @exception RuntimeException При непредвиденных ошибках работы
     */
    private static void writeDataToDB(Session session, Collection<?> sourceCollection)
    {
        if (sourceCollection == null || sourceCollection.size() == 0)
        {
            throw new IllegalArgumentException("Нечего сохранять в БД - передан пустой список данных");
        }
        EntityTransaction ta = session.getTransaction();
        try
        {
            ta.begin();
            sourceCollection.forEach(session::save);
            // Это по идее не обязательно, т.к. Hibernate и так периодически производит сброс данных в БД (если только
            //      FlushMode не установлен в MANUAL).
            // Вроде как тут уже должна производиться запись в БД, но т.к. автокоммита ТА в JPA по умолчанию нет, то
            // данных в БД не будет пока не зафиксирована ТА (commit) или точнее данные будут отброшены самой СУБД, если ТА не зафиксировать.
            session.flush();
            ta.commit();
        }
        catch (Exception ex)
        {
            try
            {
                ta.rollback();
                session.close();
            }
            catch (Exception e)
            {
                // обработка не требуется
            }
            throw new RuntimeException("Ошибка записи данных в БД: \n", ex);
        }
    }
}