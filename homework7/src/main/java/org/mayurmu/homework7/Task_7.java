package org.mayurmu.homework7;

import com.github.javafaker.Faker;

import java.util.Locale;
import java.util.Scanner;

/**
 * Класс реализующий задание 7 - Инкапсуляция
 * */
public class Task_7
{
    private static final Scanner _scn = new Scanner(System.in);
    
    static final Faker _jfaker = new Faker(new Locale("ru"));
    
    public static void main(String[] args)
    {
        try
        {
            do
            {
                System.out.println("Генерация случайных людей...");
                Human[] humans = generateRandomRussianHumansArray(_jfaker, _jfaker.random().nextInt(1, 15));
                System.out.println("Случайный массив людей:");
                System.out.println();
                printArray(humans);
                System.out.println();
                System.out.println("Отсортированный массив людей:");
                System.out.println();
                Human[] sortedHumans = bubbleSort(humans);
                printArray(sortedHumans);
                System.out.println();
                System.out.println("Введите любую строку, чтобы повторить или ПРОБЕЛ, чтобы выйти:");
            }
            while (!_scn.nextLine().equalsIgnoreCase(" "));
        }
        catch (Exception ex)
        {
            System.err.append("Непредвиденная ошибка работы программы:").append(System.lineSeparator()).println(ex);
            ex.printStackTrace();
        }
        finally
        {
            _scn.close();
            System.out.println("Программа завершена");
        }
    }
    
    /**
     * Метод генерации случайного массива людей с согласованными русскими именами
     * @return Массив людей или Null, если {@code maxCount} = 0
     * @implSpec Метод будет выдавать согласованные имена, только, если был передан {@code jfaker} с русской локалью
     * @exception IllegalArgumentException Когда {@code maxCount} < 0
     * */
    public static Human[] generateRandomRussianHumansArray(Faker jfaker, int maxCount)
    {
        if (maxCount == 0)
        {
            return null;
        }
        else if (maxCount < 0)
        {
            throw new IllegalArgumentException("maxCount должен быть больше 0");
        }
        
        final String FEMALE_NAME_OR_LASTNAME_ENDING = "а";
        final String FEMALE_NAME_ENDING_2 = "я";
        
        Human[] humans = new Human[maxCount];
        for (int i = 0; i < humans.length; i++)
        {
            String fisrtName = jfaker.name().firstName();
            String lastName = jfaker.name().lastName();
            // пытаемся создать согласованные имена, т.к. Faker, к сожалению, генерирует их без учёта пола (родовых окончаний)
            if ((fisrtName.endsWith(FEMALE_NAME_OR_LASTNAME_ENDING) || fisrtName.endsWith(FEMALE_NAME_ENDING_2))
                    && !lastName.endsWith(FEMALE_NAME_OR_LASTNAME_ENDING))
            {
                lastName += FEMALE_NAME_OR_LASTNAME_ENDING;
            }
            else if (lastName.endsWith(FEMALE_NAME_OR_LASTNAME_ENDING) && (!fisrtName.endsWith(FEMALE_NAME_OR_LASTNAME_ENDING) &&
                    !fisrtName.endsWith(FEMALE_NAME_ENDING_2)))
            {
                lastName = lastName.substring(0, lastName.length() - 1);
            }
            humans[i] = new Human(fisrtName, lastName, jfaker.random().nextInt(1, 100));
        }
        return humans;
    }
    
    /**
     * Универсальный метод вывода массива на консоль (каждый элемент с новой строки)
     * @implNote Неэффективен для примитивных типов, т.к. будет приводить к лишней упаковке (boxing)
     * */
    static void printArray(Object[] arr)
    {
        printArray(arr, System.lineSeparator());
    }
    
    /**
     * Универсальный метод печати массива с указанным разделителем
     * */
    static void printArray(Object[] arr, String separator)
    {
        for (Object ob : arr)
        {
            System.out.append(ob.toString()).print(separator);
        }
    }
    
    /**
     * Сортировка Пузырьком - самые большие числа последовательно "всплывают" в самый конец массива
     *
     * @implNote Сложность O(n^2). Метод из лекции переделан на функцию вместо процедуры
     *
     * @param array - входной неотсортированный массив
     * 
     * @return отсортированный массив или null, если {@code array.length} = 0  
     */
    public static Human[] bubbleSort(Human[] array)
    {
        if (array.length == 0)
        {
            return null;
        }
        else if (array.length == 1)
        {
            return new Human[] { array[0] };
        }
        
        Human[] result = new Human[array.length];
        System.arraycopy(array, 0, result, 0, array.length);
        
        for (int i = 0; i < result.length; i++)
        {
            for (int j = 0; j < result.length - 1; j++)
            {
                if (result[j].getAge() > result[j + 1].getAge())
                {
                    Human tempHuman = result[j];
                    result[j] = result[j + 1];
                    result[j + 1] = tempHuman;
                }
            }
        }
        return result;
    }
}
