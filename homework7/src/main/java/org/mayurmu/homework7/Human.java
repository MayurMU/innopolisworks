package org.mayurmu.homework7;

import java.util.StringJoiner;

/**
 * Простой класс - модель человека
 */
public class Human
{
    
    private String name = "";
    private String lastName = "";
    private int age = 0;
    
    
    //region 'Конструкторы'
    
    /**
     * Конструктор по умолчанию - создаёт корректный объект Человека
     */
    public Human()
    {
    }
    
    /**
     * Конструктор с параметрами
     *
     * @param _name     При null заменяется на пустую строку
     * @param _lastName При null заменяется на пустую строку
     * @param _age      При <= 0 заменяется значение {_age по умолчанию}
     */
    public Human(String _name, String _lastName, int _age)
    {
        name = _name != null ? _name : "";
        lastName = _lastName != null ? _lastName : "";
        age = _age > 0 ? _age : age;
    }
    
    //endregion 'Конструкторы'
    
    
    //region 'Свойства'
    
    /**
     * Имя
     */
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name != null ? name : "";
    }
    
    /**
     * Фамилия
     */
    public String getLastName()
    {
        return lastName;
    }
    
    public void setLastName(String lastName)
    {
        this.lastName = lastName != null ? lastName : "";
    }
    
    /**
     * Возраст
     */
    public int getAge()
    {
        return age;
    }
    
    /**
     * Устанавливает указанный возраст
     *
     * @throws IllegalArgumentException Если {@code age <= 0}
     */
    public void setAge(int age)
    {
        if (age <= 0)
        {
            throw new IllegalArgumentException("Возраст (age) должен быть > 0");
        }
        this.age = age;
    }
    
    /**
     * Полное имя (Фамилия + Имя)
     */
    public String getFullName()
    {
        return this.lastName + " " + this.name;
    }
    
    //endregion 'Свойства'
    
    
    /**
     * Перегруженный метод вывода содержимого класса {@link Human} в виде строки
     */
    @Override
    public String toString()
    {
        return new StringJoiner(", ", "[", "]")
                .add("name = '" + name + "'")
                .add("lastName = '" + lastName + "'")
                .add("age = " + age)
                .toString();
    }
}
