package org.mayurmu.homework7;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

class Task_7Test
{
    /**
     * Компаратор для сравнения людей по возрасту, используется в тесте {@link #bubbleSort_Regular_RandomArray()}
     */
    private static final Comparator<? super Human> HumanAgeComparator = new Comparator<Human>()
    {
        @Override
        public int compare(Human o1, Human o2)
        {
            return o1.getAge() - o2.getAge();
        }
    };
    
    //region 'Исключения'
    
    @Test
    @DisplayName("Метод упадёт, если среди элементов входного массива будет null, при числе элементов больше 1")
    void bubbleSort_ExceptionOnNullElementInNonEmptyArray()
    {
        Human[] testData1 = new Human[] { null };
        Human[] testData2 = new Human[] { new Human(), null };
        assertDoesNotThrow(() -> Task_7.bubbleSort(testData1));
        assertArrayEquals(testData1, Task_7.bubbleSort(testData1));
        assertThrows(NullPointerException.class, () -> Task_7.bubbleSort(testData2));
    }
    
    //endregion 'Исключения'
    
    
    
    
    //region 'Негативные сценарии'
    
    @Test
    @DisplayName("Метод не должен падать при массиве 0-вой длины")
    void bubbleSort_ZeroLengthArray()
    {
        Human[] testData = new Human[0];
        assertDoesNotThrow(() -> Task_7.bubbleSort(testData));
        assertNull(Task_7.bubbleSort(testData));
    }
    
    @Test
    @DisplayName("Метод не должен падать на массиве из единственного элемента")
    void bubbleSort_SingleElementArray()
    {
        Human[] testData = new Human[1];
        assertDoesNotThrow(() -> Task_7.bubbleSort(testData));
        assertArrayEquals(testData, Task_7.bubbleSort(testData));
    }
    
    @Test
    @DisplayName("Метод не должен падать на массиве только из пустых объектов")
    void bubbleSort_StubElementsArray()
    {
        Human[] testData = {new Human(), new Human(), new Human()};
        assertDoesNotThrow(() -> Task_7.bubbleSort(testData));
        assertArrayEquals(testData, Task_7.bubbleSort(testData));
    }
    
    //endregion 'Негативные сценарии'
    
    
    
    
    //region 'Позитивные сценарии'
    
    @RepeatedTest(value = 10)
    @DisplayName("Случайный массив людей - многократное сравнение с Arrays.sort()")
    void bubbleSort_Regular_RandomArray()
    {
        final int MIN_ELEM_COUNT = 5;
        Human[] testData = Task_7.generateRandomRussianHumansArray(Task_7._jfaker, Task_7._jfaker.random().nextInt(MIN_ELEM_COUNT, Byte.MAX_VALUE));
        assert testData != null && testData.length >= MIN_ELEM_COUNT : "Некорректная генерация случайного массива!";
        Human[] result = Task_7.bubbleSort(testData);
        assert !Arrays.equals(testData, result) : "Обнаружено изменение входного массива!";
        Arrays.sort(testData, Task_7Test.HumanAgeComparator);
        assertArrayEquals(testData, result);
    }
    
    //endregion 'Позитивные сценарии'
    
    
    
}