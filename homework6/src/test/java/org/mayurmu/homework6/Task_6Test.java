package org.mayurmu.homework6;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class Task_6Test
{
    
    //region 'Исключения'
    
    @Test
    @DisplayName("Проверка исключения - пустой массив")
    void getMostRareNumber_EmptyArrayException()
    {
        Assertions.assertThrows(IllegalArgumentException.class, () -> Task_6.getMostRareNumber(new int[0], Task_6.MIN_NUMBER,
                Task_6.MAX_NUMBER, Task_6.STOP_NUMBER), "Неожиданный тип исключения (или его отсутствие) на пустом массиве");
    }
    
    @Test
    @DisplayName("Проверка исключения - неверные положительные пределы последовательности")
    void getMostRareNumber_IllegalSequenceLimitsException()
    {
        Assertions.assertThrows(IllegalArgumentException.class, () -> Task_6.getMostRareNumber(new int[1], 150,
                12, Task_6.STOP_NUMBER), "Неожиданный тип исключения (или его отсутствие) на неверных пределах");
    }
    
    //endregion 'Исключения'
    
    
    
    
    //region 'Негативные сценарии'
    
    @Test
    @DisplayName("Проверка исключения - null-массив")
    void getMostRareNumber_NULL_ArrayException()
    {
        Assertions.assertThrows(NullPointerException.class, () -> Task_6.getMostRareNumber(null, Task_6.MIN_NUMBER,
                Task_6.MAX_NUMBER, Task_6.STOP_NUMBER), "Неожиданный тип исключения (или его отсутствие) при пустой ссылке на массив");
    }
    
    /**
     * Т.к. метод не анализирует каждое число на соот-ие пределам, то он будет падать, если число превышает их
     */
    @Test
    @DisplayName("-100..100, есть значение не вписывающееся в заданные пределы")
    void getMostRareNumber_Conventional_IllegalSequence()
    {
        int mostRareNumber = -100;
        int[] arr = new int[] {-5, mostRareNumber, -151, -5, -5, mostRareNumber, 51, 51, Task_6.STOP_NUMBER};
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> Task_6.getMostRareNumber(arr, Task_6.MIN_NUMBER, Task_6.MAX_NUMBER, Task_6.STOP_NUMBER));
    }
    
    /**
     * Т.к. метод не анализирует каждое число на соот-ие пределам, то он будет падать, если число превышает их
     */
    @Test
    @DisplayName("20..40, есть значение не вписывающееся в заданные пределы")
    void getMostRareNumber_Positive_IllegalSequence()
    {
        int[] arr = new int[] {20, 30, 0, 30, 20, 20};
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> Task_6.getMostRareNumber(arr, 20, 40, Task_6.STOP_NUMBER));
    }
    
    //endregion 'Негативные сценарии'
    
    
    
    
    //region 'Позитивные сценарии'
    
    @Test
    @DisplayName("Массив из одного эл-та")
    void getMostRareNumber_SingeElementArray()
    {
        int[] arr = new int[] {1};
        Assertions.assertEquals(arr[0], Task_6.getMostRareNumber(arr, Task_6.MIN_NUMBER, Task_6.MAX_NUMBER, Task_6.STOP_NUMBER));
    }
    
    @Test
    @DisplayName("Массив из одного эл-та со стоп-словом в конце")
    void getMostRareNumber_SingeElementArrayWithStopWord()
    {
        final int[] arr = new int[] {1, Task_6.STOP_NUMBER};
        Assertions.assertEquals(arr[0], Task_6.getMostRareNumber(arr, Task_6.MIN_NUMBER, Task_6.MAX_NUMBER, Task_6.STOP_NUMBER));
    }
    
    @Test
    @DisplayName("Cтоп-слово в начале послед-ти")
    void getMostRareNumber_StopWordAtBeginning()
    {
        final int[] arr = new int[] {Task_6.STOP_NUMBER, 1, 2, 3};
        Assertions.assertEquals(Task_6.STOP_NUMBER, Task_6.getMostRareNumber(arr, Task_6.MIN_NUMBER, Task_6.MAX_NUMBER, Task_6.STOP_NUMBER));
    }
    
    /**
     * Проверка нахождения наименее частого числа последовательности заданной по условию задачи -100..100
     *  стоп-слово в конце последовательности
     */
    @Test
    @DisplayName("-100..100, стоп-слово в конце последовательности")
    void getMostRareNumber_ConventionalSequence_StopWordAtTheEnd()
    {
        int mostRareNumber = -100;
        int[] arr = new int[] {-5, mostRareNumber, 51, -5, -5, mostRareNumber, 51, 51, Task_6.STOP_NUMBER};
        Assertions.assertEquals(mostRareNumber, Task_6.getMostRareNumber(arr, Task_6.MIN_NUMBER, Task_6.MAX_NUMBER, Task_6.STOP_NUMBER));
    }
    
    /**
     * Проверка нахождения наименее частого числа последовательности заданной по условию задачи -100..100
     *  стоп-слово в середине последовательности
     */
    @Test
    @DisplayName("-100..100, стоп-слово в середине последовательности")
    void getMostRareNumber_ConventionalSequence_StopWordInTheMiddle()
    {
        int[] arr = new int[] {0, -100, 0, -5, -5, 0, -100, -100, Task_6.STOP_NUMBER, -5, -5, 51};
        Assertions.assertEquals(-5, Task_6.getMostRareNumber(arr, Task_6.MIN_NUMBER, Task_6.MAX_NUMBER, Task_6.STOP_NUMBER));
    }
    
    @Test
    @DisplayName("20..40, только положительная послед-ть")
    void getMostRareNumber_AllPositiveSequence()
    {
        int[] arr = new int[] {21, 31, 40, 40, 31, 31, 21, 21};
        Assertions.assertEquals(40, Task_6.getMostRareNumber(arr, 20, 40, Task_6.STOP_NUMBER));
    }
    
    @Test
    @DisplayName("-40..-20, строго отрицательная послед-ть")
    void getMostRareNumber_AllNegativeSequence()
    {
        int[] arr = new int[] {-21, -31, -31, -40, -31, -21, -21, -40, -31};
        Assertions.assertEquals(-40, Task_6.getMostRareNumber(arr, -40, -20, Task_6.STOP_NUMBER));
    }
    
    //endregion 'Позитивные сценарии'
    
}