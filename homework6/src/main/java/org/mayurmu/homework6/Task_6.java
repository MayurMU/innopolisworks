package org.mayurmu.homework6;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Класс реализующий Задачу №6 - Алгоритмы
 * */
public class Task_6
{
    private static final Scanner _scn = new Scanner(System.in);
    private static final Random _rnd = new Random();
    // нижний включённый придел возможного значения в последовательности
    static final int MIN_NUMBER = -100;
    // верхний включённый придел возможного значения в последовательности
    static final int MAX_NUMBER = 100;
    static final int STOP_NUMBER = -101;
    
    public static void main(String[] args)
    {
        try
        {
            do
            {
                int[] randomArray = generateRandomArray(_rnd, MIN_NUMBER, MAX_NUMBER, 25, true);
                // имитируем условие задачи, появление числа -101 в последовательности
                randomArray[randomArray.length - 1] = STOP_NUMBER;
                System.out.println("Случайная последовательность:");
                System.out.println(Arrays.toString(randomArray));
                int mostRareNumber = getMostRareNumber(randomArray, MIN_NUMBER, MAX_NUMBER, STOP_NUMBER);
                System.out.printf("Самое редкое число: %d %n%n", mostRareNumber);
                System.out.println("Введите Пробел, чтобы выйти - любая строка - продолжить:");
            }
            while (!_scn.nextLine().equalsIgnoreCase(" "));
        }
        catch (Exception ex)
        {
            System.err.append("Неожиданная ошибка:").println(ex);
            ex.printStackTrace();
        }
        finally
        {
            _scn.close();
        }
    }
    
    /**
     * Метод поиска самого редкого числа в массиве
     *
     * @param randomArray Входная последовательность, которая будет анализироваться до её конца или пока не будет обнаружен {@code stopNumber}
     *
     * @return Самое редкое число из входной последовательности, если таких чисел несколько, то из них выбирается то,
     * что встретилось раньше - приоритет отдаётся отрицательным числам. На коллекции, начинающейся со стоп-слова - выдаёт его.
     *
     * @throws IllegalArgumentException При некорректном сочетании {@code minNumber} и {@code maxNumber} или когда массив {@code randomArray} пустой
     * @implSpec Метод будет выдавать корректный результат, пока частота появления каждого числа будет в пределах 2 147 483 647 (MAX_INTEGER)
     */
    static int getMostRareNumber(int[] randomArray, int minNumber, int maxNumber, int stopNumber)
    {
        // по условию задачи числа у нас могут быть только от -100 до 100, значит мы имеем ограниченное кол-во возможных
        // значений чисел поступающих на вход, поэтому будем считать кол-во их появлений во входной последовательности,
        // пока не получим признак конца последовательности - число (-101), тогда минимум этого массива (среди положительных значений)
        // и будет результатом, а сложность алгоритма окажется линейной, т.к. мы не будем использовать вложенные циклы.
        if (minNumber >= maxNumber)
        {
            throw new IllegalArgumentException(String.format("Нижняя граница (%d) должна быть меньше верхней (%d)",
                    minNumber, maxNumber));
        }
        else if (randomArray.length == 0)
        {
            throw new IllegalArgumentException("Размер массива должен быть больше 0");
        }
        else if (randomArray.length == 1)
        {
            return randomArray[0];
        }
        else if (randomArray[0] == stopNumber)
        {
            return stopNumber;
        }
        // рассчитываем длины массивов отрицательных и Неотрицательных чисел
        int negativeArrSize = (minNumber < 0 && maxNumber < 0) ? (Math.abs(minNumber) - Math.abs(maxNumber) + 1) : (minNumber < 0 ? -minNumber : 0);
        int positiveAndZeroArrSize = minNumber > 0 ? (maxNumber - minNumber + 1) : (maxNumber > 0 ? maxNumber + 1 : 0);
        // рассчитываем сдвиг индексов, для случаев, когда у пределов нет перехода через 0
        int indexShift = (minNumber <= 0 && maxNumber >= 0) ? 0 : Integer.min(Math.abs(minNumber), Math.abs(maxNumber));
        // учитываем, что индексы отсчитываются от 0, поэтому добавляем 1 к размеру массивов (если такой массив нужен)
        int[] positiveNumberAndZeroFrequencies = new int[positiveAndZeroArrSize > 0 ? ++positiveAndZeroArrSize : 0];
        int[] negativeNumberFrequencies = new int[negativeArrSize > 0 ? ++negativeArrSize : 0];
        
        for (int i = 0; i < randomArray.length && randomArray[i] != stopNumber; i++)
        {
            int numberIndex = randomArray[i];
            if (numberIndex < 0)
            {
                negativeNumberFrequencies[-numberIndex - indexShift]++;
            }
            else
            {
                positiveNumberAndZeroFrequencies[numberIndex - indexShift]++;
            }
        }
        // ищем самый редкий отрицательный элемент
        int mostRareNegativeNumber = findMinNumberIndex(negativeNumberFrequencies);
        int minFrequency = mostRareNegativeNumber >= 0 ? negativeNumberFrequencies[mostRareNegativeNumber] : 0;
        // ищем самый редкий Неотрицательный элемент (сравнение полученных частот самых редких эл-тов не требуется, т.к.
        //      на вход поиска мы подаём частоту отрицательного эл-та)
        int mostRarePositiveAndZeroNumber = findMinNumberIndex(positiveNumberAndZeroFrequencies, minFrequency);
        int mostRareNumber = mostRarePositiveAndZeroNumber >= 0 ? mostRarePositiveAndZeroNumber + indexShift : -(mostRareNegativeNumber + indexShift);
        return mostRareNumber;
    }
    
    /**
     * Метод получения индекса минимального положительного элемента в массиве
     * @param positiveArray Входной массив из положительных чисел (остальные не учитываются)
     * @return индекса минимального положительного элемента в массиве или (-1), если таких нет
     */
    private static int findMinNumberIndex(int[] positiveArray)
    {
        return findMinNumberIndex(positiveArray, 0);
    }
    
    /**
     * Метод получения индекса минимального положительного элемента в массиве
     * @param positiveArray Входной массив из положительных чисел (остальные не учитываются)
     * @param initMinNumber Минимальное, число, с которым будут сравниваться числа в массиве
     * @return индекса минимального положительного элемента в массиве или (-1), если таких нет, или массив пустой.
     */
    private static int findMinNumberIndex(int[] positiveArray, int initMinNumber)
    {
        if (positiveArray.length == 0)
        {
            return -1;
        }
        else if (positiveArray.length == 1 && positiveArray[0] >= 0)
        {
            return 0;
        }
        else
        {
            int index = -1;
            int minNumber = initMinNumber;
            for (int i = 0; i < positiveArray.length; i++)
            {
                int number = positiveArray[i];
                // ищем первый элемент с ненулевой частотой
                if (number > 0 && (minNumber == 0 || number < minNumber))
                {
                    minNumber = number;
                    index = i;
                }
            }
            return index;
        }
    }
    
    /**
     * Метод генерации массива случайной длины из случайных целых чисел
     * @param rnd Внешний генератор случайных чисел
     * @param lowBound  Нижняя (включённая) граница возможных чисел массива
     * @param highBound Верхняя (включённая) граница возможных чисел массива
     * @param sizeBound Предельный размер генерируемого массива
     * @return Случайный целый массив, заданной длины
     * @exception IllegalArgumentException При некорректных значениях пределов
     */
    static int[] generateRandomArray(Random rnd, int lowBound, int highBound, int sizeBound)
    {
        return generateRandomArray(rnd, lowBound, highBound, sizeBound, false);
    }
    
    /**
     * Метод генерации массива случайной длины из случайных целых чисел
     * @param rnd Внешний генератор случайных чисел
     * @param lowBound  Нижняя (включённая) граница возможных чисел массива
     * @param highBound Верхняя (включённая) граница возможных чисел массива
     * @param sizeBound Предельный размер генерируемого массива
     * @param strictSizeBound True - метод будет возвращать массив строго заданного размера ({@code sizeBound})
     * @return Случайный целый массив, заданной длины
     * @exception IllegalArgumentException При некорректных значениях пределов
     */
    static int[] generateRandomArray(Random rnd, int lowBound, int highBound, int sizeBound, boolean strictSizeBound)
    {
        if (lowBound >= highBound)
        {
            throw new IllegalArgumentException(String.format("Нижняя граница (%d) должна быть меньше верхней (%d)",
                    lowBound, highBound));
        }
        else if (sizeBound < 1)
        {
            throw new IllegalArgumentException("Предельный размер массива должен быть положительным числом!");
        }
        int size = strictSizeBound ? sizeBound : rnd.nextInt(sizeBound) + 1;
        return rnd.ints(size, lowBound, highBound + 1).toArray();
    }
}
