package org.mayurmu.homework13.model;

import org.mayurmu.homework11.HumanEx;

import java.util.StringJoiner;


public class AddressWithID extends HumanEx.Address implements EntityWithID
{
    public static final AddressWithID EMPTY = new AddressWithID();
    public static int MAX_FLAT_NUMBER_LEN = 4;
    
    private int id;
    
    @Override
    public int getId()
    {
        return id;
    }
    
    @Override
    public void setId(int id)
    {
        this.id = id;
    }
    
    
    
    //region 'Конструкторы'
    
    /**
     * Пустой конструктор (видимо под теоретический Framework)
     */
    public AddressWithID()
    {
        super("","","", "");
    }
    
    /**
     * Конструктор преобразования из унаследованного "Адреса без ИД"
     *
     * @param adr Адрес без ИД (больше ничем не отличается от {@link AddressWithID})
     *
     * @exception IllegalArgumentException Когда {@code flat > MAX_FLAT_NUMBER_LEN}
     */
    public AddressWithID(HumanEx.Address adr)
    {
        this(adr.city, adr.street, adr.house, adr.flat);
    }
    
    /**
     * Полный конструктор
     *
     * @exception IllegalArgumentException Когда {@code flat > MAX_FLAT_NUMBER_LEN}
     */
    public AddressWithID(String city, String street, String house, String flat)
    {
        super(city, street, house, flat);
        if (flat != null && flat.length() > MAX_FLAT_NUMBER_LEN)
        {
            throw new IllegalArgumentException("Номер квартиры не может быть длиннее " + MAX_FLAT_NUMBER_LEN + " символов");
        }
    }
    
    //endregion 'Конструкторы'
    
    
    @Override
    public String toString()
    {
        return new StringJoiner(", ")
                .add("id=" + id)
                .add(super.toString())
                .toString();
    }
    
    /**
     * Сравнение по всем полям без учёта регистра, ИД при этом не учитывается, т.к. это тех. поле, которое могло быть
     *          получено из разных систем
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        
        AddressWithID that = (AddressWithID) o;
    
        return city.equalsIgnoreCase(that.city) && street.equalsIgnoreCase(that.street) &&
                house.equalsIgnoreCase(that.house) && flat.equalsIgnoreCase(that.flat);
    }
    
    @Override
    public int hashCode()
    {
        return (city + street + house + flat).toLowerCase().hashCode();
    }
    
    /**
     * Более эффективная версия средства проверки Адреса на пустоту (сравнивает ссылки, а не поля)
     */
    public boolean isEmpty()
    {
        return equals(EMPTY);
    }
}
