package org.mayurmu.homework13.model;

import org.mayurmu.homework13.repository.CrudRepository;

/**
 * Базовый класс сущностей с ИД в БД
 *
 * @apiNote Нужен для поддержки обобщённого репозитория {@link CrudRepository}
 */
public interface EntityWithID
{
    /**
     * @apiNote ИД вновь созданной сущности НЕ задан, т.к. он будет сформирован в БД
     */
    int getId();
    
    /**
     * @apiNote Приходится делать ID доступным для записи, т.к. иначе как его задать из БД?
     */
    void setId(int id);
}
