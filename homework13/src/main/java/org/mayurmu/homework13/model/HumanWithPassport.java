package org.mayurmu.homework13.model;

import org.mayurmu.homework11.HumanEx;

import java.util.StringJoiner;

/**
 * Человек с паспортом и ИД
 *
 * @implNote Люди считаются равными при равенстве паспортов (но не ИД)
 */
public class HumanWithPassport extends HumanEx implements EntityWithID
{
    private int id;
    
    @Override
    public int getId()
    {
        return id;
    }
    
    @Override
    public void setId(int id)
    {
        this.id = id;
    }
    
    /**
     * @return Возвращает Адрес или {@link AddressWithID#EMPTY}
     */
    @Override
    public AddressWithID getAddress()
    {
        // обеспечиваем нормальную работу геттера, на случай, если Адрес установили через класс предка
        final HumanEx.Address superAdr = super.getAddress();
        AddressWithID addressWithID = superAdr instanceof AddressWithID ? (AddressWithID) superAdr : null;
        return addressWithID != null ? addressWithID : AddressWithID.EMPTY;
    }
    
    
    
    //region 'Конструкторы'
    
    /**
     * Пустой конструктор (видимо под теоретический Framework)
     */
    public HumanWithPassport()
    {
        super();
    }
    
    /**
     * Упрощённый конструктор без Отчества и адреса
     */
    public HumanWithPassport(String name, String lastName, int age, String passportSeries, String passportNumber)
    {
        super(name, lastName, "", age, passportSeries, passportNumber);
    }
    
    /**
     * Упрощённый конструктор без адреса
     */
    public HumanWithPassport(String name, String lastName, String patronymic, int age, String passportSeries, String passportNumber)
    {
        super(name, lastName, patronymic, age, passportSeries, passportNumber);
    }
    
    /**
     * Полный конструктор человека с паспортом и адресом
     *
     * @throws IllegalArgumentException Когда не задан паспорт или возраст его получения не достигнут (см.: {@link #MIN_AGE})
     */
    public HumanWithPassport(String name, String lastName, String patronymic, int age, String passportSeries, String passportNumber,
                             AddressWithID addressWithID)
    {
        super(name, lastName, patronymic, age, passportSeries, passportNumber);
        super.setAddress(addressWithID);
    }
    
    /**
     * Конструктор преобразования из унаследованного Человека
     *
     * @apiNote Он же по сути и конструктор копирования всех полей, кроме ИД
     */
    public HumanWithPassport(HumanEx source)
    {
        super(source.getName(), source.getLastName(), source.getPatronymic(), source.getAge(), source.getPassportSeries(),
                source.getPassportNumber());
        HumanEx.Address sourceAdr = source.getAddress();
        if (sourceAdr instanceof AddressWithID)
        {
            super.setAddress(sourceAdr);
        }
        else
        {
            super.setAddress(new AddressWithID(sourceAdr));
        }
    }
    
    //endregion 'Конструкторы'
    
    
    
    @Override
    public String toString()
    {
        final HumanEx.Address superAddress = super.getAddress();
        AddressWithID adr = superAddress instanceof AddressWithID ? (AddressWithID) superAddress : null;
        return new StringJoiner("\n")
                .add(super.getFullName() + " " + super.getPatronymic())
                .add("Возраст: " + getAge())
                .add("Паспорт:")
                .add("Серия: " + super.getPassportSeries() + " Номер: " + super.getPassportNumber())
                .add(adr == null || adr.isEmpty ? "-Адрес отсутствует-" : adr.toString())
                .toString();
    }
}
