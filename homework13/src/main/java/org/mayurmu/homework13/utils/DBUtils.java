package org.mayurmu.homework13.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

/**
 * Вспомогательный класс для взаимодействия с СУБД
 */
public class DBUtils
{
    private static final String DB_PROPS_RESOURCE_FILE_NANE = "db.properties";
    
    private static Properties properties;
    
    /**
     * Создаёт новое подключение к БД на основе файла настроек <b>db.properties</b> полученного из ресурсов.
     *
     * @apiNote Данные из файла <b>db.properties</b> парсятся один раз, так что изменения этого файла при последующих
     *      вызовах метода не учитываются.
     *
     * @return Новый экземпляр подключения к БД
     */
    public static Connection createConnection()
    {
        return getConnection(null);
    }
    
    /**
     * Проверят указанное подключение к БД или создаёт новое на основе файла настроек <b>db.properties</b> полученного из ресурсов.
     *
     * @apiNote Данные из файла <b>db.properties</b> парсятся один раз, так что изменения этого файла при последующих
     *      вызовах метода не учитываются.
     *
     * @return Новый экземпляр подключения к БД (или Существующий, если он в порядке)
     */
    public static Connection getConnection(Connection connection)
    {
        if (properties == null)
        {
            // так по идее путь должен рассчитываться динамически и быть подходящим и для тестов и для релиза в JAR
            URL dbPropsFilePath = Objects.requireNonNull(DBUtils.class.getClassLoader().getResource(DB_PROPS_RESOURCE_FILE_NANE));
            try (FileInputStream propFile = new FileInputStream(dbPropsFilePath.getPath()))
            {
                properties = new Properties();
                properties.load(propFile);
            }
            catch (IOException ex)
            {
                properties = null;
                throw new RuntimeException("Ошибка загрузки параметров подключения к БД:", ex);
            }
        }
        try
        {
            final int conTimeout = Integer.parseInt(properties.getProperty("db.timeout", "0"));
            if (connection == null || !connection.isValid(conTimeout))
            {
                connection = DriverManager.getConnection(
                        properties.getProperty("db.url"),
                        properties.getProperty("db.user"),
                        properties.getProperty("db.password"));
                // N.B. для упрощения на первый раз используем режим автоподтверждения ТА, хотя вряд ли это делают на практике?
                //connection.setAutoCommit(false);
            }
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Ошибка создания подключения к БД:", ex);
        }
        return connection;
    }
    
    /**
     * Вспомогательный метод добавления SQL-команд из ресурсных файлов в указанный пакетный скрипт
     *
     * @exception RuntimeException При неожиданных ошибках
     */
    public static void appendSqlToBatchStatement(Statement statement, ClassLoader loader, String resFileName)
    {
        try
        {
            Path scriptPath = Paths.get(Objects.requireNonNull(loader.getResource(resFileName)).toURI());
            List<String> scriptLines = Files.readAllLines(scriptPath);
            scriptLines.removeIf(line -> line.startsWith("--"));
            String script = String.join(System.lineSeparator(), scriptLines);
            if (script.startsWith("BEGIN;") || script.indexOf(";") == script.lastIndexOf(";"))
            {
                statement.addBatch(script);
            }
            else
            {
                for (String query : script.split(";"))
                {
                    statement.addBatch(query);
                }
            }
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Ошибка добавления команд в пакетный скрипт: " + resFileName, ex);
        }
    }
}
