package org.mayurmu.homework13.utils;

import org.mayurmu.homework13.model.AddressWithID;
import org.mayurmu.homework13.model.HumanWithPassport;


public class Humans
{
    /**
     * Сравнивает людей по значениям всех полей включая ИД (без учёта регистра),
     *  поле адреса при этом сравнивается
     *
     * @return true только, если все поля, включая ИД равны
     */
    public static boolean deepEquals(HumanWithPassport human1, HumanWithPassport human2)
    {
        boolean onlyOneIsNull = (human1 == null) ^ (human2 == null);
        if (onlyOneIsNull)
        {
            return false;
        }
        if (human1 == null)       // второй здесь может быть только Null из-за предыдущей проверки на ИСКЛЮЧАЮЩЕЕ ИЛИ.
        {
            return true;
        }
        boolean isHumanEquals = human1.getId() == human2.getId() && human1.getFullName().equalsIgnoreCase(human2.getFullName()) &&
                human1.getPassportSeriesAndNumber().equalsIgnoreCase(human2.getPassportSeriesAndNumber()) &&
                human1.getAge() == human2.getAge();
        
        if (!isHumanEquals)
        {
            return false;
        }
        final AddressWithID adr1 = human1.getAddress();
        final AddressWithID adr2 = human2.getAddress();
        return Addresses.deepEquals(human1.getAddress(), human2.getAddress());
    }
}
