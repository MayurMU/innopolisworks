package org.mayurmu.homework13.utils;

import org.mayurmu.homework13.model.AddressWithID;

public class Addresses
{
    /**
     * Сравнивает адреса по значениям всех полей включая ИД (без учёта регистра)
     *
     * @return true только, если все поля, включая ИД равны
     */
    public static boolean deepEquals(AddressWithID adr1, AddressWithID adr2)
    {
        boolean onlyOneAdrIsNull = (adr1 == null) ^ (adr2 == null);
        if (onlyOneAdrIsNull)
        {
            return false;
        }
        if (adr1 == null)       // adr2 здесь может быть только Null из-за предыдущей проверки на ИСКЛЮЧАЮЩЕЕ ИЛИ.
        {
            return true;
        }
        return adr1.getId() == adr2.getId() && adr1.city.equalsIgnoreCase(adr2.city) && adr1.street.equalsIgnoreCase(adr2.street) &&
                adr1.house.equalsIgnoreCase(adr2.house) && adr1.flat.equalsIgnoreCase(adr2.flat);
    }
}
