package org.mayurmu.homework13.utils;

import org.mayurmu.homework13.model.EntityWithID;


public class Entities
{
    
    /**
     * @apiNote По аналогии с методом {@link java.util.Objects#requireNonNull(Object)}
     */
    public static <T extends EntityWithID> void requireNonNullAndNonEmptyId(T entity)
    {
        requireNonNullAndNonEmptyId(entity, "");
    }
    /**
     * @apiNote По аналогии с методом {@link java.util.Objects#requireNonNull(Object, String)}
     *
     * @param name Название сущности, которое будет указано в тексте ошибки
     */
    public static <T extends EntityWithID> void requireNonNullAndNonEmptyId(T entity, String name)
    {
        if (name == null || name.trim().isEmpty())
        {
            name = "Аргумент";
        }
        if (entity == null || entity.getId() <= 0)
        {
            throw new IllegalArgumentException(entity == null ? name + " не задан" : name + " должен иметь положительный ID, " +
                    "однако был передан: " + entity.getId());
        }
    }
    
    /**
     * @apiNote По аналогии с методом {@link java.util.Objects#requireNonNull(Object)}
     */
    public static <T extends EntityWithID> void requireNonNullButEmptyId(T entity)
    {
        requireNonNullButEmptyId(entity, "");
    }
    /**
     * @apiNote По аналогии с методом {@link java.util.Objects#requireNonNull(Object, String)}
     *
     * @param name Название сущности, которое будет указано в тексте ошибки
     */
    public static <T extends EntityWithID> void requireNonNullButEmptyId(T entity, String name)
    {
        if (name == null || name.trim().isEmpty())
        {
            name = "Аргумент";
        }
        if (entity == null || entity.getId() != 0)
        {
            throw new IllegalArgumentException(entity == null ? name + " не задан" : name + " Не может иметь ID отличный от 0, " +
                    "однако был передан: " + entity.getId());
        }
    }
    
}
