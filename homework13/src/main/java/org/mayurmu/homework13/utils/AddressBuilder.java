package org.mayurmu.homework13.utils;

import org.mayurmu.homework11.HumanEx;
import org.mayurmu.homework13.model.AddressWithID;

import java.util.*;

/**
 * Класс реализующий паттерн Билдер для классов {@link AddressWithID}
 */
public final class AddressBuilder
{
    private static class Node implements Comparable<Node>
    {
        public final String value;
        public final NodeLevel level;

        public Node(String value, NodeLevel level)
        {
            this.value = value;
            this.level = level;
        }

        @Override
        public int compareTo(Node ob)
        {
            return level.compareTo(ob.level);
        }
    
        @Override
        public String toString()
        {
            return String.format("(уровень %s - \"%s\")", level.name, value);
        }
    }
    
    
    /**
     * Уровни иерархии адреса
     */
    private enum NodeLevel
    {
        City(0, "Город"),
        Street(1, "Улица"),
        House(2, "Дом"),
        Flat(3, "Квартира");
    
        public final int hierarchyLevel;
        public final String name;
    
        NodeLevel(int hierarchyLevel, String name)
        {
            this.hierarchyLevel = hierarchyLevel;
            this.name = name;
        }
    }
    
    private final List<Node> _addressNodes = new ArrayList<>();
    
    public AddressBuilder()
    {
    }
    
    /**
     * Альтернативный вариант получения Строителя
     */
    public static AddressBuilder createBuilder()
    {
        return new AddressBuilder();
    }
    
    public AddressBuilder city(String city)
    {
        _addressNodes.add(new Node(city, NodeLevel.City));
        return this;
    }
    
    public AddressBuilder street(String street)
    {
        _addressNodes.add(new Node(street, NodeLevel.Street));
        return this;
    }
    
    public AddressBuilder house(String house)
    {
        _addressNodes.add(new Node(house, NodeLevel.House));
        return this;
    }
    
    public AddressBuilder flat(String flat)
    {
        _addressNodes.add(new Node(flat, NodeLevel.Flat));
        return this;
    }
    
    private String getLevelValue(NodeLevel level)
    {
        return _addressNodes.stream().filter(x -> x.level == level).findAny().map(n -> n.value).orElse("");
    }
    
    private void throwOnIncorrectAddress()
    {
        if (_addressNodes.stream().noneMatch(x -> x.level == NodeLevel.City && !x.value.trim().isEmpty()))
        {
            throw new IllegalStateException("Адрес должен начинаться с уровня Города, текущее содержимое билдера:\n " + this);
        }
        _addressNodes.sort(Node::compareTo);
        Node prev = null;
        for (Node n : _addressNodes)
        {
            if (prev != null && (n.level.hierarchyLevel - prev.level.hierarchyLevel != 1))
            {
                throw new IllegalStateException("Нарушение иерархии элементов адреса, текущее содержимое билдера:\n " + this);
            }
            prev = n;
        }
    }
    
    /**
     * Метод конкретизации билдера
     *
     * @exception IllegalStateException Если не задан Город или задан только Город, или
     *      не соблюдена иерархия элементов Адреса (например, не задана улица, когда задан дом)
     */
    public AddressWithID toAddressWithID()
    {
        throwOnIncorrectAddress();
        return new AddressWithID(getLevelValue(NodeLevel.City), getLevelValue(NodeLevel.Street),
                getLevelValue(NodeLevel.House), getLevelValue(NodeLevel.Flat));
    }
    
    @Override
    public String toString()
    {
        return Arrays.toString(_addressNodes.stream().map(Node::toString).toArray());
    }
}
