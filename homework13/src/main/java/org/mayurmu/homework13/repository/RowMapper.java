package org.mayurmu.homework13.repository;

import org.mayurmu.homework13.model.EntityWithID;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Создан для преобразования объекта в строку БД и наоборот
 *
 * @implNote По аналогии с примером из консультации, правда не совсем ясно, зачем его офромлять именно в виде ф-го интерфейса,
 *          ведь {@link ResultSet не поддерживает Stream API} ?!
 *
 * @param <T> Модель БД
 */
@FunctionalInterface
public interface RowMapper <T extends EntityWithID>
{
    T mapRow(ResultSet row) throws SQLException;
}
