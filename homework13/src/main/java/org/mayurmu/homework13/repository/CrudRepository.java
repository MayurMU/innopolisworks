package org.mayurmu.homework13.repository;

import org.mayurmu.homework13.model.EntityWithID;
import org.mayurmu.homework13.repository.exceptions.*;

import java.util.List;
import java.util.Optional;

/**
 * Обобщённый репозиторий со всеми операциями (CRUD)
 *
 * @apiNote В данном случае нужен по большей части для единообразия методов конкретных репозиториев, т.к. на JDBC его
 *      реализация вряд ли возможна (разве что с помощью рефлексии).
 *
 * @param <T> Сущность с целочисленным идентификатором
 */
//TODO: по идее этот интерфейс нужно разбивать на несколько, вроде ReadableRepository, WritableRepository и т.д., а уже от них возможно наследовать этот.
public interface CrudRepository<T extends EntityWithID>
{
    /**
     * Возвращает список всех сущностей указанного типа
     *
     * @exception RepositoryException При непредвиденных ошибках выборки всех записей для указанной сущности
     */
    List<T> getAll();
    
    /**
     * Возвращает сущность с указанным ИД
     *
     * @exception RepositoryException При непредвиденных ошибках получения сущности по ИД
     * @exception IllegalArgumentException Когда {@code id <= 0}
     */
    Optional<T> getById(int id);
    
    /**
     * Создаёт новый экземпляр указанной сущности
     *
     * @param entity {{@link T#getId()} должен быть равен 0
     *
     * @exception IllegalArgumentException Когда {@code entity == null || entity.getId() != 0} (это исключение выбрасывается,
     *            чтобы показать, что ИД, будет задан извне - физическим хранилищем).
     * @exception RepositoryException При ошибках создания новой записи, в т.ч. из-за нарушений целостности (типа Unique, FK, PK).
     */
    void create(T entity);
    
    /**
     * Обновляет данные для указанной сущности
     *
     * @param entity {{@link T#getId()} должен быть отличен от 0
     *
     * @exception IllegalArgumentException Когда {@code entity == null || entity.getId() == 0}.
     * @exception RepositoryException При ошибках обновления данных, в т.ч. нарушениях целостности (типа Unique, FK, PK).
     */
    void update(T entity);
    
    /**
     * Удаляет экземпляр указанной сущности из репозитория
     *
     * @param entity {{@link T#getId()} должен быть отличен от 0
     *
     * @exception RepositoryException При непредвиденных ошибках удаления данных
     * @exception IllegalArgumentException Когда {@code entity == null || entity.getId() == 0}
     */
    void delete(T entity);
}
