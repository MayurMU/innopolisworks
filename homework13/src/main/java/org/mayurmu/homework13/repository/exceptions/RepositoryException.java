package org.mayurmu.homework13.repository.exceptions;

/**
 * Предок всех исключений доступа к данным
 */
public class RepositoryException extends RuntimeException
{
    public RepositoryException(String message)
    {
        super(message);
    }
    
    public RepositoryException(String message, Throwable reason)
    {
        super(message, reason);
    }
}
