package org.mayurmu.homework13.repository;

import org.mayurmu.homework13.model.HumanWithPassport;

import java.util.Map;
import java.util.Optional;

public interface CrudHuman extends CrudRepository<HumanWithPassport>
{
     /**
      * Метод возвращает распределение людей по городам (в убывающем порядке)
      *
      * @param topCount Сколько наиболее популярных городов вернуть
      *
      * @return Словарь отсортированный по численности жителей по убыванию, затем по названию городов по алфавиту,
      *   например: <p></p><p>{@code {Новосибирск=22, Кемерово=17, Ростов-на-дону=17, Ставрополь=16, Магнитогорск=15, Самара=15}}</p>
      *
      * @exception IllegalArgumentException При {@code topCount <= 0}
      */
     Map<String, Long> getHumanCountByCityStatistics(int topCount);
     
     /**
      * Возвращает человека по номеру его паспорта
      */
     Optional<HumanWithPassport> getHumanByPassport(String series, String number);
     
}
