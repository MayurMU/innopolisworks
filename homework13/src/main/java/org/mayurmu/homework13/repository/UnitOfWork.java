package org.mayurmu.homework13.repository;

/**
 * Класс - Единая точка доступа к репозиториям (а также менеджер транзакций - это не реализовано)
 */
public interface UnitOfWork extends AutoCloseable
{
    CrudAddress getAddressRepository();
    
    CrudHuman getHumansRepository();
}
