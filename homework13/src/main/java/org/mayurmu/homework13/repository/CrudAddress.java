package org.mayurmu.homework13.repository;

import org.mayurmu.homework13.model.AddressWithID;

import java.util.Optional;

/**
 * @implNote  Этот интерфейс выделен для потенциального расширения функций репозитория адресов
 */
public interface CrudAddress extends CrudRepository<AddressWithID>
{
    int getAddressIdByData(AddressWithID address);
}
