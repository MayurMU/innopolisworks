package org.mayurmu.homework13.data;

import org.mayurmu.homework13.model.EntityWithID;
import org.mayurmu.homework13.repository.CrudRepository;
import org.mayurmu.homework13.repository.RowMapper;
import org.mayurmu.homework13.repository.exceptions.RepositoryException;
import org.mayurmu.homework13.utils.Entities;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Абстрактная реализация обобщённого репозитория сущностей БД
 */
public abstract class CrudRepositoryJdbcImpl<T extends EntityWithID> implements CrudRepository<T>
{
    protected final Connection connection;
    
    protected abstract RowMapper<T> getRowMapper();
    
    protected abstract String getAllSql();
    protected abstract String getFindByIdSql();
    protected abstract String getInsertSql();
    protected abstract String getUpdateSql();
    protected abstract String getDeleteSql();
    
    /**
     * Метод заполняющий параметры SQL-запроса на основе полей экземпляра сущности {@code entity}
     *
     * @param entity Объект-источник данных для параметров запроса
     * @param dmlQuery подготовленный запрос, созданный на основе {@link #getInsertSql()}.
     *
     * @implSpec Параметры в реализации метода должны следовать ровно в том же порядке, что и в {@link #getInsertSql()}.
     */
    protected abstract void fillCreateQueryParams(T entity, PreparedStatement dmlQuery) throws SQLException;
    
    /**
     * Метод заполняющий параметры SQL-запроса на основе полей экземпляра сущности {@code entity}
     *
     * @param entity Объект-источник данных для параметров запроса
     * @param dmlQuery подготовленный запрос, созданный на основе {@link #getUpdateSql()}.
     *
     * @implSpec Параметры в реализации метода должны следовать ровно в том же порядке, что и в {@link #getUpdateSql()}.
     */
    protected abstract void fillUpdateQueryParams(T entity, PreparedStatement dmlQuery) throws SQLException;
    
    /**
     * Скрытый конструктор по умолчанию, т.к. доступ к репозиториям идёт через Unit-of-Work
     *
     * @see <a href="https://metanit.com/sharp/mvc5/23.3.php">Паттерн Unit of Work</a>
     */
    protected CrudRepositoryJdbcImpl(Connection connection)
    {
        this.connection = connection;
    }
    
    @Override
    public List<T> getAll()
    {
        try (PreparedStatement getAllQuery = connection.prepareStatement(this.getAllSql());
             ResultSet results = getAllQuery.executeQuery())
        {
            List<T> models = new ArrayList<>();
            while (results.next())
            {
                models.add(getRowMapper().mapRow(results));
            }
            return models;
        }
        catch (Exception ex)
        {
            throw new RepositoryException("Неожиданная ошибка получения списка всех сущностей", ex);
        }
    }
    
    @Override
    public Optional<T> getById(int id)
    {
        if (id <= 0)
        {
            throw new IllegalArgumentException("ИД должен быть > 0, однако был передан: " + id);
        }
        try (PreparedStatement getByIdQuery = connection.prepareStatement(this.getFindByIdSql()))
        {
            getByIdQuery.setInt(1, id);
            try (ResultSet result = getByIdQuery.executeQuery())
            {
                return result.next() ? Optional.of(getRowMapper().mapRow(result)) : Optional.empty();
            }
        }
        catch (Exception ex)
        {
            throw new RepositoryException("Ошибка получения Человека по ИД: " + id, ex);
        }
    }
    
    
    @Override
    public void create(T entity)
    {
        Entities.requireNonNullButEmptyId(entity);
        String errMes = String.format("Не удалось создать новую запись в БД для сущности (%s)", entity.getClass().getSimpleName());
        
        try (PreparedStatement createQuery = connection.prepareStatement(this.getInsertSql(), Statement.RETURN_GENERATED_KEYS))
        {
            this.fillCreateQueryParams(entity, createQuery);
            
            if (createQuery.executeUpdate() != 1)
            {
                throw new RepositoryException(errMes + ":\n " + entity);
            }
            try (ResultSet keys = createQuery.getGeneratedKeys())
            {
                if (!keys.next())
                {
                    throw new RepositoryException(errMes + " - не получены Ключи из БД:\n " + entity);
                }
                //TODO: тут по-хорошему нужен ещё один абстрактный метод вроде String[] getGeneratedKeyNames()
                entity.setId(keys.getInt(1));
            }
        }
        catch (Exception ex)
        {
            if (ex instanceof RepositoryException)
            {
                throw (RepositoryException)ex;
            }
            errMes = String.format("Неожиданная ошибка добавления записи БД для новой сущности (%s):\n %s",
                    entity.getClass().getSimpleName(), entity);
            throw new RepositoryException(errMes, ex);
        }
    }
    
    
    @Override
    public void update(T entity)
    {
        Entities.requireNonNullAndNonEmptyId(entity);
        try (PreparedStatement updateQuery = connection.prepareStatement(this.getUpdateSql()))
        {
            this.fillUpdateQueryParams(entity, updateQuery);
            
            int rowCount = updateQuery.executeUpdate();
            if (rowCount != 1)
            {
                throw new RepositoryException(rowCount > 1 ? ("Ожидалось обновление одной, однако было обновлено " +
                        rowCount + " записей!") : "Ошибка обновления данных в БД для сущности:\n " + entity);
            }
        }
        catch (Exception ex)
        {
            if (ex instanceof RepositoryException)
            {
                throw (RepositoryException)ex;
            }
            throw new RepositoryException("Неожиданная ошибка обновления записи в БД для сущности:\n " + entity, ex);
        }
    }
    
    @Override
    public void delete(T entity)
    {
        Entities.requireNonNullAndNonEmptyId(entity);
        try (PreparedStatement deleteQuery = connection.prepareStatement(this.getDeleteSql()))
        {
            deleteQuery.setInt(1, entity.getId());
            int rowCount = deleteQuery.executeUpdate();
            if (rowCount != 1)
            {
                throw new RepositoryException(rowCount > 1 ? ("Ожидалось удаление одной, однако было удалено " +
                        rowCount + " записей!") : "Ошибка удаления данных в БД для сущности:\n " + entity);
            }
        }
        catch (Exception ex)
        {
            if (ex instanceof RepositoryException)
            {
                throw (RepositoryException)ex;
            }
            throw new RepositoryException("Неожиданная ошибка удаления записи в БД для сущности:\n " + entity, ex);
        }
    }

    
}
