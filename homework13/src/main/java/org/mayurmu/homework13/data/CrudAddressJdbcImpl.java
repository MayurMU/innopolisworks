package org.mayurmu.homework13.data;

import org.mayurmu.homework13.model.AddressWithID;
import org.mayurmu.homework13.repository.CrudAddress;
import org.mayurmu.homework13.repository.RowMapper;
import org.mayurmu.homework13.repository.exceptions.RepositoryException;
import org.mayurmu.homework13.utils.AddressBuilder;
import org.mayurmu.homework13.utils.Entities;

import java.sql.*;

/**
 * Реализация репозитория адресов
 */
public class CrudAddressJdbcImpl extends CrudRepositoryJdbcImpl<AddressWithID> implements CrudAddress
{
    //language=SQL
    private static final String GET_ALL_SQL = "SELECT * FROM addresses";
    //language=SQL
    public static final String GET_BY_ID_SQL = GET_ALL_SQL + " WHERE id_address = ?";
    //language=SQL
    public static final String INSERT_SQL = "INSERT INTO addresses(city, street, house, flat) VALUES (?, ?, ?, ?)";
    //language=SQL
    public static final String UPDATE_SQL = "UPDATE addresses SET city = ?, street = ?, house = ?, flat = ? WHERE id_address = ?";
    //language=SQL
    public static final String DELETE_SQL = "DELETE FROM addresses WHERE id_address = ?";
    //language=SQL
    public static final String GET_ADDRESS_BY_ALL_DATA_SQL = "SELECT id_address FROM addresses " +
        "WHERE lower(city) = ? AND lower(street) = ? AND lower(house) = ? AND lower(flat) = ?";
    
    protected static final RowMapper<AddressWithID> addressRowMapper = row -> {
        AddressWithID adr = AddressBuilder.createBuilder().
                city(row.getString("city")).
                street(row.getString("street")).
                house(row.getString("house")).
                flat(row.getString("flat")).toAddressWithID();
        adr.setId(row.getInt("id_address"));
        return adr;
    };

    
    
    /**
     * Скрытый конструктор по умолчанию
     */
    protected CrudAddressJdbcImpl(Connection connection)
    {
        super(connection);
    }
    
    
    
    @Override
    protected RowMapper<AddressWithID> getRowMapper()
    {
        return addressRowMapper;
    }
    
    @Override
    protected String getAllSql()
    {
        return GET_ALL_SQL;
    }
    
    @Override
    protected String getFindByIdSql()
    {
        //language=SQL
        return GET_BY_ID_SQL;
    }
    
    @Override
    protected String getInsertSql()
    {
        //language=SQL
        return INSERT_SQL;
    }
    
    @Override
    protected String getUpdateSql()
    {
        //language=SQL
        return UPDATE_SQL;
    }
    
    @Override
    protected String getDeleteSql()
    {
        //language=SQL
        return DELETE_SQL;
    }
    
    @Override
    protected void fillCreateQueryParams(AddressWithID entity, PreparedStatement dmlQuery) throws SQLException
    {
        dmlQuery.setString(1, entity.city.trim());
        dmlQuery.setString(2, entity.street.trim());
        dmlQuery.setString(3, entity.house.trim());
        dmlQuery.setString(4, entity.flat.trim());
    }
    
    @Override
    protected void fillUpdateQueryParams(AddressWithID entity, PreparedStatement dmlQuery) throws SQLException
    {
        fillCreateQueryParams(entity, dmlQuery);
        dmlQuery.setInt(5, entity.getId());
    }
    
    /**
     * Ищет ИД существующего Адреса по его данным
     *
     * @return 0, если адрес не найден, иначе его ИД
     */
    @Override
    public int getAddressIdByData(AddressWithID address)
    {
        //TODO: тут перед тем, как вставлять новый адрес по идее нужно сначала поискать,
        // нет ли уже подобных, чтобы, например проигнорировать регистр, а также всякие
        // случаи вроде: { "Новочеркасск"|"г. -//-"|"Г.-//-", "город -//-"}.
        // select * from Addresses where lower(adr.city) LIKE `г.%`
        // Хотя, это под вопросом, смотря чего мы хотим больше: сократить дублирование адресов (и соот-но размер
        // этой таблицы) или ускорить их добавление (без необходимости полного просмотра таблицы для каждого нового адреса)?

//    try (PreparedStatement searchQuery = super.connection.prepareStatement("SELECT * FROM addresses WHERE trim(lower(city)) " +
//        "SIMILAR TO '(г.?|гор.?|город|село|c.?|пос.?|п.?|пгт.) {1,}'? AND trim(lower(street)) SIMILAR TO '' AND trim(lower(house)) SIMILAR TO '' " +
//            "AND trim(lower(flat)) = ?"))
        Entities.requireNonNullButEmptyId(address, "Адрес");
        try (PreparedStatement searchAdrQuery = super.connection.prepareStatement(GET_ADDRESS_BY_ALL_DATA_SQL))
        {
            searchAdrQuery.setString(1, address.city.toLowerCase());
            searchAdrQuery.setString(2, address.street.toLowerCase());
            searchAdrQuery.setString(3, address.house.toLowerCase());
            searchAdrQuery.setString(4, address.flat.toLowerCase());
            try (ResultSet res = searchAdrQuery.executeQuery())
            {
                return !res.next() ? 0 : res.getInt(1);
            }
        }
        catch (Exception ex)
        {
            throw new RepositoryException("Ошибка получения Адреса по значениям его полей: " + address, ex);
        }
    }
}
