package org.mayurmu.homework13.data;

import org.apache.commons.lang3.StringUtils;
import org.mayurmu.homework13.model.AddressWithID;
import org.mayurmu.homework13.model.HumanWithPassport;
import org.mayurmu.homework13.repository.CrudHuman;
import org.mayurmu.homework13.repository.RowMapper;
import org.mayurmu.homework13.repository.exceptions.RepositoryException;
import org.mayurmu.homework13.utils.Entities;

import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

//TODO: Возможно стоит сделать отдельную перегрузку метода {@link CrudHuman#create(EntityWithID)}, чтобы создавать людей
//  вместе с адресами (или подбирать существующий при его наличии), если Человеку назначен Адрес без ИД.
/**
 * Реализация репозитория людей
 */
public class CrudHumanJdbcImpl extends CrudRepositoryJdbcImpl<HumanWithPassport> implements CrudHuman
{
    //language=SQL
    private static final String GET_ALL_SQL = "SELECT * FROM humans LEFT JOIN addresses ON Humans.address_id = Addresses.id_address";
    public static final String GET_HUMAN_BY_PASSPORT_SQL = GET_ALL_SQL +
        " WHERE humans.passport_series = ? AND humans.passport_number = ?";
    //language=SQL
    public static final String GET_BY_ID_SQL = GET_ALL_SQL + " WHERE id_human = ?";
    //language=SQL
    public static final String INSERT_SQL = "INSERT INTO humans(name, last_name, patronymic, age, passport_series, passport_number, address_id) " +
        "VALUES (?, ?, ?, ?, ?, ?, ?)";
    //language=SQL
    public static final String UPDATE_SQL = "UPDATE humans SET name = ?, last_name = ?, patronymic = ?, age = ?, passport_series = ?, " +
        "passport_number = ?, " +
        "address_id = ?  WHERE id_human = ?";
    //language=SQL
    public static final String DELETE_SQL = "DELETE FROM humans WHERE id_human = ?";
    //language=SQL
    public static final String GET_HUMAN_COUNT_BY_CITY_SQL = "SELECT lower(addresses.city) " +
        "AS trimmed_city, count(*) AS people_count FROM humans JOIN addresses ON " +
        "addresses.id_address = humans.address_id GROUP BY trimmed_city ORDER BY people_count DESC, trimmed_city" +
        " LIMIT ?";
    //language=SQL
    public static final String GET_HUMANS_WITH_SAME_ADDRESS_SQL = "SELECT id_human FROM humans WHERE address_id = ?";
    
    protected static final RowMapper<HumanWithPassport> humanWithPassportRowMapper = row -> {
        HumanWithPassport human = new HumanWithPassport(
                row.getString("name"),
                row.getString("last_name"),
                row.getString("patronymic"),
                row.getInt("age"),
                row.getString("passport_series"),
                row.getString("passport_number")
        );
        human.setId(row.getInt("id_human"));
        int adrId = row.getInt("address_id");
        if (adrId <= 0)
        {
            human.setAddress(AddressWithID.EMPTY);
        }
        else
        {
            human.setAddress(CrudAddressJdbcImpl.addressRowMapper.mapRow(row));
        }
        return human;
    };
    
    
    
    /**
     * Скрытый конструктор по умолчанию
     */
    protected CrudHumanJdbcImpl(Connection connection)
    {
        super(connection);
    }
    
    
    
    @Override
    protected RowMapper<HumanWithPassport> getRowMapper()
    {
        return humanWithPassportRowMapper;
    }
    
    @Override
    protected String getAllSql()
    {
        return GET_ALL_SQL;
    }
    
    @Override
    protected String getFindByIdSql()
    {
        //language=SQL
        return GET_BY_ID_SQL;
    }
    
    @Override
    protected String getInsertSql()
    {
        //language=SQL
        return INSERT_SQL;
    }
    
    @Override
    protected String getUpdateSql()
    {
        //language=SQL
        return UPDATE_SQL;
    }
    
    @Override
    protected String getDeleteSql()
    {
        //language=SQL
        return DELETE_SQL;
    }
    
    @Override
    protected void fillCreateQueryParams(HumanWithPassport human, PreparedStatement createQuery) throws SQLException
    {
        createQuery.setString(1, human.getName().trim());
        createQuery.setString(2, human.getLastName().trim());
        createQuery.setString(3, human.getPatronymic().trim());
        createQuery.setInt(4, human.getAge());
        createQuery.setString(5, human.getPassportSeries().trim());
        createQuery.setString(6, human.getPassportNumber().trim());
        final AddressWithID adr = human.getAddress();
        if (adr == null || adr.isEmpty)
        {
            createQuery.setNull(7, Types.INTEGER);
        }
        else
        {
            createQuery.setInt(7, adr.getId());
        }
    }
    
    @Override
    protected void fillUpdateQueryParams(HumanWithPassport human, PreparedStatement updateQuery) throws SQLException
    {
        fillCreateQueryParams(human, updateQuery);
        updateQuery.setInt(8, human.getId());
    }
    
    @Override
    public Optional<HumanWithPassport> getHumanByPassport(String series, String number)
    {
        try (PreparedStatement existsQuery = super.connection.prepareStatement(GET_HUMAN_BY_PASSPORT_SQL))
        {
            existsQuery.setString(1, series.trim());
            existsQuery.setString(2, number.trim());
            try (ResultSet result = existsQuery.executeQuery())
            {
                return result.next() ? Optional.of(getRowMapper().mapRow(result)) : Optional.empty();
            }
        }
        catch (Exception ex)
        {
            if (ex instanceof RepositoryException)
            {
                throw (RepositoryException)ex;
            }
            throw new RepositoryException("Ошибка поиска Человека по паспорту:\n " + series + " " + number, ex);
        }
    }
    
    /**
     * Удаляет также связанный Адрес, если там никто больше не живёт
     */
    @Override
    public void delete(HumanWithPassport human)
    {
        Entities.requireNonNullAndNonEmptyId(human, "Человек");
        super.delete(human);
        AddressWithID adr = human.getAddress();
        if (adr == null || adr.isEmpty)
        {
            return;
        }
        // если здесь не вылетели - удаляем Адрес человека, т.к., если на него больше нет ссылок
        // N.B. Хотел сначала сделать каскадное удаление по внешнему ключу, но понял, что оно работает в обратную сторону,
        // т.е. активируется при удалении родительской таблицы (т.е. Адреса) и может удалить Человека!
    
        //TODO: По сути этот поиск лишний, т.к. БД всё равно будет его делать проверяя правила внешних ключей,
        // т.е. достаточно вызвать new CrudAddressJdbcImpl(connection).delete(adr); НО, как определить, почему оно
        // не сработало (из-за ограничения FK или ещё чего-то?) - нужно анализировать коды ошибок Postgres !!!
        try (PreparedStatement searchHousematesQuery = super.connection.prepareStatement(GET_HUMANS_WITH_SAME_ADDRESS_SQL))
        {
            searchHousematesQuery.setInt(1, adr.getId());
            try (ResultSet results = searchHousematesQuery.executeQuery())
            {
                if (!results.next())
                {
                    new CrudAddressJdbcImpl(connection).delete(adr);
                }
            }
        }
        catch (Exception ex)
        {
            throw new RepositoryException("Неожиданная ошибка удаления Адреса связанного с удалённым человеком:\n " + human,
                    ex);
        }
    }
    
    
    @Override
    public Map<String, Long> getHumanCountByCityStatistics(int topCount)
    {
        if (topCount <= 0)
        {
            throw new IllegalArgumentException("Количество городов должно быть положительным, однако было передано: " + topCount);
        }
        try (PreparedStatement statisticsQuery = super.connection.prepareStatement(GET_HUMAN_COUNT_BY_CITY_SQL))
        {
            statisticsQuery.setInt(1, topCount);
            try (ResultSet results = statisticsQuery.executeQuery())
            {
                Map<String, Long> cityStatistics = new LinkedHashMap<>(topCount);
                while (results.next())
                {
                    cityStatistics.put(StringUtils.capitalize(results.getString(1)), results.getLong(2));
                }
                return cityStatistics;
            }
        }
        catch (Exception ex)
        {
            throw new RepositoryException("Неожиданная ошибка получения распределения Людей по Городам", ex);
        }
    }
    

}
