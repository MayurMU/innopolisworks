package org.mayurmu.homework13.data;

import org.mayurmu.homework13.model.AddressWithID;
import org.mayurmu.homework13.model.HumanWithPassport;
import org.mayurmu.homework13.repository.CrudAddress;
import org.mayurmu.homework13.repository.CrudHuman;
import org.mayurmu.homework13.repository.CrudRepository;
import org.mayurmu.homework13.repository.UnitOfWork;
import org.mayurmu.homework13.utils.DBUtils;

import java.sql.*;

/**
 * Класс - Единая точка доступа к репозиториям (а также менеджер транзакций - это не реализовано)
 */
public class UnitOfWorkJdbcImpl implements UnitOfWork
{
    private Connection connection;
    private CrudRepository<HumanWithPassport> humansRep;
    private CrudRepository<AddressWithID> addressRep;
    
    
    public CrudAddress getAddressRepository()
    {
        if (addressRep == null)
        {
            addressRep = new CrudAddressJdbcImpl(getConnection());
        }
        return (CrudAddress) addressRep;
    }
    
    public CrudHuman getHumansRepository()
    {
        if (humansRep == null)
        {
            humansRep = new CrudHumanJdbcImpl(getConnection());
        }
        //TODO: Разобраться, почему здесь нужно делать явное преобразование, ведь CrudHumanJdbcImpl
        // реализует CrudHuman?!
        return (CrudHuman) humansRep;
    }
    
    @Override
    public void close()
    {
        if (connection == null)
        {
            return;
        }
        try
        {
            connection.close();
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Ошибка закрытия соединения с БД", ex);
        }
    }
    
    private Connection getConnection()
    {
        if (connection == null)
        {
            connection = DBUtils.createConnection();
        }
        else
        {
            connection = DBUtils.getConnection(connection);
        }
        return connection;
    }
}
