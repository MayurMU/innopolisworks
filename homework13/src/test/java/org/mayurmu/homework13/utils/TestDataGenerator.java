package org.mayurmu.homework13.utils;

import com.github.javafaker.Faker;

import com.github.javafaker.Lorem;
import org.mayurmu.common.Utils;
import org.mayurmu.homework13.model.AddressWithID;
import org.mayurmu.homework13.model.HumanWithPassport;

import java.util.*;

/**
 * Класс для генерации тестовых данных
 */
public class TestDataGenerator
{
    public enum AddressGenPolicy
    {
        /**
         * В данном случае аналог {@link #VARIYNG}
         */
        UNKNOWN,
        /**
         * Может иметь, а может и не иметь адрес
         */
        VARIYNG,
        /**
         * Адрес обязателен
         */
        ALL_ADDRESS,
        /**
         * Адресов ни у кого нет
         */
        NO_ADDRESS
    }
    
    public static final Faker jfaker = new Faker(new Locale("ru"));
    
    public final int dataLength;
    public final List<HumanWithPassport> humansList = new ArrayList<>();
    public final List<AddressWithID> addressesList = new ArrayList<>();
    
    public static AddressWithID getNonExistingAddress()
    {
        return new AddressWithID(jfaker.lorem().characters(3, 15, true),
                jfaker.lorem().word(), jfaker.number().digits(2), jfaker.number().digits(AddressWithID.MAX_FLAT_NUMBER_LEN));
    }
    
    public static HumanWithPassport getNonExistingHuman()
    {
        final Lorem wordGen = jfaker.lorem();
        return new HumanWithPassport(wordGen.word(), wordGen.word(), wordGen.word(), jfaker.number().numberBetween(
                HumanWithPassport.MIN_AGE, 99), jfaker.number().digits(4), jfaker.number().digits(6)
        );
    }
    
    /**
     * Возвращает не существующего Человека с несуществующим Адресом
     */
    public static HumanWithPassport getNonExistingHumanWithAddress()
    {
        HumanWithPassport h = getNonExistingHuman();
        h.setAddress(getNonExistingAddress());
        return h;
    }
    
    /**
     * Конструктор сразу генерирующий внутреннюю коллекцию {@link #humansList}
     *
     * @param dataLength Кол-во тестовых записей для списка Людей
     */
    public TestDataGenerator(int dataLength)
    {
        this.dataLength = dataLength;
    }
    
    /**
     * Перезаполняет массив {@link #addressesList} валидными данными (без Null)
     */
    public void regenerateHumansData()
    {
        this.regenerateHumansData(AddressGenPolicy.VARIYNG);
    }
    
    /**
     * Перезаполняет массив {@link #addressesList} валидными данными (без Null)
     */
    public void regenerateHumansData(AddressGenPolicy adrGenType)
    {
        this.fillRandomHumans(humansList, adrGenType);
    }
    
    /**
     * Перезаполняет массив {@link #humansList} валидными данными (без Null)
     */
    public void regenerateAddressData()
    {
        this.fillRandomAddresses(addressesList);
    }
    
    /**
     * То же, что и {@link #regenerateHumansData()}, но возвращает копию массива ссылок на тестовые объекты
     */
    public List<HumanWithPassport> getNewHumansData()
    {
        return getNewHumansData(AddressGenPolicy.VARIYNG);
    }
    
    /**
     * То же, что и {@link #regenerateHumansData()}, но возвращает копию массива ссылок на тестовые объекты
     */
    public List<HumanWithPassport> getNewHumansData(AddressGenPolicy adrGenType)
    {
        regenerateHumansData(adrGenType);
        return humansList;
    }
    
    /**
     * То же, что и {@link #regenerateHumansData()}, но возвращает копию массива ссылок на тестовые объекты
     */
    public List<AddressWithID> getNewAddressesData()
    {
        regenerateAddressData();
        return addressesList;
    }
    
    /**
     * Метод генерации корректного Адреса со случайными данными (может отсутствовать только № квартиры)
     */
    private AddressWithID genNewAddress()
    {
        return genNewAddress(false, true);
    }
    
    /**
     * Метод генерации Адреса со случайными данными
     *
     * @param allowAddressesWithBrokenHierarchy Разрешать ли адреса с пропущенными элементами иерархии
     * @param onlyFlatIsOptional Только номер квартиры может отсутствовать
     *
     * @return Адрес вида: "Ростов-на-Дону, проспект Калинина, 23н, 918"
     *                     "Пенза, проспект Мира, 1н, корпус 7, 431"
     *                     "Пермь, улица Железнодорожная, 4, 499"
     *                     "Курск, пр. Чехова, 527Я, корпус 5"
     */
    private AddressWithID genNewAddress(boolean allowAddressesWithBrokenHierarchy, boolean onlyFlatIsOptional)
    {
        AddressBuilder aBuilder = new AddressBuilder().city(jfaker.address().city());
        boolean hasStreet = onlyFlatIsOptional || jfaker.bool().bool();
        if (hasStreet)
        {
            aBuilder.street(Utils.getCorrectRussianStreetName(jfaker.address().streetName()));
        }
        boolean hasHouse = onlyFlatIsOptional || ((allowAddressesWithBrokenHierarchy && !hasStreet) && jfaker.bool().bool());
        // если нарушение иерархии элементов адреса Запрещено, то наличии улицы будет строго детерминировано
        boolean hasFlat = (!hasStreet || hasHouse || allowAddressesWithBrokenHierarchy) && jfaker.bool().bool();
        if (hasFlat)
        {
            String flatNumber = String.valueOf(jfaker.number().numberBetween(1, 999));
            aBuilder.flat(flatNumber);
        }
        // сокращаем вероятность дублирования адресов - если квартиры нет, принудительно добавляем Букву к номеру дома
        if (hasHouse)
        {
            //если квартиры нет, то номер дома должен состоять минимум из 2 цифр, иначе может быть и из 1 цифры.
            String houseNumber = (jfaker.regexify("[1-9]\\d{" + (hasFlat ? "0" : "1") + ",2}" + (!hasFlat ||
                    jfaker.bool().bool() ? "[А-Иа-иК-Hк-нП-Чп-чЭЮЯэюя]" : "")));
            // если квартиры нет, то Корпус обязателен
            boolean hasBuilding = !hasFlat || jfaker.bool().bool();
            if (hasBuilding)
            {
                houseNumber += ", корпус " + jfaker.number().randomDigitNotZero();
            }
            aBuilder.house(houseNumber);
        }
        return aBuilder.toAddressWithID();
    }
    
    
    private void fillRandomAddresses(List<AddressWithID> addrs)
    {
        if (addrs == null)
        {
            throw new IllegalArgumentException("Нечего заполнять - список не может быть Null!");
        }
        addrs.clear();
        for (int i = 0; i < dataLength; i++)
        {
            addressesList.add(genNewAddress());
        }
    }
    
    /**
     * Метод заполнения массива людей (и связанных адресов) случайными значениями
     *
     * @param humans Список людей, который нужно заполнить
     */
    public void fillRandomHumans(List<HumanWithPassport> humans)
    {
        this.fillRandomHumans(humans, false, AddressGenPolicy.VARIYNG);
    }
    private void fillRandomHumans(List<HumanWithPassport> humans, AddressGenPolicy adrGenType)
    {
        this.fillRandomHumans(humans, false, adrGenType);
    }
    /**
     * Метод заполнения массива юзеров случайными значениями (в т.ч. может рандомно вставлять null вместо некоторых эл-в)
     *
     * @apiNote Внимание: очищает также и список адресов {@link #addressesList}
     *
     * @param humans        Список людей, который нужно заполнить
     * @param isInsertNulls Добавлять ли случайное кол-во Null объектов (от 1 до половины длины массива)
     * @param adrGenType    Политика генерации адресов (все, некоторые, никаких,...)
     *
     * @exception IllegalArgumentException При {@code humansList == null} или неизвестной политике назначения адресов.
     */
    public void fillRandomHumans(List<HumanWithPassport> humans, boolean isInsertNulls, AddressGenPolicy adrGenType)
    {
        if (humans == null)
        {
            throw new IllegalArgumentException("Нечего заполнять - список не может быть Null!");
        }
        if (adrGenType == AddressGenPolicy.UNKNOWN || Arrays.stream(AddressGenPolicy.values()).noneMatch(x -> x == adrGenType))
        {
            throw new IllegalArgumentException("Неизвестное значение перечисления: " + adrGenType.name());
        }
        addressesList.clear();
        
        final int nullsCount = jfaker.number().numberBetween(1, dataLength) / 2;
    
        for (int i = 0, n = 0; i < dataLength; i++)
        {
            if (isInsertNulls && (n < nullsCount && jfaker.bool().bool()))
            {
                humans.add(null);
                n++;
                continue;
            }
            int age = jfaker.random().nextInt(HumanWithPassport.MIN_AGE, 99);
            String name = jfaker.name().firstName();
            String lastName = jfaker.name().lastName();
            lastName = Utils.getCorrectGenderLastName(name, lastName);
            HumanWithPassport human = new HumanWithPassport(name, lastName, age, jfaker.number().digits(4),
                    jfaker.number().digits(6));

            boolean hasAddress = adrGenType == AddressGenPolicy.ALL_ADDRESS || (adrGenType == AddressGenPolicy.VARIYNG &&
                    jfaker.bool().bool());
            if (hasAddress)
            {
                AddressWithID adr = genNewAddress();
                human.setAddress(adr);
                addressesList.add(adr);
            }
            humans.add(human);
        }
    }
    
    public int getMaxAdrIdOrZero()
    {
        return addressesList.stream().map(AddressWithID::getId).max(Comparator.comparingInt(x -> x)).orElse(0);
    }
    
    public int getMaxHumanIdOrZero()
    {
        return humansList.stream().map(HumanWithPassport::getId).max(Comparator.comparingInt(x -> x)).orElse(0);
    }
    
    public AddressWithID getRandomAddress()
    {
        return addressesList.get(jfaker.random().nextInt(addressesList.size()));
    }
    
    public HumanWithPassport getRandomHuman()
    {
        return humansList.get(jfaker.random().nextInt(humansList.size()));
    }
    
    public Optional<HumanWithPassport> getRandomHumanWithAddress()
    {
        return humansList.stream().filter(x -> !x.getAddress().isEmpty()).findAny();
    }
   
}
