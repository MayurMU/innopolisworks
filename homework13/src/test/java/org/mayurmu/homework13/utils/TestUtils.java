package org.mayurmu.homework13.utils;

import org.mayurmu.homework13.model.EntityWithID;

import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class TestUtils
{
    public static void requireNonEmptyId(EntityWithID existingEntity)
    {
        assertNotEquals(0, existingEntity.getId(), "ИД искомой сущности должен отличаться от 0, " +
                "вероятно тест был запущен ДО вставки всех данных в БД - проверьте порядок запуска тестов!");
    }
}
