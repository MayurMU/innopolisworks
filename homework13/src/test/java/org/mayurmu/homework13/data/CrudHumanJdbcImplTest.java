package org.mayurmu.homework13.data;

import com.github.javafaker.Lorem;
import org.junit.jupiter.api.*;
import org.mayurmu.homework13.model.HumanWithPassport;
import org.mayurmu.homework13.repository.UnitOfWork;
import org.mayurmu.homework13.repository.exceptions.RepositoryException;
import org.mayurmu.homework13.utils.*;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Сначала тестируем Людей без адресов, а затем с адресами (см.: {@link CrudHumanWithAddressTest})
 */
@Order(2)
@TestClassOrder(ClassOrderer.OrderAnnotation.class)
@DisplayName("--------------- Тестируем Людей БЕЗ Адресов --------------")
public class CrudHumanJdbcImplTest
{
    // сколько генерировать Людей
    private static final int TEST_DATA_COUNT = 1000;
    private static final TestDataGenerator.AddressGenPolicy ADR_GEN_POLICY = TestDataGenerator.AddressGenPolicy.NO_ADDRESS;
    
    protected static UnitOfWork ufo;
    protected static TestDataGenerator dataGenerator;
    protected static List<HumanWithPassport> testHumans;

    /**
     * Выполняет настройку теста и генерацию тестовых данных
     *
     * @apiNote На момент запуска теста БД указанная в <b>db.properties</b> должна существовать!
     */
    @BeforeAll
    public static void setUp()
    {
        dataGenerator = new TestDataGenerator(TEST_DATA_COUNT);
        testHumans = dataGenerator.getNewHumansData(ADR_GEN_POLICY);
        // убеждаемся, что все люди не имеют ИД до записи в БД
        assertTrue(testHumans.stream().allMatch(h -> h.getId() == 0), "Ошибка генерации тестовых данных");
        // создаём тестовую базу - (тестовый файл настроек берётся из тестовых же ресурсов).
        try (Connection con = DBUtils.createConnection(); Statement statement = con.createStatement())
        {
            statement.execute("TRUNCATE TABLE humans RESTART IDENTITY CASCADE");
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Не удалось очистить таблицу Людей", ex);
        }
        ufo = new UnitOfWorkJdbcImpl();
        // Первоначальная запись всех сгенерированных Людей в БД
        assertDoesNotThrow(() -> testHumans.forEach(h -> ufo.getHumansRepository().create(h)));
        // убеждаемся, что все Люди получили ИД из БД
        assertTrue(testHumans.stream().noneMatch(h -> h.getId() == 0),
                "Не удалась первоначальная запись всех сгенерированных Людей в БД");
    }
    
    /**
     * При завершении теста данные из базы Не удаляем
     */
    @AfterAll
    public static void shutDown()
    {
        if (ufo != null)
        {
            assertDoesNotThrow(() -> ufo.close());
        }
    }
    
    /**
     * @apiNote Обязательно должен вызываться первым, т.к. он проверяет, что все данные записались в БД корректно
     */
    @Nested
    @Order(1)
    @DisplayName("Запросы на выборку всех Людей")
    class getAll
    {
        @Test
        @DisplayName("Считывание всех Людей после первоначальной записи")
        void getAll_afterImport()
        {
            List<HumanWithPassport> allHumans = ufo.getHumansRepository().getAll();
            // размер коллекций мы тоже проверяем, т.к. containsAll() не учитывает дубликаты
            assertEquals(testHumans.size(), allHumans.size());
            assertTrue(allHumans.containsAll(testHumans));
        }
    }
    
    /**
     * @apiNote Проверка поиска по ИД может и не быть 2-м тестом, но обязательно должна идти перед изменением, созданием
     *      и удалением, т.к. они проверяют свою работу через неё!
     */
    @Nested
    @Order(2)
    @DisplayName("Ищем Людей по ИД")
    class getById
    {
        @RepeatedTest(3)
        @DisplayName("Ищем 3 случайных людей, которые заведомо существуют")
        void getById_existingPeople()
        {
            HumanWithPassport randomHuman = dataGenerator.getRandomHuman();
            TestUtils.requireNonEmptyId(randomHuman);
            HumanWithPassport foundHuman = ufo.getHumansRepository().getById(randomHuman.getId()).orElse(null);
            assertNotNull(foundHuman);
            assertTrue(Humans.deepEquals(randomHuman, foundHuman));
        }
        
        @Test
        @DisplayName("Ищем Человека, который заведомо не существует")
        void getById_nonExistingHuman()
        {
            int maxId = dataGenerator.getMaxHumanIdOrZero();
            assertNotEquals(0, maxId, "Не удалось получить тестового Человека с ИД отличным от 0, убедитесь, " +
                    "что люди уже добавлены в БД!");
            Optional<HumanWithPassport> foundHuman = ufo.getHumansRepository().getById(maxId + 1);
            assertFalse(foundHuman.isPresent());
        }
    }
    
    @Nested
    @DisplayName("Ищем людей по паспорту")
    class getHumanByPassport
    {
        @RepeatedTest(3)
        @DisplayName("Ищем 3 случайных людей с заведомо существующими Паспортами")
        void getHumanByPassport_existingPeople()
        {
            HumanWithPassport randomHuman = dataGenerator.getRandomHuman();
            TestUtils.requireNonEmptyId(randomHuman);
            HumanWithPassport foundHuman = ufo.getHumansRepository().getHumanByPassport(randomHuman.getPassportSeries(),
                    randomHuman.getPassportNumber()).orElse(null);
            assertNotNull(foundHuman);
            assertTrue(Humans.deepEquals(randomHuman, foundHuman));
        }
    
        @Test
        @DisplayName("Ищем человека с несуществующим Паспортом")
        void getHumanByPassport_nonExistingPassport()
        {
            Optional<HumanWithPassport> foundHumanOption = ufo.getHumansRepository().getHumanByPassport(
                TestDataGenerator.jfaker.number().digits(4),
                TestDataGenerator.jfaker.number().digits(6));
            assertFalse(foundHumanOption.isPresent());
        }

    }
    
    @Nested
    @DisplayName("Запросы на создание Людей")
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    class create
    {
        @Test
        @DisplayName("Попытка человека, у которого уже есть ИД (т.е. он скорее всего уже существует)")
        void create_probablyExistingHumanWithID()
        {
            HumanWithPassport existingHuman = dataGenerator.getRandomHuman();
            TestUtils.requireNonEmptyId(existingHuman);
            assertThrows(IllegalArgumentException.class, () -> ufo.getHumansRepository().create(existingHuman));
        }

        @Test
        @DisplayName("Попытка создать человека, у которого отличается только паспорт")
        void create_humanCopy()
        {
            HumanWithPassport existingHuman = dataGenerator.getRandomHuman();
            HumanWithPassport humanCopy = new HumanWithPassport(existingHuman);
            assertEquals(humanCopy.getId(),0, "Ошибка генерации тестовых данных");
            humanCopy.setPassportSeries(TestDataGenerator.jfaker.number().digits(4));
            humanCopy.setPassportNumber(TestDataGenerator.jfaker.number().digits(6));
            assertDoesNotThrow(() -> ufo.getHumansRepository().create(humanCopy));
            assertNotEquals(humanCopy.getId(),0);
            testHumans.add(humanCopy);
        }
    
        @Test
        @DisplayName("Попытка создать человека с повторяющимся паспортом")
        void create_existingHuman()
        {
            HumanWithPassport existingHuman = dataGenerator.getRandomHuman();
            HumanWithPassport humanCopy = new HumanWithPassport(existingHuman);
            assertEquals(humanCopy.getId(),0, "Ошибка генерации тестовых данных");
            assertThrows(RepositoryException.class, () -> ufo.getHumansRepository().create(humanCopy));
        }

        @RepeatedTest(3)
        @DisplayName("Создаём 3-х совершенно новых Людей")
        void create_newHumans()
        {
            HumanWithPassport allNewHuman = TestDataGenerator.getNonExistingHuman();
            ufo.getHumansRepository().create(allNewHuman);
            assertNotEquals(0, allNewHuman.getId());
            HumanWithPassport foundHuman = ufo.getHumansRepository().getById(allNewHuman.getId()).orElse(null);
            assertNotNull(foundHuman);
            assertEquals(allNewHuman, foundHuman);
            testHumans.add(allNewHuman);
        }
    }
    
    @Nested
    @DisplayName("Изменяем Людей в БД")
    class update
    {
        @Test
        @DisplayName("Пытаемся изменить Человека, который заведомо Не существует")
        void update_nonExistingHuman()
        {
            HumanWithPassport nonExistHuman = TestDataGenerator.getNonExistingHuman();
            assertEquals(0, nonExistHuman.getId(), "Ошибка генерации тестовых данных - несуществующий человек Не должен иметь ИД");
            assertThrows(IllegalArgumentException.class, () -> ufo.getHumansRepository().update(nonExistHuman));
            int maxId = dataGenerator.getMaxHumanIdOrZero();
            nonExistHuman.setId(maxId + 1);
            assertNotEquals(1, nonExistHuman.getId());
            assertThrows(RepositoryException.class, () -> ufo.getHumansRepository().update(nonExistHuman));
        }
    
        @RepeatedTest(3)
        @DisplayName("Изменяем 3-х заведомо существующих Людей")
        void update_existingHumans()
        {
            HumanWithPassport existingHuman = dataGenerator.getRandomHuman();
            TestUtils.requireNonEmptyId(existingHuman);
            final Lorem wordGen = TestDataGenerator.jfaker.lorem();
            // изменяем копию, а не Человека из "кэша" на случай провала теста
            HumanWithPassport changedHuman = new HumanWithPassport(existingHuman);
            changedHuman.setId(existingHuman.getId());
            changedHuman.setAge(TestDataGenerator.jfaker.number().numberBetween(100, 200));
            changedHuman.setName(wordGen.word());
            changedHuman.setLastName(wordGen.word());
            changedHuman.setPatronymic(wordGen.word());
            assertDoesNotThrow(() -> ufo.getHumansRepository().update(changedHuman));
            HumanWithPassport foundHuman = ufo.getHumansRepository().getById(changedHuman.getId()).orElse(null);
            assertNotNull(foundHuman);
            assertEquals(changedHuman, foundHuman);
            testHumans.remove(existingHuman);
            testHumans.add(changedHuman);
        }
    }
    
    @Nested
    @DisplayName("Удаление людей")
    class delete
    {
        @Test
        @DisplayName("Пытаемся удалить несуществующего Человека")
        void delete_nonExistingHuman()
        {
            HumanWithPassport nonExistHuman = TestDataGenerator.getNonExistingHuman();
            assertEquals(0, nonExistHuman.getId(), "Ошибка генерации тестовых данных - несуществующий Человек Не должен иметь ИД");
            assertThrows(IllegalArgumentException.class, () -> ufo.getHumansRepository().delete(nonExistHuman));
            int maxId = dataGenerator.getMaxHumanIdOrZero();
            nonExistHuman.setId(maxId + 1);
            assertNotEquals(1, nonExistHuman.getId());
            assertThrows(RepositoryException.class, () -> ufo.getHumansRepository().delete(nonExistHuman));
        }
    
        @RepeatedTest(3)
        @DisplayName("Удаляем 3-х существующих Людей")
        void delete_existingHuman()
        {
            HumanWithPassport existingHuman = dataGenerator.getRandomHuman();
            TestUtils.requireNonEmptyId(existingHuman);
            assertDoesNotThrow(() -> ufo.getHumansRepository().delete(existingHuman));
            assertFalse(ufo.getHumansRepository().getById(existingHuman.getId()).isPresent());
            testHumans.remove(existingHuman);
        }
    }
    

    


}