package org.mayurmu.homework13.data;

import org.junit.jupiter.api.*;
import org.mayurmu.homework13.model.AddressWithID;
import org.mayurmu.homework13.model.HumanWithPassport;
import org.mayurmu.homework13.repository.exceptions.RepositoryException;
import org.mayurmu.homework13.utils.DBUtils;
import org.mayurmu.homework13.utils.Humans;
import org.mayurmu.homework13.utils.TestDataGenerator;
import org.mayurmu.homework13.utils.TestUtils;

import java.sql.Connection;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @implNote  Класс полностью аналогичен своему предку за исключением первоначальной настройки, в данном случае создаются
 *          Люди и связанные с ними Адреса.
 */
@Order(3)
@TestClassOrder(ClassOrderer.OrderAnnotation.class)
@DisplayName("--------------- Тестируем Людей с Адресами и без ------------")
public class CrudHumanWithAddressTest extends CrudHumanJdbcImplTest
{
    // сколько генерировать Людей и возможно Адресов (адрес будет не у всех людей)
    private static final int TEST_DATA_COUNT = 1000;
    private static final TestDataGenerator.AddressGenPolicy ADR_GEN_POLICY = TestDataGenerator.AddressGenPolicy.VARIYNG;

    /**
     * Выполняет настройку теста и генерацию тестовых данных
     *
     * @apiNote На момент запуска теста БД указанная в <b>db.properties</b> должна существовать!
     */
    @BeforeAll
    //TODO: возможно тут как-то можно сократить дублирование кода, но не знаю как, т.к. этот метод должен быть статическим?
    public static void setUp()
    {
        dataGenerator = new TestDataGenerator(TEST_DATA_COUNT);
        testHumans = dataGenerator.getNewHumansData(ADR_GEN_POLICY);
        // убеждаемся, что все люди не имеют ИД до записи в БД
        assertTrue(testHumans.stream().allMatch(h -> h.getId() == 0), "Ошибка генерации тестовых данных");
        // создаём тестовую базу - (тестовый файл настроек берётся из тестовых же ресурсов).
        try (Connection con = DBUtils.createConnection(); Statement statement = con.createStatement())
        {
            statement.execute("TRUNCATE TABLE addresses RESTART IDENTITY CASCADE");
            statement.execute("TRUNCATE TABLE humans RESTART IDENTITY CASCADE");
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Не удалось очистить таблицы Адресов и Людей", ex);
        }
        ufo = new UnitOfWorkJdbcImpl();
        // Первоначальная запись всех сгенерированных Адресов в БД (сначала пишем Адреса, т.к. на них ссылаются Люди)
        assertDoesNotThrow(() -> dataGenerator.addressesList.forEach(adr -> ufo.getAddressRepository().create(adr)));
        // убеждаемся, что все адреса получили ИД из БД
        assertTrue(dataGenerator.addressesList.stream().noneMatch(adr -> adr.getId() == 0),
                "Не удалась Первоначальная запись всех сгенерированных Адресов в БД");
        // Первоначальная запись всех сгенерированных Людей в БД
        assertDoesNotThrow(() -> testHumans.forEach(h -> ufo.getHumansRepository().create(h)));
        // убеждаемся, что все Люди получили ИД из БД
        assertTrue(testHumans.stream().noneMatch(h -> h.getId() == 0),
                "Не удалась Первоначальная запись всех сгенерированных Людей в БД");
    }
    
    /**
     * Тесты на создание идут в начале, т.к. они понадобятся для проверки удаления {@link delete}
     */
    @Nested
    @Order(1)
    @DisplayName("Создаём людей с Адресами")
    class create
    {
        @Test
        @DisplayName("Пытаемся создать человека с несуществующим Адресом")
        void create_HumanWithNonExistingAdr()
        {
            HumanWithPassport human = TestDataGenerator.getNonExistingHumanWithAddress();
            assertEquals(0, human.getId(), "Ошибка генерации тестовых данных Человека");
            assertEquals(0, human.getAddress().getId(), "Ошибка генерации тестовых данных Адреса");
            assertThrows(RepositoryException.class, () -> ufo.getHumansRepository().create(human));
        }
    
        @RepeatedTest(3)
        @DisplayName("Пытаемся создать 3 людей с существующими Адресами")
        void create_HumanWithAdr()
        {
            HumanWithPassport human = TestDataGenerator.getNonExistingHuman();
            assertEquals(0, human.getId(), "Ошибка генерации тестовых данных Человека");
            AddressWithID randomAdr = dataGenerator.getRandomAddress();
            assertNotEquals(0, randomAdr.getId(), "Ошибка генерации тестовых данных Адреса");
            human.setAddress(randomAdr);
            assertDoesNotThrow(() -> ufo.getHumansRepository().create(human));
            assertNotEquals(0, human.getId(), "Ошибка создания Человека");
            HumanWithPassport found = ufo.getHumansRepository().getById(human.getId()).orElse(null);
            assertNotNull(found, "добавленный Человек Не найден?!");
            assertTrue(Humans.deepEquals(human, found));
            testHumans.add(human);
        }
    }
    
    @Nested
    @DisplayName("Удаление Людей и возможно Адресов")
    class delete
    {
        @Test
        @DisplayName("Удаляем существующего Человека, вместе с адресом, т.к. на этот адрес больше никто не ссылается")
        void delete_Human_Cascade()
        {
            // создание Адреса
            AddressWithID adr = TestDataGenerator.getNonExistingAddress();
            ufo.getAddressRepository().create(adr);
            assertNotEquals(0, adr.getId());
            assertTrue(ufo.getAddressRepository().getById(adr.getId()).isPresent());
            // создание Человека
            HumanWithPassport human = TestDataGenerator.getNonExistingHuman();
            human.setAddress(adr);
            ufo.getHumansRepository().create(human);
            assertNotEquals(0, human.getId());
            assertTrue(ufo.getHumansRepository().getById(human.getId()).isPresent());
            // Удаление человека (вместе с адресом)
            ufo.getHumansRepository().delete(human);
            assertFalse(ufo.getAddressRepository().getById(adr.getId()).isPresent());
            assertFalse(ufo.getHumansRepository().getById(human.getId()).isPresent());
        }

        @Test
        @DisplayName("Пытаемся удалить существующего Человека, вместе с адресом, на который ещё есть ссылки")
        void delete_Human_NoCascade()
        {
            AddressWithID existingAdr = dataGenerator.getRandomHumanWithAddress().orElseThrow(() ->
                    new IllegalStateException("Проверьте тестовые данные - не найден ни один Человек с Адресом!")).getAddress();
            TestUtils.requireNonEmptyId(existingAdr);
            // создание Человека с тем же адресом
            HumanWithPassport human = TestDataGenerator.getNonExistingHuman();
            human.setAddress(existingAdr);
            ufo.getHumansRepository().create(human);
            assertNotEquals(0, human.getId());
            assertTrue(ufo.getHumansRepository().getById(human.getId()).isPresent());
            // попытка удалить человека (адрес, на который есть ссылки Не будет удалён)
            ufo.getHumansRepository().delete(human);
            assertFalse(ufo.getHumansRepository().getById(human.getId()).isPresent());
            assertTrue(ufo.getAddressRepository().getById(existingAdr.getId()).isPresent());
        }
    }

    
    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    class Grouping_test
    {
        @Test
        void get_People_Count_By_City_Statistics()
        {
            final int maxCities = TestDataGenerator.jfaker.number().numberBetween(5, 20);
            Map<String, Long> results = ufo.getHumansRepository().getHumanCountByCityStatistics(maxCities);
            
            Stream<Map.Entry<String, Long>> expectedResults = testHumans.stream().filter(h -> !h.getAddress().isEmpty()).
                    collect(Collectors.groupingBy(h -> h.getAddress().city, Collectors.counting())).entrySet().stream().
                        sorted((o1, o2) -> {
                            int valueCompResult = o2.getValue().compareTo(o1.getValue());
                            return valueCompResult != 0 ? valueCompResult : o1.getKey().compareTo(o2.getKey());
                        }).limit(maxCities);
            
            // сравнение без преобразования коллекций (на основе кода отсюда: https://stackoverflow.com/a/34818800/2323972)
            Iterator<Map.Entry<String, Long>> iterActual = results.entrySet().iterator();
            Iterator<Map.Entry<String, Long>> iterExpected = expectedResults.iterator();

            while (iterActual.hasNext() && iterExpected.hasNext())
            {
                Map.Entry<String, Long> entryActual = iterActual.next();
                Map.Entry<String, Long> entryExpected = iterExpected.next();
                assertTrue(entryExpected.getKey().equalsIgnoreCase(entryActual.getKey()));
                assertEquals(entryExpected.getValue(), entryExpected.getValue());
            }
            assert !iterActual.hasNext() && !iterExpected.hasNext() : "Длины коллекций не совпадают!";
        }
    }
    
    
}
