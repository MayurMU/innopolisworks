package org.mayurmu.homework13.data;

import com.github.javafaker.Lorem;
import org.junit.jupiter.api.*;

import org.mayurmu.homework13.model.AddressWithID;
import org.mayurmu.homework13.repository.UnitOfWork;
import org.mayurmu.homework13.repository.exceptions.RepositoryException;
import org.mayurmu.homework13.utils.Addresses;
import org.mayurmu.homework13.utils.DBUtils;
import org.mayurmu.homework13.utils.TestDataGenerator;
import org.mayurmu.homework13.utils.TestUtils;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Сначала тестируем Адреса, т.к. это родительская ("главная") сущность, на которую ссылается Человек
 *
 * @implSpec  Чтобы аннотация {@link Order} работала на глобальном уровне, а не только для {@link Nested}-классов,
 *          в каталоге <i>/test/resources/</i> должен лежать файл <b>junit-platform.properties</b> с определённым содержимым,
 *          (см.: <a href="https://stackoverflow.com/a/66169140/2323972">Пост</a>
 *          и <a href="https://junit.org/junit5/docs/current/user-guide/#running-tests-config-params">Configuration Parameters</a>)
 */
@Order(1)
@TestClassOrder(ClassOrderer.OrderAnnotation.class)
@DisplayName("----- Тестируем Адреса --------")
public class CrudAddressJdbcImplTest
{
    // сколько генерировать Адресов
    private static final int TEST_DATA_COUNT = 1000;
    
    private static UnitOfWork ufo;
    private static TestDataGenerator dataGenerator;
    private static List<AddressWithID> testAddresses;
    
    /**
     * Выполняет настройку теста и генерацию тестовых данных
     *
     * @apiNote На момент запуска теста БД указанная в <b>db.properties</b> должна существовать!
     */
    @BeforeAll
    public static void setUp()
    {
        dataGenerator = new TestDataGenerator(TEST_DATA_COUNT);
        testAddresses = dataGenerator.getNewAddressesData();
        // убеждаемся, что все адреса не имеют ИД до записи в БД
        assertTrue(testAddresses.stream().allMatch(h -> h.getId() == 0), "Ошибка генерации тестовых данных");
        // создаём тестовую базу - (тестовый файл настроек берётся из тестовых же ресурсов).
        try (Connection con = DBUtils.createConnection(); Statement statement = con.createStatement())
        {
            statement.execute("TRUNCATE TABLE addresses RESTART IDENTITY CASCADE");
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Не удалось очистить таблицу Адресов", ex);
        }
        ufo = new UnitOfWorkJdbcImpl();
        // "Первоначальная запись всех сгенерированных данных в БД"
        assertDoesNotThrow(() -> testAddresses.forEach(adr -> ufo.getAddressRepository().create(adr)));
        // убеждаемся, что все адреса получили ИД из БД
        assertTrue(testAddresses.stream().noneMatch(adr -> adr.getId() == 0), "Не удалась Первоначальная запись всех сгенерированных данных в БД");
    }
    
    /**
     * При завершении теста данные из базы Не удаляем
     */
    @AfterAll
    public static void shutDown()
    {
        if (ufo != null)
        {
            assertDoesNotThrow(() -> ufo.close());
        }
    }
    
    /**
     * @apiNote Обязательно должен вызываться первым, т.к. он проверяет, что все данные записались в БД корректно
     */
    @Nested
    @Order(1)
    @DisplayName("Запросы на выборку всех Адресов")
    class getAll
    {
        @Test
        @DisplayName("Считывание всех Адресов после первоначальной записи")
        void getAll_afterImport()
        {
            List<AddressWithID> allAddresses = ufo.getAddressRepository().getAll();
            // размер коллекций мы тоже проверяем, т.к. containsAll() не учитывает дубликаты
            assertEquals(testAddresses.size(), allAddresses.size());
            assertTrue(allAddresses.containsAll(testAddresses));
        }
    }
    
    /**
     * @apiNote Проверка поиска по ИД может и не быть 2-м тестом, но обязательно должна идти перед изменением, созданием
     *      и удалением, т.к. они проверяют свою работу через неё!
     */
    @Nested
    @Order(2)
    @DisplayName("Ищем Адреса по ИД")
    class getById
    {
        @RepeatedTest(3)
        @DisplayName("Ищем 3 случайных адреса, которые заведомо существуют")
        void getById_existingAddresses()
        {
            AddressWithID randomAdr = dataGenerator.getRandomAddress();
            TestUtils.requireNonEmptyId(randomAdr);
            AddressWithID foundAdr = ufo.getAddressRepository().getById(randomAdr.getId()).orElse(AddressWithID.EMPTY);
            assertTrue(Addresses.deepEquals(randomAdr, foundAdr));
        }
        
        @Test
        @DisplayName("Ищем адрес, который заведомо не существует")
        void getById_nonExistingAddress()
        {
            int maxId = dataGenerator.getMaxAdrIdOrZero();
            assertNotEquals(0, maxId, "Не удалось получить тестовый Адрес с ИД отличным от 0, убедитесь, " +
                    "что адреса уже добавлены в БД!");
            Optional<AddressWithID> foundAdr = ufo.getAddressRepository().getById(maxId + 1);
            assertFalse(foundAdr.isPresent());
        }
    }
    

    @Nested
    @DisplayName("Запросы на создание Адресов")
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    class create
    {
        @Test
        @DisplayName("Попытка создать адрес, у которого уже есть ИД (т.е. он скорее всего уже существует)")
        void create_probablyExistingAddressWithID()
        {
            AddressWithID existingAddress = dataGenerator.getRandomAddress();
            TestUtils.requireNonEmptyId(existingAddress);
            assertThrows(IllegalArgumentException.class, () -> ufo.getAddressRepository().create(existingAddress));
        }
    
        @Test
        @DisplayName("Попытка создать уже существующий адрес")
        void create_alreadyExisting()
        {
            AddressWithID existingAddress = dataGenerator.getRandomAddress();
            AddressWithID adrWithoutID = new AddressWithID(existingAddress.city, existingAddress.street,
                    existingAddress.house, existingAddress.flat);
            assertThrows(RepositoryException.class, () -> ufo.getAddressRepository().create(adrWithoutID));
        }
    
        @RepeatedTest(3)
        @DisplayName("Создаём 3 совершенно новых Адреса")
        void create_newAddresses()
        {
            AddressWithID allNewAdr = TestDataGenerator.getNonExistingAddress();
            ufo.getAddressRepository().create(allNewAdr);
            assertNotEquals(0, allNewAdr.getId());
            AddressWithID foundAdr = ufo.getAddressRepository().getById(allNewAdr.getId()).orElse(AddressWithID.EMPTY);
            assertFalse(foundAdr.isEmpty);
            assertEquals(allNewAdr, foundAdr);
            testAddresses.add(allNewAdr);
        }
    }
    
    
    @Nested
    @DisplayName("Ищем ИД адресов по известным данным")
    class getAddressIdByData
    {
        @RepeatedTest(3)
        @DisplayName("Ищем 3 случайных адреса, которые заведомо существуют (без учёта регистра)")
        void getAddressIdByData_existingAddresses()
        {
            AddressWithID randomAdr = dataGenerator.getRandomAddress();
            TestUtils.requireNonEmptyId(randomAdr);
            int expectedId = randomAdr.getId();
            AddressWithID adrWithoutId = new AddressWithID(
                    TestDataGenerator.jfaker.bool().bool() ? randomAdr.city.toUpperCase() : randomAdr.city.toLowerCase(),
                    TestDataGenerator.jfaker.bool().bool() ? randomAdr.street.toUpperCase() : randomAdr.street.toLowerCase(),
                    TestDataGenerator.jfaker.bool().bool() ? randomAdr.house.toUpperCase() : randomAdr.house.toLowerCase(),
                    TestDataGenerator.jfaker.bool().bool() ? randomAdr.flat.toUpperCase() : randomAdr.flat.toLowerCase());
            int foundId = ufo.getAddressRepository().getAddressIdByData(adrWithoutId);
            assertEquals(expectedId, foundId);
        }
    
        @Test
        @DisplayName("Ищем адрес, который заведомо не существует")
        void getAddressIdByData_nonExistingAddress()
        {
            AddressWithID nonExistingAdr = TestDataGenerator.getNonExistingAddress();
            int foundId = ufo.getAddressRepository().getAddressIdByData(nonExistingAdr);
            assertEquals(0, foundId);
        }
    }
    
    
    @Nested
    @DisplayName("Изменяем адреса в БД")
    class update
    {
        @Test
        @DisplayName("Пытаемся изменить адрес, который заведомо Не существует")
        void update_nonExistingAddress()
        {
            AddressWithID nonExistAdr = TestDataGenerator.getNonExistingAddress();
            assertEquals(0, nonExistAdr.getId(), "Ошибка генерации тестовых данных - несуществующий адрес Не должен иметь ИД");
            assertThrows(IllegalArgumentException.class, () -> ufo.getAddressRepository().update(nonExistAdr));
            int maxId = dataGenerator.getMaxAdrIdOrZero();
            nonExistAdr.setId(maxId + 1);
            assertNotEquals(1, nonExistAdr.getId());
            assertThrows(RepositoryException.class, () -> ufo.getAddressRepository().update(nonExistAdr));
        }
    
        @RepeatedTest(3)
        @DisplayName("Изменяем 3 заведомо существующих Адреса")
        void update_existingAddress()
        {
            AddressWithID existingAdr = dataGenerator.getRandomAddress();
            TestUtils.requireNonEmptyId(existingAdr);
            final Lorem wordGen = TestDataGenerator.jfaker.lorem();
            AddressWithID changedAddress = new AddressWithID(wordGen.word(), wordGen.word(), wordGen.word(), String.valueOf(
                    TestDataGenerator.jfaker.number().randomNumber(AddressWithID.MAX_FLAT_NUMBER_LEN, false)));
            changedAddress.setId(existingAdr.getId());
            ufo.getAddressRepository().update(changedAddress);
            AddressWithID foundAdr = ufo.getAddressRepository().getById(changedAddress.getId()).orElse(AddressWithID.EMPTY);
            assertEquals(changedAddress, foundAdr);
            testAddresses.remove(existingAdr);
            testAddresses.add(changedAddress);
        }
    }
    
    @Nested
    @DisplayName("Удаляем несколько Адресов")
    class delete
    {
        @Test
        @DisplayName("Пытаемся удалить несуществующий адрес")
        void delete_nonExistingAddress()
        {
            AddressWithID nonExistAdr = TestDataGenerator.getNonExistingAddress();
            assertEquals(0, nonExistAdr.getId(), "Ошибка генерации тестовых данных - несуществующий адрес Не должен иметь ИД");
            assertThrows(IllegalArgumentException.class, () -> ufo.getAddressRepository().delete(nonExistAdr));
            int maxId = dataGenerator.getMaxAdrIdOrZero();
            nonExistAdr.setId(maxId + 1);
            assertThrows(RepositoryException.class, () -> ufo.getAddressRepository().delete(nonExistAdr));
        }
    
        @RepeatedTest(3)
        @DisplayName("Пытаемся удалить 3 существующих адреса")
        void delete_existingAddress()
        {
            AddressWithID existingAdr = dataGenerator.getRandomAddress();
            TestUtils.requireNonEmptyId(existingAdr);
            ufo.getAddressRepository().delete(existingAdr);
            assertFalse(ufo.getAddressRepository().getById(existingAdr.getId()).isPresent());
            testAddresses.remove(existingAdr);
        }
    }
}