--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

-- Started on 2022-09-07 13:12:55

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 212 (class 1259 OID 24801)
-- Name: addresses; Type: TABLE; Schema: public; Owner: dbmanager
--

CREATE TABLE public.addresses (
    id_address integer NOT NULL,
    city character varying(50)[],
    street character varying(100)[],
    house character varying(50)[],
    flat character varying(4)[]
);


ALTER TABLE public.addresses OWNER TO dbmanager;

--
-- TOC entry 211 (class 1259 OID 24800)
-- Name: Addresses_address_id_seq; Type: SEQUENCE; Schema: public; Owner: dbmanager
--

CREATE SEQUENCE public."Addresses_address_id_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Addresses_address_id_seq" OWNER TO dbmanager;

--
-- TOC entry 3327 (class 0 OID 0)
-- Dependencies: 211
-- Name: Addresses_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dbmanager
--

ALTER SEQUENCE public."Addresses_address_id_seq" OWNED BY public.addresses.id_address;


--
-- TOC entry 210 (class 1259 OID 24792)
-- Name: humans; Type: TABLE; Schema: public; Owner: dbmanager
--

CREATE TABLE public.humans (
    id_human integer NOT NULL,
    name character varying(30)[] NOT NULL,
    last_name character varying(30)[] NOT NULL,
    patronymic character varying(30)[],
    passport_series character(4) NOT NULL,
    passport_number character(6) NOT NULL,
    age smallint DEFAULT 14 NOT NULL,
    address_id integer,
    CONSTRAINT min_age_constraint CHECK ((age >= 14))
);


ALTER TABLE public.humans OWNER TO dbmanager;

--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 210
-- Name: TABLE humans; Type: COMMENT; Schema: public; Owner: dbmanager
--

COMMENT ON TABLE public.humans IS 'Люди, людишки и они же человеки, живущие в Телемилитряндии.';


--
-- TOC entry 3329 (class 0 OID 0)
-- Dependencies: 210
-- Name: CONSTRAINT min_age_constraint ON humans; Type: COMMENT; Schema: public; Owner: dbmanager
--

COMMENT ON CONSTRAINT min_age_constraint ON public.humans IS 'Минимальный возраст получения паспорта в РФ: 14 лет.';


--
-- TOC entry 209 (class 1259 OID 24791)
-- Name: Humans_id_human_seq; Type: SEQUENCE; Schema: public; Owner: dbmanager
--

CREATE SEQUENCE public."Humans_id_human_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Humans_id_human_seq" OWNER TO dbmanager;

--
-- TOC entry 3331 (class 0 OID 0)
-- Dependencies: 209
-- Name: Humans_id_human_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dbmanager
--

ALTER SEQUENCE public."Humans_id_human_seq" OWNED BY public.humans.id_human;


--
-- TOC entry 3172 (class 2604 OID 24819)
-- Name: addresses id_address; Type: DEFAULT; Schema: public; Owner: dbmanager
--

ALTER TABLE ONLY public.addresses ALTER COLUMN id_address SET DEFAULT nextval('public."Addresses_address_id_seq"'::regclass);


--
-- TOC entry 3169 (class 2604 OID 24809)
-- Name: humans id_human; Type: DEFAULT; Schema: public; Owner: dbmanager
--

ALTER TABLE ONLY public.humans ALTER COLUMN id_human SET DEFAULT nextval('public."Humans_id_human_seq"'::regclass);


--
-- TOC entry 3178 (class 2606 OID 24851)
-- Name: addresses AddressMustNotDuplicate; Type: CONSTRAINT; Schema: public; Owner: dbmanager
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT "AddressMustNotDuplicate" UNIQUE (city, street, house, flat);


--
-- TOC entry 3332 (class 0 OID 0)
-- Dependencies: 3178
-- Name: CONSTRAINT "AddressMustNotDuplicate" ON addresses; Type: COMMENT; Schema: public; Owner: dbmanager
--

COMMENT ON CONSTRAINT "AddressMustNotDuplicate" ON public.addresses IS 'Адреса по возможности НЕ должны дублировться, чтобы не захломлять БД.
P.S. Это, конечно, слабое ограничение, т.к. оно не учитывает даже регистр!';


--
-- TOC entry 3180 (class 2606 OID 24821)
-- Name: addresses Addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: dbmanager
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT "Addresses_pkey" PRIMARY KEY (id_address);


--
-- TOC entry 3174 (class 2606 OID 24811)
-- Name: humans Humans_pkey; Type: CONSTRAINT; Schema: public; Owner: dbmanager
--

ALTER TABLE ONLY public.humans
    ADD CONSTRAINT "Humans_pkey" PRIMARY KEY (id_human);


--
-- TOC entry 3176 (class 2606 OID 24862)
-- Name: humans passport_is_unique; Type: CONSTRAINT; Schema: public; Owner: dbmanager
--

ALTER TABLE ONLY public.humans
    ADD CONSTRAINT passport_is_unique UNIQUE (passport_series, passport_number);


--
-- TOC entry 3333 (class 0 OID 0)
-- Dependencies: 3176
-- Name: CONSTRAINT passport_is_unique ON humans; Type: COMMENT; Schema: public; Owner: dbmanager
--

COMMENT ON CONSTRAINT passport_is_unique ON public.humans IS 'Комбинация из серии и номера паспорта НЕ должна повторяться!';


--
-- TOC entry 3181 (class 2606 OID 24831)
-- Name: humans Addresses_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dbmanager
--

ALTER TABLE ONLY public.humans
    ADD CONSTRAINT "Addresses_fkey" FOREIGN KEY (address_id) REFERENCES public.addresses(id_address) NOT VALID;


--
-- TOC entry 3334 (class 0 OID 0)
-- Dependencies: 3181
-- Name: CONSTRAINT "Addresses_fkey" ON humans; Type: COMMENT; Schema: public; Owner: dbmanager
--

COMMENT ON CONSTRAINT "Addresses_fkey" ON public.humans IS 'Ссылка на таблицу Адреса, наличие такой ссылки делает таблицу Человек "зависимой" от таблицы Адрес.
';


--
-- TOC entry 3326 (class 0 OID 0)
-- Dependencies: 212
-- Name: TABLE addresses; Type: ACL; Schema: public; Owner: dbmanager
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.addresses TO task_13_manager;


--
-- TOC entry 3330 (class 0 OID 0)
-- Dependencies: 210
-- Name: TABLE humans; Type: ACL; Schema: public; Owner: dbmanager
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.humans TO task_13_manager;


-- Completed on 2022-09-07 13:12:55

--
-- PostgreSQL database dump complete
--

