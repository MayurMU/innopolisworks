create sequence "Humans_id_human_seq"
    as smallint;

create sequence "Addresses_address_id_seq"
    as smallint;

create table addresses
(
    id_address integer default nextval('"Addresses_address_id_seq"'::regclass) not null
        constraint "Addresses_pkey"
            primary key,
    city       varchar(50)[],
    street     varchar(100)[],
    house      varchar(50)[],
    flat       varchar(4)[],
    constraint "AddressMustNotDuplicate"
        unique (city, street, house, flat)
);

comment on constraint "AddressMustNotDuplicate" on addresses is 'Адреса по возможности НЕ должны дублировться, чтобы не захломлять БД.
P.S. Это, конечно, слабое ограничение, т.к. оно не учитывает даже регистр!';

alter sequence "Addresses_address_id_seq" owned by addresses.id_address;

create table humans
(
    id_human        integer  default nextval('"Humans_id_human_seq"'::regclass) not null
        constraint "Humans_pkey"
            primary key,
    name            varchar(30)[]                                               not null,
    last_name       varchar(30)[]                                               not null,
    patronymic      varchar(30)[],
    passport_series char(4)                                                     not null,
    passport_number char(6)                                                     not null,
    age             smallint default 14                                         not null
        constraint min_age_constraint
            check (age >= 14),
    address_id      integer
        constraint "Addresses_fkey"
            references addresses,
    constraint passport_is_unique
        unique (passport_series, passport_number)
);

comment on table humans is 'Люди, людишки и они же человеки, живущие в Телемилитряндии.';

comment on constraint min_age_constraint on humans is 'Минимальный возраст получения паспорта в РФ: 14 лет.';

comment on constraint "Addresses_fkey" on humans is 'Ссылка на таблицу Адреса, наличие такой ссылки делает таблицу Человек "зависимой" от таблицы Адрес.
';

comment on constraint passport_is_unique on humans is 'Комбинация из серии и номера паспорта НЕ должна повторяться!';

alter sequence "Humans_id_human_seq" owned by humans.id_human;


