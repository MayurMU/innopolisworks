-- Database: task_13_test

DROP DATABASE IF EXISTS task_13_test;

CREATE DATABASE task_13_test
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;

COMMENT ON DATABASE task_13_test
    IS 'БД под 13-ю задачу Иннополиса ("База данных населения города Телемилитряндии")';