SELECT trim(lower(CAST(addresses.city as text))) AS trimmed_city, count(*) AS people_count FROM humans JOIN addresses
    ON addresses.id_address = humans.address_id GROUP BY trimmed_city ORDER BY people_count DESC, trimmed_city LIMIT 10
