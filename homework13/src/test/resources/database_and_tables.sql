-- Скрипт полного создания базы вместе с таблицами - подходит для консольной утилиты psql.exe
--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

-- Started on 2022-09-07 13:05:32

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE task_13_test;
--
-- TOC entry 3326 (class 1262 OID 24790)
-- Name: task_13_test; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE task_13_test WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Russian_Russia.1251';


\connect task_13_test

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 212 (class 1259 OID 24801)
-- Name: addresses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.addresses (
    id_address integer NOT NULL,
    city character varying(50),
    street character varying(100),
    house character varying(50),
    flat character varying(4)
);


--
-- TOC entry 211 (class 1259 OID 24800)
-- Name: Addresses_address_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Addresses_address_id_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3327 (class 0 OID 0)
-- Dependencies: 211
-- Name: Addresses_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."Addresses_address_id_seq" OWNED BY public.addresses.id_address;


--
-- TOC entry 210 (class 1259 OID 24792)
-- Name: humans; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.humans (
    id_human integer NOT NULL,
    name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    patronymic character varying(30),
    passport_series character(4) NOT NULL,
    passport_number character(6) NOT NULL,
    age smallint DEFAULT 14 NOT NULL,
    address_id integer,
    CONSTRAINT min_age_constraint CHECK ((age >= 14))
);


--
-- TOC entry 209 (class 1259 OID 24791)
-- Name: Humans_id_human_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Humans_id_human_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 209
-- Name: Humans_id_human_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."Humans_id_human_seq" OWNED BY public.humans.id_human;


--
-- TOC entry 3172 (class 2604 OID 24819)
-- Name: addresses id_address; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses ALTER COLUMN id_address SET DEFAULT nextval('public."Addresses_address_id_seq"'::regclass);


--
-- TOC entry 3169 (class 2604 OID 24809)
-- Name: humans id_human; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.humans ALTER COLUMN id_human SET DEFAULT nextval('public."Humans_id_human_seq"'::regclass);


--
-- TOC entry 3178 (class 2606 OID 24851)
-- Name: addresses AddressMustNotDuplicate; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT "AddressMustNotDuplicate" UNIQUE (city, street, house, flat);


--
-- TOC entry 3180 (class 2606 OID 24821)
-- Name: addresses Addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT "Addresses_pkey" PRIMARY KEY (id_address);


--
-- TOC entry 3174 (class 2606 OID 24811)
-- Name: humans Humans_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.humans
    ADD CONSTRAINT "Humans_pkey" PRIMARY KEY (id_human);


--
-- TOC entry 3176 (class 2606 OID 24862)
-- Name: humans passport_is_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.humans
    ADD CONSTRAINT passport_is_unique UNIQUE (passport_series, passport_number);


--
-- TOC entry 3181 (class 2606 OID 24831)
-- Name: humans Addresses_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.humans
    ADD CONSTRAINT "Addresses_fkey" FOREIGN KEY (address_id) REFERENCES public.addresses(id_address) NOT VALID;


-- Completed on 2022-09-07 13:05:33

--
-- PostgreSQL database dump complete
--

