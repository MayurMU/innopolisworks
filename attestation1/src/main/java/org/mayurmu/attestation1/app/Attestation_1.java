package org.mayurmu.attestation1.app;

import org.mayurmu.attestation1.data.UsersRepositoryFileImpl;
import org.mayurmu.attestation1.model.User;
import org.mayurmu.attestation1.repository.UsersRepository;
import org.mayurmu.attestation1.repository.exceptions.*;
import org.mayurmu.attestation1.utils.TestDataGenerator;

import org.mayurmu.attestation1.utils.Users;
import org.mayurmu.common.Utils;
import org.mayurmu.exceptions.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Stream;

/**
 * Класс-приложения для проверки работы с репозиторием
 */
public class Attestation_1
{
    /**
     * Макс. кол-во линий файла выводимых на экран
     */
    private static final int MAX_DISPLAYED_LINES = 50;
    private static final String CSV_SEPARATOR = ";";
    private static final Scanner _scn = new Scanner(System.in);
    
    /**
     * Программа создаёт тестовый файл во временном каталоге системы %TEMP%\org.mayurmu.attestation1.Attestation_1\дата_время.csv
     *      И удаляет его (и подкаталог) автоматически по завершении работы JVM
     */
    public static void main(String[] args)
    {
        boolean isNormalExit = true, isUserCancelled = false;
        try
        {
            int count = Utils.inputIntNumber(_scn, "Введите количество пользователей для генерации тестовых данных", true);
            if (count == 0)
            {
                throw new UserInputCancelledException();
            }
            System.out.println("Ожидайте, выполняется создание временного файла...");
            // почему-то в Java это единственный способ получения каталога %TEMP% (нет готовой константы?!)
            String systemTempDirPath = System.getProperty("java.io.tmpdir");
            Path appTempDir = Paths.get(systemTempDirPath, Attestation_1.class.getName());
            appTempDir.toFile().deleteOnExit();
            if (Files.notExists(appTempDir))
            {
                Files.createDirectory(appTempDir);
            }
            Path tempFilePath = Paths.get(appTempDir.toString(), LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).
                replace("T", "_T_").replace(":", "_") + ".csv");
            tempFilePath.toFile().deleteOnExit();
            System.out.printf("Ожидайте, выполняется генерация тестовых данных и их запись во временный файл:%n%s%n...%n",
                tempFilePath);
            final TestDataGenerator dataGenerator = new TestDataGenerator(count);
            dataGenerator.writeToCSV(tempFilePath, CSV_SEPARATOR);
            System.out.println();
            printFileOrFirstLines(tempFilePath, MAX_DISPLAYED_LINES);
    
            try (UsersRepository repo = new UsersRepositoryFileImpl())
            {
                // здесь будем запоминать всех вновь добавленных и изменённых юзеров, чтобы потом проверить, что они сохранились в файле
                List<User> changedUsers = new ArrayList<>();
                //
                // 1. Открываем репозиторий и настраиваем его
                //
                repo.open(tempFilePath.toString(), "separator=;");
                //
                // 2а. Попытка доступа к элементу без загрузки данных - элемент не будет найден
                //
                System.out.println("**********************************************************************************");
                System.out.println("\t\t\tПроверка поиска");
                System.out.println("**********************************************************************************");
                System.out.println();
                UUID existingId = dataGenerator.getRandomUser().id;
                System.out.println("Ищем заведомо существующего пользователя по ИД, не загрузив данные в репозиторий...");
                printUserOrDefaultString(repo.findById(existingId).orElse(null), "ID", existingId.toString());
                //
                // 2б. Попытка доступа к известному элементу после загрузки данных
                //
                System.out.println("Загружаем данные и ищем того же пользователя...");
                repo.load();
                printUserOrDefaultString(repo.findById(existingId).orElse(null), "ID", existingId.toString());
                //
                // 2в. Ищем ещё двух юзеров (первый существует - второй нет)
                //
                System.out.println("Ищем ещё двух пользователей: одного заведомо известного и второго - не существующего");
                UUID existingId_2 = dataGenerator.getRandomUser().id;
                printUserOrDefaultString(repo.findById(existingId_2).orElse(null), "ID", existingId_2.toString());
                UUID nonexistentId = UUID.randomUUID();
                printUserOrDefaultString(repo.findById(nonexistentId).orElse(null), "ID", nonexistentId.toString());
                //
                // 2г. Ищем пользователя по логину (регистр не имеет значения)
                //
                String existingLogin = dataGenerator.getRandomUser().login.toUpperCase();
                System.out.printf("Ищем заведомо существующего пользователя по Логину \"%s\"...%n", existingLogin);
                printUserOrDefaultString(repo.findByLogin(existingLogin).orElse(null), "Логин", existingLogin);
                //
                // 2д. Искажаем логин и пытаемся найти пользователя по такому Логину
                //
                String corruptedLogin = existingLogin + TestDataGenerator.jfaker.lorem().word();
                System.out.printf("Искажаем логин и пытаемся найти пользователя по Логину \"%s\"...%n", corruptedLogin);
                printUserOrDefaultString(repo.findByLogin(corruptedLogin).orElse(null), "Логин", corruptedLogin);
                //
                // 3а. Добавляем нового пользователя
                //
                System.out.println("**********************************************************************************");
                System.out.println("\t\t\tПоверка добавления новых пользователей:");
                System.out.println("**********************************************************************************");
                System.out.println();
                User newUserWithId = new User(nonexistentId, "Михаил", "Маюров", 22, "MayMih", true);
                System.out.printf("Пытаемся добавить нового пользователя с ранее не существовавшим ИД: %n%s%n...%n", newUserWithId);
                repo.create(newUserWithId);
                System.out.println("Проверяем, что пользователь успешно добавлен:");
                printUserOrDefaultString(repo.findById(nonexistentId).orElse(null), "ID", nonexistentId.toString());
                changedUsers.add(newUserWithId);
                //
                // 3б. Cоздаём нового юзера с логином, который мы только что безуспешно пытались найти и снова ищем его
                //
                User newUserWithLogin = new User("Юрий", "Маюров", 63, corruptedLogin, false);
                System.out.printf("Пытаемся добавить нового пользователя с ранее не существовавшим Логином: %n%s%n...%n", newUserWithLogin);
                System.out.println("Проверяем, что пользователь успешно добавлен:");
                repo.create(newUserWithLogin);
                printUserOrDefaultString(repo.findByLogin(corruptedLogin).orElse(null), "Login", corruptedLogin);
                changedUsers.add(newUserWithLogin);
                //
                // 3в. Cоздаём нового юзера - полного тёзку существующего - допустимая ситуация
                //
                User allNewUser = new User("Михаил", "Маюров", 33, "mayurmu", true);
                System.out.printf("Пытаемся добавить совершенно нового пользователя (полного тёзку): %n%s%n...%n", allNewUser);
                repo.create(allNewUser);
                System.out.println("Проверяем, что пользователь успешно добавлен:");
                printUserOrDefaultString(repo.findById(allNewUser.id).orElse(null), "ID", allNewUser.id.toString());
                changedUsers.add(allNewUser);
                //
                // 3д. Попытка создать пользователя уже существующего в репозитории
                //
                User userClone = new User(allNewUser);
                System.out.printf("Пытаемся добавить пользователя, который уже есть в репозитории: %n%s%n...%n", userClone);
                boolean isDuplicateAdded = false;
                try
                {
                    repo.create(userClone);
                    isDuplicateAdded = true;
                }
                catch (DataExistsException dex)
                {
                    System.out.println("Такой польз-ль уже существует");
                    System.out.append(dex.getClass().getSimpleName()).append(" ").println(dex.getMessage());
                }
                System.out.printf("Дубликат - %s%n%n", isDuplicateAdded ? "!! Добавлен !!" : "Не добавлен");
                //
                // 3е. Пытаемся добавить пустого пользователя в репозиторий
                //
                System.out.println("Пытаемся добавить пустого пользователя в репозиторий...");
                boolean isEmptyAdded = false;
                try
                {
                    repo.create(User.EMPTY);
                    isEmptyAdded = true;
                }
                catch (RequiredFieldsEmptyException rex)
                {
                    System.out.append(rex.getClass().getSimpleName()).append(" ").println(rex.getMessage());
                }
                System.out.printf("%nПустой юзер - %s%n%n", isEmptyAdded ? "!! Добавлен !!" : "Не добавлен");
                //
                // 4а. Пытаемся изменить пользователя и проверяем, что изменения его копии НЕ влияют на состояние репозитория
                //
                System.out.println("**********************************************************************************");
                System.out.println("\t\t\tПоверка изменения существующих пользователей:");
                System.out.println("**********************************************************************************");
                System.out.println();
                System.out.println("Изменяем пользователя и проверяем, что изменения его копии НЕ влияют на состояние репозитория:");
                allNewUser.setEmployed(false);
                allNewUser.setAge(1);
                System.out.append("Изменённая копия пользователя:\n").append(allNewUser.toString()).println();
                System.out.println();
                System.out.println("Тот же польз-ль считанный из репозитория, до вызова метода update():");
                printUserOrDefaultString(repo.findById(allNewUser.id).orElse(null), "ID", allNewUser.id.toString());
                //
                // 4б. Обновляем репозиторий и убеждаемся, что изменения сохранились
                //
                System.out.println("Тот же польз-ль считанный из репозитория, ПОСЛЕ вызова метода update():");
                repo.update(allNewUser);
                printUserOrDefaultString(repo.findById(allNewUser.id).orElse(null), "ID", allNewUser.id.toString());
                //
                // 4в. Пытаемся обновить юзера, которого нет в репозитории
                //
                System.out.println("Пытаемся обновить юзера, которого нет в репозитории...");
                User nonExistentUser = new User("Тест", "Тестов", 99, "test", false);
                try
                {
                    repo.update(nonExistentUser);
                }
                catch (NoDataFoundException nex)
                {
                    System.out.println(nex.getClass().getSimpleName() + " " + nex.getMessage());
                }
                //
                // 4г. Обновляем ещё одного случайного юзера, делая его "анонимом" - допустимая операция
                //
                User randomUser = repo.findById(dataGenerator.getRandomUser().id).orElse(User.EMPTY);
                System.out.printf("Случайный пользователь из репозитория:%n%n %s %n%n", randomUser);
                System.out.println("Пытаемся изменить польз-ля так, чтобы он стал анонимом (скрыл ФИО):");
                randomUser.setName("");
                randomUser.setLastName("");
                System.out.println();
                System.out.println(randomUser);
                repo.update(randomUser);
                System.out.println();
                System.out.println("Проверяем, что изменения применились:");
                Optional<User> opt = repo.findById(randomUser.id);
                boolean isAnonOk = opt.map(user -> Users.deepEquals(user, randomUser)).isPresent();
                System.out.println(isAnonOk ? "OK" : "!! ОШИБКА !!");
                printUserOrDefaultString(opt.orElse(null), "ID", randomUser.id.toString());
                if (isAnonOk)
                {
                    changedUsers.add(randomUser);
                }
                //
                // 5а. Удаляем до трёх случайных пользователей и проверяем, что их больше нет в репозитории
                //
                System.out.println("**********************************************************************************");
                System.out.println("\t\t\tПоверка удаления пользователей:");
                System.out.println("**********************************************************************************");
                System.out.println();
                int userCountToDelete = Integer.min(dataGenerator.testDataLength, 3);
                System.out.printf("Удаляем %d пользователей...%n", userCountToDelete);
                final List<User> deletedUsers = new ArrayList<>(userCountToDelete);
                for (int i = 0; i < userCountToDelete; i++)
                {
                    User userToDelete = dataGenerator.getRandomUser();
                    final UUID deletedId = userToDelete.id;
                    try
                    {
                        repo.delete(deletedId);
                        deletedUsers.add(userToDelete);
                        System.out.printf("Пользователь с ИД %s удалён%n", deletedId);
                    }
                    catch (NoDataFoundException nex)
                    {
                        System.out.println("Видимо этот юзер уже удалён: " + nex.getMessage());
                    }
                    System.out.printf("Ищем удалённого пользователя с ИД: %s...%n", deletedId.toString());
                    printUserOrDefaultString(repo.findById(deletedId).orElse(null), "ID", deletedId.toString());
                }
                changedUsers.removeAll(deletedUsers);
                //
                // 5б. Пытаемся удалить заведомо несуществующего юзера
                //
                System.out.println("Пытаемся удалить несуществующего юзера...");
                try
                {
                    repo.delete(nonExistentUser.id);
                    deletedUsers.add(nonExistentUser);
                }
                catch (NoDataFoundException nex)
                {
                    System.out.println(nex.getMessage());
                }
                System.out.printf("Несуществующий пользователь: %s%n%n", deletedUsers.contains(nonExistentUser) ? "!! Удалён !!" :
                    "не найден - OK");
                //
                // 6. Сохраняем репозиторий в физическое хранилище, затем загружаем его и проверяем, что изменения сохранились
                //
                System.out.println("**********************************************************************************");
                System.out.println("\t\t\tПоверка сохранения в файл:");
                System.out.println("**********************************************************************************");
                System.out.println();
                System.out.println("Все запланированные операции с репозиторием завершены, Вы хотите сохранить изменения," +
                    " [Y|Yes] - Да, [Остальной ввод] - Отмена:");
                String answer = _scn.nextLine().trim();
                if (answer.equalsIgnoreCase("yes") || answer.equalsIgnoreCase("y"))
                {
                    System.out.println("Сохраняем данные в файл...");
                    repo.save();
                    System.out.println("Запись прошла успешно.");
                }
                else
                {
                    System.out.println();
                    System.out.println("Пользователь отказался от сохранения!");
                    System.out.println();
                }
                System.out.println("Перезагружаем данные из файла и проверяем, правильно ли они записались...");
                repo.load();
                boolean isChangedOk = changedUsers.stream().allMatch(user -> repo.findById(user.id).map(foundUser ->
                    Users.deepEquals(foundUser, user)).isPresent());
                System.out.printf("Изменённые и добавленные пользователи: %s%n", isChangedOk ? "Успешно" :
                    "!! не совпадают с файлом !!");
                System.out.println();
                System.out.println("Ищем удалённых...");
                boolean isDeletedOk = deletedUsers.stream().noneMatch(user -> repo.findById(user.id).isPresent());
                System.out.printf("Удаление пользователей прошло: %s%n", isDeletedOk ? "Успешно" : "!! Неуспешно !!");
            }
        }
        catch (UserInputCancelledException uix)
        {
            isUserCancelled = true;
            System.out.println("Пользователь отказался от ввода. Программа будет завершена.");
        }
        catch (RepositoryException rex)
        {
            isNormalExit = false;
            System.err.append("Неожиданная ошибка работы с репозиторием: \n").println(rex.getMessage());
            rex.printStackTrace();
        }
        catch (Exception ex)
        {
            isNormalExit = false;
            System.err.append("Неожиданная ошибка работы программы: \n").println(ex.getMessage());
            ex.printStackTrace();
        }
        finally
        {
            if (!isUserCancelled)
            {
                System.out.println();
                // не выходим из программы сразу, т.к. это удалит тестовый файл и временную папку
                System.out.println("Введите любой символ для выхода:");
                try
                {
                    _scn.nextLine();
                }
                catch (Exception ex)
                { /* Обработка не требуется */ }
            }
            _scn.close();
            System.out.println("Программа завершена " + (isNormalExit ? "нормально" : "с ошибками"));
            System.exit(isNormalExit ? (isUserCancelled ? 2 : 0) : -1);
        }
    }
    
    /**
     * Выводит на консоль все строки из указанного файла или только первые {@code maxDisplayedLines}, если файл больше
     *
     * @exception RuntimeException при проблемах с файлом
     */
    private static void printFileOrFirstLines(Path filePath, int maxDisplayedLines)
    {
        System.out.printf("Данные готовы, содержимое файла (или его первые %d строк):%n", maxDisplayedLines);
        System.out.println();
        try (Stream<String> lines = Files.lines(filePath).limit(maxDisplayedLines))
        {
            lines.forEach(System.out::println);
        }
        catch (IOException ex)
        {
            throw new RuntimeException("Ошибка работы с файлом \"" + filePath + "\"" + ex.getMessage(), ex);
        }
        System.out.println();
    }
    
    /**
     * Выводит на консоль найденного пользователя или строку "{@code fieldName=fieldValue} Не найден"
     *
     * @param user - может быть Null - тогда будет выведена стандартная строка "Не найден"
     */
    private static void printUserOrDefaultString(User user, String fieldName, String fieldValue)
    {
        System.out.printf("Пользователь c %s=%s:%n%s %n%n", fieldName, fieldValue, user != null ? "\n" + user : "Не найден");
    }
}