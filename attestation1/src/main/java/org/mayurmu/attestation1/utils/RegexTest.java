package org.mayurmu.attestation1.utils;

import java.util.regex.Matcher;

import java.util.regex.Pattern;

// код взят отсюда: https://gist.github.com/awwsmm/886ac0ce0cef517ad7092915f708175f
// (-) не захватывает пустое значение в начале строки (как добавить не придумал)

/**
 * Класс для проверки регулярки разбора экранированной строки из CSV
 */
public class RegexTest
{
    public static void main(String[] args)
    {
        String delimiter = ";";
        String escapedDelimiter = Pattern.quote(delimiter);
        //String regex = "(?:\\||\\n|^)(\"(?:(?:\"\")*[^\"]*)*\"|[^\"|\\n]*|(?:\\n|$))";
        String regex = "(?:" + escapedDelimiter + "|\\n|^)(\"(?:(?:\"\")*[^\"]*)*\"|[^\"" + escapedDelimiter + "\\n]*|(?:\\n|$))";
        
        //String test1 = "|ID|NAME|\"FAMI|LY\"|\"AG|\"\"I\"\"NG\"|\"\"\"L\"\"|\"\"OG\"\"IN\"|||\"HAS_WORK\"|";
        String test1 = ";ca266b28-ed86-432c-b2a3-2c8d846b4c3e;Екатерина;Кабанова;17;;Vipir_17;false";
        
        Matcher matcher = Pattern.compile(regex).matcher(test1);
        
        int i = 0;
        while (matcher.find())
        {
            System.out.println(i + ": " + matcher.group(1));
            ++i;
        }
    }
}