package org.mayurmu.attestation1.utils;

import com.github.javafaker.Faker;

import org.mayurmu.attestation1.model.User;
import org.mayurmu.common.Utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Stream;

/**
 * Класс для генерации тестовых данных
 */
public class TestDataGenerator
{
    public static final Faker jfaker = new Faker(new Locale("ru"));
    
    public final int testDataLength;
    public final User[] users;
    
    
    
    /**
     * Конструктор без генерации внутренней коллекции
     */
    public TestDataGenerator()
    {
        this(0);
    }
    
    /**
     * Конструктор сразу генерирующий внутреннюю коллекцию {@link #users}
     *
     * @param dataLength Кол-во тестовых записей
     */
    public TestDataGenerator(int dataLength)
    {
        testDataLength = dataLength;
        users = new User[dataLength];
        regenerateData();
    }
    
    
    
    /**
     * Перезаполняет массив {@link #users} валидными данными (без Null)
     */
    public void regenerateData()
    {
        fillRandomUsersArray(users, false);
    }
    
    /**
     * То же, что и {@link #regenerateData()}, но возвращает копию массива ссылок на тестовые объекты
     */
    public User[] getNewData()
    {
        regenerateData();
        return Arrays.copyOf(users, users.length);
    }
    
    /**
     * Метод заполнения массива людей случайными значениями
     *
     * @param users Массив людей ненулевой длины
     */
    public void fillRandomUsersArray(User[] users)
    {
        fillRandomUsersArray(users, false);
    }
    
    /**
     * Метод заполнения массива юзеров случайными значениями (в т.ч. может рандомно вставлять null вместо некоторых эл-в)
     *
     * @apiNote На больших данных (порядка нескольких тысяч) вполне может генерировать юзеров с одинаковыми логинами
     *      (как этого избежать, кроме как через UUID не знаю)
     *
     * @param users         Массив людей ненулевой длины
     * @param isInsertNulls Добавлять ли случайное кол-во Null объектов (от 1 до половины длины массива)
     *
     * @exception IllegalArgumentException При пустом массиве людей ({@code users == null || users == 0})
     */
    public void fillRandomUsersArray(User[] users, boolean isInsertNulls)
    {
        if (users == null || users.length == 0)
        {
            throw new IllegalArgumentException("Нечего заполнять - Передан пустой массив!");
        }
        
        final int nullsCount = jfaker.number().numberBetween(1, users.length / 2);
    
        for (int i = 0, n = 0; i < users.length; i++)
        {
            if (isInsertNulls && (n < nullsCount && jfaker.bool().bool()))
            {
                users[i] = null;
                n++;
                continue;
            }
            int age = jfaker.random().nextInt(5, 99);
            String name = jfaker.name().firstName();
            String lastName = jfaker.name().lastName();
            lastName = Utils.getCorrectGenderLastName(name, lastName);
            // компенсируем "косяк" jFaker'a, который может вместо Фамилии выдать Отчество
            String login = (jfaker.elderScrolls().firstName() + jfaker.number().digits(5)).replaceAll(
                "\\s|\\p{Punct}", "") + "_" + age;
            users[i] = new User(name, lastName, age, login, jfaker.random().nextBoolean());
        }
        
    }
    
    /**
     * Записывает сгенерированные данные в указанный CSV-файл (Перезаписывает данные!)
     *
     * @param tempFilePath Если файл не существует - он будет создан иначе - перезаписан
     *
     * @exception RuntimeException При ошибках записи в файл
     */
    public void writeToCSV(Path tempFilePath, String separator)
    {
        Stream<String> userDataStream = Arrays.stream(users).map(u -> String.join(separator,
            u.id.toString(),
            u.getName(),
            u.getLastName(),
            String.valueOf(u.getAge()),
            u.login,
            String.valueOf(u.isEmployed())
        ));
        Iterable<String> iterable = userDataStream::iterator;
        try
        {
            Files.write(tempFilePath, iterable, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        }
        catch (IOException ex)
        {
            throw new RuntimeException("Ошибка записи тестового файла:\n " + tempFilePath, ex);
        }
    }
    
    public User getRandomUser()
    {
        return users[jfaker.random().nextInt(users.length)];
    }
}
