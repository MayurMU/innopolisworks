package org.mayurmu.attestation1.utils;

import org.mayurmu.attestation1.model.User;

public class Users
{
    /**
     * Сравнивает все поля указанных пользователей
     */
    public static boolean deepEquals(User user1, User user2)
    {
        return  user1.id.equals(user2.id) &&
                (user1.isEmployed() == user2.isEmployed()) &&
                user1.login.equals(user2.login) &&
                user1.getLastName().equals(user2.getLastName()) &&
                user1.getName().equals(user2.getName()) &&
                (user1.getAge() == user2.getAge());
    }
}
