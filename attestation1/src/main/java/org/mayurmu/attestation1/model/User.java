package org.mayurmu.attestation1.model;

import org.mayurmu.homework7.Human;

import java.util.StringJoiner;
import java.util.UUID;

/**
 * Класс Пользователь - наследует класс Человек из 7-й задачи, дополняет его полями {@code ID, Login и "есть_работа"}
 */
//TODO: Добавить реализацию Comparable по свойству getFullName();
public class User extends Human
{
    // "пустой" ID для новых пользователей с пустым конструктором
    private static final UUID EMPTY_ID = new UUID(0L, 0L);
    
    public static final User EMPTY = new User();
    
    public final UUID id;
    public final String login;
    private boolean isEmployed;
    
    
    public boolean isEmployed()
    {
        return isEmployed;
    }
    
    public void setEmployed(boolean employed)
    {
        isEmployed = employed;
    }
   
    
    
    //region 'Конструкторы'
    
    public User()
    {
        login = "";
        id = EMPTY_ID;
    }
    
    /**
     * Конструктор для новых пользователей (генерирует ID)
     * @param name          Имя
     * @param lastName      Фамилия
     * @param age           Возраст
     * @param login         Логин
     * @param isEmployed    Признак трудоустройства
     *
     * @exception IllegalArgumentException Когда логин ее является "словом" ([A-Za-z_0-9]) или короче 4 символов
     */
    public User(String name, String lastName, int age, String login, boolean isEmployed)
    {
        this(UUID.randomUUID(), name, lastName, age, login, isEmployed);
    }
    
    /**
     * Конструктор для загрузки пользователей с известным ID
     * @param id            ИД
     * @param name          Имя
     * @param lastName      Фамилия
     * @param age           Возраст
     * @param login         Логин
     * @param isEmployed    Признак трудоустройства
     *
     * @exception IllegalArgumentException Когда логин ее является "словом" ([A-Za-z_0-9]) или короче 4 символов
     */
    public User(UUID id, String name, String lastName, int age, String login, boolean isEmployed)
    {
        super(name, lastName, age);
        this.id = id;
        this.isEmployed = isEmployed;
        String trimmedLogin = login.trim();
        if (!trimmedLogin.matches("^\\w{4,}$"))
        {
            throw new IllegalArgumentException("Логин должен состоять как минимум из 4 букв и цифр латинского алфавита и знака подчёркивания (_)!\n" +
                " Однако был предоставлен \"" + login + "\"");
        }
        this.login = trimmedLogin;
    }
    
    /**
     * Конструктор копирования
     */
    public User(User user)
    {
        this.id = user.id;
        this.login = user.login;
        copyDataFrom(user);
    }
    
    //endregion 'Конструкторы'
    
    
    
    /**
     * Объекты считаются равными при совпадении Login'ов (без учёта регистра)
     *
     * @apiNote Сравнение идёт по содержимому, т.е. по уникальным полям объекта (в данном случае по {@link #login}),
     *          Т.к. ID является техническим полем, то онно не учитывается в операции сравнения. Отсутствие пользователей
     *              с одинаковым ID должен контролировать внешний код!
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        User user = (User) o;
        return login.equalsIgnoreCase(user.login);
    }
    
    /**
     * Объекты считаются равными при совпадении Login'ов (без учёта регистра)
     *
     * @apiNote Сравнение идёт по содержимому, т.е. по уникальным полям объекта (в данном случае по {@link #login})
     */
    @Override
    public int hashCode()
    {
        return login.toLowerCase().hashCode();
    }
    
    /**
     * Возвращает строковое представление Пользователя
     * @implNote Т.к. предок использовал грубо сгенерированный ToString(), то тут обращаемся к его свойствам
     */
    @Override
    public String toString()
    {
        return new StringJoiner("\n")
                .add(String.format("%12s: %-50s", "ID", id.toString()))
                .add(String.format("%12s: %-50s", "Login", login))
                .add(String.format("%12s: %-50s", "Имя", super.getName()))
                .add(String.format("%12s: %-50s", "Фамилия", super.getLastName()))
                .add(String.format("%12s: %-50d", "Возраст", super.getAge()))
                .add(String.format("%12s: %-50s", "Есть работа", isEmployed ? "Да" : "Нет"))
                .toString();
    }
    
    /**
     * Метод обновления всех публичных доступных для записи полей объекта на основе другого объекта
     */
    public void copyDataFrom(User sourceUser)
    {
        this.isEmployed = sourceUser.isEmployed;
        super.setAge(sourceUser.getAge());
        super.setLastName(sourceUser.getLastName());
        super.setName(sourceUser.getName());
    }
    
    public boolean isEmpty()
    {
        return this.equals(EMPTY);
    }
}
