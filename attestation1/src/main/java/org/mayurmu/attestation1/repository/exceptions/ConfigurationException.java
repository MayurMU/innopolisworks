package org.mayurmu.attestation1.repository.exceptions;

/**
 * Выбрасывается, при невозможности открыть/настроить репозиторий
 */
public class ConfigurationException extends RepositoryException
{
    public ConfigurationException(String message)
    {
        super(message);
    }
    
    public ConfigurationException(String message, Throwable reason)
    {
        super(message, reason);
    }
}
