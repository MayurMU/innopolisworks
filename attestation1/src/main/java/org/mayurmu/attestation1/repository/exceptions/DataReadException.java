package org.mayurmu.attestation1.repository.exceptions;

/**
 * Выбрасывается, при невозможности прочитать данные из репозитория
 */
public class DataReadException extends RepositoryException
{
    public DataReadException(String message)
    {
        super(message);
    }
    
    public DataReadException(String message, Throwable reason)
    {
        super(message, reason);
    }
}
