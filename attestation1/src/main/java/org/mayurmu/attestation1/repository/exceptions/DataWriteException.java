package org.mayurmu.attestation1.repository.exceptions;

/**
 * Выбрасывается, при невозможности сохранить данные в репозитории
 */
public class DataWriteException extends RepositoryException
{
    public DataWriteException(String message)
    {
        super(message);
    }
    
    public DataWriteException(String message, Throwable reason)
    {
        super(message, reason);
    }
}
