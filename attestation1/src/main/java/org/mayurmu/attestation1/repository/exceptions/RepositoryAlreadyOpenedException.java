package org.mayurmu.attestation1.repository.exceptions;

/**
 * Выбрасывается, при невозможности открыть репозиторий, т.к. он уже открыт
 */
public class RepositoryAlreadyOpenedException extends RepositoryException
{
    public RepositoryAlreadyOpenedException(String message)
    {
        super(message);
    }
    
    public RepositoryAlreadyOpenedException(String message, Throwable reason)
    {
        super(message, reason);
    }
}
