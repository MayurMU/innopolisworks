package org.mayurmu.attestation1.repository.exceptions;

/**
 * Выбрасывается, при попытке добавить в репозиторий записи, которые дублируют уже имеющиеся данные с уникальными полями
 */
public class DataExistsException extends RepositoryException
{
    public final String[] conflictFields;
    /**
     * Исключение возникает при нарушении целостности данных
     *
     * @param message   Сообщение об ошибке
     * @param conflictFields Список имён полей, по которым есть дублирование
     */
    public DataExistsException(String message, String ... conflictFields)
    {
        super(message);
        this.conflictFields = conflictFields;
    }
}
