package org.mayurmu.attestation1.repository;

import org.mayurmu.attestation1.model.User;
import org.mayurmu.attestation1.repository.exceptions.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;

public interface UsersRepository extends DataRepository
{
    /**
     * Метод поиска юзера по ID
     *
     * @return Возвращает копию Юзера - для внесения изменений в репозиторий нужно вызвать метод {@link #update(User)}
     */
    Optional<User> findById(UUID id);
    
    /**
     * Метод поиска юзера по логину
     *
     * @return Возвращает копию Юзера - для внесения изменений в репозиторий нужно вызвать метод {@link #update(User)}
     */
    Optional<User> findByLogin(String login);
    
    /**
     * Метод проверки наличия польз-ля с указанными уникальными полями в репозитории
     */
    boolean isUserExists(UUID id);
    /**
     * Метод проверки наличия польз-ля с указанными уникальными полями в репозитории
     */
    boolean isUserExists(String login);
    /**
     * Метод проверки наличия польз-ля с указанными уникальными полями в репозитории
     */
    boolean isUserExists(UUID id, String login);
    
    /**
     * Метод создания нового юзера
     *
     * @param user Объект пользователя подлежащий записи в репозиторий
     *
     * @exception DataExistsException Пользователь(и) с такими уникальными полями уже существует(ют)
     * @exception RequiredFieldsEmptyException При попытке добавить пользователя с пустыми полями
     */
    void create(User user);
    
    /**
     * Метод обновления информации по юзеру
     *
     * @apiNote Позволяет менять только НЕуникальные поля, иначе выбрасывает {@link NoDataFoundException}
     *
     * @exception NoDataFoundException Польз-ль с такими уникальными полями не найден
     * @exception RequiredFieldsEmptyException При попытке "обнулить" обязательные поля пользователя
     */
    void update(User user);
    
    /**
     * Удаление юзера по ID
     *
     * @exception NoDataFoundException Польз-ль с таким ID не найден
     */
    void delete(UUID id);
}
