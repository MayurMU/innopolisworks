package org.mayurmu.attestation1.repository.exceptions;

/**
 * Выбрасывается, при попытке изменить запись в репозитории, которой не существует
 */
public class NoDataFoundException extends RepositoryException
{
    public final String[] absentFields;
    /**
     * Исключение возникает при нарушении целостности данных
     *
     * @param message   Сообщение об ошибке
     * @param absentFields Список имён полей, которые не найдены в репозитории
     */
    public NoDataFoundException(String message, String ... absentFields)
    {
        super(message);
        this.absentFields = absentFields;
    }
}
