package org.mayurmu.attestation1.repository;

import org.mayurmu.attestation1.repository.exceptions.*;

public interface DataRepository extends AutoCloseable
{
    /**
     * Метод первоначальной настройки репозитория
     *
     * @param config Путь к физическому хранилищу (и/или настройки репозитория)
     * @param params Прочие настройки репозитория
     *
     * @implSpec Если уже открыто - ничего не делает
     *
     * @exception ConfigurationException При невозможности открыть указанное хранилище, либо при ошибочном наборе параметров
     *
     * @exception RepositoryAlreadyOpenedException При попытке открыть новый репозиторий, когда старый ещё не закрыт.
     */
    void open(String config, String ... params);
    
    //TODO: Добавить свойство boolean isOpened();
    
    /**
     * Загружает данные из физического хранилище в объект репозитория
     *
     * @apiNote При повторных вызовах перезаписывает данные в объекте репозитория
     *
     * @exception DataReadException При ошибках чтения физ. хранилища репозитория
     * @exception DataExistsException При обнаружении дублирования данных
     */
    void load();
    
    /**
     * Записывает данные в физическое хранилище
     *
     * @apiNote Аналог метода {@code file.flush()}, при закрытии репозитория через {@link #close()} данные тоже сохраняться
     *
     * @exception DataWriteException При ошибках записи или, если репозиторий открыт только для чтения
     */
    void save();
    
    /**
     * Возвращает признак того, открыт ли репозиторий в режиме "Только чтение"
     */
    boolean isReadOnly();
    
    /**
     * Возвращает имя репозитория (задаётся методом {@link #open(String, String...)})
     *
     * @apiNote Полезно, чтобы различать объекты внутри программы (например, при выбросе исключений)
     */
    String getName();
}
