package org.mayurmu.attestation1.repository.exceptions;

/**
 * Выбрасывается, при попытке добавить в репозиторий запись, с пустыми Обязательными полями
 */
public class RequiredFieldsEmptyException extends RepositoryException
{
    public final String[] conflictFields;
    /**
     * Исключение возникает при нарушении целостности данных
     *
     * @param message   Сообщение об ошибке
     * @param conflictFields Список имён пустых полей
     */
    public RequiredFieldsEmptyException(String message, String ... conflictFields)
    {
        super(message);
        this.conflictFields = conflictFields;
    }
}
