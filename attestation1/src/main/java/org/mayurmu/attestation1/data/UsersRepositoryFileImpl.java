/**
 * Уровень доступа к данным (реализация репозитория находится здесь согласно рекомендации
 * <a href="https://gist.github.com/maestrow/594fd9aee859c809b043#%D1%80%D0%B5%D0%BF%D0%BE%D0%B7%D0%B8%D1%82%D0%BE%D1%80%D0%B8%D0%B9-%D0%B8-dal-data-access-layer-persistence-layer"></a>)
 */
package org.mayurmu.attestation1.data;

import org.mayurmu.attestation1.model.User;
import org.mayurmu.attestation1.repository.UsersRepository;
import org.mayurmu.attestation1.repository.exceptions.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Реализация хранилища пользователей в простых CSV-файлах
 *
 * @implNote Если значения содержат в своём составе символ разделитель, то они должны быть заключены в кавычки (""),
 *      сами же кавычки удваиваются, если они являются значимым символом.
 */
public class UsersRepositoryFileImpl implements UsersRepository
{
    
    //region 'Типы'
    
    /**
     * Возможные параметры Файлового хранилища для метода {@link #open(String, String...)}
     */
    public enum KnownParam
    {
        SEPARATOR("separator"),
        CREATE_IF_NOT_EXISTS("create"),
        READONLY("readonly");
        
        public final String name;
        
        KnownParam(String paramName)
        {
            name = paramName;
        }
    }
    
    /**
     * Вспомогательный класс подготовки данных для записи в ячейки CSV
     */
    protected static class Quoter
    {
        public final String delimiter;
        private final Pattern wordPattern;
    
        public Quoter(String delimiter)
        {
            this.delimiter = delimiter;
            String escapedDelimiter = Pattern.quote(delimiter);
            // Регулярка взята отсюда: "https://gist.github.com/awwsmm/886ac0ce0cef517ad7092915f708175f"
            wordPattern = Pattern.compile("(?:" + escapedDelimiter + "|\\n|^)(\"(?:(?:\"\")*[^\"]*)*\"|[^\"" +
                escapedDelimiter + "\\n]*|(?:\\n|$))");
        }
    
        /**
         * Метод разбивает строку CSV на значения и убирает лишние кавычки (если нужно)
         *
         * @param row Не может начинаться с символа разделителя {@link #delimiter}, но обязательно должен его содержать
         *
         * @exception IllegalArgumentException Если строка начинается с разделителя или не содержит его
         */
        public String[] splitRow(String row)
        {
            // В данном случае мы не разбиваем строку, а ищем шаблоны слов
            // Регулярка взята отсюда: "https://gist.github.com/awwsmm/886ac0ce0cef517ad7092915f708175f"
            // Но она не захватывает пустое значение в начале строки (как добавить не придумал), поэтому обрабатываю отдельно
            final String trimmedRow = row.trim();
            if (trimmedRow.startsWith(delimiter) || !trimmedRow.contains(delimiter))
            {
                throw new IllegalArgumentException("Неверный формат строки CSV: \"" + row + "\"");
            }
            final Matcher mr = wordPattern.matcher(trimmedRow);
            List<String> result = new ArrayList<>();
            while (mr.find())
            {
                // 0-я группа является группой, которую не надо рассматривать (non-capturing), поэтому начинаем с 1-й
                result.add(mr.group(1));
            }
            return result.toArray(new String[0]);
        }
    
        /**
         * Оборачивает значение будущей ячейки CSV в кавычки (если нужно)
         *
         * @param cellText Значение для одной ячейки CSV
         *
         * @see <a href="https://stackoverflow.com/a/59912119/2323972">На основе кода отсюда</a>
         * @see <a href="https://www.convertcsv.com/csv-escape-tool.htm">Проверочная страница</a>
         * @see <a href="https://www.rfc-editor.org/rfc/rfc4180">Стандарт CSV</a>
         */
        public String quoteIfNecessary(String cellText)
        {
            boolean containsDelimiter = cellText.contains(delimiter);
            boolean containsQuotes = cellText.contains("\"");
            //TODO: скорее всего здесь избыточен replaceAll() - попробовать заменить на replace()!
            cellText = cellText.replaceAll("\"", "\"\"").replaceAll("\n", " ");
            
            if (containsDelimiter || containsQuotes)
            {
                cellText = "\"" + cellText + "\"";
            }
            return cellText;
        }
    }
    
    /**
     * Вспомогательный класс парсера входных параметров репозитория
     *
     * @apiNote Поддерживает два режима использования - парсинг всех параметров разом {@link #getParsedParams()} и
     *      выборочный запрос каждого параметра через {@link #getParam(KnownParam)} или {@link #getParam(String)}
     */
    protected static class ParamsParser
    {
        private final String[] source;
        private final HashMap<KnownParam, String> parsedParams = new HashMap<>();
        
        public ParamsParser(String[] source)
        {
            this.source = source;
        }
        
        public Optional<String> getParam(KnownParam param)
        {
            return getParam(param.name);
        }
        
        public Optional<String> getParam(String paramName)
        {
            return Arrays.stream(source).filter(x -> x.matches("^" + paramName.trim() + "=.+")).map(x ->
                x.split("=")[1].trim()).findFirst();
        }
    
        /**
         * Проверяет, нет ли среди параметров неизвестных
         *
         * @apiNote Не имеет смысла при работе через метод {@link #getParsedParams()}
         */
        public boolean checkParams()
        {
            return Arrays.stream(source).allMatch(p -> Arrays.stream(KnownParam.values()).anyMatch(k ->
                p.matches("^" + k.name + "=.+")
            ));
        }
    
        /**
         * Метод парсинга входных параметров
         *
         * @implNote Выполняет парсинг только один раз - последующие вызовы возвращают готовую коллекцию
         *
         * @exception ConfigurationException При обнаружении неизвестного параметра
         */
        public HashMap<KnownParam, String> getParsedParams()
        {
            if (parsedParams.isEmpty())
            {
                Arrays.stream(source).forEach(sourceParam -> {
                    KnownParam par = Arrays.stream(KnownParam.values()).filter(k ->
                        sourceParam.matches("^" + k.name + "=.+")).findAny().orElseThrow(() ->
                        new ConfigurationException("Обнаружен неизвестный параметр \"" + sourceParam + "\""));
                    parsedParams.put(par, sourceParam.split("=")[1].trim());
                    }
                );
            }
            return parsedParams;
        }
    }
    
    /**
     * Вспомогательный класс генерации однотипных исключений
     */
    protected class ExceptionHelper
    {
        /**
         * Метод проверки, что такого пользователя ещё нет
         */
        public void checkDataExistsException(User user)
        {
            // проверяем, что такого польз-ля ещё нет
            Optional<User> userOption = _users.stream().filter(u -> u.login.equalsIgnoreCase(user.login) || u.id.equals(user.id)).findFirst();
            if (userOption.isPresent())
            {
                User existedUser = userOption.get();
                throw new DataExistsException("Пользователь с такими данными уже существует:", existedUser.id.equals(user.id) ?
                    "id" : "", existedUser.login.equalsIgnoreCase(user.login) ? "login" : "");
            }
        }
    
        public void checkRequiredFieldsEmptyException(User user)
        {
            String[] errorFields = checkUniqueFields(user);
            if (errorFields.length > 0)
            {
                throw new RequiredFieldsEmptyException("Не указаны обязательные поля для пользователя \"" + user + "\"",
                    errorFields);
            }
        }
        
        public void throwNoDataFoundException(Object userData)
        {
            throw new NoDataFoundException("Такой пользователь не найден \"" + userData + "\"");
        }
        
        private String[] checkUniqueFields(User user1)
        {
            return checkUniqueFields(user1, User.EMPTY);
        }
    
        /**
         * Возвращает имена полей по которым выявлено несовпадение
         */
        public String[] checkUniqueFields(User user1, User user2)
        {
            String userIdEmptyMes = "", userLoginEmptyMes = "";
            if (user1.id.equals(user2.id))
            {
                userIdEmptyMes = "id";
            }
            if (user1.login.trim().isEmpty())
            {
                userLoginEmptyMes = "login";
            }
            return userIdEmptyMes.isEmpty() && userLoginEmptyMes.isEmpty() ? new String[0] : new String[] { userIdEmptyMes, userLoginEmptyMes };
        }
    }
    
    //endregion 'Типы'
    
    
    
    
    //region 'Поля и константы'
    
    /**
     * Дескриптор блокировки файла - блокировка снимается методом {@link #close()}, кроме того блокировка будет снята
     *  автоматически при завершении нити установившей эту блокировку.
     */
    private String _repoName;
    private Path _sourcePath;
    private boolean _isOpened;
    /**
     * Разделитель элементов строки в файле
     */
    private String _separator;
    /**
     * Признак того, что Репозиторий открыт в режиме "только чтение"
     */
    private boolean _isReadonly;
    /**
     * Кешированный список польз-лей
     *
     * @implNote Используем Set, чтобы усилить запрет дублирования элементов
     */
    //TODO: Заменить на TreeSet и добавить реализацию Comparable в User (по getFullName()), чтобы данные не писались в файл
    //  в "хаотичном" порядке
    private final Set<User> _users = new HashSet<>();
    private ExceptionHelper _exceptionHelper;
    
    //endregion 'Поля и константы'
    
    
    
    
    //region 'Свойства'
    
    @Override
    public boolean isReadOnly()
    {
        return _isReadonly;
    }
    

    @Override
    public String getName()
    {
        return _repoName;
    }
    
    protected ExceptionHelper getExceptionHelper()
    {
        if (_exceptionHelper == null)
        {
            _exceptionHelper = new ExceptionHelper();
        }
        return _exceptionHelper;
    }
    
    //endregion 'Свойства'
    
    
    
    
    //region 'Конструкторы'
    
    /**
     * Конструктор по умолчанию
     */
    public UsersRepositoryFileImpl()
    {}
    
    /**
     * Создаёт экземпляр репозитория и сразу же пытается его открыть
     *
     * @apiNote Описание параметров и исключений см. в методе {@link #open(String, String...)}
     */
    public UsersRepositoryFileImpl(String config, String ... params)
    {
        this();
        open(config, params);
    }
    
    //endregion 'Конструкторы'
    
    
    
    
    //region 'Методы'
    
    /**
     * @param config Путь к файлу CSV
     *
     * @param params Набор пар вида <b><i>"Имя=Значение"</i></b> - в данном случае поддерживаются следующие параметры:
     *               <ul>
     *                  <li><b>readonly={true|false}</b> - режим Только Чтение - по умолчанию False
     *                     <small>(не совместимо с параметром <b>create</b>)</small>
     *                  </li>
     *                  <li><b>create={true|false}</b> - создать репозиторий, если он не существует - по умолчанию True</li>
     *                      <small>(не совместимо с параметром <b>readonly</b>)</small>
     *                  </li>
     *                  <li><b>separator=?</b> - символ разделителя для CSV - по умолчанию (<b>,</b>) - может быть строкой</li>
     *               </ul>
     */
    @Override
    public void open(String config, String ... params)
    {
        if (_isOpened)
        {
            throw new RepositoryAlreadyOpenedException("Невозможно открыть новый Репозиторий, т.к. старый ещё не закрыт!");
        }
        try
        {
            _isReadonly = false;
            _sourcePath = Paths.get(config);
            _repoName = _sourcePath.getFileName().toString();
            final ParamsParser parser = new ParamsParser(params);
            final Map<KnownParam, String> parsedParams = parser.getParsedParams();
            //TODO: Раз enum KnownParam относится только к этому репозиторию, то и Default-значения тоже можно записать в него
            _separator = parsedParams.getOrDefault(KnownParam.SEPARATOR, ",");
            _isReadonly = Boolean.parseBoolean(parsedParams.getOrDefault(KnownParam.READONLY, "false"));
            boolean createIfNotExists = Boolean.parseBoolean(parsedParams.getOrDefault(KnownParam.CREATE_IF_NOT_EXISTS, "true"));
            if (_isReadonly && createIfNotExists)
            {
                throw new ConfigurationException("Обнаружены несовместимые параметры репозитория, проверьте строку:\n " +
                    "\"" + Arrays.toString(params) + "\"");
            }
            if (Files.notExists(_sourcePath))
            {
                if (createIfNotExists)
                {
                     Files.createFile(_sourcePath);
                }
                else
                {
                    throw new ConfigurationException("Файл не существует, проверьте правильность указания пути либо добавьте параметр \"create=true\"");
                }
            }
            // проверяем, есть ли у нас доступ к файлу
            else
            {
                if (!Files.isReadable(_sourcePath))
                {
                    throw new ConfigurationException("Не доступа на чтение файла \"" + config + "\" ");
                }
                if (!_isReadonly && !Files.isWritable(_sourcePath))
                {
                    throw new ConfigurationException("Не доступа на запись файла \"" + config + "\" ");
                }
            }
            _isOpened = true;
        }
        catch (Exception ex)
        {
            // Как правильно поступать в таких случаях - в Java же нет фильтра исключений?
            if (ex instanceof ConfigurationException)
            {
                throw (ConfigurationException)ex;
            }
            throw new ConfigurationException("Неожиданная ошибка конфигурации репозитория " + _repoName, ex);
        }
    }
    
    @Override
    public void load()
    {
        // Формат хранения одной записи файла (ID|ИМЯ|ФАМИЛИЯ|ВОЗРАСТ|ЛОГИН|ЕСТЬ_РАБОТА)
        _users.clear();
        try (Stream<String> reader = Files.lines(_sourcePath))
        {
            final Quoter rowQuoter = new Quoter(_separator);
            // В случае наличия кавычек и разделителя среди значений строки здесь может быть нечто вроде:
            // ID|ИМЯ|"ФАМИ|ЛИЯ"|"ВОЗ|""Р""АСТ"|"Л|О""Г""ИН"|ЕСТЬ_РАБОТА
            reader.forEach(line -> {
                String[] fields = rowQuoter.splitRow(line);
                if (!_users.add(new User(UUID.fromString(fields[0]), fields[1], fields[2], Integer.parseInt(fields[3]),
                    fields[4], Boolean.parseBoolean(fields[5]))))
                {
                    throw new DataExistsException("Обнаружено задвоение данных в файле - загрузка невозможна!");
                }
            });
        }
        catch (Exception ex)
        // Как правильно поступать в таких случаях - как я понял в Java нет фильтра исключений,
        // а я бы хотел любые ошибки в библиотеки оборачивать в своё исключение, при этом, здесь
        // по идее не надо перехватывать те исключения, которые я выбросил сам, т.е. RepositoryException ?
        {
            if (ex instanceof RepositoryException)
            {
                throw (RepositoryException)ex;
            }
            throw new DataReadException("Ошибка чтения данных из репозитория", ex);
        }
    }
    
    /**
     * @implNote Если записываемые значения содержат в своём составе символ разделитель, то значение заключается в кавычки ("")
     */
    @Override
    public void save()
    {
        if (_isReadonly)
        {
            throw new DataWriteException("Запись невозможна - Репозиторий открыт в режиме ТОЛЬКО ЧТЕНИЕ");
        }
        // Формат хранения одной записи файла (ID|ИМЯ|ФАМИЛИЯ|ВОЗРАСТ|ЛОГИН|ЕСТЬ_РАБОТА)
        try
        {
            // Учитываем случаи наличия кавычек и разделителя среди значений, в итоге запись файла будет иметь вид:
            // (ID|ИМЯ|"ФАМИ""|""ЛИЯ"|"ВО""З""РАСТ"|"ЛО""|""""Г""ИН"|ЕСТЬ_РАБОТА)
            final Quoter cellQuoter = new Quoter(_separator);
            Stream<String> userStream = _users.stream().map(u -> String.join(_separator,
                cellQuoter.quoteIfNecessary(u.id.toString()),
                cellQuoter.quoteIfNecessary(u.getName()),
                cellQuoter.quoteIfNecessary(u.getLastName()),
                cellQuoter.quoteIfNecessary(String.valueOf(u.getAge())),
                cellQuoter.quoteIfNecessary(u.login),
                cellQuoter.quoteIfNecessary(String.valueOf(u.isEmployed())))
            );
            Iterable<String> iterable = userStream::iterator;
            // Наличие StandardOpenOption.TRUNCATE_EXISTING обязательно - иначе файл оказывается в повреждённом состоянии
            Files.write(_sourcePath, iterable, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        }
        catch (Exception ex)
        {
            throw new DataWriteException("Ошибка записи данных", ex);
        }
    }
    
    
    @Override
    public Optional<User> findById(UUID id)
    {
        Optional<User> optionalUser = _users.stream().filter(u -> u.id.equals(id)).findAny();
        // здесь мы должны вернуть копию пользователя, иначе любые изменения его объекта будут тут же отражены в репозитории
        // БЕЗ необходимости вызова update()
        return optionalUser.flatMap(user -> Optional.of(new User(user)));
    }
    
    /**
     * Метод поиска юзера по логину
     */
    @Override
    public Optional<User> findByLogin(String login)
    {
        Optional<User> optionalUser = _users.stream().filter(u -> u.login.equalsIgnoreCase(login)).findAny();
        // здесь мы должны вернуть копию пользователя, иначе любые изменения его объекта будут тут же отражены в репозитории
        return optionalUser.flatMap(user -> Optional.of(new User(user)));
    }
    

    @Override
    public boolean isUserExists(UUID id)
    {
        return _users.stream().anyMatch(user -> user.id.equals(id));
    }
    

    @Override
    public boolean isUserExists(String login)
    {
        return _users.stream().anyMatch(user -> user.login.equalsIgnoreCase(login));
    }
    
    /**
     * Метод проверки наличия польз-ля с указанными уникальными полями в репозитории
     */
    @Override
    public boolean isUserExists(UUID id, String login)
    {
        return _users.stream().anyMatch(user -> user.login.equalsIgnoreCase(login) && user.id.equals(id));
    }
    

    @Override
    public void create(User user)
    {
        if (user == null)
        {
            throw new IllegalArgumentException("Пользователь не может быть Null");
        }
        final ExceptionHelper exceptionHelper = getExceptionHelper();
        exceptionHelper.checkRequiredFieldsEmptyException(user);
        exceptionHelper.checkDataExistsException(user);
        if (!_users.add(new User(user)))
        {
            throw new DataExistsException("Пользователь уже существует: " + user);
        }
    }

    
    @Override
    public void update(User user)
    {
        if (user == null)
        {
            throw new IllegalArgumentException("Пользователь не может быть Null");
        }
        final ExceptionHelper exceptionHelper = getExceptionHelper();
        exceptionHelper.checkRequiredFieldsEmptyException(user);
        Optional<User> optionalUser = _users.stream().filter(u -> u.login.equalsIgnoreCase(user.login) && u.id.equals(user.id)).findAny();
        if (optionalUser.isPresent())
        {
            optionalUser.get().copyDataFrom(user);
        }
        else
        {
            exceptionHelper.throwNoDataFoundException(user);
        }
    }
    

    @Override
    public void delete(UUID id)
    {
        if (!_users.removeIf(user -> user.id.equals(id)))
        {
            getExceptionHelper().throwNoDataFoundException(id);
        }
    }
    /**
     * Сохраняет результаты и производит выход
     */
    @Override
    public void close()
    {
        if (_isOpened)
        {
            _isOpened = false;      // в соответствии с рекомендациями AutoClosable сразу помечаем ресурс как закрытый
            // сначала здесь предполагались дополнительные действия по освобождению блокировки файла, но оказалось, что
            // блокировка с одновременным доступом на чтение и запись возможна только с классом RandomAccessFile, который
            // требует дополнительных ухищрений, чтобы писать в UTF-8.
            if (!_isReadonly)
            {
                save();
            }
        }
    }
    
    //endregion 'Методы'
    
}
